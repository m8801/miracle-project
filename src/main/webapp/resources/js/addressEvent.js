window.onload = function(){
		if(document.getElementById("addressInsert1")) {		
			const $regist = document.getElementById("addressInsert1");	
			$regist.onclick = function() {	    
		   		location.href = "/miracle/address/addressInsertManager";
			}
		}	
	
		if(document.getElementById("myInsert")) {		
			const $regist = document.getElementById("myInsert");	
			$regist.onclick = function() {	    
		   		location.href = "/miracle/address/myAddressInsertManager";
			}
		}	
			
		if(document.getElementById("myUserInsert")) {		
			const $regist = document.getElementById("myUserInsert");	
			$regist.onclick = function() {	    
		   		location.href = "/miracle/address/myAddressInsertUser";
			}
		}	
			
		
		if(document.getElementById("shareBtn")) {
			const $shareBtn = document.getElementById("shareBtn");	
		
			$shareBtn.onclick = function() {
				var addressNo = $("input[name=addressNoArr]").val().split(",");
				
				console.log(addressNo);
				console.log(addressNo.toString());					
			
				var cnt = $("input:checkbox[name='chk']:checked");
				var count = cnt.length;
				var noArr = [];
					
				console.log(cnt.length);
			
				for(var i = 0; i < cnt.length;i++) {
					 console.log(cnt);
					 console.log(cnt[0]);
	 
					if(cnt.length == 0) { 
						alert("공유할 사원을 선택해 주세요");
					} else {				
						if(cnt) {
							noArr.push(cnt[i].dataset.userno);
					}			
		        }
		   }
 
			   console.log(noArr);		
			
			   $.ajax({
					 type:"post",
					 url:"/miracle/address/sharingRegist",
					 data:{  addressNoArr : addressNo.toString(),
					 		 shareUserNoArr : noArr.toString() },
					 async:false,
					 success:function(data) {
						if(data == "success"){
							alert("주소록 공유에 성공하였습니다.");
							location.href = "/miracle/address/list";
						} else {
							alert("주소록 공유에 실패하였습니다.");
							location.href = "/miracle/address/list";
						}
					 },
					 error:function(data){
						alert("22 : " + data);
					 }
	      	  });
	      }
      }
	 
	 
	 
	 
	 
		 if(document.getElementById("CompanySharedSelectBtn")) {		
			  const $CompanySharedSelectBtn = document.getElementById("CompanySharedSelectBtn");	
		 	  $CompanySharedSelectBtn.onclick = function() {	    
		 	     var cnt = $("input:checkbox[name='chk']:checked");
		     	 var count = cnt.length;
			 	 var noArr = [];
			  	
			 	 console.log(cnt.length);
			
				 if(cnt.length == 0) { 
				  	  alert("공유할 주소록을 선택해 주세요");
			     }
				  for(var i = 0; i < cnt.length;i++) {
			
				 	  console.log(cnt);
					  console.log(cnt[0]);
			 				
				 	 	   if(cnt) {
							noArr.push(cnt[i].dataset.userno);
							location.href = "/miracle/address/companyManagerSharing/"+noArr;
					       }			 
		  	          }
			  } 
		} 
		if(document.getElementById("companyShareBtn")) {
			const $companyShareBtn = document.getElementById("companyShareBtn");	
		
			$companyShareBtn.onclick = function() {
				var addressNo = $("input[name=addressNoArr]").val().split(",");
				
				console.log(addressNo);
				console.log(addressNo.toString());					
			
				var cnt = $("input:checkbox[name='chk']:checked");
				var count = cnt.length;
				var noArr = [];
					
				console.log(cnt.length);
			
				for(var i = 0; i < cnt.length;i++) {
					 console.log(cnt);
					 console.log(cnt[0]);
	 
					if(cnt.length == 0) { 
						alert("공유할 사원을 선택해 주세요");
					} else {				
						if(cnt) {
							noArr.push(cnt[i].dataset.userno);
					}			
		        }
		   }
 
			 console.log(noArr);		
			
			 $.ajax({
					 type:"post",
					 url:"/miracle/address/companySharingRegist",
					 data:{  addressNoArr : addressNo.toString(),
					 		 shareUserNoArr : noArr.toString() },
					 async:false,
					 success:function(data) {
						if(data == "success"){
							alert("주소록 공유에 성공하였습니다.");
							location.href = "/miracle/address/addressCompanyManager";
						} else {
							alert("주소록 공유에 실패하였습니다.");
							location.href = "/miracle/address/addressCompanyManager";
						}
					 },
					 error:function(data){
						alert("22 : " + data);
					 }
	      	  });
	      }
      }

	      
	      if(document.getElementById("myModify")) {		
			  const $myModify = document.getElementById("myModify");	
		 	  $myModify.onclick = function() {	    
		 	     var cnt = $("input:checkbox[name='chk']:checked");
		     	 var count = cnt.length;
			 	 var noArr = [];
			 	 var nameArr = [];
			  	
			 	 console.log(cnt.length);
			

			 
				  if(cnt.length == 0) { 
					alert("수정할 주소록을 선택해 주세요");
					} else if(cnt.length == 1){				
						if(!confirm("정말 수정하시겠습니까?")){
							 alert("수정을 취소했습니다.");							 
						 } else {
						 	var no = cnt[0].dataset.addressno
						 	console.log(no);	
						    location.href = "/miracle/address/myAddressModifyManager/"+no;
						 }							
				  	 } else {
						alert("하나의 주소록만 선택해 주세요");
				  	 }
		 
			}
		} 

	     if(document.getElementById("mySharedSelectBtn")) {		
			  const $mySharedSelectBtn = document.getElementById("mySharedSelectBtn");	
		 	  $mySharedSelectBtn.onclick = function() {	    
		 	     var cnt = $("input:checkbox[name='chk']:checked");
		     	 var count = cnt.length;
			 	 var noArr = [];
			 	 var nameArr = [];
			  	
			 	 console.log(cnt.length);
			
				  if(cnt.length == 0) { 
				  		alert("공유할 주소록을 선택해 주세요");
				  }
					  
				  for(var i = 0; i < cnt.length;i++) {
				 	 	   if(cnt) {
							noArr.push(cnt[i].dataset.userno);
							location.href = "/miracle/address/myManagerSharing/"+noArr;
					       }			
		  	      }
			  } 
	 	} 
		if(document.getElementById("myShareBtn")) {
			const $myShareBtn = document.getElementById("myShareBtn");	
		
			$myShareBtn.onclick = function() {
				var addressNo = $("input[name=addressNoArr]").val().split(",");
				
				console.log(addressNo);
				console.log(addressNo.toString());					
			
				var cnt = $("input:checkbox[name='chk']:checked");
				var count = cnt.length;
				var noArr = [];
					
				console.log(cnt.length);
			
				for(var i = 0; i < cnt.length;i++) {
					 console.log(cnt);
					 console.log(cnt[0]);
	 
					if(cnt.length == 0) { 
						alert("공유할 사원을 선택해 주세요");
					} else {				
						if(cnt) {
							noArr.push(cnt[i].dataset.userno);
					}			
		        }
		   }
 
			   console.log(noArr);		
			
			   $.ajax({
					 type:"post",
					 url:"/miracle/address/mySharingRegist",
					 data:{  addressNoArr : addressNo.toString(),
					 		 shareUserNoArr : noArr.toString() },
					 async:false,
					 success:function(data) {
						if(data == "success"){
							alert("주소록 공유에 성공하였습니다.");
							location.href = "/miracle/address/myManagerList";
						} else {
							alert("주소록 공유에 실패하였습니다.");
							location.href = "/miracle/address/myManagerList";
						}
					 },
					 error:function(data){
						alert("22 : " + data);
					 }
	      	  });
	      }
      }
      
	  if(document.getElementById("sharedModify")) {		
			  const $sharedModify = document.getElementById("sharedModify");	
		 	  $sharedModify.onclick = function() {	    
		 	     var cnt = $("input:checkbox[name='chk']:checked");
			 	 console.log(cnt.length);
			

			 
				  if(cnt.length == 0) { 
					alert("수정할 주소록을 선택해 주세요");
					} else if(cnt.length == 1){				
						if(!confirm("정말 수정하시겠습니까?")){
							 alert("수정을 취소했습니다.");							 
						 } else {
						 	var no = cnt[0].dataset.addressno;
						 	console.log(no);	
						    location.href = "/miracle/address/sharedAddressModifyManager/"+no;
						 }							
				  	 } else {
						alert("하나의 주소록만 선택해 주세요");
				  	 }
		 
			}
		}  
	
	
	
	
	
		if(document.getElementById("CompanyUserSharedSelectBtn")) {		
			  const $CompanyUserSharedSelectBtn = document.getElementById("CompanyUserSharedSelectBtn");	
		 	  $CompanyUserSharedSelectBtn.onclick = function() {	    
		 	     var cnt = $("input:checkbox[name='chk']:checked");
		     	 var count = cnt.length;
			 	 var noArr = [];
			  	
			 	 console.log(cnt.length);
			
				 if(cnt.length == 0) { 
				  	  alert("공유할 주소록을 선택해 주세요");
			     }
				  for(var i = 0; i < cnt.length;i++) {
			
				 	  console.log(cnt);
					  console.log(cnt[0]);
			 				
				 	 	   if(cnt) {
							noArr.push(cnt[i].dataset.userno);
							location.href = "/miracle/address/companyUserSharing/"+noArr;
					       }			 
		  	          }
			  } 
		} 
		
		if(document.getElementById("companyUserShareBtn")) {
			const $companyUserShareBtn = document.getElementById("companyUserShareBtn");	
		
			$companyUserShareBtn.onclick = function() {
				var addressNo = $("input[name=addressNoArr]").val().split(",");
				
				console.log(addressNo);
				console.log(addressNo.toString());					
			
				var cnt = $("input:checkbox[name='chk']:checked");
				var count = cnt.length;
				var noArr = [];
					
				console.log(cnt.length);
			
				for(var i = 0; i < cnt.length;i++) {
					 console.log(cnt);
					 console.log(cnt[0]);
	 
					if(cnt.length == 0) { 
						alert("공유할 사원을 선택해 주세요");
					} else {				
						if(cnt) {
							noArr.push(cnt[i].dataset.userno);
					}			
		        }
		   }
 
			 console.log(noArr);		
			
			 $.ajax({
					 type:"post",
					 url:"/miracle/address/companyUserSharingRegist",
					 data:{  addressNoArr : addressNo.toString(),
					 		 shareUserNoArr : noArr.toString() },
					 async:false,
					 success:function(data) {
						if(data == "success"){
							alert("주소록 공유에 성공하였습니다.");
							location.href = "/miracle/address/addressCompanyUser";
						} else {
							alert("주소록 공유에 실패하였습니다.");
							location.href = "/miracle/address/addressCompanyUser";
						}
					 },
					 error:function(data){
						alert("22 : " + data);
					 }
	      	  });
	      }
      }	
		
 
	  if(document.getElementById("myUserModify")) {		
		  const $myUserModify = document.getElementById("myUserModify");	
	 	  $myUserModify.onclick = function() {	    
	 	     var cnt = $("input:checkbox[name='chk']:checked");
	     	 var count = cnt.length;
		 	 var noArr = [];
		 	 var nameArr = [];
		  	
		 	 console.log(cnt.length);
		
	
		 
			  if(cnt.length == 0) { 
				alert("수정할 주소록을 선택해 주세요");
				} else if(cnt.length == 1){				
					if(!confirm("정말 수정하시겠습니까?")){
						 alert("수정을 취소했습니다.");							 
					 } else {
					 	var no = cnt[0].dataset.addressno
					 	console.log(no);	
					    location.href = "/miracle/address/myAddressModifyUser/"+no;
					 }							
			  	 } else {
					alert("하나의 주소록만 선택해 주세요");
			  	 }
	 
		 }
	} 
	  	
	if(document.getElementById("myUserSharedSelectBtn")) {		
			  const $myUserSharedSelectBtn = document.getElementById("myUserSharedSelectBtn");	
		 	  $myUserSharedSelectBtn.onclick = function() {	    
		 	     var cnt = $("input:checkbox[name='chk']:checked");
		     	 var count = cnt.length;
			 	 var noArr = [];
			 	 var nameArr = [];
			  	
			 	 console.log(cnt.length);
			
				  if(cnt.length == 0) { 
				  		alert("공유할 주소록을 선택해 주세요");
				  }
					  
				  for(var i = 0; i < cnt.length;i++) {
				 	 	   if(cnt) {
							noArr.push(cnt[i].dataset.userno);
							location.href = "/miracle/address/myUserSharing/"+noArr;
					       }			
		  	      }
			  } 
	 	} 
		if(document.getElementById("myUserShareBtn")) {
			const $myUserShareBtn = document.getElementById("myUserShareBtn");	
		
			$myUserShareBtn.onclick = function() {
				var addressNo = $("input[name=addressNoArr]").val().split(",");
				
				console.log(addressNo);
				console.log(addressNo.toString());					
			
				var cnt = $("input:checkbox[name='chk']:checked");
				var count = cnt.length;
				var noArr = [];
					
				console.log(cnt.length);
			
				for(var i = 0; i < cnt.length;i++) {
					 console.log(cnt);
					 console.log(cnt[0]);
	 
					if(cnt.length == 0) { 
						alert("공유할 사원을 선택해 주세요");
					} else {				
						if(cnt) {
							noArr.push(cnt[i].dataset.userno);
					}			
		        }
		   }
 
			   console.log(noArr);		
			
			   $.ajax({
					 type:"post",
					 url:"/miracle/address/myUserSharingRegist",
					 data:{  addressNoArr : addressNo.toString(),
					 		 shareUserNoArr : noArr.toString() },
					 async:false,
					 success:function(data) {
						if(data == "success"){
							alert("주소록 공유에 성공하였습니다.");
							location.href = "/miracle/address/myUserList";
						} else {
							alert("주소록 공유에 실패하였습니다.");
							location.href = "/miracle/address/myUserList";
						}
					 },
					 error:function(data){
						alert("22 : " + data);
					 }
	      	  });
	      }
      }	
      
      if(document.getElementById("sharedUserModify")) {		
			  const $sharedUserModify = document.getElementById("sharedUserModify");	
		 	  $sharedUserModify.onclick = function() {	    
		 	     var cnt = $("input:checkbox[name='chk']:checked");
			 	 console.log(cnt.length);
  
			 
				  if(cnt.length == 0) { 
					alert("수정할 주소록을 선택해 주세요");
					} else if(cnt.length == 1){				
						if(!confirm("정말 수정하시겠습니까?")){
							 alert("수정을 취소했습니다.");							 
						 } else {
						 	var no = cnt[0].dataset.addressno;
						 	console.log(no);	
						    location.href = "/miracle/address/sharedAddressModifyUser/"+no;
						 }							
				  	 } else {
						alert("하나의 주소록만 선택해 주세요");
				  	 }
		 
			}
		}  
}