$(function() {
    $('.payment_menu').click(function(){
        $('.sub_menu:nth-child(3)').toggleClass('active');
        if($('.sub_menu:nth-child(3)').hasClass('active')) {
            $(this).css('backgroundColor','#FFF282');
        } else {
            $(this).css('backgroundColor','#FFFEE2');
        }
    })
    
    $('.board_menu').click(function(){
        $('.sub_menu:nth-child(5)').toggleClass('active');
        if($('.sub_menu:nth-child(5)').hasClass('active')) {
            $(this).css('backgroundColor','#FFF282');
        } else {
            $(this).css('backgroundColor','#FFFEE2');
        }
    })
    
    $('.address_menu').click(function(){
        $('.sub_menu:nth-child(8)').toggleClass('active');
        if($('.sub_menu:nth-child(8)').hasClass('active')) {
            $(this).css('backgroundColor','#FFF282');
        } else {
            $(this).css('backgroundColor','#FFFEE2');
        }
    })
    
    $('#ck1').click(function(){
		if($('#ck1').is(":checked")) $("input").prop("checked", true);
		else $("input").prop("checked", false);
	});
	
	$('.bottom #chk1').click(function(){
		if($('.bottom #chk1').is(":checked")) $("input:checkbox[name='chk']").prop("checked", true);
		else $("input").prop("checked", false);
	});
	
	$('.bottom #ck1').click(function(){
		if($('.bottom #ck1').is(":checked")) $("input:checkbox[name='chk']").prop("checked", true);
		else $("input").prop("checked", false);
	});
	
	const expandBox = $("div.expand-box"); 
		
	for(var i = 1; i < expandBox.length + 1; i++) {	
		$('#ckg' + i).click(function(e){
			if($(e.target).is(":checked")) 
			$(e.target).parent().nextUntil("div.groupName_bar").children().children("input:checkbox[name='chk']").prop("checked", true);
			else $("input").prop("checked", false);
		});
	}
	
	for(var i = 1; i < expandBox.length + 1; i++) {
		$('#dept' + i).click(function(e){
			$(e.target).parent().nextUntil("div.groupName_bar").toggleClass('active');
			if($(e.target).parent().nextUntil("div.groupName_bar").hasClass('active')) {
				$((e.target).children[1]).css("transform","rotate(0)");
			} else {
				$((e.target).children[1]).css("transform","rotate(90deg)");
			}
		});
	}
	
});

