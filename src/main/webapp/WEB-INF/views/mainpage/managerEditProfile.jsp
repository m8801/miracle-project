<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>프로필 수정</title>
<style>
	.section{
	    flex: 1;
	    height: 100%;
	    display: flex;
	    flex-direction: column;
	    padding: 50px;
	    box-sizing: border-box;
	    font-size: 24px;
	}
	
	.profileBox {
		border: 5px solid #FDE82D;
		background: #FFFEE2;
		border-radius: 20px;
		width: 300px;
		height: 300px;
		margin: 0 auto 30px;
		text-align: center;
		box-sizing: border-box;
		padding: 20px 0;
	}
	
	.profileBox .profileImg {
		width: 160px;
		height: 160px;
		background: #fff;
		border-radius: 50%;
		overflow: hidden;
		margin: 0 auto 20px;
		border: 10px solid #fff;
		box-sizing: border-box;
	}
	
	.profileBox .profileImg img {
		width: 100%;
		height: 100%;
	/* 	position: relative;
		right: 15px; */
	}
	
	.profileBox span {
		display: block;
		cursor: pointer;
		font-size: 18px;
	}
	
/* 	.profileBox .profileChange {
		margin-bottom: 10px;
	} */
	
	.outerBox {
		width: 50%;
		height: 400px;
		border: 5px solid #FDE82D;
		background: #FFFEE2;
		border-radius: 20px;
		margin: 0 auto 30px;
	}
	
	.innerBox {
		width: 95%; 
		height: 90%;
		background: #fff;
		margin: 20px auto;
		border-radius: 20px;
		padding: 30px 50px;
		box-sizing: border-box;
		font-size: 18px;
	}
	
	.innerBox .name {
		font-size: 36px;
		margin-right: 10px;
	}
	
	.innerBox .rank {
		color: #7E7E7E;
	}
	
	.innerBox tr {
		height: 50px;
	}
	
	.innerBox .title {
		width: 100px;
	}
	
	.innerBox .content {
		width: 200px;
		color: #7E7E7E;
	}
	
	.innerBox .content input {
		border: 2px solid #D9D9D9;
	}
	
	.innerBox .phoneChange {
		cursor: pointer;
	}
	
	#profileTable {
		margin-bottom: 70px;
	}
	
	.btn {
		text-align: center;
	}
	
	.ok, .cancel {
		padding: 10px 50px;
		border: none;
		font-size: 24px;
		background: #FFF282;
		color: #fff;
		border-radius: 10px;
	}
	
	/* #pwdChange {
		padding: 0 5px;
		border: none;
		font-size: 18px;
		background: #FFF282;
		color: #fff;
		border-radius: 10px;
	} */
	
	.cancel {
		display: inlint-block;
		margin-left: 200px;
	}
	
	#profileChangeForm {
		display: none;
		position: absolute;
		width: 400px;
		/* height: 200px; */
		left: 48%;
		top: 30%;
		border: 5px solid #FDE82D;
		background: #FFF;
		border-radius: 20px;
		text-align: center;
		padding: 30px;
	}
	
	#profileModify {
		background: none;
		border: none;
		font-size: 18px;
		cursor: pointer;
		margin-bottom: 10px;
	}
	
	#profileSpan {
		font-size: 24px;	
		margin-bottom: 30px;
	}
	
	#profileUpload {
		margin-bottom: 40px;
		font-size: 14px;
	}
	
	.profileBtnBox {
		
	}
	
	#profileChange {
		margin-right: 50px;
	
	}
	
	#profileChange, #profileCancel {
		padding: 10px 30px;
		font-size: 18px;
		border: none;
		background: #FFF282;
		border-radius: 10px;
		color: #fff;
		cursor: pointer;
	}
		
	#profileName {
		display: inline-block;
	    height: 40px;
	    padding: 0 10px;
	    border: 1px solid #dddddd;
	    width: 60%;
	    color: #999999;
	    font-size: 16px;
	}
	
	#profileLabel { 
		display: inline-block;
	    padding: 7px 10px;
	    color: #fff;
	  	background: #FFF282;
		border-radius: 5px;
	    cursor: pointer;
	    margin-left: 10px;
	    font-size: 18px;
	    margin-bottom: 30px;
	} 
	
	#profileUpload { 
		/* 파일 필드 숨기기 */ 
		position: absolute; 
		width: 0; 
		height: 0; 
		padding: 0; 
		overflow: hidden; 
		border: 0; 
	}  

</style>
</head>
<body>
	<div class="container">
		<jsp:include page="../common/managerSidebar.jsp"/>
	    <section class="section">
	   		<div class="profileBox">
		   		<div class="profileImg">
		   			<img id="profileView" src="${pageContext.servletContext.contextPath}/resources/images/profileFiles/${ sessionScope.loginMember.updateFile }" onerror="this.src='${pageContext.servletContext.contextPath}/resources/images/profileFiles/adminUser.png'" alt="프로필 이미지">
		   		</div>
				<button id="profileModify">프로필 사진 변경</button>
				<form id="profileChangeForm" action="${ pageContext.servletContext.contextPath }/mainpage/profile" method="post" encType="multipart/form-data"> 
					<span id="profileSpan"> 프로필 사진 변경</span>
					<input id="profileName" value="프로필 이미지" placeholder="프로필 이미지">
					<label id="profileLabel" for="profileUpload">파일 찾기</label>
					<input type="file" name="profileUpload" id="profileUpload" accept=".jpg, .png, .jpeg, .gif" onchange="setThumbnail(event);">
					<div class="profileBtnBox">
						<input id="profileChange" type="submit" value="변경">
						<input id="profileCancel" type="button" value="취소">
					</div>
				</form>
	   			<span class="passwordChange"><a href="${ pageContext.servletContext.contextPath }/login/pwChange">비밀번호 변경</a></span>
	   		</div>
	   		
	   		<div class="outerBox">
	   			<div class="innerBox">
	   				<form id="editForm" action="${ pageContext.servletContext.contextPath }/mainpage/managerEditProfile" method="post">
		   				<span class="name"><c:out value="${ sessionScope.loginMember.userName }"/></span>
		   				<span class="rank"><c:out value="${ sessionScope.loginMember.jobName }"/></span>
		   				<table id="profileTable">
		   					<tbody>
		   						<tr>
		   							<td class="title">사번 &nbsp;</td>
		   							<td class="content"><c:out value="${ sessionScope.loginMember.userNo }"/></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">부서 &nbsp;</td>
		   							<td class="content"><c:out value="${ sessionScope.loginMember.deptName }"/></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">직무 &nbsp;</td>
		   							<td class="content"><c:out value="${ sessionScope.loginMember.jobName }"/></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">연락처 &nbsp;</td>
		   							<td class="content"><input type="tel" name="phone" value="${ sessionScope.loginMember.phone }"></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">이메일 &nbsp;</td>
		   							<td class="content"><input type="email" name="email" value="${ sessionScope.loginMember.email }"></td>
		   						</tr>
		   					</tbody>
		   				</table>
		   				<div class="btn">
				   			<button class="ok">수정</button>
				   			<a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain" class="cancel">취소</a>
			   			</div>
		   			</form>
	   			</div>
	   		</div>

	   </section>
	</div>
	
	<script>
		
	    const modify = document.getElementById("profileModify");
		const form = document.querySelector("#profileChangeForm");
		const cancel = document.getElementById("profileCancel");
		
	    modify.addEventListener("click", function() {
	    	form.style.display = 'block';
	    });
	    
	    cancel.addEventListener("click", function() {
	    	form.style.display = 'none';
	    });
	    
	    function setThumbnail(event) 
		{ 
			var reader = new FileReader(); 
			
			reader.onload = function(event) { 
				var img = document.querySelector(".profileImg img"); 
				img.setAttribute("src", event.target.result); 
				document.querySelector(".profileImg").append(img);
				
				var profileName = document.getElementById("profileUpload").value;
				document.getElementById("profileName").value = profileName;
			}; 
			
			reader.readAsDataURL(event.target.files[0]); 
		}
	    
	    const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			alert(message);
		}
		
	</script>

</body>
</html>