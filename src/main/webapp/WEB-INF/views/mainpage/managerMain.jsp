<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자 메인</title>
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>
<style>
	.menu div:nth-child(1){
	   background: #FFF282;
	}
	
	.section{
	    flex: 1;
	    height: 100%;
	    display: flex;
	    padding: 50px;
	    box-sizing: border-box;
	}
	
	.wrap1 {
		flex: 3;
		display: flex;
		flex-direction: column;
		margin-right: 60px;
	}
	
	.wrap2 {
		flex: 2;
		display: flex;
		flex-direction: column;
	}
	
	.quickSlotBox {
		display: flex;
		width: 100%;
		height: 150px;
		justify-content: space-between;
		text-align: center;
		margin-bottom: 20px;
	}
	
	.quickSlot-item {
		width: 150px;
		padding: 30px 0;
		border: 2px solid #EEEEEE;
	}
	
	.quickSlot-item img {
		width: 40px;
	}
	
	.quickSlot-item span {
		display: block;
		font-size: 18px;
		margin-top: 10px;
	}
	
	.notice {
		flex: 1;
		border: 2px solid #EEEEEE;
	}
	
	.notice tr {
		height: 50px;
	}
	
	.notice td {
		padding: 0 30px;
		font-size: 18px;
	}
	
	.notice .head {
		font-size: 24px;
		background: #FFFEE2;
		font-weight: 700;
	}
	
	.notice .td-date {
		width: 200px;
	}
	
	.news {
		margin-bottom: 30px;
	}
	
	.td-hidden {
		display: none;
	}
	
		.approval {
		flex: 1;
		border: 2px solid #EEEEEE;
		padding: 20px 30px;
	}
	
	.approval td {
		font-size: 18px;
		height: 35px;
	}
	
	.approval .ap-head {
		font-size: 24px;
	}
	
	.approval .ap-title {
		width: 150px;
	}
	
	/* .calendar {
		flex: 2;
		border: 2px solid #EEEEEE;
		text-align: center;
		font-size: 18px;
	}
	
	.fa-chevron-left,
	.fa-chevron-right {
		cursor: pointer;
	}
	
	.calendar .month {
		border-bottom: 2px solid #EEEEEE;
		font-size: 32px;
	}
	
	.calendar .sun {
		color: red;
	}
	
	.calendar .sat {
		color: blue;
	}
	
	.calendar .pre,
	.calendar .next {
		color: #ddd;
	}
	
	.calendar-box {
		flex: 1;
		border: 2px solid #EEEEEE;
		text-align: center;
		font-size: 18px;
	}
	
	.calendar-box td {
		border-bottom: 1px solid #EEEEEE;
	}
	
	.calendar-box .last {
		border-bottom: none;
	}
	
	.calendar-box .day {
		width: 150px;
		border-right: 1px solid #EEEEEE;
	} */

</style>
</head>
<body>

	<div class="container">
		<jsp:include page="../common/managerSidebar.jsp"/>
	   	<section class="section">
	   		<div class="wrap1">
		   		<div class="quickSlotBox">
		   			<div class="quickSlot-item">
		   				<a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">
		   					<img src="/miracle/resources/images/document.png"/>
		   				</a>
		   				<span>결제 조회</span>
		   			</div>
		   			<div class="quickSlot-item">
		   				<a href="${ pageContext.servletContext.contextPath }/userManagement/addUser">
		   					<img src="/miracle/resources/images/notice.png"/>
		   				</a>
		   				<span>사용자 추가</span>
		   			</div>
		   			<div class="quickSlot-item">
		   				<a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleAdd">
		   					<img src="/miracle/resources/images/calendar.png"/>
		   				</a>
		   				<span>일정 추가</span>
		   			</div>
		   			<div class="quickSlot-item">
		   				<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">
		   					<img src="/miracle/resources/images/task.png"/>
		   				</a>
		   				<span>사용자 조회</span>
		   			</div>
		   		</div>
		   		
		   		<table class="notice news">
		   			<tbody>
		   				<tr>
		   					<td colspan="2" class="head">
		   						<a href="${ pageContext.request.contextPath }/board/list">새소식 게시판&nbsp;
		   							<i class="fas fa-chevron-right" aria-hidden="true"></i>
		   						</a>
		   					</td>
		   				</tr>
		   				<c:forEach var="newsBoardList" items="${ requestScope.newsBoardList }">
							<tr>
								<td class="td-hidden"><c:out value="${ newsBoardList.boardNo }" /></td>
								<td><c:out value="${ newsBoardList.title }" /></td>
								<td class="td-date"><c:out value="${ newsBoardList.createDate }" /></td>
							</tr>
						</c:forEach>
		   			</tbody>
		   		</table>
		   		
		   		<table class="notice company">
		   			<tbody>
		   				<tr>
		   					<td colspan="2" class="head">
		   						<a href="${ pageContext.request.contextPath }/board/jobList">사내 게시판&nbsp;
		   							<i class="fas fa-chevron-right" aria-hidden="true"></i>
		   						</a>
		   					</td>
		   				</tr>
		   				<c:forEach var="jobBoardList" items="${ requestScope.jobBoardList }">
							<tr>
								<td class="td-hidden"><c:out value="${ jobBoardList.boardNo }" /></td>
								<td><c:out value="${ jobBoardList.title }" /></td>
								<td class="td-date"><c:out value="${ jobBoardList.createDate }" /></td>
							</tr>
						</c:forEach>
		   			</tbody>
		   		</table>
	   		</div>
	   		
	   		<div class="wrap2">	 
	   			<table class="approval">
	   				<tbody>
	   					<tr>
	   						<td colspan="2" class="ap-head">전자결재</td>
	   					</tr>
	   					<tr>
	   						<td class="ap-title">업무 보고 - 대기</td>
	   						<td class="all-ap">총 <c:out value="${ reportWaitingCount }"/> 건</td>
	   					</tr>
	   					<tr>
	   						<td class="ap-title">휴가 신청 - 대기</td>
	   						<td class="all-ap">총 <c:out value="${ vacationWaitingCount }"/> 건</td>
	   					</tr>
	   					<tr>
	   						<td class="ap-title">업무 보고 - 승인</td>
	   						<td class="all-ap">총 <c:out value="${ reportCompleteCount }"/> 건</td>
	   					</tr>
	   					<tr>
	   						<td class="ap-title">휴가 신청 - 승인</td>
	   						<td class="all-ap">총 <c:out value="${ vacationCompleteCount }"/> 건</td>
	   					</tr>
	   					<tr>
	   						<td class="ap-title">업무 보고 - 반려</td>
	   						<td class="all-ap">총 <c:out value="${ reportRejectCount }"/> 건</td>
	   					</tr>
	   					<tr>
	   						<td class="ap-title">휴가 신청 - 반려</td>
	   						<td class="all-ap">총 <c:out value="${ vacationRejectCount }"/> 건</td>
	   					</tr>
	   				</tbody>
	   			</table>
	   		
	   		
	   		  			
	   			<!-- <table class="calendar">
	   				<tbody>
	   					<tr>
	   						<td><i class="fas fa-chevron-left" aria-hidden="true"></i></td>
	   						<td colspan="5" class="month">2022.03</td>
	   						<td><i class="fas fa-chevron-right" aria-hidden="true"></i></td>
	   					</tr>
	   					<tr>
	   						<td>SUN</td>
	   						<td>MON</td>
	   						<td>TUE</td>
	   						<td>WED</td>
	   						<td>THU</td>
	   						<td>FRI</td>
	   						<td>SAT</td>
	   					</tr>
	   					
	   					<tr>
	   						<td class="pre">27</td>
	   						<td class="pre">28</td>
	   						<td>1</td>
	   						<td>2</td>
	   						<td>3</td>
	   						<td>4</td>
	   						<td class="sat">5</td>
	   					</tr>
	   					
	   					<tr>
	   						<td class="sun">6</td>
	   						<td>7</td>
	   						<td>8</td>
	   						<td>9</td>
	   						<td>10</td>
	   						<td>11</td>
	   						<td class="sat">12</td>
	   					</tr>
	   					
	   					<tr>
	   						<td class="sun">13</td>
	   						<td>14</td>
	   						<td>15</td>
	   						<td>16</td>
	   						<td>17</td>
	   						<td>18</td>
	   						<td class="sat">19</td>
	   					</tr>
	   					
	   					<tr>
	   						<td class="sun">20</td>
	   						<td>21</td>
	   						<td>22</td>
	   						<td>23</td>
	   						<td>24</td>
	   						<td>25</td>
	   						<td class="sat">26</td>
	   					</tr>
	   					
	   					<tr>
	   						<td class="sun">27</td>
	   						<td>28</td>
	   						<td>29</td>
	   						<td>30</td>
	   						<td>31</td>
	   						<td class="next">1</td>
	   						<td class="next">2</td>
	   					</tr>
	   				</tbody>
	   			</table>
	   			
	   			<table class="calendar-box">
	   				<tbody>
	   					<tr>
	   						<td class="day">6<br>일요일</td>
	   						<td>등록된 일정이 없습니다.</td>
	   					</tr>
	   					<tr>
	   						<td class="day">7<br>월요일</td>
	   						<td>등록된 일정이 없습니다.</td>
	   					</tr>
	   					<tr>
	   						<td class="day last">8<br>화요일</td>
	   						<td class="last">등록된 일정이 없습니다.</td>
	   					</tr>
	   				</tbody>
	   			</table> -->
	   		</div>
	   </section>
	</div>
	

	
	
	<!-- <script src="/miracle/resources/js/jquery-3.6.0.min.js"></script> -->
	<script src="https://kit.fontawesome.com/6522322312.js"></script>
	<script>
	   /* $('.menu div:nth-child(4)').click(function () {
	      $('.sub-menu').toggleClass('active');
	   }) */
	   
	   
	   /* if(document.getElementById("logout")) {
		
			const $logout = document.getElementById("logout");
			
			$logout.onclick = function() {
			
				location.href = "/miracle/login/logout";
				
			}
		} */
	   
	   /* 비지니스 로직 성공 alert 메시지 처리 */
		const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			alert(message);
		}
		
		if(document.querySelectorAll(".news td")) {
			const $tds = document.querySelectorAll(".news td");
			
			for(let i = 1; i < $tds.length; i++) {
				$tds[i].onmouseenter = function() {
					this.parentNode.style.cursor = "pointer";
				}
				
				$tds[i].onclick = function() {
					const no = this.parentNode.children[0].innerText;
					location.href = "${ pageContext.servletContext.contextPath }/board/detail?no=" + no;
				}
			}
		}
		
		if(document.querySelectorAll(".company td")) {
			const $tds = document.querySelectorAll(".company td");
			
			for(let i = 1; i < $tds.length; i++) {
				$tds[i].onmouseenter = function() {
					this.parentNode.style.cursor = "pointer";
				}
				
				$tds[i].onclick = function() {
					const no = this.parentNode.children[0].innerText;
					location.href = "${ pageContext.servletContext.contextPath }/board/jobDetail?no=" + no;
				}
			}
		}
	
	   
	</script>


</body>
</html>