<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/managerScheduleAdd.css">

<style>
	.fc-event-main {
		color: blue;
	}
	.fc-event-main:hover {
		background-color: #ffe957;
	}	
	.fc-h-event {
		background-color: #fff7c1;
        border: 1px solid #ffe957;
        text-align: center;
	}
	.fc-h-event .fc-event-main {
		color: #000;
		font-weight: bold;
	}
</style>

<title>사용자 일정 추가화면</title>

</head>
<body>

<div class="container">

   <aside class="side">
	      <div class="profile">
	         <div class="img"><img src="/miracle/resources/images/profile.jpg" alt=""></div>
	         <div class="set">
	         	<a href="${ pageContext.servletContext.contextPath }/mainpage/managerEditProfile">
	         		<img src="/miracle/resources/images/setting.png" alt="">
	         	</a>
	         </div>
	         <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <a href="${ pageContext.servletContext.contextPath }/login/logout" id="logout">로그아웃</a>
	      </div>
	      <div class="menu">
	         <div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
	         <div>
	            <a href="${ pageContext.servletContext.contextPath }/board/main">게시판</a>
	         </div>
	         <div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
			</div>

	   </aside>

    <section class="sectionBox">

        <div class="scheduleBox">

            <div class="container02">
				

                <div id="submenu" class="SubAddBar">
                    <h3>일정이 추가 되었습니다.</h3>
                    <span><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain" onclick="ScAdd01()">확인</a></span>
                </div>

                <div class="section02">


                    <div id='external-events'></div>
                    <div id='calendar-container'>
                        <div id='calendar'></div>
                    </div>


                </div>
                <div>
	                <div class="radioBoxMenu" id="radioCheck">
		                <label for="">부서 : </label>
		                <input type="radio" id="deptCheck" name="changType" value="dept">
		
		                <label for="">개인 : </label>
		                <input type="radio" id="userCheck" name="changType" value="user">
	            	</div>
                </div>
                
                

                <div id="se5" class="section05">
                    <!--  <span class="a1"><a id="ScAdd2" href="#" onclick="ScAdd02()">일정추가</a></span>-->
                    <span class="a1"><button type="submit" id="ScAdd2" onclick="allSaveSchedule()">일정추가</button></span>
                    <span class="a2"><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">취소</a></span>
                </div>

            </div>
        </div>
    </section>
</div>

<script>
let calendar = null;
let Alldata = {};

	document.addEventListener('DOMContentLoaded', function() {
		
		  let Calendar = FullCalendar.Calendar;
		  let Draggable = FullCalendar.Draggable;
	
		  let calendarEl = document.getElementById('calendar');
		  let checkbox = document.getElementById('drop-remove');
	
		  
		  calendar = new Calendar(calendarEl, {
			  
		    headerToolbar: {
		      left: 'prev,next today',
		      center: 'title',
		      right: 'dayGridMonth,timeGridWeek,timeGridDay'
		    },
		    
            initialView: 'dayGridMonth',
            editable: true,
            selectable: true, // 달력 일자 드래그 설정가능
            nowIndicator: true, // 현재 시간 마크
            dayMaxEvents: true, // 이벤트가 오버되면 높이 제한 (+ 몇 개식으로 표현)
            locale: 'ko', // 한국어 설정
		    
            eventAdd: function(obj) {
            	
           	sDate = 
                   obj.event._instance.range["start"].getFullYear() + 
                   '-'+(obj.event._instance.range["start"].getMonth() +1) +
                   '-' +  obj.event._instance.range["start"].getDate();
           	
           	eDate =
           		  obj.event._instance.range["end"].getFullYear() + 
                     '-'+(obj.event._instance.range["end"].getMonth() +1) +
                     '-' +  (obj.event._instance.range["end"].getDate() -1);
            	
           	 Alldata = {
                     "start": sDate,
                     "end": eDate,
                     "title": obj.event._def["title"],
                     "allday": obj.event._def["allDay"],
                     "defId": obj.event._instance["defId"],
                     "instanceId": obj.event._instance["instanceId"]
                 };
           	 
            	let allEvent = calendar.getEvents();
                console.log("allEvent : " +allEvent);
                console.log("Alldata : " + Alldata);
                
                let jsondata = JSON.stringify(Alldata);
				console.log("jsondata : " + jsondata);
				
            },
            
            select: function(arg) { // 캘린더에서이벤트를 생성
                let title = prompt('일정 내용을 입력하세요 :');
                if (title) {
                    calendar.addEvent({
                        title: title,
                        start: arg.start,
                        end: arg.end,
                        allDay: arg.allDay
                    })
                }
                calendar.unselect()
            },
            
		  });
	
		  calendar.render();
		  
		  
		  
		});
	

	function allSaveSchedule() {
		
		document.getElementById("submenu").style.display = "block";
		
		$.ajax({
			url:"/miracle/schedule/managerScAdd",
			type:"post",
            data: {
            	
            	      addEvent : JSON.stringify(Alldata),
            	      "checked" : $('input[name=changType]:checked').val()
            	      
            	     },
            
		    success:function(data, textStatus, xhr){
				 console.log(data);
					 
			},
			error:function(xhr, status, error){
				console.log(error);
				alert("추가실패")
			}
			
		});
	
		
	}	
	
    function ScAdd01() {
        document.getElementById("submenu").style.display = "none";
    }
</script>


</body>
</html>






















