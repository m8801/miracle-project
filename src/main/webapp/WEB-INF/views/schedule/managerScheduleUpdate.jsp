<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/managerScheduleUpdate.css">
<style>
	.fc-event-main {
		cursor: pointer;
		color: blue;
	}
	.fc-event-main:hover {
		background-color: #ffe957;
	}	
	.fc-h-event {
		background-color: #fff7c1;
        border: 1px solid #ffe957;
        text-align: center;
	}
	.fc-h-event .fc-event-main {
		color: #000;
		font-weight: bold;
	}
</style>
<title>매니저 일정수정</title>
</head>
<body>


<div class="container">

    <aside class="side">
	      <div class="profile">
	         <div class="img"><img src="/miracle/resources/images/profile.jpg" alt=""></div>
	         <div class="set">
	         	<a href="${ pageContext.servletContext.contextPath }/mainpage/managerEditProfile">
	         		<img src="/miracle/resources/images/setting.png" alt="">
	         	</a>
	         </div>
	         <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <a href="${ pageContext.servletContext.contextPath }/login/logout" id="logout">로그아웃</a>
	      </div>
	      <div class="menu">
	         <div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
	         <div>
	            <a href="${ pageContext.servletContext.contextPath }/board/main">게시판</a>
	         </div>
	         <div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
			</div>

	   </aside>

    <section class="sectionBox">

        <div class="scheduleBox">

            <div class="container02">


                <div id="submenu" class="SubAddBar">
                    <h3>일정이 수정 되었습니다.</h3>
                    <span><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain" onclick="ScUpdate00()">확인</a></span>
                </div>

                <div class="section02">

                    <div id='external-events'></div>

                    <div id='calendar-container'>
                        <div id='calendar'></div>
                    </div>


                </div>
                <div class="section03">
                    <label>변경할 일정 내용 :  </label>
                    <input type="text" id="textModify" placeholder="수정할 이벤트를 클릭하세요.">
                </div>

                <div id="se5" class="section05">
                    <!--  <span class="a1"><a id="ScAdd2" href="#" onclick="ScAdd02()">일정추가</a></span>-->
                    <span class="a1"><button type="submit" id="ScAdd2" onclick="mangerScUpdate()">일정수정</button></span>
                    <span class="a2"><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">취소</a></span>
                </div>

            </div>
        </div>
    </section>
</div>

<script>
  let Modify = {}; 
  
   $(function(){

          	$.ajax({
          	url: "/miracle/schedule/managerListAll",
           	type: "get",
           	data: {type : "${ type }"},

			success:function(data, textStatus, xhr){
				 console.log(data);

					
				let allData = data;								
					
				let allDataArray = [];							
				let oneDayData = {};							
					
				for(let i = 0; i < allData.length; i++){
						
					oneDayaData = {
								"start" : allData[i].start,			 
								"end" : allData[i].end,
								"title": allData[i].title,
								"no" : allData[i].no
								
					}
					allDataArray.push(oneDayaData);		
				}

			
				let calendarEl = $('#calendar')[0];
	            let calendar = new FullCalendar.Calendar(calendarEl, {
	                     headerToolbar: {
					                             left: 'prev,next today',
					                             center: 'title',
					                             right: 'dayGridMonth'
	                    },
	                           
	                    events: allDataArray,
	                    locale: 'ko',
	                       	
	                    eventClick: function(info) {
	
	   		            // 클릭한 title명 
	   		            let titleUpdate = info.event.title;
	   		            console.log("클릭한 title : " + titleUpdate);
	   		            	    
	    		        let textModify = document.getElementById("textModify");
	   		            textModify.value =  info.event.title; 
	   		            	    
	   		            	    
	   		            Modify = {
	  		            	    		"title" : info.event.title,
	   		            	    		 "no" : info.event._def.extendedProps.no.toString()
	   		                          };
	   		            	    
	   		             },
	             });
                       calendar.render();
		 	},
				 
			error:function(xhr, status, error){
					 console.log(error);
			},
				 
	});

});
   
   
   function mangerScUpdate() {
	   
	   document.getElementById("submenu").style.display = "block"; 
	   
	   $.ajax({
			url:"/miracle/schedule/managerscheduleUpdate",
			type:"post",	
			data:{
				
				     "Modify" : JSON.stringify(Modify),
				     "textModify" : document.getElementById("textModify").value     
				    	 
				     },
			
			success:function(data, textStatus, xhr){
				 console.log(data);
				 
				 if(data == "success"){
					console.log(data);
				 } else {
					 alert("수정 실패하였습니다.");
				 }

			},
			
			error:function(error){
				 console.log(error);
			}
			
		}); 
   }
   

    function ScUpdate00() {
       document.getElementById("submenu").style.display = "none";
   }

   


</script>

</body>
</html>