<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/userScheduleMain.css">

<style>
	.fc-event-main {
		color: blue;
	}
	.fc-event-main:hover {
		background-color: #ffe957;
	}	
	.fc-h-event {
		background-color: #fff7c1;
        border: 1px solid #ffe957;
        text-align: center;
	}
	.fc-h-event .fc-event-main {
		color: #000;
		font-weight: bold;
	}
</style>

<title>사용자 일정 메인화면</title>

</head>
<body>

<div class="container">

   <aside class="side">
	      <div class="profile">
	         <div class="img"><img src="/miracle/resources/images/profile.jpg" alt=""></div>
	         <div class="set">
	         	<a href="${ pageContext.servletContext.contextPath }/mainpage/userEditProfile">
	         		<img src="/miracle/resources/images/setting.png" alt="">
	         	</a>
	         </div>
	         <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <a href="${ pageContext.servletContext.contextPath }/login/logout" id="logout">로그아웃</a>
	      </div>
	      <div class="menu">
	         <div><a href="${ pageContext.servletContext.contextPath }/mainpage/userMain">홈</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
	         <div>
	            <a href="${ pageContext.servletContext.contextPath }/board/main-boardList">게시판</a>
	         </div>
	         <div><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain">일정</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
			</div>

	   </aside>

    <section class="sectionBox">

        <div class="sectionMain">

            <div class="radioBoxMenu" id="radioCheck">
                <label for="">부서 : </label>
                <input type="radio" id="check2" name="changType" value="dept">

                <label for="">개인 : </label>
                <input type="radio" id="check3" name="changType" value="user"  checked>
            </div>

            <div class="DayContainer" id="DayController">
                <h3 class="classDay"><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleAdd">추가</a></h3>
                <h3 class="classDay"><a id="userScheduleModify">수정</a></h3>
                <h3 class="classDay"><a id="userScheduleDel">삭제</a></h3>
            </div>


            <div id='external-events'></div>

            <div id='calendar-container'>
                <div id='calendar'></div>
            </div>

        </div>

    </section>

</div>


<script>

     $(function(){
           
          let type = $('input[name=changType]:checked').val();
           

          $("#userScheduleModify").prop("href","${ pageContext.servletContext.contextPath }/schedule/userScheduleUpdate?type=user");
          $("#userScheduleDel").prop("href","${ pageContext.servletContext.contextPath }/schedule/userScheduleDelete?type=user");
         	   
           userSchedule("user");
           
           
           
           	$("#check2").click(function() {
           		userSchedule("dept");

           		document.getElementById("DayController").style.display = "none";
			});

           
           $("#check3").click(function(){
        	   userSchedule("user");
        	   
        	   document.getElementById("DayController").style.display = "block";
        	   
               $("#userScheduleModify").prop("href","${ pageContext.servletContext.contextPath }/schedule/userScheduleUpdate?type=user");
               $("#userScheduleDel").prop("href","${ pageContext.servletContext.contextPath }/schedule/userScheduleDelete?type=user");
        	   

           });
           
           
           
           
   });
          
      function userSchedule(type) {
    	  
    	  $.ajax({
	             url: "/miracle/schedule/userListAll",
	             type: "get",
	             data: {"type" : type },

				 success:function(data, textStatus, xhr){
				 	console.log(data);
				 	
					let allData = data;								// 받아온 data를 변수 allData 에 저장 하고 ,
						
					let allDataArray = [];							//  allDataArray = []; 빈 배열 생성
					let oneDayData = {};							//  oneDayData = {}; 빈 josn 생성  
						
					for(let i = 0; i < allData.length; i++){
							
						oneDayaData = {
								"start" : allData[i].start,			// 반복문 돌면서 원하는 값 . 넣어준다. 
								"end" : allData[i].end,
								"title": allData[i].title,
								"divide": allData[i].divide,
								"deptCode": allData[i].deptCode
						}
							
						allDataArray.push(oneDayaData);		// 그리고 빈 배열 allDataArray.push(oneDayaData)를 넣어준다. 
																				// 그러면 배열로 json 형태로 나오게 된다. 
					}
	
					 let calendarEl = $('#calendar')[0];
	                    let calendar = new FullCalendar.Calendar(calendarEl, {
	                          headerToolbar: {
	                               left: 'prev,next today',
	                               center: 'title',
	                               right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
	                           },
	                            
	 			               eventClick: function(info) {
					        	   console.log("title : " + info.event.title);
					        	   console.log("divide : " + info.event._def.extendedProps.divide);
				               },
				               
	                           events: allDataArray					// 풀캘린더 보면 events : [ title: title ] 이렇게 값을주는데 
	    						 												// 우리는 값을담은 allDataArry를 넣어준다. 
	                            ,locale: 'ko'
	                        });
	                        calendar.render();
					 },
					 error:function(xhr, status, error){
					 console.log(error);
				 }
  });	
    	  
      }
/*    function deptCheck() {
 	  alert("확인중")
   } */
</script>


</body>
</html>






