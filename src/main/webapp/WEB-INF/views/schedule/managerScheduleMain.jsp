<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

    <link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet' />
    <script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/managerScheduleMain.css">

<style>
	.fc-event-main {
		color: blue;
	}
	.fc-event-main:hover {
		background-color: #ffe957;
	}	
	.fc-h-event {
		background-color: #fff7c1;
        border: 1px solid #ffe957;
        text-align: center;
	}
	.fc-h-event .fc-event-main {
		color: #000;
		font-weight: bold;
	}
</style>

<title> 관리자 일정화면 </title>
</head>
<body>

<div class="container">
    <aside class="side">
	      <div class="profile">
	         <div class="img"><img src="/miracle/resources/images/profile.jpg" alt=""></div>
	         <div class="set">
	         	<a href="${ pageContext.servletContext.contextPath }/mainpage/managerEditProfile">
	         		<img src="/miracle/resources/images/setting.png" alt="">
	         	</a>
	         </div>
	         <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <a href="${ pageContext.servletContext.contextPath }/login/logout" id="logout">로그아웃</a>
	      </div>
	      <div class="menu">
	         <div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
	         <div>
	            <a href="${ pageContext.servletContext.contextPath }/board/main-boardList">게시판</a>
	         </div>
	         <div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
			</div>

	   </aside>

    <section class="sectionBox">

        <div class="sectionMain">

            <div class="radioBoxMenu" id="radioCheck">
                <label for="">부서 : </label>
                <input type="radio" id="check2" value="dept" checked name="changType" >

                <label for="">개인 : </label>
                <input type="radio" id="check3" value="user"  name="changType">
            </div>

            <div class="DayContainer">
                <h3 class="classDay"><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleAdd">추가</a></h3>
				<h3 class="classDay"><a id="modifySchedule">수정</a></h3>
				<h3 class="classDay"><a id="deleteSchedule">삭제</a></h3>
            </div>


            <div id='external-events'></div>

            <div id='calendar-container'>
                <div id='calendar'></div>
            </div>

        </div>

    </section>

</div>

<script>
// 메인에서 부서/ 개인  클릭했을때 수정을 누르면
// 수정add로 가는데, 쿼리스트링 타입속성을 붙인다. 

	$(function(){
		
	     // 클릭한 이벤트 divide 확인
	   	 //document.getElementsByClassName("fc-daygrid-day-events").divide == "dept" */
	   	 
	   	 // 이거 새로고침, 
	   	 
	   	$("#modifySchedule").prop("href","${ pageContext.servletContext.contextPath }/schedule/managerScheduleUpdate?type=dept");
	   	$("#deleteSchedule").prop("href", "${ pageContext.servletContext.contextPath }/schedule/managerScheduleDelete?type=dept");
	   	
	     
	   	managerSchedule("dept");
	     
        $("#check2").click(function(){
        	managerSchedule("dept");
        	$("#modifySchedule").prop("href","${ pageContext.servletContext.contextPath }/schedule/managerScheduleUpdate?type=dept");
        	$("#deleteSchedule").prop("href", "${ pageContext.servletContext.contextPath }/schedule/managerScheduleDelete?type=dept");
        });
	   	 
        $("#check3").click(function(){
        	managerSchedule("user");
        	$("#modifySchedule").prop("href","${ pageContext.servletContext.contextPath }/schedule/managerScheduleUpdate?type=user");
        	$("#deleteSchedule").prop("href", "${ pageContext.servletContext.contextPath }/schedule/managerScheduleDelete?type=user");
        });
        
        // prop , attr  확인 ( 속성값 , 차이점확인)
        // 쿼리스트링 넘기는거 확인
        // $ {type} ,  -> alert 창으로먼저확인

	});
	

	function managerSchedule(type){
		
	         $.ajax({
		         url: "/miracle/schedule/managerListAll",
		         type: "get",
		         data:{"type": type},
		               
				 success:function(data, textStatus, xhr){
				 	 console.log(data);
		
					 let allData = data;								
						
					 let allDataArray = [];							
					 let oneDayData = {};						
						
					 for(let i = 0; i < allData.length; i++){
						
						oneDayaData = {
							"start" : allData[i].start,			
							"end" : allData[i].end,
							"title": allData[i].title,
							"divide": allData[i].divide,
							"deptCode": allData[i].deptCode
						}
						allDataArray.push(oneDayaData);				 
					}
	
					 let calendarEl = $('#calendar')[0];
			         let calendar = new FullCalendar.Calendar(calendarEl, {
				              headerToolbar: {
				                  left: 'prev,next today',
				                  center: 'title',
				                  right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
				              },
				                         
				               eventClick: function(info) {
					        	   console.log("title : " + info.event.title);
					        	   console.log("divide : " + info.event._def.extendedProps.divide);
				               },
				           	   events: allDataArray					
				           	   
				               ,locale: 'ko'
				           });
				           calendar.render();
			     },
				 error:function(xhr, status, error){
					 console.log(error);
				 }
	        });
	}


</script>



</body>
</html>