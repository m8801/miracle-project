<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
                <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<link rel="stylesheet"  href="${ pageContext.servletContext.contextPath }/resources/css/userScheduleAdd.css">
<style>
	.fc-event-main {
		color: blue;
	}
	.fc-event-main:hover {
		background-color: #ffe957;
	}	
	.fc-h-event {
		background-color: #fff7c1;
        border: 1px solid #ffe957;
        text-align: center;
	}
	.fc-h-event .fc-event-main {
		color: #000;
		font-weight: bold;
	}
</style>
<title>Insert title here</title>
</head>
<body>

<div class="container">

    <aside class="side">
	      <div class="profile">
	         <div class="img"><img src="/miracle/resources/images/profile.jpg" alt=""></div>
	         <div class="set">
	         	<a href="${ pageContext.servletContext.contextPath }/mainpage/userEditProfile">
	         		<img src="/miracle/resources/images/setting.png" alt="">
	         	</a>
	         </div>
	         <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <a href="${ pageContext.servletContext.contextPath }/login/logout" id="logout">로그아웃</a>
	      </div>
	      <div class="menu">
	         <div><a href="${ pageContext.servletContext.contextPath }/mainpage/userMain">홈</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
	         <div>
	            <a href="${ pageContext.servletContext.contextPath }/board/main">게시판</a>
	         </div>
	         <div><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain">일정</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
			</div>

	   </aside>

   <section class="sectionBox">
        <div class="scheduleBox">
            <div class="container02">

                <div id="submenu" class="SubAddBar">
                    <h3>일정이 추가 되었습니다.</h3>
                    <span><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain" onclick="ScAdd01()">확인</a></span>
                </div>

                <div class="section02" id="ScheduleCheck">
                    <div id='external-events'></div>
                    <div id='calendar-container'></div>
            		<div><div id='calendar'></div></div>
            		
            	<div>
	                <div class="radioBoxMenu" id="radioCheck">
		                <label for="">개인 : </label>
		                <input type="radio" id="userCheck" name="changType" value="user" checked>
	            	</div>
                </div>
                
                    <div id="se5" class="section05">
                        <span class="a1"><button id="ScAdd2" onclick="allSave();">일정추가</button></span>
                        <span class="a2"><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain">돌아가기</a></span>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </section>
</div>

<script>
        let calendar = null;
        let Alldata = {};
        $(function(){

            let calendarEl = $('#calendar')[0];

             calendar = new FullCalendar.Calendar(calendarEl, { 

                expandRows: true, // 화면에 맞게 높이 재설정
                slotMinTime: '08:00', // Day 캘린더에서 시작 시간
                slotMaxTime: '20:00', // Day 캘린더에서 종료 시간

                headerToolbar: {
                    left: 'prev,next',
                    center: 'title',
                    right: 'dayGridMonth'
                },

                initialView: 'dayGridMonth',
                editable: true,
                selectable: true, // 달력 일자 드래그 설정가능
                nowIndicator: true, // 현재 시간 마크
                dayMaxEvents: true, // 이벤트가 오버되면 높이 제한 (+ 몇 개식으로 표현)
                locale: 'ko', // 한국어 설정
                
                eventAdd: function(obj) { // 이벤트가 추가되면 발생하는 이벤트
                
                	sDate = 
                        obj.event._instance.range["start"].getFullYear() + 
                        '-'+(obj.event._instance.range["start"].getMonth() +1) +
                        '-' +  obj.event._instance.range["start"].getDate();
                	
                	eDate =
                		  obj.event._instance.range["end"].getFullYear() + 
                          '-'+(obj.event._instance.range["end"].getMonth() +1) +
                          '-' +  (obj.event._instance.range["end"].getDate() -1);
                	
                	 Alldata = {
                        "start": sDate,
                        "end": eDate,
                        "title": obj.event._def["title"],
                        "allday": obj.event._def["allDay"],
                        "defId": obj.event._instance["defId"],
                        "instanceId": obj.event._instance["instanceId"]
                    };

                    let allEvent = calendar.getEvents();
                    console.log(allEvent);

                     let jsondata = JSON.stringify(Alldata);
   /*                   let jsondata = JSON.parse(Alldata); */
                     console.log("jsondata : " + jsondata);

                     
                },

                select: function(arg) { // 캘린더에서이벤트를 생성
                    let title = prompt('일정 내용을 입력하세요 :');
                    if (title) {
                        calendar.addEvent({
                            title: title,
                            start: arg.start,
                            end: arg.end,
                            allDay: arg.allDay
                        })
                    }
                    calendar.unselect()
                },
            });
            // 캘린더 랜더링
            calendar.render();

        });

        function allSave() {
        	
/* 			 let allData = JSON.stringify(Alldata);
			 console.log(allData); */
		document.getElementById("submenu").style.display = "block";

			 
             $.ajax({
                 url: "/miracle/schedule/userScAdd",
                 type: "post",
                 data: {
                 	
		           	        addEvent : JSON.stringify(Alldata),
		           	        "checked" : $('input[name=changType]:checked').val()
           	      
           	   			  },

				 success:function(data, textStatus, xhr){
					 console.log(data);
					 
				 },
				 error:function(xhr, status, error){
					 console.log(error);
					 alert("개인일정 추가 실패")
				 }
				
				 

             });
        }
        
 
        
       function ScAdd01() {
            document.getElementById("submenu").style.display = "none";
        }


</script>
</body>
</html>



















