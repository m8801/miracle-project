<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>휴가 신청서</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/paymentVacationManager.css">
    <script src="https://kit.fontawesome.com/4be044bf97.js"></script>
	<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
	<script src="${pageContext.servletContext.contextPath}/resources/js/indexManager.js"></script>
</head>
<body>
   <div class="side-bar">
		<div class="profile">
			<div class="picture">
				<img src="${pageContext.servletContext.contextPath}/resources/images/jjanggu.jpg" alt="짱구">
				<div class="set">
					<a href="${pageContext.servletContext.contextPath}/mainpage/managerEditProfile">
						<img src="${pageContext.servletContext.contextPath}/resources/images/setting.png" alt="">
					</a>
				</div>
			</div>
			<p><c:out value="${ sessionScope.loginMember.userName }"/></p>
			 <span class="logout"><a href="${ pageContext.servletContext.contextPath }/login/logout">로그아웃</a></span>
		</div>
		<div class="menu_bar">
			<div class="home">
				  <a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a>
			</div>
			
			<div class="admin">
				<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a>
			</div>		
			<span class="sub_menu"> 
				<span class="admin_page"><a href="../userManagement/managementMain.jsp">관리자 페이지</a></span> 
				<span class="regist"><a href="../userManagement/addUser.jsp">회원 추가</a></span>
			</span>
			
			<div class="payment_menu">
				<a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a>
			</div>			
			<span class="sub_menu"> 
				<span class="report_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/reportManager">업무보고서 기안</a></span>
				<span class="vacation_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/vacationManager">휴가신청서 기안</a></span> 
				<span class="payment_view"><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListManager">결재문서함</a> </span>
			</span>
			
			<div class="board_menu">
				<a href="${ pageContext.servletContext.contextPath }/board/manager-board-main">게시판</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="shared_com"><a href="#">공유게시판</a></span> 
				<span class="new_com"><a href="${ pageContext.request.contextPath }/newsBoard/list">새소식게시판</a></span>
				<span class="free_com"><a href="../board/manager-board1-1.jsp">자유게시판</a></span> 
				<span class="question_com"><a href="#">질문게시판</a></span> 
				<span class="company_com"><a href="#">사내게시판</a></span>
			</span>
			
			<div class="calendar_menu">
				<a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a>
			</div>
	
			<div class="address_menu">
				<a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="company_ad"><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyManager">회사 공용 주소록</a></span>
				<span class="personal_ad"><a href="${ pageContext.servletContext.contextPath }/address/myManagerList">내 주소록</a></span>
		   		<span class="shaed_ad"><a href="${ pageContext.servletContext.contextPath }/address/sharedManagerList">공유받은 주소록</a></span>
		   		<span class="admin_ad"><a href="${ pageContext.servletContext.contextPath }/address/list">주소록관리자</a></span>
			</span>
		</div>
	</div>
    <div class="main">
	    <form action="vacationUserRegist" method="post">
	        <div class="wrap">
	            <h2>휴가 신청서</h2>
	            <div class="content">
	                <div class="top">
	                    <div class="top1">
	                        <label>기안자 <input type="text" readonly="readonly"  value="<c:out value='${ sessionScope.loginMember.userName }'/>"></label>
	                        <label>기안 부서 <input type="text" readonly="readonly" value="<c:out  value='${ sessionScope.loginMember.deptName  }'/>"></label>
	                    </div>
	                    <div class="top2">
	                        <label>문서 제목 <input type="text" id="title" name="title" value="<c:out value='${ requestScope.title }'/>"></label>
	                    </div>
	                </div>
	                <div class="bottom">
	                    <div class="bottom_box">
	                        <div class="bottom_box1">
	                            <div class="left_box">
	                                <div class="kind_key">휴가 종류</div>
	                                <div class="date_key">휴가 기간</div>
	                                <div class="reason_key">휴가 사유</div>
	                            </div>
	                            <div class="right_box">
	                                <div class="kind_value"><textarea name="kind">${ requestScope.kind }</textarea></div>
	                                <div class="date_value"> <textarea name="period">${ requestScope.period }</textarea></div>
	                                <div class="reason_value"> <textarea name="reason">${ requestScope.reason }</textarea></div>
	                            </div>
	                        </div>
	                        <div class="bottom_box2">
	                            <div class="payment_line">
	                                <div class="line_title">결재선</div>
	                                <div>
	                                    <span>부서</span>
	                                    <span>직위</span>
	                                    <span>성명</span>
	                                </div>
	                                <div>
	                                     <span>${ approver.deptDTO.deptName }</span>
		                                 <span>${ approver.jobDTO.jobName }</span>
		                                 <span>${ approver.memberDTO.userName }</span>
	                                </div>
	                            </div>
	                        </div>
	
	                    </div>
	                </div>
	                <div class="btn">
	                    <button onclick="location.href='paymentMain(user).jsp'">등록</button>
	                    <button type="reset">초기화</button>
	                </div>
	            </div>
	        </div>
	        </form>
	    </div> 
	</body>
</html>        
            