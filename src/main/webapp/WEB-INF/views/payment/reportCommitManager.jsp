<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>      
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <title>승인 반려 팝업</title>
	    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/paymentReportCommit.css">
	    <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
		<script src="${pageContext.servletContext.contextPath}/resources/js/indexUser.js"></script>
	    <script src="${pageContext.servletContext.contextPath}/resources/js/paymentEvent.js"></script>
	    <script src="https://kit.fontawesome.com/4be044bf97.js"></script>
	    <script>  	
	    	$(function(){
	     		$("#commit").click(function(){
	    			$("#fm2").attr("action","${ pageContext.servletContext.contextPath }/payment/reportCommit").attr("method","post").submit();
	    		});
	    	
	    		$("#return").click(function(){
	    			$("#fm2").attr("action","${ pageContext.servletContext.contextPath }/payment/reportReturn").attr("method","post").submit();
	    		});
	    	});
	    </script>
	</head>
	<body>
		<jsp:include page="../payment/paymentMainManager.jsp"/>
	    <div class="main commitMain">
		    <form id="fm2">
		        <div class="wrap">
		            <h2>업무 보고서</h2>
		            <input type="hidden" name="reportNo" value="${ reportNo }"/>
		            <input type="hidden" name="paymentNo" value="${ userInfo.paymentNo }"/>
		            <div class="x-box manager">
	            		<div class="one"></div>
	            		<div class="two"></div>
	          		</div>
		            <div class="content commitContent">
		                <div class="top" >          
		                    <div class="top1" >
		                        <label>기안자 <input type="text" name="writer" readonly="readonly"  value="<c:out value='${ userInfo.reportDTO.writer }'/>"></label>
		                        <label>기안 부서 <input type="text" readonly="readonly" value="<c:out  value='${ userInfo.reportDTO.deptDTO.deptName  }'/>"></label>
		                    </div>
		                    <div class="top2">
		                        <label>문서 제목 <input type="text" id="title" name="title" readonly="readonly" value="<c:out value='${ userInfo.reportDTO.title }'/>"/></label>
		                    </div>
		                </div>
		                <div class="bottom">
		                    <div class="bottom_box">
		                        <div class="bottom_box1">
		                            <div class="left_box">
		                                <div class="box1">전일실적</div>
		                                <div class="box2"><textarea name="reasonPrev">${ userInfo.reportDTO.reasonPrev }</textarea></div>
		                            </div>
		                            <div class="right_box">
		                                <div class="box1">금일계획</div>
		                                <div class="box2"><textarea name="reasonAfter">${ userInfo.reportDTO.reasonAfter }</textarea></div>
		                            </div>
		                        </div>
		                        <div class="bottom_box2">
		                            <div class="payment_line">
		                                <div class="line_title">결재선</div>
		                                <div>
		                                    <span>부서</span>
		                                    <span>직위</span>
		                                    <span>성명</span>
		                                </div>
		                                <div>
		                                    <span>${ userInfo.reportDTO.approverDTO.deptDTO.deptName }</span>
		                                    <span>${ userInfo.reportDTO.approverDTO.jobDTO.jobName }</span>
		                                    <span>${ userInfo.reportDTO.approverDTO.approverName }</span>
		                                </div> 
		                            </div>
		                        </div>
							</div>
		                </div>
		            </div>
 		             <c:if test="${ sessionScope.loginMember.userName == userInfo.reportDTO.approverDTO.approverName }">  
			            <div class="btn">
			                <input type="submit" id="commit" value="승인">
			                <input type="button" id="return" value="반려">
			            </div>
					 </c:if> 
		        </div>
		    </form>
		</div>     
	</body>
	
</html>