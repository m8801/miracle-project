<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <title>승인 반려 팝업</title>
	    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/paymentVacationCommit.css">
	    <script src="https://kit.fontawesome.com/4be044bf97.js"></script>
		<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
		<script src="${pageContext.servletContext.contextPath}/resources/js/indexUser.js"></script>
		<script src="${pageContext.servletContext.contextPath}/resources/js/paymentEvent.js"></script>
	</head>
	<body>
		<jsp:include page="../payment/paymentMainUser.jsp"/>
	    <div class="main commitMain">
		    <form action="vacationUserCommit"id="fm2" method="post">
		        <div class="wrap">
		            <h2>휴가 신청서</h2>
		            <input type="hidden" name="vacationNo" value="${ vacationNo }"/>
		            <div class="x-box">
		            		<div class="one"></div>
		            		<div class="two"></div>
		          		</div>
		            <div class="content commitContent">
		                <div class="top">
		                    <div class="top1">
		                        <label>기안자 <input type="text" readonly="readonly"  value="<c:out value='${ userInfo.vacationDTO.writer }'/>"></label>
		                        <label>기안 부서 <input type="text" readonly="readonly" value="<c:out  value='${ userInfo.vacationDTO.deptDTO.deptName  }'/>"></label>
		                    </div>
		                    <div class="top2">
		                        <label>문서 제목 <input type="text" id="title" name="title" readonly="readonly" value="<c:out value='${ userInfo.vacationDTO.title }'/>"></label>
		                    </div>
		                </div>
		                <div class="bottom">
		                    <div class="bottom_box">
		                        <div class="bottom_box1">
		                            <div class="left_box">
		                                <div class="kind_key">휴가 종류</div>
		                                <div class="date_key">휴가 기간</div>
		                                <div class="reason_key">휴가 사유</div>
		                            </div>
		                            <div class="right_box">
		                                <div class="kind_value"><textarea readonly name="kind">${ userInfo.vacationDTO.kind }</textarea></div>
		                                <div class="date_value"> <textarea readonly name="period">${ userInfo.vacationDTO.period }</textarea></div>
		                                <div class="reason_value"> <textarea readonly name="reason">${ userInfo.vacationDTO.reason }</textarea></div>
		                            </div>
		                        </div>
		                        <div class="bottom_box2">
		                            <div class="payment_line">
		                                <div class="line_title">결재선</div>
		                                <div>
		                                    <span>부서</span>
		                                    <span>직위</span>
		                                    <span>성명</span>
		                                </div>
		                                <div>
		                                     <span>${ userInfo.vacationDTO.approverDTO.deptDTO.deptName }</span>
			                                 <span>${ userInfo.vacationDTO.approverDTO.jobDTO.jobName }</span>
			                                 <span>${ userInfo.vacationDTO.approverDTO.approverName }</span>
		                                </div>
		                            </div>
		                        </div>		
		                    </div>
		                    </div>
		                </div>
		        </div>
		    </form>
	    </div>
	</body>
</html>           
            