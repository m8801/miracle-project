<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="pagingArea" align="center">
	
		<!-- 맨 앞으로 이동 버튼 -->
	    <button id="startPage"><<</button>
		
		<!-- 이전 페이지 버튼 -->
		<c:if test="${ requestScope.selectReportCriteria.pageNo <= 1 }">
			<button disabled><</button>
		</c:if>
		<c:if test="${ requestScope.selectReportCriteria.pageNo > 1 }">
			<button id="prevPage"><</button>
		</c:if>
		
		<!-- 숫자 버튼 -->
		<c:forEach var="p" begin="${ requestScope.selectReportCriteria.startPage }" end="${ requestScope.selectReportCriteria.endPage }" step="1">
			<c:if test="${ requestScope.selectReportCriteria.pageNo eq p }">
				<button disabled><c:out value="${ p }"/></button>
			</c:if>
			<c:if test="${ requestScope.selectReportCriteria.pageNo ne p }">
				<button onclick="pageButtonAction(this.innerText);"><c:out value="${ p }"/></button>
			</c:if> 
		</c:forEach>
		
		<!-- 다음 페이지 버튼 -->
		<c:if test="${ requestScope.selectReportCriteria.pageNo >= requestScope.selectReportCriteria.maxPage }">
			<button disabled>></button>
		</c:if>
		<c:if test="${ requestScope.selectReportCriteria.pageNo < requestScope.selectReportCriteria.maxPage }">
			<button id="nextPage">></button>
		</c:if>
		
		<!-- 마지막 페이지로 이동 버튼 -->
		<button id="maxPage">>></button> 
	</div>
	
	<script>
		 let type= "${type}";
		let link = '';
		if( type == "paymentReportListManager") {
			link = "${ pageContext.servletContext.contextPath }/payment/paymentMainListManager";
		} else if( type == "paymentMainListUser") {
			link = "${ pageContext.servletContext.contextPath }/payment/paymentMainListUser";
		}
		
		/* const link = "${ pageContext.servletContext.contextPath }/address/list"; */
		let searchText = "";
		
		/* 보고서 검색 조건 유무에 따른 경로 처리 */
		if(${ !empty requestScope.selectReportCriteria.searchCondition? true: false }) {
			searchText += "&searchCondition=${ requestScope.selectReportCriteria.searchCondition }";
		} 
		
		/* 보고서 검색 내용 유무에 따른 경로 처리 */
		if(${ !empty requestScope.selectReportCriteria.searchValue? true: false }) {
			searchText += "&searchValue=${ requestScope.selectReportCriteria.searchValue }";
		}
			
		/* 휴가 신청서 검색 조건 유무에 따른 경로 처리 */
		if(${ !empty requestScope.selectVacationCriteria.searchCondition? true: false }) {
			searchText += "&searchCondition=${ requestScope.selectVacationCriteria.searchCondition }";
		} 
		
		/* 휴가 신청서 검색 내용 유무에 따른 경로 처리 */
		if(${ !empty requestScope.selectVacationCriteria.searchValue? true: false }) {
			searchText += "&searchValue=${ requestScope.selectVacationCriteria.searchValue }";
		}
		
		/* 첫 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("startPage")) {
			const $startPage = document.getElementById("startPage");
			$startPage.onclick = function() {
				location.href = link + "?currentPage=1" + searchText;
			}
		}
		
		/* 이전 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("prevPage")) {
			const $prevPage = document.getElementById("prevPage");
			/* if( $(".report.active").length > 0) {
				$prevPage.onclick = function() {
					location.href = link + "?currentPage=${ requestScope.selectReportCriteria.pageNo - 1 }" + searchText;
				}
			} else */ if( $(".vacation.active").length > 0) {	
				$prevPage.onclick = function() {
					location.href = link + "?currentPage=${ requestScope.selectVacationCriteria.pageNo -1 }" + searchText;
			  }
		   }
		}
			
		
		/* 다음 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("nextPage")) {
			const $nextPage = document.getElementById("nextPage");
			/* if( $(".report.active").length > 0) {
				$nextPage.onclick = function() {
					location.href = link + "?currentPage=${ requestScope.selectReportCriteria.pageNo + 1 }" + searchText;
				}
			} else */ if( $(".vacation.active").length > 0) {
				$nextPage.onclick = function() {
					location.href = link + "?currentPage=${ requestScope.selectVacationCriteria.pageNo + 1 }" + searchText;
			  }
		   }
		}	
		
		/* 마지막 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("maxPage")) {
			const $maxPage = document.getElementById("maxPage");
			/* if( $(".report.active").length > 0) {
				$maxPage.onclick = function() {
					location.href = link + "?currentPage=${ requestScope.selectReportCriteria.maxPage}" + searchText;
				}
			} else */ if( $(".vacation.active").length > 0) {
				$maxPage.onclick = function() {
					location.href = link + "?currentPage=${ requestScope.selectVacationCriteria.maxPage }" + searchText;
				}
			}
		}
		
		/* 페이지 번호 버튼 click 이벤트 처리 */
		function pageButtonAction(text) {
			location.href = link + "?currentPage=" + text + searchText;
		}
	</script>
</body>
</html>