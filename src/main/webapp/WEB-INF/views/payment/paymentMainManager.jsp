<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>결재 문서함</title>
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/paymentMainManager.css">	
	<script src="https://kit.fontawesome.com/4be044bf97.js"></script>
	<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
	<script src="${pageContext.servletContext.contextPath}/resources/js/indexManager.js"></script>
	<script src="${pageContext.servletContext.contextPath}/resources/js/paymentEvent.js"></script>
</head>
<script>
	/* 비지니스 로직 성공 alert 메시지 처리 */
	const message = '${ requestScope.message }';
	if(message != null && message !== '') {
		alert(message);
	}
</script>
<body>
    <div class="side-bar">
		<div class="profile">
			<div class="picture">
				<img src="${pageContext.servletContext.contextPath}/resources/images/jjanggu.jpg" alt="짱구">
				<div class="set">
					<a href="${pageContext.servletContext.contextPath}/mainpage/managerEditProfile">
						<img src="${pageContext.servletContext.contextPath}/resources/images/setting.png" alt="">
					</a>
				</div>
			</div>
			<p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <span><c:out value="${ sessionScope.loginMember.deptName }"/> 
	         	/ <c:out value="${ sessionScope.loginMember.jobName }"/>
	         </span><br>
			 <span class="logout"><a href="${ pageContext.servletContext.contextPath }/login/logout">로그아웃</a></span>
		</div>
		<div class="menu_bar">
			<div class="home">
				  <a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a>
			</div>
			
			<div class="admin">
				<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a>
			</div>		
			<span class="sub_menu"> 
				<span class="admin_page"><a href="../userManagement/managementMain.jsp">관리자 페이지</a></span> 
				<span class="regist"><a href="../userManagement/addUser.jsp">회원 추가</a></span>
			</span>
			
			<div class="payment_menu">
				<a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a>
			</div>			
			<span class="sub_menu"> 
				<span class="report_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/reportManager">업무보고서 기안</a></span>
				<span class="vacation_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/vacationManager">휴가신청서 기안</a></span> 
				<span class="payment_view"><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListManager">결재문서함</a> </span>
			</span>
			
			<div class="board_menu">
				<a href="${ pageContext.servletContext.contextPath }/board/manager-board-main">게시판</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="shared_com"><a href="${ pageContext.request.contextPath }/sharingBoard/list">공유게시판</a></span> 
				<span class="new_com"><a href="${ pageContext.request.contextPath }/newsBoard/list">새소식게시판</a></span>
				<span class="free_com"><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></span> 
				<span class="question_com"><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></span> 
				<span class="company_com"><a href="${ pageContext.request.contextPath }/board/manager-JobBoard-Main">사내게시판</a></span>
			</span>
			
			<div class="calendar_menu">
				<a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a>
			</div>
	
			<div class="address_menu">
				<a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="company_ad"><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyManager">회사 공용 주소록</a></span>
				<span class="personal_ad"><a href="${ pageContext.servletContext.contextPath }/address/myManagerList">내 주소록</a></span>
		   		<span class="shaed_ad"><a href="${ pageContext.servletContext.contextPath }/address/sharedManagerList">공유받은 주소록</a></span>
		   		<span class="admin_ad"><a href="${ pageContext.servletContext.contextPath }/address/list">주소록관리자</a></span>
			</span>
		</div>
	</div>
    <div class="main">
        <div class="wrap">
            <h2>결재문서함</h2>
            <form action="${ pageContext.servletContext.contextPath }/payment/paymentMainListManager" method="get" >
            <div class="search_bar">
                <div class="search_box">
                	<input type="hidden" name="currentPage" value="1">
					<select id="searchCondition" name="searchCondition">
						<option value="paymentNo"
							${ requestScope.paymentCriteria.searchCondition eq "paymentNo"? "selected": "" }>문서 번호</option>
						<option value="title"
							${ requestScope.paymentCriteria.searchCondition eq "title"? "selected": "" }>문서 제목</option>
						<option value="writer"
							${ requestScope.paymentCriteria.searchCondition eq "writer"? "selected": "" }>기안자</option>
						<option value="deptName"
							${ requestScope.paymentCriteria.searchCondition eq "deptName"? "selected": "" }>부서명</option>
					</select>
					
					<input type="search" id="payment_search" name="searchValue" 
					 		value="<c:out value="${ requestScope.paymentCriteria.searchValue }"/>"> 
                </div>
                <div class="search_btn">
                    <button type="submit">검색</button>
                </div>
            </div>
            </form>
            <ul class="tab_btn">
            	<li data-value="list1" class="active">보고</li>
            	<li data-value="list2">휴가</li>
            </ul>
            <div class="content">
                <div class="local_nav">
                    <span>순번</span>
                    <span>서식함</span>
                    <span>문서 번호</span>
                    <span>문서 제목</span>
                    <span>기안자</span>
                    <span>기안 부서</span>
                    <span>문서 상태</span>
                </div>
                <c:forEach items="${ reportList }" var="report" >
                	<div class="report active list1 manager" >
                		<input type="hidden" data-no="${ report.reportDTO.payNo }">
	                   	<span><c:out value="${ report.paymentNo }"/></span>
	                    <span><c:out value="${ report.dueForm }"/></span>
	                    <span><c:out value="${ report.reportDTO.payNo }"/></span>
	                    <span><c:out value="${ report.reportDTO.title }"/></span>
	                    <span><c:out value="${ report.reportDTO.writer }"/></span>
	                    <span><c:out value="${ report.reportDTO.deptDTO.deptName}"/></span>
	                    <span><c:out value="${ report.paymentStatus }"/></span>
              	  </div>
                </c:forEach>
                <c:forEach items="${ vacationList }" var="vacation" >
	                <div class="vacation list2 manager" >
	                	<input type="hidden" data-no="${ vacation.vacationDTO.payNo }">
	                    <span><c:out value="${ vacation.paymentNo }"/></span>
	                    <span><c:out value="${ vacation.dueForm }"/></span>
	                    <span><c:out value="${ vacation.vacationDTO.payNo }"/></span>
	                    <span><c:out value="${ vacation.vacationDTO.title }"/></span>
	                    <span><c:out value="${ vacation.vacationDTO.writer }"/></span>
	                    <span><c:out value="${ vacation.vacationDTO.deptDTO.deptName}"/></span>
	                    <span><c:out value="${ vacation.paymentStatus }"/></span>
	               </div>
                </c:forEach>
                </div>
            </div>
            <div class="report list1 page active">
            <jsp:include page="../payment/paymentReportListPaging.jsp" />
            </div>
            <div class="vacation list2 page">
            <jsp:include page="../payment/paymentVacationListPaging.jsp" />
            </div>
        </div>
</body>
</html>