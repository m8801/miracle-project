<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>사용자 삭제</title>
<style>
	.menu div:nth-child(2){
	   background: #FFF282;
	}
	
	.section{
	    flex: 1;
	    height: 100%;
	    display: flex;
	    flex-direction: column;
	    padding: 50px;
	    box-sizing: border-box;
	}
	
	.memTableWrap {
		width: 100%;
		overflow: auto;
		height: 750px;
		margin-bottom: 20px;
		border: 2px solid #D9D9D9;
		border-radius: 10px;
	}
	
	.memTableWrap::-webkit-scrollbar {
	    width: 20px;
	}
	
	.memTableWrap::-webkit-scrollbar-thumb {
	    background-color: #FFFEE2;
	    border-radius: 10px;
	    background-clip: padding-box;
	    border: 2px solid transparent;
	}
  	.memTableWrap::-webkit-scrollbar-track {
    	background-color: #D9D9D9;
    	border-radius: 7px;
    	box-shadow: inset 0px 0px 5px white;
    	border: 2px solid #D9D9D9;
  	}
	
	.memTable {
		width: 100%;
		font-size: 18px;
		text-align: center;
	}
	
	.memTable tr {
		height: 50px;
	}

	.memTable .head {
		background: #FFFEE2;
	}
	
	.btnBox {
		text-align: right;
	}
	
	.btnBox .del,
	.btnBox .cancel {
		display: inline-block;
		width: 200px;
		font-size: 18px;
		background: #FFF282;
		color: #fff;
		border: none;
		border-radius: 10px;
		padding: 5px 0;
		cursor: pointer;
		text-align: center;
	}
	
	.btnBox .del{
		margin-right: 50px;
	}

</style>
</head>
<body>
	<div class="container">
		<jsp:include page="../common/managerSidebar.jsp"/>
	   <section class="section">
	   		<form id="deleteForm" action="${ pageContext.servletContext.contextPath }/userManagement/deleteUser" method="post">
		   		<div class="memTableWrap">
			   		<table class="memTable">
			   			<thead>
			   				<tr class="head">
			   					<th class="num">사원번호</th>
			   					<th class="name">이름</th>
			   					<th class="age">나이</th>
			   					<th class="birth">생년월일</th>
			   					<th class="phone">연락처</th>
			   					<th class="email">이메일</th>
			   					<th class="dept">부서</th>
			   					<th class="job">직급</th>
			   					<th class="work">근무여부</th>
			   					<th class="authority">권한</th>
			   					<th class="select"><input id="allCheck" type="checkbox" name="allCheck"/></th>
			   				</tr>
			   			</thead>
			   			<tbody>
			   				<c:forEach var="deleteUser" items="${ requestScope.deleteList }">
			   				<tr>
			   					<td class="num"><c:out value="${ deleteUser.userNo }"/></td>
			   					<td class="name"><c:out value="${ deleteUser.userName }"/></td>
			   					<td class="age"><c:out value="${ deleteUser.age }"/></td>
			   					<td class="birth"><c:out value="${ deleteUser.birthDate }"/></td>
			   					<td class="phone"><c:out value="${ deleteUser.phone }"/></td>
			   					<td class="email"><c:out value="${ deleteUser.email }"/></td>
			   					<td class="dept"><c:out value="${ deleteUser.deptName }"/></td>
			   					<td class="job"><c:out value="${ deleteUser.jobName }"/></td>
			   					<td class="work"><c:out value="${ deleteUser.userYN }"/></td>
			   					<td class="authority"><c:out value="${ deleteUser.control }"/></td>
			   					<td class="select"><input type="checkbox" name="delChk" value="${ deleteUser.userNo }"/></td>
			   				</tr>
			   				</c:forEach>
			   			</tbody>
			   		</table>
		   		</div>
		   		<div class="btnBox">
		   			<button class="del" type="button" id="delBtn" onclick="deleteUser();">삭제</button>
		   			<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain" class="cancel">취소</a>
		   		</div>
	   		</form>
	   </section>
	</div>
	
	<!-- <script src="/miracle/resources/js/jquery-3.6.0.min.js"></script> -->
	<!-- <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script> -->
	<script src="https://kit.fontawesome.com/6522322312.js"></script>
	<script>
		const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			alert(message);
		}
	  
		/* $(function() {
			var delChk = document.getElementByName("delChk");
			var cnt = delChk.length;
			
			$("input[name='allCheck']").click(function(){
				
				var chkListArr = $("input[name='delChk']");
				
				for (var i = 0; i < chkListArr.length; i++) {
					
					chkListArr[i].checked = this.checked;
					
				}
				
			});
			
			$("input[name='delChk']").click(function(){
				
				if($("input[name='delChk']:checked").length == cnt) {
					
					$("input[name='allCheck']")[0].checked = true;
					
				}
				else {
					
					$("input[name='allCheck']")[0].checked = false;
					
				}
				
			});
		}); */
		
		function deleteUser() {
				
			var conResult = confirm("정말 삭제하시겠습니까?");
			
			if(conResult == false){
					
				alert("삭제를 취소했습니다.");
				return;
					 
			} else if(conResult == true) {
					 
				document.getElementById('delBtn').setAttribute("type", "submit");
					 
			}
				
		}
			
			
			
			
			
			
			/* var url = "miracle/userManagement/deleteUser";
			var valueArr = new Array();
			var list = $("input[name='delChk']");
			
			for(var i = 0; i < list.length; i++) {
				
				if(list[i].checked) {
					
					valueArr.push(list[i].value);
					
				}
				
			}
			
			if(valueArr.length == 0) {
				
				alert("선택된 사용자가 없습니다.");
				
			} else {
				
				var chk = confirm("정말 삭제하시겠습니까?");
				$.ajax({
					url : url,
					type : 'POST',
					traditional : true,
					data : {
						valueArr : valueArr
					},
					success : function(data) {
						if(data = 1) {
							alert("삭제 성공");
							location.replace("deleteUser");
						} else {
							alert("삭제 실패");
						}
					}
				});
				
			} */
		/* 	
		} */
		
		
		/*
		function deleteUser(){
			
			location.href = "/miracle/userManagement/deleteUser"; */
			
		/* 	var userNoArr = [];
			var delChk = $("input:checkbox[name='delChk']:checked").each(function() {
				userNoArr.push($(this.val());
				console.log(userNoArr);
			}); */
			
			 /* for(var i = 0; i < delChk.length;i++) {
				 console.log(delChk);
				 console.log(delChk[0]);
				 
				if(delChk.length == 0) { 
					alert("삭제할 사용자를 선택해 주세요.");
				} else {				
					if(delChk) {
						noArr.push(delChk[i].dataset.userno);
					}			
				}
			 } */
			 	
			/*  if(!confirm("정말 삭제하시겠습니까?")){
				 alert("삭제를 취소했습니다.");
				 
			 } else {
				 $.ajax({
						type:"post",
					 	url:"/miracle/userManagement/deleteUser",
					 	data:  { userNoArr : userNoArr },
					 	success:function(data) {
							if(data == null) {
								alert("삭제에 실패하였습니다.");
							} else {
								alert("삭제에 성공하였습니다.");
							}
						 },
						 error:function(data){
						 	alert(data);
					     }
				     });
			 } 	 */
/* 		 } */
		
		
	</script>

</body>
</html>