<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>사용자 관리</title>
<style>
	.menu div:nth-child(2){
	   background: #FFF282;
	}
	
	.section{
	    flex: 1;
	    height: 100%;
	    display: flex;
	    flex-direction: column;
	    padding: 50px;
	    box-sizing: border-box;
	}
	
	.search {
		background: #FFFEE2;
		border: 2px solid #D9D9D9;
		margin-bottom: 20px;
		font-size: 18px;
		padding: 10px 0;
		box-sizing: border-box;
		border-radius: 10px;
	}
	
	.search tr {
		height: 50px;
	}
	
	.search .memSearch {
		width: 150px;
		border-right: 2px solid #D9D9D9;
		text-align: center;
	}
	
	.search .title {
		width: 200px;
		text-align: right;
	}
	
	.search .content input {
		width: 200px;
		height: 30px;
		border: 2px solid #D9D9D9;
		border-radius: 10px;
		text-align: center;
		font-size: 18px;
	}
	
	.deptSelect, .jobSelect {
		width: 203px;
		height: 30px;
		font-size: 18px;
		border-radius: 10px;
		border: 2px solid #D9D9D9;
		text-align: center;
	}
	
	
	.search .btn {
		text-align: left;
	}
	
	.search button {
		width: 200px;
		font-size: 20px;
		background: #FFF282;
		color: #fff;
		border: none;
		border-radius: 10px;
		padding: 5px 0;
		cursor: pointer;
		margin-right: 50px;
	}
	
	.memTableWrap {
		width: 100%;
		overflow: auto;
		height: 630px;
		margin-bottom: 20px;
		border: 2px solid #D9D9D9;
		border-radius: 10px;
	}
	
	.memTableWrap::-webkit-scrollbar {
	    width: 20px;
	}
	
	.memTableWrap::-webkit-scrollbar-thumb {
	    background-color: #FFFEE2;
	    border-radius: 10px;
	    background-clip: padding-box;
	    border: 2px solid transparent;
	}
  	.memTableWrap::-webkit-scrollbar-track {
    	background-color: #D9D9D9;
    	border-radius: 7px;
    	box-shadow: inset 0px 0px 5px white;
    	border: 2px solid #D9D9D9;
  	}
	
	.memTable {
		width: 100%;
		font-size: 18px;
		text-align: center;
	}
	
	.memTable tr {
		height: 50px;
	}

	.memTable .head {
		background: #FFFEE2;
	}
	
	.memTable .select input {
		width: 20px;
		height: 20px;
	}
	
	.memTable select {
		width: 50%;
		font-size: 18px;
		border: none;
	}
	
	.btnBox {
		text-align: right;
		margin-top: 20px;
	}
	
	.btnBox .add,
	.btnBox .update,
	.btnBox .del {
		display: inline-block;
		width: 200px;
		font-size: 18px;
		background: #FFF282;
		color: #fff;
		border: none;
		border-radius: 10px;
		padding: 5px 0;
		cursor: pointer;
		text-align: center;
	}
	
	.btnBox .add,
	.btnBox .update {
		margin-right: 50px;
	}

</style>
</head>
<body>
	<div class="container">
		<jsp:include page="../common/managerSidebar.jsp"/>
	   	<section class="section">
	   		<form id="userSearchForm" action="${ pageContext.servletContext.contextPath }/userManagement/search" method="get">
		   		<table class="search">
		   			<tbody>
		   				<tr>
		   					<td rowspan="2" class="memSearch">회원검색</td>
		   					<td class="title">사원번호 : &nbsp;</td>
		   					<td class="content">
		   						<input type="search" id="userNo" name="userNo" value="<c:out value="${ requestScope.selectUserCriteria.userNo }"/>"></td>
		   					<td class="title">이름 : &nbsp;</td>
		   					<td class="content">
		   						<input type="search" id="userName" name="userName" value="<c:out value="${ requestScope.selectUserCriteria.userName }"/>">
		   					</td>
		   					<td class="title">연락처 : &nbsp;</td>
		   					<td class="content">
		   						<input type="search" id="phone" name="phone" value="<c:out value="${ requestScope.selectUserCriteria.phone }"/>">
		   					</td>
		   				</tr>
		   				<tr>
		   					<td class="title">부서 : &nbsp;</td>
		   					<td class="content">
		   						<select name="deptCode" class="deptSelect">
		   							<option value="" ${ requestScope.selectUserCriteria.deptCode eq "" ? "selected" : "" }>전체</option>
		   							<option value="D1" ${ requestScope.selectUserCriteria.deptCode eq "D1" ? "selected" : "" }>총무팀</option>
		   							<option value="D2" ${ requestScope.selectUserCriteria.deptCode eq "D2" ? "selected" : "" }>마케팅팀</option>
		   							<option value="D3" ${ requestScope.selectUserCriteria.deptCode eq "D3" ? "selected" : "" }>개발팀</option>
		   							<option value="D4" ${ requestScope.selectUserCriteria.deptCode eq "D4" ? "selected" : "" }>인사팀</option>
		   							<option value="D5" ${ requestScope.selectUserCriteria.deptCode eq "D5" ? "selected" : "" }>운영팀</option>
		   							<option value="D6" ${ requestScope.selectUserCriteria.deptCode eq "D6" ? "selected" : "" }>홍보팀</option>
		   						</select>
		   					</td>
		   					<td class="title">직급 : &nbsp;</td>
		   					<td class="content">
		   						<select name="jobCode" class="jobSelect">
		   							<option value="" ${ requestScope.selectUserCriteria.jobCode eq "" ? "selected" : "" }>전체</option>
									<option value="J1" ${ requestScope.selectUserCriteria.jobCode eq "J1" ? "selected" : "" }>사장</option>
									<option value="J2" ${ requestScope.selectUserCriteria.jobCode eq "J2" ? "selected" : "" }>부장</option>
									<option value="J3" ${ requestScope.selectUserCriteria.jobCode eq "J3" ? "selected" : "" }>차장</option>
									<option value="J4" ${ requestScope.selectUserCriteria.jobCode eq "J4" ? "selected" : "" }>과장</option>
									<option value="J5" ${ requestScope.selectUserCriteria.jobCode eq "J5" ? "selected" : "" }>대리</option>
									<option value="J6" ${ requestScope.selectUserCriteria.jobCode eq "J6" ? "selected" : "" }>주임</option>
									<option value="J7" ${ requestScope.selectUserCriteria.jobCode eq "J7" ? "selected" : "" }>사원</option>
								</select>
		   					</td>
		   					<td></td>
		   					<td class="btn"><button id="searchBtn" type="submit">검 색</button></td>
		   				</tr>
		   			</tbody>
		   		</table>
	   		</form> 
	   		
	   		<div class="memTableWrap">
		   		<table class="memTable">
		   			<thead>
		   				<tr class="head">
		   					<th class="num">사원번호</th>
		   					<th class="name">이름</th>
		   					<th class="age">나이</th>
		   					<th class="birth">생년월일</th>
		   					<th class="phone">연락처</th>
		   					<th class="email">이메일</th>
		   					<th class="dept">부서</th>
		   					<th class="job">직급</th>
		   					<th class="work">근무여부</th>
		   					<th class="authority">권한</th>
		   				</tr>
		   			</thead>
		   			<tbody>
		   				<c:forEach var="userManagement" items="${ requestScope.userManagementList }">
		   				<tr>
		   					<td class="num"><c:out value="${ userManagement.userNo }"/></td>
		   					<td class="name"><c:out value="${ userManagement.userName }"/></td>
		   					<td class="age"><c:out value="${ userManagement.age }"/></td>
		   					<td class="birth"><c:out value="${ userManagement.birthDate }"/></td>
		   					<td class="phone"><c:out value="${ userManagement.phone }"/></td>
		   					<td class="email"><c:out value="${ userManagement.email }"/></td>
		   					<td class="dept"><c:out value="${ userManagement.deptName }"/></td>
		   					<td class="job"><c:out value="${ userManagement.jobName }"/></td>
		   					<td class="work"><c:out value="${ userManagement.userYN }"/></td>
		   					<td class="authority"><c:out value="${ userManagement.control }"/></td>
		   				</tr>
		   				</c:forEach>
		   			</tbody>
		   		</table>
	   		</div>
	   		<jsp:include page="../common/userManagementPaging.jsp"/>
	   		<div class="btnBox">
		   		<a class="add" href="${ pageContext.servletContext.contextPath }/userManagement/addUser">사원 추가</a>
		   		<a class="update" href="${ pageContext.servletContext.contextPath }/userManagement/updateUser">사원 수정</a>
		   		<a class="del" href="${ pageContext.servletContext.contextPath }/userManagement/deleteUser">사원 삭제</a>
	   		</div>
	   </section>
	</div>
	
	<!-- <script src="/miracle/resources/js/jquery-3.6.0.min.js"></script> -->
	<script src="https://kit.fontawesome.com/6522322312.js"></script>
	<script>
	   /* $('.menu div:nth-child(4)').click(function () {
	      $('.sub-menu').toggleClass('active');
	   }) */
	   
	/*     if(document.getElementById("logout")) {
		
			const $logout = document.getElementById("logout");
			
			$logout.onclick = function() {
			
				location.href = "/miracle/login/logout";
				
			}
		} */

		const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			alert(message);
		}
				/*function deleteUser(){
			
			location.href = "/miracle/userManagement/deleteUser"; */
			
		/* 	var userNoArr = [];
			var delChk = $("input:checkbox[name='delChk']:checked").each(function() {
				userNoArr.push($(this.val());
				console.log(userNoArr);
			}); */
			
			 /* for(var i = 0; i < delChk.length;i++) {
				 console.log(delChk);
				 console.log(delChk[0]);
				 
				if(delChk.length == 0) { 
					alert("삭제할 사용자를 선택해 주세요.");
				} else {				
					if(delChk) {
						noArr.push(delChk[i].dataset.userno);
					}			
				}
			 } */
			 	
			/*  if(!confirm("정말 삭제하시겠습니까?")){
				 alert("삭제를 취소했습니다.");
				 
			 } else {
				 $.ajax({
						type:"post",
					 	url:"/miracle/userManagement/deleteUser",
					 	data:  { userNoArr : userNoArr },
					 	success:function(data) {
							if(data == null) {
								alert("삭제에 실패하였습니다.");
							} else {
								alert("삭제에 성공하였습니다.");
							}
						 },
						 error:function(data){
						 	alert(data);
					     }
				     });
			 } 	 */
/* 		 } */
		
		
	</script>

</body>
</html>