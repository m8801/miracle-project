<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>사용자 추가</title>
<style>
	.menu div:nth-child(2){
	   background: #FFF282;
	}

	.section{
	    flex: 1;
	    height: 100%;
	    display: flex;
	    flex-direction: column;
	    padding: 50px 200px;
	    box-sizing: border-box;
	}
	
	.spanHead {
		font-size: 36px;
		margin-bottom: 30px;
	}
	
	.outerBox {
		width: 100%;
		border: 5px solid #FDE82D;
		background: #FFFEE2;
		border-radius: 20px;
		margin-bottom: 30px;
	}
	
	.innerBox {
		width: 90%; 
		height: 90%;
		background: #fff;
		margin: 50px auto;
		display: flex;
		font-size: 18px;
	}
	
	.profileBox {
		border: 5px solid #FDE82D;
		background: #FFFEE2;
		border-radius: 20px;
		width: 300px;
		height: 400px;
		margin: 50px;
		text-align: center;
	}
	
	.profileBox .addProfile {
		width: 200px;
		height: 200px;
		background: #fff;
		border-radius: 50%;
		overflow: hidden;
		margin: 40px auto 20px;;
		border: 10px solid #fff;
	}
	
	.profileBox .addProfile img {
		height: 100%;
		width: 200px;
		/* position: relative;
		right: 20px; */
	}
	
	.profileBox span {
		cursor: pointer;
	}
	
	.addBox {
		flex: 1;
		padding: 30px 0;
	}
	
	.addBox .title {
		text-align: right;
		width: 150px;
	}
	
	.addBox .content input {
		width: 250px;
		height: 25px;
		font-size: 18px;
	}
	
	.addBox select {
		width: 257px;
		font-size: 18px;
	}
	
	.btn {
		text-align: center;
	}
	
	.ok, .cancel {
		padding: 15px 70px;
		border: none;
		font-size: 24px;
		background: #FFF282;
		color: #fff;
		border-radius: 10px;
	}
	
	.cancel {
		display: inlint-block;
		margin-left: 200px;
	}
	

	#profileName {
		display: inline-block;
	    height: 40px;
	    padding: 0 10px;
	    border: 1px solid #dddddd;
	    width: 60%;
	    color: #999999;
	    font-size: 16px;
	    margin-bottom: 10px;
	}
	
	#profileLabel { 
		display: inline-block;
	    padding: 7px 10px;
	    color: #fff;
	  	background: #FFF282;
		border-radius: 5px;
	    cursor: pointer;
	    margin-left: 10px;
	    font-size: 18px;
	    margin-bottom: 30px;
	} 
	
	#profileUpload { 
		/* 파일 필드 숨기기 */ 
		position: absolute; 
		width: 0; 
		height: 0; 
		padding: 0; 
		overflow: hidden; 
		border: 0; 
	}  
	
	
	

</style>
</head>
<body>
	<div class="container">
		<jsp:include page="../common/managerSidebar.jsp"/>
	   <%-- <aside class="side">
	      <div class="profile">
	         <div class="img"><img src="/miracle/resources/images/profile.jpg" alt=""></div>
	         <div class="set">
	         	<a href="${ pageContext.servletContext.contextPath }/mainpage/managerEditProfile">
	         		<img src="/miracle/resources/images/setting.png" alt="">
	         	</a>
	         </div>
	         <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <a href="${ pageContext.servletContext.contextPath }/login/loout" id="logout">로그아웃</a>
	      </div>
	      <div class="menu">
	         <div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
	         <div>
	            <a href="${ pageContext.servletContext.contextPath }/board/manager-board-main">게시판</a>
	         </div>
	         <div class="sub-menu">
	            <p><a href="${ pageContext.servletContext.contextPath }/board/manager-board1-4">공유게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/manager-board1-5">새소식게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/manager-board1-1">자유게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/manager-board1-2">질문게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/manager-board1-3">사내게시판</a></p>
	         </div>
	         
	         <div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/address/addressMyManager">주소록</a></div>
	      </div>
	   </aside> --%>
	   <section class="section">
	   		<span class="spanHead">사원 추가하기</span>
	   		<form id="registForm" action="${ pageContext.servletContext.contextPath }/userManagement/addUser" method="post" encType="multipart/form-data">
		   		<div class="outerBox">
		   			<div class="innerBox">
		   				<div class="profileBox">
		   					<div class="addProfile">
		   						<img src="/miracle/resources/images/adminUser.png" alt=""> 
		   					</div>
							<input id="profileName" value="프로필 이미지" placeholder="프로필 이미지">
							<label id="profileLabel" for="profileUpload">파일 찾기</label>
							<input type="file" name="uploadFile" id="profileUpload" accept=".jpg, .png, .jpeg, .gif" onchange="setThumbnail(event);">
							
		   				</div>
		   				
		   				<table class="addBox">
		   					<tbody>
		   						<!-- <tr>
		   							<td class="title">사번 &nbsp;</td>
		   							<td class="content"><input type="text" name="userNo"></td>
		   						</tr> -->
		   						
		   						<tr>
		   							<td class="title">비밀번호 &nbsp;</td>
		   							<td class="content"><input type="password" name="userPwd" value="<c:out value="${ requestScope.registUserDTO.userNo }"/>"></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">이름 &nbsp;</td>
		   							<td class="content"><input type="text" name="userName" value="<c:out value="${ requestScope.registUserDTO.userName }"/>"></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">성별 &nbsp;</td>
		   							<td class="content">
		   								<select name="gender" class="genderSelect">
											<option value="남" ${ requestScope.registUserDTO.gender eq "남" ? "selected" : "" }>남</option>
											<option value="여" ${ requestScope.registUserDTO.gender eq "여" ? "selected" : "" }>여</option>
										</select>
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">부서 &nbsp;</td>
		   							<td class="content">
		   								<select name="deptCode" class="deptSelect">
											<option value="D1" ${ requestScope.registUserDTO.deptCode eq "D1" ? "selected" : "" }>총무팀</option>
				   							<option value="D2" ${ requestScope.registUserDTO.deptCode eq "D2" ? "selected" : "" }>마케팅팀</option>
				   							<option value="D3" ${ requestScope.registUserDTO.deptCode eq "D3" ? "selected" : "" }>개발팀</option>
				   							<option value="D4" ${ requestScope.registUserDTO.deptCode eq "D4" ? "selected" : "" }>인사팀</option>
				   							<option value="D5" ${ requestScope.registUserDTO.deptCode eq "D5" ? "selected" : "" }>운영팀</option>
				   							<option value="D6" ${ requestScope.registUserDTO.deptCode eq "D6" ? "selected" : "" }>홍보팀</option>
										</select>
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">연락처 &nbsp;</td>
		   							<td class="content"><input type="text" name="phone" value="<c:out value="${ requestScope.registUserDTO.phone }"/>"></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">이메일 &nbsp;</td>
		   							<td class="content"><input type="text" name="email" value="<c:out value="${ requestScope.registUserDTO.email }"/>"></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">생년월일 &nbsp;</td>
		   							<td class="content"><input type="date" name="birthDate" value="<c:out value="${ requestScope.registUserDTO.birthDate }"/>"></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">직급 &nbsp;</td>
		   							<td class="content">
		   								<select name="jobCode" class="jobSelect">
											<option value="J1" ${ requestScope.registUserDTO.jobCode eq "J1" ? "selected" : "" }>사장</option>
											<option value="J2" ${ requestScope.registUserDTO.jobCode eq "J2" ? "selected" : "" }>부장</option>
											<option value="J3" ${ requestScope.registUserDTO.jobCode eq "J3" ? "selected" : "" }>차장</option>
											<option value="J4" ${ requestScope.registUserDTO.jobCode eq "J4" ? "selected" : "" }>과장</option>
											<option value="J5" ${ requestScope.registUserDTO.jobCode eq "J5" ? "selected" : "" }>대리</option>
											<option value="J6" ${ requestScope.registUserDTO.jobCode eq "J6" ? "selected" : "" }>주임</option>
											<option value="J7" ${ requestScope.registUserDTO.jobCode eq "J7" ? "selected" : "" }>사원</option>
										</select>	
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">근무여부 &nbsp;</td>
		   							<td class="content">
		   								<select name="userYn" class="workSelect">
											<option value="Y" ${ requestScope.registUserDTO.userYn eq "Y" ? "selected" : "" }>Y</option>
											<option value="N" ${ requestScope.registUserDTO.userYn eq "N" ? "selected" : "" }>N</option>
										</select>
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">권한 &nbsp;</td>
		   							<td class="content">
		   								<select name="control" class="authoritySelect">
											<option value="관리자" ${ requestScope.registUserDTO.control eq "관리자" ? "selected" : "" }>관리자</option>
											<option value="사용자" ${ requestScope.registUserDTO.control eq "사용자" ? "selected" : "" }>사용자</option>
										</select>
		   							</td>
		   						</tr>
		   					</tbody>
		   				</table>
		   			</div>
		   		</div>
		   		
		   		<div class="btn">
		   			<button class="ok">등록</button>
		   			<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain" class="cancel">취소</a>
		   		</div>
	   		</form>
	   </section>
	</div>
	
	<!-- <script src="/miracle/resources/js/jquery-3.6.0.min.js"></script> -->
	<script src="https://kit.fontawesome.com/6522322312.js"></script>
	<script>
	   /* $('.menu div:nth-child(4)').click(function () {
	      $('.sub-menu').toggleClass('active');
	   }) */
	   
	    /* if(document.getElementById("logout")) {
		
			const $logout = document.getElementById("logout");
			
			$logout.onclick = function() {
			
				location.href = "/miracle/login/logout";
				
			}
		} */
		
		const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			alert(message);
		}
		
		
/* 	    $("#profileUpload").on("change",function(){
		    var profileName = $("#profileUpload").val();
		    $("#profileName").val(profileName);
	    }); */
		
		function setThumbnail(event) 
		{ 
			var reader = new FileReader(); 
			
			reader.onload = function(event) { 
				var img = document.querySelector(".addProfile img"); 
				img.setAttribute("src", event.target.result); 
				document.querySelector("div.addProfile").append(img);
				
				var profileName = document.getElementById("profileUpload").value;
				document.getElementById("profileName").value = profileName;
			}; 
			
			reader.readAsDataURL(event.target.files[0]); 
		}

		
	</script>

</body>
</html>