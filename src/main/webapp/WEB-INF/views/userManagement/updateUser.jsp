<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>사용자 정보 변경</title>
<style>
	.menu div:nth-child(2){
	   background: #FFF282;
	}
	
	.section{
	    flex: 1;
	    height: 100%;
	    display: flex;
	    flex-direction: column;
	    padding: 50px 200px;
	    box-sizing: border-box;
	}
	
	#modifyForm {
		
	}
	
	.spanHead {
		font-size: 36px;
		margin-bottom: 30px;
		text-align: center;
	}
	
	.outerBox {
		width: 600px;
		height: 650px;
		border: 5px solid #FDE82D;
		background: #FFFEE2;
		border-radius: 20px;
		margin: 0 auto 30px;
	}
	
	.innerBox {
		width: 90%; 
		height: 90%;
		background: #fff;
		margin: 30px auto;
		display: flex;
		font-size: 18px;
	}
	
	.modifyBox {
		flex: 1;
		padding: 30px 0;
	}
	
	.modifyBox .title {
		text-align: right;
		width: 150px;
	}
	
	.modifyBox .content input {
		width: 230px;
		height: 25px;
		font-size: 18px;
	}
	
	.modifyBox select {
		width: 235px;
		font-size: 18px;
	}
	
	.btn {
		text-align: center;
	}
	
	.ok, .cancel {
		padding: 10px 50px;
		border: none;
		font-size: 24px;
		background: #FFF282;
		color: #fff;
		border-radius: 10px;
	}
	
	.cancel {
		display: inlint-block;
		margin-left: 100px;
	}
	
	.search {
		padding-right: 40px;
	}
	
	#search {
		padding: 4px 15px;
		background: #FFF282;
		color: #fff;
		border-radius: 10px;
		border: none;
		font-size: 18px;
	}
	
	

</style>
</head>
<body>
	<div class="container">
		<jsp:include page="../common/managerSidebar.jsp"/>
	   	<section class="section">
	   		<span class="spanHead">사원 정보 변경</span>
		   	<form id="modifyForm">
		   		<div class="outerBox">
		   			<div class="innerBox">
		 				<table class="modifyBox">
		   					<tbody>
		   						<tr>
		   							<td class="title">사번 &nbsp;</td>
		   							<td class="content"><input type="text" name="userNo" value="<c:out value="${ modifyUser.userNo }"/>"></td>
		   							<td class="search"><button id="search" type="button">조회</button></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">이름 &nbsp;</td>
		   							<td class="content"><input type="text" name="userName" value="<c:out value="${ modifyUser.userName }"/>"></td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">성별 &nbsp;</td>
		   							<td class="content">
		   								<select name="gender" class="genderSelect">
											<option value="남" ${ modifyUser.gender eq "남" ? "selected" : "" }>남</option>
											<option value="여" ${ modifyUser.gender eq "여" ? "selected" : "" }>여</option>
										</select>
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">부서 &nbsp;</td>
		   							<td class="content">
		   								<select name="deptCode" class="departmentSelect">
											<option value="D1" ${ modifyUser.deptCode eq "D1" ? "selected" : "" }>총무팀</option>
				   							<option value="D2" ${ modifyUser.deptCode eq "D2" ? "selected" : "" }>마케팅팀</option>
				   							<option value="D3" ${ modifyUser.deptCode eq "D3" ? "selected" : "" }>개발팀</option>
				   							<option value="D4" ${ modifyUser.deptCode eq "D4" ? "selected" : "" }>인사팀</option>
				   							<option value="D5" ${ modifyUser.deptCode eq "D5" ? "selected" : "" }>운영팀</option>
				   							<option value="D6" ${ modifyUser.deptCode eq "D6" ? "selected" : "" }>홍보팀</option>
										</select>
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">직급 &nbsp;</td>
		   							<td class="content">
		   								<select name="jobCode" class="jobSelect">
											<option value="J1" ${ modifyUser.jobCode eq "J1" ? "selected" : "" }>사장</option>
											<option value="J2" ${ modifyUser.jobCode eq "J2" ? "selected" : "" }>부장</option>
											<option value="J3" ${ modifyUser.jobCode eq "J3" ? "selected" : "" }>차장</option>
											<option value="J4" ${ modifyUser.jobCode eq "J4" ? "selected" : "" }>과장</option>
											<option value="J5" ${ modifyUser.jobCode eq "J5" ? "selected" : "" }>대리</option>
											<option value="J6" ${ modifyUser.jobCode eq "J6" ? "selected" : "" }>주임</option>
											<option value="J7" ${ modifyUser.jobCode eq "J7" ? "selected" : "" }>사원</option>
										</select>	
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">근무여부 &nbsp;</td>
		   							<td class="content">
		   								<select name="userYN" class="workSelect">
											<option value="Y" ${ modifyUser.userYN eq "Y" ? "selected" : "" }>Y</option>
											<option value="N" ${ modifyUser.userYN eq "N" ? "selected" : "" }>N</option>
										</select>
		   							</td>
		   						</tr>
		   						
		   						<tr>
		   							<td class="title">권한 &nbsp;</td>
		   							<td class="content">
		   								<select name="control" class="authoritySelect">
											<option value="관리자" ${ modifyUser.control eq "관리자" ? "selected" : "" }>관리자</option>
											<option value="사용자" ${ modifyUser.control eq "사용자" ? "selected" : "" }>사용자</option>
										</select>
		   							</td>
		   						</tr>
		   					</tbody>
		   				</table>
		   			</div>
		   		</div>
		   		
		   		<div class="btn">
		   			<button class="ok" id="modifyBtn">변경</button>
		   			<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain" class="cancel">취소</a>
		   		</div>
	   		</form>
	   </section>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="https://kit.fontawesome.com/6522322312.js"></script>
	<script>
	   /* $('.menu div:nth-child(4)').click(function () {
	      $('.sub-menu').toggleClass('active');
	   }) */
	   
	    /* if(document.getElementById("logout")) {
		
			const $logout = document.getElementById("logout");
			
			$logout.onclick = function() {
			
				location.href = "/miracle/login/logout";
				
			}
		} */
		
	/* 	if(document.getElementById("search")) {
			
			const $search = document.getElementById("search");
			
			$search.onclick = function() {
				
				location.href = "/miracle/userManagement/searchUserNo";
				
			}
			
		} */
		
	/* if(document.getElementById("modifyBtn")) {
			
			const $modifyBtn = document.getElementById("modifyBtn");
			
			$modifyBtn.onclick = function() {
				
				location.href = "/miracle/userManagement/modifyUser";
				
			}
			
		} */
		

		$(function(){
			
			$("#search").click(function(){
				
				$("#modifyForm").attr("action","${ pageContext.servletContext.contextPath }/userManagement/searchUserNo").attr("method","get").submit();
			});
			
			$("#modifyBtn").click(function(){
				
				$("#modifyForm").attr("action","${ pageContext.servletContext.contextPath }/userManagement/updateUser").attr("method","post").submit();
			});
		});
		
	</script>

</body>
</html>