<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	button {
		background: #FFF282;
		color: #fff;
		border: none;
		border-radius: 5px;
		font-size: 16px;
	}
</style>
</head>
<body>
	<div class="pagingArea" align="center">
	
		<!-- 맨 앞으로 이동 버튼 -->
	    <button id="startPage"><<</button>
		
		<!-- 이전 페이지 버튼 -->
		<c:if test="${ requestScope.selectUserCriteria.pageNo <= 1 }">
			<button disabled><</button>
		</c:if>
		<c:if test="${ requestScope.selectUserCriteria.pageNo > 1 }">
			<button id="prevPage"><</button>
		</c:if>
		
		<!-- 숫자 버튼 -->
		<c:forEach var="p" begin="${ requestScope.selectUserCriteria.startPage }" end="${ requestScope.selectUserCriteria.endPage }" step="1">
			<c:if test="${ requestScope.selectUserCriteria.pageNo eq p }">
				<button disabled><c:out value="${ p }"/></button>
			</c:if>
			<c:if test="${ requestScope.selectUserCriteria.pageNo ne p }">
				<button onclick="pageButtonAction(this.innerText);"><c:out value="${ p }"/></button>
			</c:if>
		</c:forEach>
		
		<!-- 다음 페이지 버튼 -->
		<c:if test="${ requestScope.SelectUserCriteria.pageNo >= requestScope.selectUserCriteria.maxPage }">
			<button disabled>></button>
		</c:if>
		<c:if test="${ requestScope.SelectUserCriteria.pageNo < requestScope.selectUserCriteria.maxPage }">
			<button id="nextPage">></button>
		</c:if>
		
		<!-- 마지막 페이지로 이동 버튼 -->
		<button id="maxPage">>></button> 
	</div>
	
	<script>
		const link = "${ pageContext.servletContext.contextPath }/userManagement/search";
		let searchText = "";
		
		/* 검색 조건 유무에 따른 경로 처리 */
		if(${ !empty requestScope.SelectUserCriteria.deptName? true: false }) {
			searchText += "&depeName=${ requestScope.selectUserCriteria.deptName }";
		}
		
		if(${ !empty requestScope.SelectUserCriteria.jobName? true: false }) {
			searchText += "&jobName=${ requestScope.selectUserCriteria.deptName }";
		}
		
		/* 검색 내용 유무에 따른 경로 처리 */
		if(${ !empty requestScope.SelectUserCriteria.userNo? true: false }) {
			searchText += "&userNo=${ requestScope.selectUserCriteria.userNo }";
		}
		
		if(${ !empty requestScope.SelectUserCriteria.userName? true: false }) {
			searchText += "&userName=${ requestScope.selectUserCriteria.userName }";
		}
		
		if(${ !empty requestScope.SelectUserCriteria.phone? true: false }) {
			searchText += "&phone=${ requestScope.selectUserCriteria.phone }";
		}
			
		/* 첫 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("startPage")) {
			const $startPage = document.getElementById("startPage");
			$startPage.onclick = function() {
				location.href = link + "?currentPage=1" + searchText;
			}
		}
		
		/* 이전 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("prevPage")) {
			const $prevPage = document.getElementById("prevPage");
			$prevPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectUserCriteria.pageNo - 1 }" + searchText;
			}
		}
		
		/* 다음 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("nextPage")) {
			const $nextPage = document.getElementById("nextPage");
			$nextPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectUserCriteria.pageNo + 1 }" + searchText;
			}
		}
		
		/* 마지막 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("maxPage")) {
			const $maxPage = document.getElementById("maxPage");
			$maxPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectUserCriteria.maxPage }" + searchText;
			}
		}
		
		/* 페이지 번호 버튼 click 이벤트 처리 */
		function pageButtonAction(text) {
			location.href = link + "?currentPage=" + text + searchText;
		}
	</script>
</body>
</html>