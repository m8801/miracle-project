<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/common/userCommon.css">
</head>
<body>
	<aside class="side">
	      <div class="profile">
	         <div class="img"><img src="${pageContext.servletContext.contextPath}/resources/images/profileFiles/${ sessionScope.loginMember.updateFile }" onerror="this.src='${pageContext.servletContext.contextPath}/resources/images/profileFiles/adminUser.png'" alt="프로필 이미지"></div>
	         <div class="set">
	         	<a href="${ pageContext.servletContext.contextPath }/mainpage/userEditProfile">
	         		<img src="${pageContext.servletContext.contextPath}/resources/images/setting.png" alt="">
	         	</a>
	         </div>
	         <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <span><c:out value="${ sessionScope.loginMember.deptName }"/> 
	         	/ <c:out value="${ sessionScope.loginMember.jobName }"/>
	         </span><br>
	         <a href="${ pageContext.servletContext.contextPath }/login/logout" id="logout">로그아웃</a>
	      </div>
	      <div class="menu">
	         <div><a href="${ pageContext.servletContext.contextPath }/mainpage/userMain">홈</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListUser">전자결재</a></div>
	         <div>
	            <a href="${ pageContext.servletContext.contextPath }/board/main-boardList">게시판</a>
	         </div>
	         <%-- <div class="sub-menu">
	            <p><a href="${ pageContext.servletContext.contextPath }/board/user-board1-4">공유게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/user-board1-5">새소식게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/user-board1-1">자유게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/user-board1-2">질문게시판</a></p>
	            <p><a href="${ pageContext.servletContext.contextPath }/board/user-board1-3">사내게시판</a></p>
	         </div> --%>
	         
	         <div><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain">일정</a></div>
	         <div><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyUser">주소록</a></div>
	      </div>
	   </aside>

	<script src="/miracle/resources/js/jquery-3.6.0.min.js"></script>
	<script>

	    if(document.getElementById("logout")) {
		
			const $logout = document.getElementById("logout");
			
			$logout.onclick = function() {
			
				location.href = "/miracle/login/logout";
				
			}
		}
	</script>

</body>
</html>