<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jobBoard.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

	const message = '${ requestScope.message }';
	if(message != null && message !== '') {
		alert(message);
	}
	
</script>
</head>
<body>
<div class="container">
	<c:choose>
		<c:when test="${ sessionScope.loginMember.userNo gt 10000 }"><jsp:include page="../board/manager-side.jsp" /></c:when>
		<c:otherwise><jsp:include page="../board/user-side.jsp" /></c:otherwise>
	</c:choose>



	<section class="section">

		<table class="table" >
			<tr class=first>
				<td class="date">작성일</td>
				<td class="no">번호</td>
				<td class="name">제목</td>
				<td class="writer">작성자</td>
			</tr>
			<c:forEach var="jobBoardList" items="${ requestScope.jobBoardList }">
				<tr>
					<td><c:out value="${ jobBoardList.createDate }" /></td>
					<td><c:out value="${ jobBoardList.boardNo }" /></td>
					<td><c:out value="${ jobBoardList.title }" /></td>
					<td><%-- <c:out value="${ board.userName }" /> --%>
						<c:choose>
							<c:when test="${ jobBoardList.anonymousYn eq 'Y' }">익명</c:when>
							<c:when test="${ jobBoardList.anonymousYn eq 'N' }"><c:out value="${ jobBoardList.userName }" /></c:when>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<jsp:include page="../common/jobBoardPaging.jsp" />

        <div class="btn-bar">
	        	<button id="regist">생성</button>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/main'">뒤로가기</button>
        </div>

	</section>
</div>


<script>
	if(document.querySelectorAll(".table td")) {
		const $tds = document.querySelectorAll(".table td");
		const $fisrstTd = document.querySelectorAll(".first");
		
		for(let i = 0; i < $tds.length; i++) {
			
			$tds[i].onmouseenter = function() {
				/* this.parentNode.style.backgroundColor = "#FFF282"; */
				this.parentNode.style.cursor = "pointer";
			}
			
			$tds[i].onmouseout = function() {
				/* this.parentNode.style.backgroundColor = "white"; */
			}
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[1].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/jobDetail?no=" + no;
			}
		}
		
		
	}
	
	if(document.getElementById("regist")) {
		
		const $regist = document.getElementById("regist"); 
		
		$regist.onclick = function() {
		
			location.href = "/miracle/board/jobBoard-regist";
		}
	}


	/* $('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	}) */
</script>


</body>
</html>