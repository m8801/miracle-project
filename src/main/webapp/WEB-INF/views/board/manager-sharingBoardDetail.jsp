<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/manager-board-select.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">사용자관리</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="#">자유게시판</a></p>
				<p><a href="#">질문게시판</a></p>
				<p><a href="#">사내게시판</a></p>
			</div>

			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">
		
		<h3>
			<p>
				<input type="hidden" id="boardNo" value="${ requestScope.sharingBoardDetail.boardNo }">
				<c:out value="${ sharingBoardDetail.title }" />
			</p>
		</h3>
		<div class="info">
			<div class="info-left"></div>
			<div class="info-right">
				<span>
					<c:choose>
						<c:when test="${ sharingBoardDetail.anonymousYn eq 'Y' }">익명</c:when>
						<c:when test="${ sharingBoardDetail.anonymousYn eq 'N' }"><c:out value="${ sharingBoardDetail.userName }" /></c:when>
						<%-- <c:otherwise>${ replyList.userNo.userName }</c:otherwise> --%>
					</c:choose>
				</span>
				<p><c:out value="${ sharingBoardDetail.createDate }" /></p>
			</div>
		</div>
		<a href="${ pageContext.servletContext.contextPath }/resources/uploadFiles/${ requestScope.sharingBoardDetail.attachmentDTO.saveName }" download class="download"><c:out value="${ requestScope.sharingBoardDetail.attachmentDTO.originName }"/></a>
		<div class="contents">
			<p><c:out value="${ sharingBoardDetail.content }" /></p>
		</div>
		
		<!-- 댓글 작성용 -->
		<table id="replyWrite">
			<input type="hidden" id="boardNo" value="${ requestScope.sharingBoardDetail.boardNo }">
			<tr>
				<td>댓글</td>
				<td><textarea cols="120" rows="1" id="replyComments" class="replyComments" style="resize: none;" placeholder="댓글 입력"></textarea></td>
				<td>
					<button type="button" id="registSharingReply" class="registSharingReply">작성하기</button>
				</td>
			</tr>
		</table>
		
		<table id="replyResult">
			<c:if test="${ not empty requestScope.replyList }">
				<c:forEach var="replyList" items="${ requestScope.replyList }" varStatus="st">
						<%-- <input type="hidden" value="${ replyList.writer.userNo }">
						<input type="hidden" value="${ sessionScope.loginMember.userNo }"> --%>
					<tr>
						<input type="hidden" id="${ replyList.replyNo }" value="${ replyList.replyNo }">
						<td>${ replyList.writer.userName }</td>
						<td>${ replyList.comments }</td>
						<td>${ replyList.createDate }</td>
						<td>
							<c:if test="${ replyList.writer.userNo eq sessionScope.loginMember.userNo }">
								<button type="button" onclick="removeReply(${ replyList.replyNo })">댓글삭제</button>
							</c:if>
						</td>
					</tr>
				</c:forEach>

			</c:if>
		</table>
		
		
			
			
		<div class="btn-bar">
			<c:if test="${ requestScope.sharingBoardDetail.userNo eq sessionScope.loginMember.userNo }">
				<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/sharingUpdate?boardNo=${ requestScope.sharingBoardDetail.boardNo }'">수정</button>
			</c:if>
			<c:choose>
				<c:when test="${ requestScope.sharingBoardDetail.userNo eq sessionScope.loginMember.userNo }"><button onclick="location.href='${ pageContext.servletContext.contextPath }/board/delete?boardNo=${ requestScope.newsBoardDetail.boardNo }'">삭제</button></c:when>
				<c:otherwise><button onclick="location.href='${ pageContext.servletContext.contextPath }/board/delete?boardNo=${ requestScope.newsBoardDetail.boardNo }'">강제삭제</button></c:otherwise>
			</c:choose>
        	<%-- <button onclick="location.href='${ pageContext.servletContext.contextPath }/board/sharingUpdate?boardNo=${ requestScope.sharingBoardDetail.boardNo }'">수정</button> --%>
        	<%-- <button onclick="location.href='${ pageContext.servletContext.contextPath }/board/sharingDelete?boardNo=${ requestScope.sharingBoardDetail.boardNo }'">삭제</button> --%>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/sharingList'">뒤로가기</button>
        </div>
		

	</section>
</div>


<script>

	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
	
	
	/* 댓글 작성 이벤트 처리 */
	if(document.getElementById("registSharingReply")) {
		const $registSharingReply = document.getElementById("registSharingReply");
		$registSharingReply.onclick = function() {
			let boardNo = document.getElementById("boardNo").value;
			let replyComments = document.getElementById("replyComments").value;
			
			
			if(replyComments.trim() == ""){
				$("#replyComments").val("");
				alert('댓글을 입력해 주십시오');
			} else {
				
				$.ajax({
					url:"/miracle/board/registSharingReply",
					type:"post",
					data:{boardNo:boardNo, comments:replyComments},
					success:function(data){
						console.table(data);
						
						
						$("#replyCommnets").val("");
						
						const $table = $("#replyResult");
						$table.html("");
						
						for(var index in data){
							$tr = $("<tr>");
							$commnetsTd = $("<td>").text(data[index].comments);
							$userTd = $("<td>").text(data[index].writer.userName);
							$createDateTd = $("<td>").text(data[index].createDate);
							if(data[index].writer.userNo == ${sessionScope.loginMember.userNo}){
								$removeTd = $("<td>").append("<button type='button' onclick='removeReply(" + data[index].replyNo + ")'>댓글삭제</button>");
							} else {
								$removeTd = $("<td>");
							}
							
							$tr.append("<input type='hidden' id=" + ${ data[index].replyNo } + " value='" + data[index].replyNo + "'>");
							$tr.append($commnetsTd);
							$tr.append($userTd);
							$tr.append($createDateTd);
							$tr.append($removeTd);
							$table.append($tr);
						}
					}, error:function(data){
						console.log(data);
					}
				});
			}
		}
	}
	
	
	/* 댓글 삭제 이벤트 처리 함수*/
	function removeReply(replyNo){
		let boardNo = document.getElementById("boardNo").value;
		$.ajax({
			url:"/miracle/board/removeSharingReply",
			type:"post",
			data:{boardNo:boardNo, replyNo:replyNo},
			success:function(data){
				console.table(data);
				const $table = $("#replyResult");
				$table.html("");
				
				for(var index in data){
					$tr = $("<tr>");
					$userTd = $("<td>").text(data[index].writer.userName);
					$commentsTd = $("<td>").text(data[index].comments);
					$createDateTd = $("<td>").text(data[index].createdDate);
					if(data[index].userNo == ${sessionScope.loginMember.userNo}){
						$removeTd = $("<td>").append("<button type='button' onclick='removeReply(" + data[index].replyNo + ")'>댓글삭제</button>");
					} else {
						$removeTd = $("<td>");
					}
					
					$tr.append("<input type='hidden' id=" + ${ data[index].replyNo } + " value='" + data[index].replyNo + "'>");
					$tr.append($userTd);
					$tr.append($commentsTd);
					$tr.append($createDateTd);
					$tr.append($removeTd);
					
					$table.append($tr);
				}
			}, error:function(data){
				console.log(data);
			}
		});
	}
</script>




</body>
</html>