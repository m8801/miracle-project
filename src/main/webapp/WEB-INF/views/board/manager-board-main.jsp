<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/manager-board-main.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">사용자관리</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-JobBoard-Main">사내게시판</a></p>
			</div>

			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">
		<article class="board1">
			<div>
				<p>공지사항</p>
				<hr>
			</div>
			<div class="board-content">
				<div class="board-title">
					<a href="#">[공지] 2022 4월 인사발령[10]</a><br>
					<a href="#">[공지] 2022 3월 25일 우리회사 이벤트</a><br>
					<a href="#">[공지] 2022 3월 인사발령[7]</a><br>
					<a href="#">[공지] 2022 2월 21일 부서별 이벤트</a><br>
					<a href="#">[공지] 2022 1월 인사발령[73]</a><br>
				</div>
				<div class="board-date">
					<a href="#">2022.03.01 13:59</a><br>
					<a href="#">2022.03.01 13:59</a><br>
					<a href="#">2022.03.01 13:59</a><br>
					<a href="#">2022.03.01 13:59</a><br>
					<a href="#">2022.03.01 13:59</a><br>
				</div>
			</div>
		</article>

		<article class="board2">
			<ul class="tap-btn">
				<li data-value="tab1"  class="active">공유</li>
				<li data-value="tab2">자유</li>
				<li data-value="tab3">질문</li>
				<li data-value="tab4">사내</li>
				<!-- <li data-value="tab5">사내</li> -->
			</ul>
			<div class="tabs">
				<div id="tab1" class="active">
					<article class="board-content">
						<div class="board-title">
							<a href="#">[익명] 개꿀팁 공유해드려요 ~</a><br>
							<a href="#">[김문섭] mybatis DB연동 공유방법</a><br>
							<a href="#">[익명] 아직도 이거 모르시는분있나요</a><br>
							<a href="#">[익명] 스프링 이렇게 해보세요</a><br>
							<a href="#">[황경재] 제 꿀팁 공유합니다.</a><br>
						</div>
						<div class="board-date">
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
						</div>
					</article>
				</div>
				<div id="tab2">
					<article class="board-content">
						<div class="board-title">
							<a href="#">[익명] 개꿀팁 공유해드려요 ~</a><br>
							<a href="#">[김문섭] mybatis DB연동 공유방법</a><br>
							<a href="#">[익명] 아직도 이거 모르시는분있나요</a><br>
							<a href="#">[익명] 스프링 이렇게 해보세요</a><br>
							<a href="#">[황경재] 제 꿀팁 공유합니다.</a><br>
						</div>
						<div class="board-date">
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
						</div>
					</article>
				</div>
				<div id="tab3">
					<article class="board-content">
						<div class="board-title">
							<a href="#">[익명] 개꿀팁 공유해드려요 ~</a><br>
							<a href="#">[김문섭] mybatis DB연동 공유방법</a><br>
							<a href="#">[익명] 아직도 이거 모르시는분있나요</a><br>
							<a href="#">[익명] 스프링 이렇게 해보세요</a><br>
							<a href="#">[황경재] 제 꿀팁 공유합니다.</a><br>
						</div>
						<div class="board-date">
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
						</div>
					</article>
				</div>
				<div id="tab4">
					<article class="board-content">
						<div class="board-title">
							<a href="#">[익명] 개꿀팁 공유해드려요 ~</a><br>
							<a href="#">[김문섭] mybatis DB연동 공유방법</a><br>
							<a href="#">[익명] 아직도 이거 모르시는분있나요</a><br>
							<a href="#">[익명] 스프링 이렇게 해보세요</a><br>
							<a href="#">[황경재] 제 꿀팁 공유합니다.</a><br>
						</div>
						<div class="board-date">
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
						</div>
					</article>
				</div>
				<!-- <div id="tab5">
					<article class="board-content">
						<div class="board-title">
							<a href="#">[익명] 개꿀팁 공유해드려요 ~</a><br>
							<a href="#">[김문섭] mybatis DB연동 공유방법</a><br>
							<a href="#">[익명] 아직도 이거 모르시는분있나요</a><br>
							<a href="#">[익명] 스프링 이렇게 해보세요</a><br>
							<a href="#">[황경재] 제 꿀팁 공유합니다.</a><br>
						</div>
						<div class="board-date">
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
							<a href="#">2022.03.01 13:59</a><br>
						</div>
					</article>
				</div> -->
			</div>

		</article>
	</section>
</div>



<script>
	$('.tap-btn li').click(function () {
		$(this).addClass('active').siblings().removeClass('active')

		const value = $(this).attr('data-value');
		$('.tabs > div').removeClass('active');
		$('#' + value).addClass('active');
	})

	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>





</body>
</html>