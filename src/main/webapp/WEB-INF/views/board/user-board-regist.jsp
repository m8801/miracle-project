<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/user-board-regist.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>

<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="#">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="#">자유게시판</a></p>
				<p><a href="#">질문게시판</a></p>
				<p><a href="#">사내게시판</a></p>
			</div>

			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">

		<h2>새소식 게시판 글쓰기</h2>
		<!-- <div class="select">
			<select name="board-type">
				<option value="1">공유게시판</option>
				<option value="2">새소식게시판</option>
				<option value="3">자유게시판</option>
				<option value="4">질문게시판</option>
				<option value="5">사내게시판</option>
			</select>
		</div> -->
		
		<form action="${ pageContext.servletContext.contextPath }/board/user-board-regist" method="post" encType="multipart/form-data">
			<div class="board-add">
					<div class="board-title">
						<p>제목 : </p>
						<input type="text" name="title">
					</div>
					<div class="board-file">
						<p>파일첨부 : </p> 
						<div class="drag"></div>
						<input type="file" name="file" >
					</div>
			</div>
			<textarea type="text" class="contents" name="content" ></textarea>
			<div class="btn-bar">
				<button type="submit">등록</button>
				<button type="reset">취소</button>
			</div>
		</form>

	</section>
</div>


<script>
	$('.menu div:nth-child(3)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>


</body>
</html>