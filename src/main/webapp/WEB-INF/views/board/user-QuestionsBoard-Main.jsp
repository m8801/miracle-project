<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>질문게시판 메인화면</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/user-QuestionsBoard-Main.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

	const message = '${ requestScope.message }';
	if(message != null && message !== '') {
		alert(message);
	}
	

	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})



</script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="${ pageContext.servletContext.contextPath }/mainpage/userMain">홈</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
			<div>
				<a href="${ pageContext.servletContext.contextPath }/board/main-boardList">게시판</a>
			</div>
			<div class="sub-menu active">
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/user-FreeBoard-Main">자유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/user-QuestionsBoard-Main">질문게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/jobList">사내게시판</a></p>
			</div>

			<div><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain">일정</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
		</div>
	</aside>
	<section class="section">

		<h1>질문게시판</h1>
		<table class="table">
			<tr>
				<td class="date">작성일</td>
				<td class="no">번호</td>
				<td class="name">제목</td>
				<td class="writer">작성자</td>
			</tr>
			
			<c:forEach var="questionsBoard" items="${ requestScope.userQuestionslist }">
				<tr>
					<td><c:out value="${ questionsBoard.date }" /></td>
					<td><c:out value="${ questionsBoard.boardNo }" /></td>
					<td><c:out value="${ questionsBoard.title }" /></td>
					<td><c:out value="${ questionsBoard.userName }" /></td>
				</tr>
			</c:forEach>
		</table>
		
		<!-- 페이지 처리 -->
		<jsp:include page="../common/questionsUserBoardPaging.jsp" />
		
				<!-- 검색 폼 -->
		<div class="search-area" align="center">
			<form id="loginForm"
				action="${ pageContext.servletContext.contextPath }/board/user-QuestionsBoard-Main"
				method="get" style="display: inline-block">
				<input type="hidden" name="currentPage" value="1"> <select
					id="searchCondition" name="searchCondition">
					<option value="writer"
						${ requestScope.selectCriteria.searchCondition eq "writer"? "selected": "" }>작성자</option>
					<option value="title"
						${ requestScope.selectCriteria.searchCondition eq "title"? "selected": "" }>제목</option>
					<option value="content"
						${ requestScope.selectCriteria.searchCondition eq "content"? "selected": "" }>내용</option>
				</select> <input type="search" id="searchValue" name="searchValue"
					value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">

				<button class="btn btn-bs" type="submit">검색하기</button>
			</form>
		</div>
		

        <div class="btn-bar">
        	<button onclick="location.href='${ pageContext.servletContext.contextPath}/board/user-QuestionsBoard-Regist'">생성</button>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/main-boardList'">뒤로가기</button>
        </div>

	</section>
</div>


<script>

	if(document.querySelectorAll(".table td")) {
		const $tds = document.querySelectorAll(".table td");
		for(let i = 0; i < $tds.length; i++) {
			
			$tds[i].onmouseenter = function() {
				this.parentNode.style.cursor = "pointer";
			}
			
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[1].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/user-QuestionsBoard-Detail?no=" + no;
			}
		}
	}

	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>


</body>
</html>