<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/board_main.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>

<div class="container">
	<c:choose>
		<c:when test="${ sessionScope.loginMember.userNo gt 10000 }"><jsp:include page="../board/manager-side.jsp" /></c:when>
		<c:otherwise><jsp:include page="../board/user-side.jsp" /></c:otherwise>
	</c:choose>

	 <section class="section">
		    <article class="board1">
		      <div>
		        <p>새소식 게시판</p>
		        <hr>
		      </div>
		      <table class="table" >
			    <tr>
					<td>번호</td>
					<td>제목</td>
					<td>작성일</td>
				</tr>
				<c:forEach var="newsBoardList" items="${ requestScope.newsBoardList }">
					<tr>
						<td><c:out value="${ newsBoardList.boardNo }" /></td>
						<td><c:out value="${ newsBoardList.title }" /></td>
						<td><c:out value="${ newsBoardList.createDate }" /></td>
					</tr>
				</c:forEach>
			  </table>
		    </article>

		    <article class="board2">
			  <ul class="tap-btn">
				  <li data-value="tab1"  class="active">공유</li>
				  <li data-value="tab2">자유</li>
				  <li data-value="tab3">질문</li>
				  <li data-value="tab4">사내</li>
			  </ul>
			  <div class="tabs">
				  <div id="tab1" class="active">
						  <table class="table2" >
						    <tr>
								<td>번호</td>
								<td>제목</td>
								<td>작성일</td>
							</tr>
							<c:forEach var="sharingBoardList" items="${ requestScope.sharingBoardList }">
								<tr>
									<td><c:out value="${ sharingBoardList.boardNo }" /></td>
									<td><c:out value="${ sharingBoardList.title }" /></td>
									<td><c:out value="${ sharingBoardList.createDate }" /></td>
								</tr>
							</c:forEach>
						  </table>
				  </div>
				  <div id="tab2">
					  <table class="table3" >
					    <tr>
							<td>번호</td>
							<td>제목</td>
							<td>작성일</td>
						</tr>
						<c:forEach var="freeBoardList" items="${ requestScope.freeBoardList }">
							<tr>
								<td><c:out value="${ freeBoardList.boardNo }" /></td>
								<td><c:out value="${ freeBoardList.title }" /></td>
								<td><c:out value="${ freeBoardList.date }" /></td>
							</tr>
						</c:forEach>
					  </table>
				  </div>
				  <div id="tab3">
					  <table class="table3" >
					    <tr>
							<td>번호</td>
							<td>제목</td>
							<td>작성일</td>
						</tr>
						<c:forEach var="questionBoardList" items="${ requestScope.questionBoardList }">
							<tr>
								<td><c:out value="${ questionBoardList.boardNo }" /></td>
								<td><c:out value="${ questionBoardList.title }" /></td>
								<td><c:out value="${ questionBoardList.date }" /></td>
							</tr>
						</c:forEach>
					  </table>
				  </div>
				  <div id="tab4">
					  <table class="table5" >
					    <tr>
							<td>번호</td>
							<td>제목</td>
							<td>작성일</td>
						</tr>
						<c:forEach var="jobBoardList" items="${ requestScope.jobBoardList }">
							<tr>
								<td><c:out value="${ jobBoardList.boardNo }" /></td>
								<td><c:out value="${ jobBoardList.title }" /></td>
								<td><c:out value="${ jobBoardList.createDate }" /></td>
							</tr>
						</c:forEach>
					  </table>
				  </div>
			  </div>

		  </article>
	  </section>
</div>



<script>
	$('.tap-btn li').click(function () {
		$(this).addClass('active').siblings().removeClass('active')

		const value = $(this).attr('data-value');
		$('.tabs > div').removeClass('active');
		$('#' + value).addClass('active');
	})
	
	
	if(document.querySelectorAll(".table td")) {
		const $tds = document.querySelectorAll(".table td");
		
		for(let i = 0; i < $tds.length; i++) {
			$tds[i].onmouseenter = function() {
				this.parentNode.style.cursor = "pointer";
			}
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[0].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/detail?no=" + no;
			}
		}
	}
	
	if(document.querySelectorAll(".table2 td")) {
		const $tds = document.querySelectorAll(".table2 td");
		
		for(let i = 0; i < $tds.length; i++) {
			$tds[i].onmouseenter = function() {
				this.parentNode.style.cursor = "pointer";
			}
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[0].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/sharingDetail?no=" + no;
			}
		}
	}
	if(document.querySelectorAll(".table3 td")) {
		const $tds = document.querySelectorAll(".table3 td");
		
		for(let i = 0; i < $tds.length; i++) {
			$tds[i].onmouseenter = function() {
				this.parentNode.style.cursor = "pointer";
			}
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[0].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/manager-FreeBoard-Detail?no=" + no;
			}
		}
	}
	if(document.querySelectorAll(".table4 td")) {
		const $tds = document.querySelectorAll(".table4 td");
		
		for(let i = 0; i < $tds.length; i++) {
			$tds[i].onmouseenter = function() {
				this.parentNode.style.cursor = "pointer";
			}
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[0].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/manager-QuestionsBoard-Detail?no=" + no;
			}
		}
	} 
	if(document.querySelectorAll(".table5 td")) {
		const $tds = document.querySelectorAll(".table5 td");
		
		for(let i = 0; i < $tds.length; i++) {
			$tds[i].onmouseenter = function() {
				this.parentNode.style.cursor = "pointer";
			}
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[0].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/jobDetail?no=" + no;
			}
		}
	}
</script>


</body>
</html>