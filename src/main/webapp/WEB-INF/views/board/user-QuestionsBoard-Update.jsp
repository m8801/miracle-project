<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/user-QuestionsBoard-Update.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$('.menu div:nth-child(4)').click(function () {
	$('.sub-menu').toggleClass('active');
})
</script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="${ pageContext.servletContext.contextPath }/mainpage/userMain">홈</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
			<div>
				<a href="${ pageContext.servletContext.contextPath }/board/main-boardList">게시판</a>
			</div>
			<div class="sub-menu active">
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/user-FreeBoard-Main">자유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/user-QuestionsBoard-Main">질문게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/jobList">사내게시판</a></p>
			</div>

			<div><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain">일정</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
		</div>
	</aside>
	<section class="section">

		<h2>수정 페이지</h2>
		<div class="select">
		</div>
		<form action="${ pageContext.servletContext.contextPath }/board/user-QuestionsBoard-Update" method="post" encType="multipart/form-data">
			<div class="board-add">
				<input name="boardNo" type="hidden" value="${ requestScope.freeDetile.boardNo }">
				<div class="board-title">
					<p>제목 : </p>
					<input type="text" name="title" value="<c:out value="${ requestScope.freeDetile.title }"/>">
				</div>
				<div class="board-file">
					<p>파일첨부 : </p>
					<div class="drag"></div>
					<input type="file" name="files">현재 파일 : <c:out value="${ requestScope.freeDetile.attachmentDTO.originName }"/>
					
 					<input type="text" name="originFile" value="${ requestScope.freeDetile.attachmentDTO.originName }">
					
				</div>
			</div>
			<textarea type="text" class="contents" name="content" ><c:out value="${ requestScope.freeDetile.content }"/></textarea>
			<div class="btn-bar">
				<button type="submit">수정</button>
				<%-- <a href="${ pageContext.servletContext.contextPath }/board/user-QuestionsBoard-Main">취소</a> --%>
				<button type="reset" id="regist2">취소</button>
			</div>

		</form>


	</section>
</div>


<script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
	
	if(document.getElementById("regist2")) {
		
		const $regist = document.getElementById("regist2"); 
		
		$regist.onclick = function() {
		
			location.href = "${ pageContext.servletContext.contextPath }/board/user-FreeBoard-Main";
		}
	}
	
</script>


</body>
</html>