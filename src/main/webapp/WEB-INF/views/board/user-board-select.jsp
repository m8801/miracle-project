<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/miracle-frontproject/resources/css/user-board-select.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>

<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="/miracle-frontproject/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="/miracle-frontproject/resources/images/setting.png" alt=""></div>
			<p>나대리</p>
			<span>꿀잠 지원실 / 대리</span><br>
			<a href="#">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="#">공유게시판</a></p>
				<p><a href="#">새소식게시판</a></p>
				<p><a href="#">자유게시판</a></p>
				<p><a href="#">질문게시판</a></p>
				<p><a href="#">사내게시판</a></p>
			</div>

			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">

		<h3>[217724] 개꿀팁 공유해드려요</h3>
		<div class="info">
			<div class="info-left"></div>
			<div class="info-right">
				<span>익명</span>
				<p>2022.03.09</p>
			</div>
		</div>
		<div class="contents">
			<p>1. 포기하면 빠르다.</p>
			<p>1. 포기하면 빠르다.</p>
			<p>1. 포기하면 빠르다.</p>
			<p>1. 포기하면 빠르다.</p>
			<p>1. 포기하면 빠르다.</p>
			<p>1. 포기하면 빠르다.</p>
		</div>

		<button>뒤로가기</button>

	</section>
</div>


<script>
	$('.menu div:nth-child(3)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>



</body>
</html>