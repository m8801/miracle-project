<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/newsBoardDetail.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>

<div class="container">
	<c:choose>
		<c:when test="${ sessionScope.loginMember.userNo gt 10000 }"><jsp:include page="../board/manager-side.jsp" /></c:when>
		<c:otherwise><jsp:include page="../board/user-side.jsp" /></c:otherwise>
	</c:choose>

	<section class="section">
		
		<h3>
			<p>
				<input type="hidden" id="boardNo" value="${ requestScope.newsBoardDetail.boardNo }">
				<c:out value="${ newsBoardDetail.title }" />
			</p>
		</h3>
		<div class="info">
			<div class="info-left"><img src="${pageContext.servletContext.contextPath}/resources/images/profileFiles/${ sessionScope.loginMember.updateFile }" onerror="this.src='${pageContext.servletContext.contextPath}/resources/images/profileFiles/adminUser.png'" alt="프로필 이미지"></div>
			<div class="info-right">
				<span>
					<span><c:out value="${ newsBoardDetail.userName }" /></span>
				</span>
				<p><c:out value="${ newsBoardDetail.createDate }" /></p>
			</div>
		</div>
		<a href="${ pageContext.servletContext.contextPath }/resources/uploadFiles/${ requestScope.newsBoardDetail.attachmentDTO.saveName }" download class="download"><c:out value="${ requestScope.newsBoardDetail.attachmentDTO.originName }"/></a>
		<div class="contents">
			<p><c:out value="${ newsBoardDetail.content }" /></p>
		</div>
		
		<div class="btn-bar">
			<c:if test="${ sessionScope.loginMember.userNo gt 10000 && requestScope.newsBoardDetail.userNo eq sessionScope.loginMember.userNo }">
				<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/newsBoard-update?boardNo=${ requestScope.newsBoardDetail.boardNo }'">수정</button>
			</c:if>
			<c:choose>
   				<c:when test="${ sessionScope.loginMember.userNo gt 10000 && requestScope.newsBoardDetail.userNo ne sessionScope.loginMember.userNo }">
   					<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/delete?boardNo=${ requestScope.newsBoardDetail.boardNo }'">강제삭제</button>
   				</c:when>
				<c:when test="${ sessionScope.loginMember.userNo gt 10000 && requestScope.newsBoardDetail.userNo eq sessionScope.loginMember.userNo }">
					<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/delete?boardNo=${ requestScope.newsBoardDetail.boardNo }'">삭제</button>
				</c:when>
			</c:choose>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/list'">뒤로가기</button>
        </div>

	</section>
</div>


<script>

	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
	
	
	
</script>




</body>
</html>