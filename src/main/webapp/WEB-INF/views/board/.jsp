<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/manager-board1-3.css">
	<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-3.6.0.min.js"></script>
</head>
<body>
<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.servletContext.contextPath}
/resources/images/jjanggu.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.servletContext.contextPath}
/resources/images/seting.png" alt=""></div>
			<p>나대리</p>
			<span>꿀잠 지원실 / 대리</span><br>
			<a href="#">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="#">공유게시판</a></p>
				<p><a href="#">새소식게시판</a></p>
				<p><a href="#">자유게시판</a></p>
				<p><a href="#">질문게시판</a></p>
				<p><a href="#">사내게시판</a></p>
			</div>
			
			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">
		<div class="board">
			<h2>자유 게시판</h2>
		<table>
			<tr>
				<td><input type="checkbox"></td>
				<td>작성일</td>
				<td>번호</td>
				<td colspan="5">제목</td>
				<td>작성자</td>
			</tr>
			<tr>
				<td><input type="checkbox"></td>
				<td>2022.03.20</td>
				<td>1</td>
				<td colspan="5">공지사항입니다.</td>
				<td>관리자</td>
			</tr>
			<tr>
				<td><input type="checkbox"></td>
				<td>2022.03.09</td>
				<td>2</td>
				<td colspan="5">혹시 선거일에 출근하나요?</td>
				<td>익명</td>
			</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
			<tr>
			<td><input type="checkbox"></td>
			<td>2022.03.20</td>
			<td>1</td>
			<td colspan="5">공지사항입니다.</td>
			<td>관리자</td>
		</tr>
		</table>
		</div>
		<div class="buttons">
			<button>등록</button>
			<button>수정</button>
			<button>삭제</button>
		</div>
	</section>
</div>


<script>
	$('.menu div:nth-child(3)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>

</body>
</html>