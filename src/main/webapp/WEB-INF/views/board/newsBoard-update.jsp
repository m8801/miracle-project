<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/newsBoard-update.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>


<div class="container">
	<c:choose>
		<c:when test="${ sessionScope.loginMember.userNo gt 10000 }"><jsp:include page="../board/manager-side.jsp" /></c:when>
		<c:otherwise><jsp:include page="../board/user-side.jsp" /></c:otherwise>
	</c:choose>
	<section class="section">

		<h2>글쓰기</h2>
		<div class="select">
		</div>
		<form action="${ pageContext.servletContext.contextPath }/board/newsBoard-update" method="post" encType="multipart/form-data">
			<div class="board-add">
				<input name="boardNo" type="hidden" value="${ requestScope.newsBoardDetail.boardNo }">
				<div class="board-title">
					<p>제목 : </p>
					<input type="text" name="title" value="<c:out value="${ requestScope.newsBoardDetail.title }"/>">
				</div>
				<div class="board-file">
					<c:choose>
						<c:when test="${ empty requestScope.newsBoardDetail.attachmentDTO.saveName }">
							<p>파일첨부 : </p>
							<input type="file" name="file">
						</c:when>
						<c:when test="${ not empty requestScope.newsBoardDetail.attachmentDTO.saveName }">
							<p>변경할 파일첨부 : </p>
							<input type="file" name="file">
							<span>
								현재 파일 : <a href="${ pageContext.servletContext.contextPath }/resources/uploadFiles/${ requestScope.newsBoardDetail.attachmentDTO.saveName }" download><c:out value="${ requestScope.newsBoardDetail.attachmentDTO.originName }"/></a>
							</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" class="preFile" name="preFile">이전 파일 그대로 업로드
						</c:when>
					</c:choose>
					<input type="hidden" name="fileSaveName" value="${ requestScope.newsBoardDetail.attachmentDTO.saveName }">
				</div>
			</div>
			<textarea type="text" class="contents" name="content" ><c:out value="${ requestScope.newsBoardDetail.content }"/></textarea>
			<div class="btn-bar">
				<button type="submit">등록</button>
				<button type="reset">취소</button>
				<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/list'">뒤로가기</button>
			</div>

		</form>


	</section>
</div>


<!-- <script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script> -->


</body>
</html>