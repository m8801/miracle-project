<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/miracle-frontproject/resources/css/user-board1-5.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>

<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="/miracle-frontproject/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="/miracle-frontproject/resources/images/setting.png" alt=""></div>
			<p>나대리</p>
			<span>꿀잠 지원실 / 대리</span><br>
			<a href="#">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="#">공유게시판</a></p>
				<p><a href="#">새소식게시판</a></p>
				<p><a href="#">자유게시판</a></p>
				<p><a href="#">질문게시판</a></p>
				<p><a href="#">사내게시판</a></p>
			</div>

			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">


		<table class="table">
			<tr>
				<td class="check"></td>
				<td class="date">작성일</td>
				<td class="no">번호</td>
				<td class="name">제목</td>
				<td class="writer">작성자</td>
			</tr>
			<tr>
				<td class="check"><input type="checkbox"></td>
				<td class="date">2022.03.09</td>
				<td class="no">6352</td>
				<td class="name">[공지] 3월 팀별 부서별 매출 근황</td>
				<td class="writer">관리자</td>
			</tr>
			<tr>
				<td class="check"><input type="checkbox"></td>
				<td class="date">2022.03.08</td>
				<td class="no">4876</td>
				<td class="name">[이벤트] 4월 우리 회사 이벤트</td>
				<td class="writer">관리자</td>
			</tr>
			<tr>
				<td class="check"><input type="checkbox"></td>
				<td class="date">2022.03.08</td>
				<td class="no">9968</td>
				<td class="name">[공지] 3월 NEW 소식</td>
				<td class="writer">관리자</td>
			</tr>
			<tr>
				<td class="check"><input type="checkbox"></td>
				<td class="date">2022.03.08</td>
				<td class="no">6565</td>
				<td class="name">[이벤트] 3월 우리 회사 이벤트</td>
				<td class="writer">관리자</td>
			</tr>
			<tr>
				<td class="check"><input type="checkbox"></td>
				<td class="date">2022.02.08</td>
				<td class="no">7532</td>
				<td class="name">[공지] 2월 NEW 소식</td>
				<td class="writer">관리자</td>
			</tr>
			<tr>
				<td class="check"><input type="checkbox"></td>
				<td class="date">2022.02.08</td>
				<td class="no">4323</td>
				<td class="name">[이벤트] 2월 우리 회사 이벤트</td>
				<td class="writer">관리자</td>
			</tr>
			<tr>
				<td class="check"><input type="checkbox"></td>
				<td class="date">2022.02.08</td>
				<td class="no">5753</td>
				<td class="name">[공지] 2월 팀별 부서별 매출 근황</td>
				<td class="writer">관리자</td>
			</tr>
		</table>


	</section>
</div>


<script>
	$('.menu div:nth-child(3)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>




</body>
</html>