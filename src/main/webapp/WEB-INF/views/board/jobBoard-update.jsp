<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jobBoard-update.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>

<div class="container">
	<c:choose>
		<c:when test="${ sessionScope.loginMember.userNo gt 10000 }"><jsp:include page="../board/manager-side.jsp" /></c:when>
		<c:otherwise><jsp:include page="../board/user-side.jsp" /></c:otherwise>
	</c:choose>

	<section class="section">

		<h2>글쓰기</h2>
		<div class="select">
		</div>
		<form action="${ pageContext.servletContext.contextPath }/board/jobBoard-update" method="post" encType="multipart/form-data">
			<div class="board-add">
				<input name="boardNo" type="hidden" value="${ requestScope.jobBoardDetail.boardNo }">
				<div class="board-title">
					<p>제목 : </p>
					<input type="text" name="title" value="<c:out value="${ requestScope.jobBoardDetail.title }"/>">
				</div>
				<input type="checkbox" class="anonymous" name="anonymous" > 익명
				<div class="board-file">
					<!-- <p>파일첨부 : </p>
					<div class="drag"></div> -->
					<c:choose>
						<c:when test="${ empty requestScope.jobBoardDetail.attachmentDTO.saveName }">
							<p>파일첨부 : </p>
							<input type="file" name="file">
						</c:when>
						<c:when test="${ not empty requestScope.jobBoardDetail.attachmentDTO.saveName }">
							<p>변경할 파일첨부 : </p>
							<input type="file" name="file">
							<span>현재 파일 : <a href="${ pageContext.servletContext.contextPath }/resources/uploadFiles/${ requestScope.jobBoardDetail.attachmentDTO.saveName }" download><c:out value="${ requestScope.jobBoardDetail.attachmentDTO.originName }"/></a></span>
						</c:when>
					</c:choose>
					<%-- <input type="file" name="file"> --%>
					<input type="hidden" name="fileSaveName" value="${ requestScope.jobBoardDetail.attachmentDTO.saveName }">
				</div>
			</div>
			<textarea type="text" class="contents" name="content" ><c:out value="${ requestScope.jobBoardDetail.content }"/></textarea>
			<div class="btn-bar">
				<button type="submit">등록</button>
				<button type="reset">취소</button>
				<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/jobList'">뒤로가기</button>
			</div>

		</form>


	</section>
</div>


<script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>


</body>
</html>