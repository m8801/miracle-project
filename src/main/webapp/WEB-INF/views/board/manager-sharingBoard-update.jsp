<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/manager-board-update.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">사용자관리</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="#">자유게시판</a></p>
				<p><a href="#">질문게시판</a></p>
				<p><a href="#">사내게시판</a></p>
			</div>

			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">

		<h2>글쓰기</h2>
		<div class="select">
		</div>
		<form action="${ pageContext.servletContext.contextPath }/board/manager-sharingBoard-update" method="post" encType="multipart/form-data">
			<div class="board-add">
				<input name="boardNo" type="hidden" value="${ requestScope.sharingBoardDetail.boardNo }">
				<div class="board-title">
					<p>제목 : </p>
					<input type="text" name="title" value="<c:out value="${ requestScope.sharingBoardDetail.title }"/>">
				</div>
				<input type="checkbox" class="anonymous" name="anonymous" > 익명
				<div class="board-file">
					<!-- <p>파일첨부 : </p>
					<div class="drag"></div> -->
					<c:choose>
						<c:when test="${ empty requestScope.sharingBoardDetail.attachmentDTO.saveName }">
							<p>파일첨부 : </p>
							<input type="file" name="file">
						</c:when>
						<c:when test="${ not empty requestScope.sharingBoardDetail.attachmentDTO.saveName }">
							<p>변경할 파일첨부 : </p>
							<input type="file" name="file">
							<span>현재 파일 : <a href="${ pageContext.servletContext.contextPath }/resources/uploadFiles/${ requestScope.sharingBoardDetail.attachmentDTO.saveName }" download><c:out value="${ requestScope.sharingBoardDetail.attachmentDTO.originName }"/></a></span>
						</c:when>
					</c:choose>
					<%-- <input type="file" name="file"> --%>
					<input type="hidden" name="fileSaveName" value="${ requestScope.sharingBoardDetail.attachmentDTO.saveName }">
				</div>
			</div>
			<textarea type="text" class="contents" name="content" ><c:out value="${ requestScope.sharingBoardDetail.content }"/></textarea>
			<div class="btn-bar">
				<button type="submit">등록</button>
				<button type="reset">취소</button>
			</div>

		</form>


	</section>
</div>


<script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>


</body>
</html>