<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/manager-side.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.servletContext.contextPath}/resources/images/profileFiles/${ sessionScope.loginMember.updateFile }" onerror="this.src='${pageContext.servletContext.contextPath}/resources/images/profileFiles/adminUser.png'" alt="프로필 이미지"></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.deptName } / ${ sessionScope.loginMember.jobName }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자관리</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListManager">전자결재</a></div>
			<div style="background: #FFF282;">
				<a href="${ pageContext.request.contextPath }/board/main-boardList"  >게시판</a>
			</div>
			<div class="sub-menu active" >
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/jobList">사내게시판</a></p>
			</div>

			<div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
		</div>
	</aside>

</div>


<!-- <script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script> -->


</body>
</html>