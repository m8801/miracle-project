<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/manager-board1-5.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

	const message = '${ requestScope.message }';
	if(message != null && message !== '') {
		alert(message);
	}
</script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.deptName } / ${ sessionScope.loginMember.jobName }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="#">홈</a></div>
			<div><a href="#">사용자관리</a></div>
			<div><a href="#">전자결재</a></div>
			<div>
				<a href="#">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="#">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></p>
				<p><a href="#">질문게시판</a></p>
				<p><a href="#">사내게시판</a></p>
			</div>

			<div><a href="#">일정</a></div>
			<div><a href="#">주소록</a></div>
		</div>
	</aside>
	<section class="section">


		<table class="table">
			<tr>
				<td class="date">작성일</td>
				<td class="no">번호</td>
				<td class="name">제목</td>
				<td class="writer">작성자</td>
			</tr>
			<c:forEach var="board" items="${ requestScope.newsBoardList }">
				<tr>
					<td><c:out value="${ board.createDate }" /></td>
					<td><c:out value="${ board.boardNo }" /></td>
					<td><c:out value="${ board.title }" /></td>
					<td><c:out value="${ board.userName }" /></td>
				</tr>
			</c:forEach>
		</table>
		
		<jsp:include page="../common/newsBoardPaging.jsp" />

        <div class="btn-bar">
        	<button id="regist">생성</button>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/main'">뒤로가기</button>
        </div>

	</section>
</div>


<script>

	if(document.querySelectorAll(".table td")) {
		const $tds = document.querySelectorAll(".table td");
		for(let i = 0; i < $tds.length; i++) {
			
			$tds[i].onmouseenter = function() {
				this.parentNode.style.backgroundColor = "#FFF282";
				this.parentNode.style.cursor = "pointer";
			}
			
			$tds[i].onmouseout = function() {
				this.parentNode.style.backgroundColor = "white";
			}
			
			$tds[i].onclick = function() {
				const no = this.parentNode.children[1].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/board/detail?no=" + no;
			}
		}
	}
	
	if(document.getElementById("regist")) {
		
		const $regist = document.getElementById("regist"); 
		
		$regist.onclick = function() {
		
			location.href = "/miracle/board/manager-board-regist";
		}
	}

	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>


</body>
</html>