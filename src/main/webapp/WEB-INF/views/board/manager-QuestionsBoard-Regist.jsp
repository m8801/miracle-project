<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>질문게시판 게시글 등록</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/manager-QuestionsBoard-Regist.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>

	const message = '${ requestScope.message }';
	if(message != null && message !== '') {
		alert(message);
	}
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
	
</script>
</head>
<body>



<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자관리</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
			<div>
				<a href="${ pageContext.servletContext.contextPath }/board/main-boardList">게시판</a>
			</div>
			<div class="sub-menu active">
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/jobList">사내게시판</a></p>
			</div>

			<div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
		</div>
	</aside>
	<section class="section">

		<h2>질문게시판 게시글 등록</h2>

		<form action="${ pageContext.servletContext.contextPath }/board/manager-QuestionsBoard-Regist" method="post" encType="multipart/form-data">
			<div class="board-add">
					<div class="board-title">
						<p>제목 : </p><br>
						<input type="text" name="title" value="${ requestScope.title }">
					</div>
					<input type="checkbox" id="checkId" name="check2" > 체크시 익명으로 게시글이 올라갑니다.
					<div class="board-file">
						<p>파일첨부 : </p> 
						<input type="file" name="files" id="filetrue"><br>
					</div>
			</div>
			<textarea type="text" class="contents" name="content"><c:out value="${ requestScope.content }"/></textarea>
			<div class="btn-bar">
				<button type="submit">등록</button>
				<button type="reset" id="regist2">취소</button>
			</div>
		</form>


	</section>
</div>


<script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	}) 
	
	if(document.getElementById("regist2")) {
		
		const $regist = document.getElementById("regist2"); 
		
		$regist.onclick = function() {
		
			location.href = "${ pageContext.servletContext.contextPath }/board/manager-QuestionsBoard-Main";
		}
	}


</script>




</body>
</html>








