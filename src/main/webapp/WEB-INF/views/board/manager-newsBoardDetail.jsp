<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/manager-board-select.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자관리</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
			<div>
				<a href="${ pageContext.servletContext.contextPath }/board/main">게시판</a>
			</div>
			<div class="sub-menu">
				<p><a href="#">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-JobBoard-Main">사내게시판</a></p>
			</div>

			<div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
		</div>
	</aside>
	<section class="section">
		<input type="hidden" id="boardNo" value="${ requestScope.newsBoardDetail.boardNo }">
		<h3>
			<p><c:out value="${ newsBoardDetail.title }" /></p>
		</h3>
		<div class="info">
			<div class="info-left"></div>
			<div class="info-right">
				<span><c:out value="${ newsBoardDetail.userName }" /></span>
				<p><c:out value="${ newsBoardDetail.createDate }" /></p>
			</div>
		</div>
		<a href="${ pageContext.servletContext.contextPath }/resources/uploadFiles/${ requestScope.newsBoardDetail.attachmentDTO.saveName }" download><c:out value="${ requestScope.newsBoardDetail.attachmentDTO.originName }"/></a>
		<div class="contents">
			<p><c:out value="${ newsBoardDetail.content }" /></p>
		</div>
		
		<div class="btn-bar">
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/update?boardNo=${ requestScope.newsBoardDetail.boardNo }'">수정</button>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/delete?boardNo=${ requestScope.newsBoardDetail.boardNo }'">삭제</button>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/list'">뒤로가기</button>
        </div>
		

	</section>
</div>


<script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
</script>




</body>
</html>