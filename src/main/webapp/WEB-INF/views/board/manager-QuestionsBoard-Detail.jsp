<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>질문게시판 게시글 상세보기</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/manager-QuestionsBoard-Detail.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$('.menu div:nth-child(4)').click(function () {
	$('.sub-menu').toggleClass('active');
})
</script>
</head>
<body>


<div class="container">
	<aside class="side">
		<div class="profile">
			<div class="img"><img src="${pageContext.request.contextPath}/resources/images/profile.jpg" alt=""></div>
			<div class="set"><img src="${pageContext.request.contextPath}/resources/images/setting.png" alt=""></div>
			<p>${ sessionScope.loginMember.userName }</p>
			<span>${ sessionScope.loginMember.jobCode } / ${ sessionScope.loginMember.deptCode }</span><br>
			<a href="${ pageContext.servletContext.contextPath }/login/login">로그아웃</a>
		</div>
		<div class="menu">
			<div><a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자관리</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a></div>
			<div>
				<a href="${ pageContext.servletContext.contextPath }/board/main-boardList">게시판</a>
			</div>
			<div class="sub-menu active">
				<p><a href="${ pageContext.request.contextPath }/board/sharingList">공유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/list">새소식게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></p>
				<p><a href="${ pageContext.request.contextPath }/board/jobList">사내게시판</a></p>
			</div>

			<div><a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a></div>
			<div><a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a></div>
		</div>
	</aside>
	<section class="section">
		
		<h3>
			<p><c:out value="${ freeDetile.title }" /></p>
		</h3>
		<div class="info">
			<div class="info-left"></div>
			<div class="info-right">
				<span><c:out value="${ freeDetile.userName }" /></span>
				<p><c:out value="${ freeDetile.date }" /></p>
			</div>
		</div>
		<a href="${ pageContext.servletContext.contextPath }/resources/uploadFiles/questionsBoardFiles/${ requestScope.freeDetile.attachmentDTO.saveName }" download><c:out value="${ requestScope.freeDetile.attachmentDTO.originName }"/></a>

		<div class="contents">
			<p><c:out value="${ freeDetile.content }" /></p>
		</div>
		
		<div class="btn-bar">
			<c:if test="${ freeDetile.userName eq sessionScope.loginMember.userName }">
	        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/manager-QuestionsBoard-UpdateList?boardNo=${ requestScope.freeDetile.boardNo }'">수정</button>
			</c:if>
			<c:choose>
				<c:when test="${ !(freeDetile.userName eq sessionScope.loginMember.userName) }">
					<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/manager-QuestionsBoard-DeleteAll?boardNo=${ requestScope.freeDetile.boardNo }'">강제삭제</button>
				</c:when>
			</c:choose>
			<c:choose>
        		<c:when  test="${ freeDetile.userName eq sessionScope.loginMember.userName }">
        			<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/manager-QuestionsBoard-DeleteOne?boardNo=${ requestScope.freeDetile.boardNo }'">삭제</button>
        		</c:when>
			</c:choose>
        	<button onclick="location.href='${ pageContext.servletContext.contextPath }/board/manager-QuestionsBoard-Main'">뒤로가기</button>
        </div>
        
        
      <!-- 댓글 작성용 table -->
			<table id="replyWrite">
				<input type="hidden" id="boardNo" value="${ requestScope.freeDetile.boardNo }">
				<tr>
					<td>댓글</td>
					<td><textarea cols="40" rows="3" id="replyBody"
							style="resize: none;"></textarea></td>
					<td>
						<button type="button" id="registReply">작성하기</button>
					</td>
				</tr>
			</table>
			
			<!-- 댓글 내용 출력용 table -->
			<table id="replyResult">
				<c:if test="${ not empty requestScope.replyList }">
					<c:forEach var="reply" items="${requestScope.replyList}"
						varStatus="st">
						<tr>
							<input type="hidden" id="${ reply.boardNo }" value="${ reply.boardNo }">
							<td>${ reply.no }</td>
							<td>${ reply.member.userName }</td>
							<td>${ reply.comments }</td>
							<td>${ reply.userNo }</td>
							<td>${ reply.date }</td>
							<td>
								<c:if test="${ reply.userNo eq sessionScope.loginMember.userNo }">
									<button type="button" onclick="removeReply(${ reply.no })">댓글삭제</button>
								</c:if>
							</td>
						</tr>
					</c:forEach>

				</c:if>
			</table>
	</section>
</div>


<script>
	$('.menu div:nth-child(4)').click(function () {
		$('.sub-menu').toggleClass('active');
	})
	
		/*  댓글 작성 이벤트 처리 */
		if(document.getElementById("registReply")) {
			const $registReply = document.getElementById("registReply");
			$registReply.onclick = function() {
				// 현재 있는 게시글 번호 
				let boardNo = document.getElementById("boardNo").value;
				// 현재 댓글 내용 
				let replyBody = document.getElementById("replyBody").value;
				
				// 댓글 내용이 없으면 alert
				if(replyBody.trim() == ""){
					$("#replyBody").val("");
					alert('댓글을 입력해 주십시오');
					
				} else {
					$.ajax({
						url:"/miracle/board/manager-QuestionsBoard-Reply",
						type:"post",
						// 게시글 번호랑, 현재 댓글 내용 넘기기 
						data:{refBoardNo:boardNo, body:replyBody},
						// 성공하면 반환
						success:function(data){
							console.table(data);
							
							$("#replyBody").val("");
							
							const $table = $("#replyResult");
							$table.html("");
							
							for(var index in data){
								$tr = $("<tr>");
								$boardNoTd = $("<td>").text(data[index].boardNo);
								$writerTd = $("<td>").text(data[index].member.userName);
								$bodyTd = $("<td>").text(data[index].comments);
								$createDateTd = $("<td>").text(data[index].date);
								
								// 로그인한 사용자는 자기 댓글만 삭제 
								if(${sessionScope.loginMember.userNo}){
									$removeTd = $("<td>").append("<button type='button' onclick='removeReply(" + data[index].no + ")'>댓글삭제</button>");
								} else {
									$removeTd = $("<td>");
								}
								
								$tr.append("<input type='hidden' id=" + ${ data[index].no } + " value='" + data[index].no + "'>");
								$tr.append($boardNoTd);
								$tr.append($writerTd);
								$tr.append($bodyTd);
								$tr.append($createDateTd);
								$tr.append($removeTd);
								
								$table.append($tr);
								
								location.href="/miracle/board/manager-QuestionsBoard-Detail?no=" + boardNo;
							}
						}, error:function(data){
							console.log(data);
						}
					});
				}
			}
		}
		
		/* 댓글 삭제 이벤트 처리 함수*/
		function removeReply(replyNo){
			let boardNo = document.getElementById("boardNo").value;
			$.ajax({
				url:"/miracle/board/manager-QuestionsBoard-RemoveReply",
				type:"post",
				data:{refBoardNo:boardNo, no:replyNo},
				success:function(data){
					console.table(data);

					location.href="/miracle/board/manager-QuestionsBoard-Detail?no=" + boardNo;
				}, error:function(data){
					console.log(data);
				}
			});
		}
		
</script>




</body>
</html>