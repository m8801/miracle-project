<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
 <!DOCTYPE html>
 <html lang="ko">
 <head>
     <meta charset="UTF-8">
     <title>주소록 수정</title>
     <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/addressModifyManager.css">
     <script src="https://kit.fontawesome.com/4be044bf97.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/indexManager.js"></script>
 <body>
  <div class="side-bar">
		<div class="profile">
			<div class="picture">
				<img src="${pageContext.servletContext.contextPath}/resources/images/jjanggu.jpg" alt="짱구">
				<div class="set">
					<a href="${pageContext.servletContext.contextPath}/mainpage/managerEditProfile">
						<img src="${pageContext.servletContext.contextPath}/resources/images/setting.png" alt="">
					</a>
				</div>
			</div>
			<p><c:out value="${ sessionScope.loginMember.userName }"/></p>
			 <span class="logout"><a href="${ pageContext.servletContext.contextPath }/login/logout">로그아웃</a></span>
		</div>
		<div class="menu_bar">
			<div class="home">
				  <a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a>
			</div>
			
			<div class="admin">
				<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a>
			</div>		
			<span class="sub_menu"> 
				<span class="admin_page"><a href="../userManagement/managementMain.jsp">관리자 페이지</a></span> 
				<span class="regist"><a href="../userManagement/addUser.jsp">회원 추가</a></span>
			</span>
			
			<div class="payment_menu">
				<a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a>
			</div>			
			<span class="sub_menu"> 
				<span class="report_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/reportManager">업무보고서 기안</a></span>
				<span class="vacation_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/vacationManager">휴가신청서 기안</a></span> 
				<span class="payment_view"><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListManager">결재문서함</a> </span>
			</span>
			
			<div class="board_menu">
				<a href="${ pageContext.servletContext.contextPath }/board/manager-board-main">게시판</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="shared_com"><a href="#">공유게시판</a></span> 
				<span class="new_com"><a href="${ pageContext.request.contextPath }/newsBoard/list">새소식게시판</a></span>
				<span class="free_com"><a href="../board/manager-board1-1.jsp">자유게시판</a></span> 
				<span class="question_com"><a href="#">질문게시판</a></span> 
				<span class="company_com"><a href="#">사내게시판</a></span>
			</span>
			
			<div class="calendar_menu">
				<a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a>
			</div>
	
			<div class="address_menu">
				<a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="company_ad"><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyManager">회사 공용 주소록</a></span>
				<span class="personal_ad"><a href="${ pageContext.servletContext.contextPath }/address/myManagerList">내 주소록</a></span>
		   		<span class="shaed_ad"><a href="${ pageContext.servletContext.contextPath }/address/sharedManagerList">공유받은 주소록</a></span>
		   		<span class="admin_ad"><a href="${ pageContext.servletContext.contextPath }/address/list">주소록관리자</a></span>
			</span>
		</div>
	</div>
    <div class="main">
        <form class="wrap">
            <div class="title">주소록 수정</div>
            <div class="content">
                <label>이름 : 
                    <input type="text"> 
                </label>
                <label>사원번호 : 
                    <input type="text">
                </label>
                <label>부서명 : 
                    <input type="text">
                </label>
                <label>휴대전화 : 
                    <input type="text">
                </label>
                <label>이메일 : 
                    <input type="text">
                </label>
                <label>그룹 : 
                    <input type="text">
                </label>
            </div>
            <div class="button">
                <button onclick="location.href='addressManager(admin).jsp'">추가</button>
                <button onclick="location.href='addressManager(admin).jsp'">취소</button>
            </div>
        </form>
    </div>
 </body>
 </html>