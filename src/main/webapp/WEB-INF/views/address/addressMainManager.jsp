<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>주소록 메인</title>

<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath}/resources/css/addressMainManager.css">
<script src="https://kit.fontawesome.com/4be044bf97.js"></script>

<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/indexManager.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/addressEvent.js"></script>
<script>
   		const message = '${ requestScope.message }';
  		if(message != '' && message != null) {
   		alert(message);
  		}
 </script>
</head>
<body>
	<div class="side-bar">
		<div class="profile">
			<div class="picture">
				<img src="${pageContext.servletContext.contextPath}/resources/images/jjanggu.jpg" alt="짱구">
				<div class="set">
					<a href="${pageContext.servletContext.contextPath}/mainpage/managerEditProfile">
						<img src="${pageContext.servletContext.contextPath}/resources/images/setting.png" alt="">
					</a>
				</div>
			</div>
			<p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <span><c:out value="${ sessionScope.loginMember.deptName }"/> 
	         	/ <c:out value="${ sessionScope.loginMember.jobName }"/>
	         </span><br>
			 <span class="logout"><a href="${ pageContext.servletContext.contextPath }/login/logout">로그아웃</a></span>
		</div>
		<div class="menu_bar">
			<div class="home">
				  <a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a>
			</div>
			
			<div class="admin">
				<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a>
			</div>		
			<span class="sub_menu"> 
				<span class="admin_page"><a href="../userManagement/managementMain.jsp">관리자 페이지</a></span> 
				<span class="regist"><a href="../userManagement/addUser.jsp">회원 추가</a></span>
			</span>
			
			<div class="payment_menu">
				<a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a>
			</div>			
			<span class="sub_menu"> 
				<span class="report_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/reportManager">업무보고서 기안</a></span>
				<span class="vacation_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/vacationManager">휴가신청서 기안</a></span> 
				<span class="payment_view"><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListManager">결재문서함</a> </span>
			</span>
			
			<div class="board_menu">
				<a href="${ pageContext.servletContext.contextPath }/board/manager-board-main">게시판</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="shared_com"><a href="${ pageContext.request.contextPath }/sharingBoard/list">공유게시판</a></span> 
				<span class="new_com"><a href="${ pageContext.request.contextPath }/newsBoard/list">새소식게시판</a></span>
				<span class="free_com"><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></span> 
				<span class="question_com"><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></span> 
				<span class="company_com"><a href="${ pageContext.request.contextPath }/board/manager-JobBoard-Main">사내게시판</a></span>
			</span>
			
			<div class="calendar_menu">
				<a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a>
			</div>
	
			<div class="address_menu">
				<a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="company_ad"><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyManager">회사 공용 주소록</a></span>
				<span class="personal_ad"><a href="${ pageContext.servletContext.contextPath }/address/myManagerList">내 주소록</a></span>
		   		<span class="shaed_ad"><a href="${ pageContext.servletContext.contextPath }/address/sharedManagerList">공유받은 주소록</a></span>
		   		<span class="admin_ad"><a href="${ pageContext.servletContext.contextPath }/address/list">주소록관리자</a></span>
			</span>
		</div>
	</div>
	<div class="main">
		<h2>주소록 관리자</h2>
		<div class="wrap">
			<div class="content">
				<div class="yellow_nav">
					<div class="top">
						<form id="search" action="${ pageContext.servletContext.contextPath }/address/list" method="get">
							<input type="hidden" name="currentPage" value="1">	
							 <select id="searchCondition" name="searchCondition">
								<option value="userName"
									${ requestScope.selectCriteria.searchCondition eq "userName"? "selected": "" }>사원이름</option>
								<option value="userNo"
									${ requestScope.selectCriteria.searchCondition eq "userNo"? "selected": "" }>사원번호</option>
								<option value="deptName"
									${ requestScope.selectCriteria.searchCondition eq "deptName"? "selected": "" }>부서명</option>
							</select> <input type="search" id="address_search" name="searchValue"
								value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">

							<button class="icon" type="submit">
								<i class="fa fa-search" aria-hidden="true"></i>
							</button>
						</form>
						
						<div class="button">
							<button id="addressInsert1">주소록 추가</button>	
							<button onclick="deleteAddress();">주소록 삭제</button>
							<button id="sharedSelectBtn">주소록 공유</button>
						</div>
					</div>
					<div class="bottom">
						<input type="checkbox" id="chk1">
						<div class="menu">
							<!-- <span>그룹명</span> -->
							<span>이름</span>
							<span>사원번호</span>
							<span>부서명</span> 
							<span>휴대전화</span>
							<span>이메일</span>
						</div>
					</div>
				</div>
				<div class="address" id="addressList">
					<c:forEach items="${ addressList }" var="address">
						<c:set var="i" value="${i+1}" />
						<div class="user">
							<div class="dept1">
								<input type="checkbox" name="chk" id="chk${i}" data-addressno="${ address.addressNo }" data-userno="${ address.userNo }"/>
								<%-- <span><c:out value="${ address.groupName}"/>1</span> --%>
								<span><c:out value="${ address.memberDTO.userName}"/></span>
								<span><c:out value="${ address.userNo}"/></span> 
								<span><c:out value="${ address.deptDTO.deptName}"/></span> 
								<span><c:out value="${ address.memberDTO.phone}"/></span>
								<span><c:out value="${ address.memberDTO.email}"/></span>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div> 
		<!-- 페이지 처리 -->
     	<jsp:include page="../address/ManagerListPaging.jsp" /> 
		<script>
	/* 주소록 삭제 이벤트 함수 */
	function deleteAddress(){
		var cnt = $("input:checkbox[name='chk']:checked");
		var count = cnt.length;
		var noArr = [];
		console.log(cnt.length);
		 for(var i = 0; i < cnt.length;i++) {
			 console.log(cnt);
			 console.log(cnt[0]);
			 
			if(cnt.length == 0) { 
				alert("삭제할 주소록을 선택해 주세요");
			} else {				
				if(cnt) {
					noArr.push(cnt[i].dataset.addressno);
				}			
			}
		 }
		 
		 console.log(noArr);		
		 if(!confirm("정말 삭제하시겠습니까?")){
			 alert("삭제를 취소했습니다.");
			 
		 } else {
			 $.ajax({
					type:"post",
				 	url:"/miracle/address/deleteAddress",
				 	data:  { noArr : noArr.toString() },
				 	success:function(data) {
						if(data == "fail") {
							alert("삭제 오류");
							location.href = "/miracle/address/list";
						} else {
							alert("삭제 성공");
							location.href = "/miracle/address/list";
						}
					 },
					 error:function(data){
					 alert(data);
				     }
			     });
		 } 	
	 }
	</script>	
	<script>
	  if(document.getElementById("sharedSelectBtn")) {		
		  const $sharedSelect = document.getElementById("sharedSelectBtn");	
		  $sharedSelect.onclick = function() {	    
		 	  var cnt = $("input:checkbox[name='chk']:checked");
		      var count = cnt.length;
			  var noArr = [];
			  	
			  console.log(cnt.length);
			  
			  if(cnt.length == 0) { 
				  	alert("공유할 주소록을 선택해 주세요");
				  } 
			  
			  for(var i = 0; i < cnt.length;i++) {			
				  	   if(cnt) {
						noArr.push(cnt[i].dataset.userno);
						location.href = "/miracle/address/mainManagerSharing/"+noArr;
					   }			
				  }
		  	 		 
			 
			/*  $.ajax({
			 	 type:"post",
				 url:"/miracle/address/sharingRegist",
				 data:  { addressNoArr : noArr.toString() 
				 },
				 success:function(data) {
					if(data == null) {
						alert("번호 등록 오류");
					} else {
						alert("번호 등록 성공");
						alert(data);
						location.href = "/miracle/address/sharedAddress1/"+data;
					}
				 },
				 error:function(data){
					alert("공유할 주소록을 체크해주세요");
				 }
			 }); */
		 }
	 }	      
	</script>
	</div>
</body>
</html>