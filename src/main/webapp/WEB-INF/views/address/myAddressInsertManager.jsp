<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <title>주소록 추가</title>
     <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/addressInsertManager.css">
     <script src="https://kit.fontawesome.com/4be044bf97.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/indexManager.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/addressEvent.js"></script>
	 <script>	 
	 	/* 비지니스 로직 성공 alert 메시지 처리 */
		const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			alert(message);
		}
		
		$(function(){
			
			$("#btn1").click(function(){
				
				$("#frm").attr("action","${ pageContext.servletContext.contextPath }/address/myAddressInsertSelect").submit();
			});
			
			$("#btn2").click(function(){
				
				$("#frm").attr("action","${ pageContext.servletContext.contextPath }/address/myAddressInsertManager").attr("method","post").submit();
			});
		});
	 </script>
 </head>
 <body>
 <div class="side-bar">
		<div class="profile">
			<div class="picture">
				<img src="${pageContext.servletContext.contextPath}/resources/images/jjanggu.jpg" alt="짱구">
				<div class="set">
					<a href="${pageContext.servletContext.contextPath}/mainpage/managerEditProfile">
						<img src="${pageContext.servletContext.contextPath}/resources/images/setting.png" alt="">
					</a>
				</div>
			</div>
			<p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <span><c:out value="${ sessionScope.loginMember.deptName }"/> 
	         	/ <c:out value="${ sessionScope.loginMember.jobName }"/>
	         </span><br>
			 <span class="logout"><a href="${ pageContext.servletContext.contextPath }/login/logout">로그아웃</a></span>
		</div>
		<div class="menu_bar">
			<div class="home">
				  <a href="${ pageContext.servletContext.contextPath }/mainpage/managerMain">홈</a>
			</div>
			
			<div class="admin">
				<a href="${ pageContext.servletContext.contextPath }/userManagement/managementMain">사용자 관리</a>
			</div>		
			<span class="sub_menu"> 
				<span class="admin_page"><a href="../userManagement/managementMain.jsp">관리자 페이지</a></span> 
				<span class="regist"><a href="../userManagement/addUser.jsp">회원 추가</a></span>
			</span>
			
			<div class="payment_menu">
				<a href="${ pageContext.servletContext.contextPath }/payment/paymentMainManager">전자결재</a>
			</div>			
			<span class="sub_menu"> 
				<span class="report_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/reportManager">업무보고서 기안</a></span>
				<span class="vacation_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/vacationManager">휴가신청서 기안</a></span> 
				<span class="payment_view"><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListManager">결재문서함</a> </span>
			</span>
			
			<div class="board_menu">
				<a href="${ pageContext.servletContext.contextPath }/board/manager-board-main">게시판</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="shared_com"><a href="${ pageContext.request.contextPath }/sharingBoard/list">공유게시판</a></span> 
				<span class="new_com"><a href="${ pageContext.request.contextPath }/newsBoard/list">새소식게시판</a></span>
				<span class="free_com"><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></span> 
				<span class="question_com"><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></span> 
				<span class="company_com"><a href="${ pageContext.request.contextPath }/board/manager-JobBoard-Main">사내게시판</a></span>
			</span>
			
			<div class="calendar_menu">
				<a href="${ pageContext.servletContext.contextPath }/schedule/managerScheduleMain">일정</a>
			</div>
	
			<div class="address_menu">
				<a href="${ pageContext.servletContext.contextPath }/address/list">주소록</a>
			</div>
			
			<span class="sub_menu"> 
				<span class="company_ad"><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyManager">회사 공용 주소록</a></span>
				<span class="personal_ad"><a href="${ pageContext.servletContext.contextPath }/address/myManagerList">내 주소록</a></span>
		   		<span class="shaed_ad"><a href="${ pageContext.servletContext.contextPath }/address/sharedManagerList">공유받은 주소록</a></span>
		   		<span class="admin_ad"><a href="${ pageContext.servletContext.contextPath }/address/list">주소록관리자</a></span>
			</span>
		</div>
	</div>
    
    <!-- 주소록 추가 -->
    <div class="main">
        <div class="wrap">  
            <div class="title">주소록 추가</div>
            <form class="content" id="frm">	
                <label>사원번호 : 
                    <input type="text" name="userNo" value="<c:out value='${ myAddress.memberDTO.userNo }'/>"> <button id="btn1" type="submit">조회</button>
                </label>
                <label>이름 : 
                    <input type="text" value="<c:out value="${ myAddress.memberDTO.userName }"/>">
                </label>
                <label>부서코드 : 
                    <input type="text" value="<c:out value="${ myAddress.memberDTO.deptCode }"/>">
                </label>
                <label>휴대전화 : 
                    <input type="text" value="<c:out value="${ myAddress.memberDTO.phone }"/>">
                </label>
                <label>이메일 : 
                    <input type="text" value="<c:out value="${ myAddress.memberDTO.email }"/>">
                </label>
                <label>그룹명 : 
                    <input type="text" name="groupName" value="<c:out value="${ requestScope.groupName }"/>">
                </label>
                <div class="button">   
               	  <button type="submit" id="btn2">추가</button>
             	  <button type="button" id="myCancleBtn" onclick="location.href='/miracle/address/myManagerList'">취소</button>
           	    </div>
           </form>
          </div>  
    </div>
    
    
 </body>
 </html>