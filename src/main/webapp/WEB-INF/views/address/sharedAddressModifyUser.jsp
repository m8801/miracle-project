<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
 <!DOCTYPE html>
 <html lang="ko">
 <head>
     <meta charset="UTF-8">
     <title>주소록 수정</title>
     <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/css/addressModifyManager.css">
     <script src="https://kit.fontawesome.com/4be044bf97.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/indexUser.js"></script>
	 <script src="${pageContext.servletContext.contextPath}/resources/js/addressEvent.js"></script>
	 <script>
   	    const message = '${ requestScope.message }';
  	  	if(message != '' && message != null) {
    	alert(message);
  	  	}
	 </script>
 <body>
    <div class="side-bar">
        <div class="profile">
            <div class="picture">
                <img src="${pageContext.servletContext.contextPath}/resources/images/jjanggu.jpg" alt="짱구">
               <div class="set">
               	<a href="${pageContext.servletContext.contextPath}/views/mainpage/managerEditProfile.jsp">
                <img src="${pageContext.servletContext.contextPath}/resources/images/setting.png" alt="">
				</a>
             </div>
            </div>
           	 <p><c:out value="${ sessionScope.loginMember.userName }"/></p>
	         <span><c:out value="${ sessionScope.loginMember.deptName }"/> 
	         	/ <c:out value="${ sessionScope.loginMember.jobName }"/>
	         </span><br>
            <span class="logout"><a href="${ pageContext.servletContext.contextPath }/login/logout" id="logout">로그아웃</a></span>
        </div>
        <div class="menu_bar">
            <div class="home"><a href="${ pageContext.servletContext.contextPath }/mainpage/userMain">홈</a></div>
            <div class="payment_menu">
                <a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListUser">전자결재</a>
            </div>
            <span class="sub_menu">
                <span class="report_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/reportUser">업무보고서 기안</a></span>
				<span class="vacation_Draft"><a href="${ pageContext.servletContext.contextPath }/payment/vacationUser">휴가신청서 기안</a></span> 
				<span class="payment_view"><a href="${ pageContext.servletContext.contextPath }/payment/paymentMainListUser">결재문서함</a> </span>
                </span>
            <div class="board_menu"><a href="../board/manager-board-main.jsp">게시판</a>
            </div>
            <span class="sub_menu">
                <span class="shared_com"><a href="${ pageContext.request.contextPath }/sharingBoard/list">공유게시판</a></span> 
				<span class="new_com"><a href="${ pageContext.request.contextPath }/newsBoard/list">새소식게시판</a></span>
				<span class="free_com"><a href="${ pageContext.request.contextPath }/board/manager-FreeBoard-Main">자유게시판</a></span> 
				<span class="question_com"><a href="${ pageContext.request.contextPath }/board/manager-QuestionsBoard-Main">질문게시판</a></span> 
				<span class="company_com"><a href="${ pageContext.request.contextPath }/board/manager-JobBoard-Main">사내게시판</a></span>
            </span>
            
            <div class="calendar_menu"><a href="${ pageContext.servletContext.contextPath }/schedule/userScheduleMain">일정</a>
            </div>
            
            <div class="address_menu"><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyUser">주소록</a>
            </div>
            <span class="sub_menu">
                <span class="company_ad"><a href="${ pageContext.servletContext.contextPath }/address/addressCompanyUser">회사 공용 주소록</a></span>
                <span class="personal_ad"><a href="${ pageContext.servletContext.contextPath }/address/myUserList">내 주소록</a></span>
		   		<span class="shaed_ad"><a href="${ pageContext.servletContext.contextPath }/address/sharedUserList">공유받은 주소록</a></span>
            </span>
            </div>
    </div>
    <div class="main">
        <form class="wrap" action="${ pageContext.servletContext.contextPath }/address/modifyUserSharedAddress" method="post">
            <div class="title">주소록 수정</div>
            <input type="hidden" name="no" value="${no}">
            <div class="content">       
                <label>그룹 : 
                    <input type="text" name="groupName" value="<c:out value='${ requestScope.groupName }'/>">
                </label>
            </div>
            <div class="button">
                <button type="submit" id="myUserModifySubmit">수정</button>
                <button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/address/sharedUserList'">취소</button>
            </div>
        </form>
    </div>
 </body>
 </html>