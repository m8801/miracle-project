<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>	
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<script src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.8.1.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/addressEvent.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/indexManager.js"></script>
<script> 
$(function(){
	
	$("#searchBtn").click(function(){		
		$("#frm2").attr("action","${ pageContext.servletContext.contextPath }/address/myManagerSearchList").attr("method","post").submit();
	});
	
	$("#shareBtn").click(function(){
		$("#frm2").attr("action","${ pageContext.servletContext.contextPath }/address/mySharingRegist").attr("method","post").submit();
	});
});
</script>
<title>Document</title>
</head>
<body>
 	<jsp:include page="../address/addressMyManager.jsp" /> 
	<form class="searchUser" action="${ pageContext.servletContext.contextPath }/address/myManagerSearchList" id="frm2" method="post">
	<c:set var="i" value="${i+1}"/>
	<input type="hidden" name="currentPage" value="1">
		<h3>공유할 사원 찾기</h3>
		 <input name="addressNoArr" type="hidden" value=" ${ addressNoArr }" > 
		<div class="searchWrap">
			<select name="searchCondition1" id="dept">
				<option value="D1"
				${ requestScope.SharedSelectCriteria.searchCondition1 eq "D1"? "selected": "" }>총무팀
				</option>
				<option value="D2"
				${ requestScope.SharedSelectCriteria.searchCondition1 eq "D2"? "selected": "" }>마케팅팀
				</option>
				<option value="D3"
				${ requestScope.SharedSelectCriteria.searchCondition1 eq "D3"? "selected": "" }>개발팀
				</option>
				<option value="D4"
				${ requestScope.SharedSelectCriteria.searchCondition1 eq "D4"? "selected": "" }>인사팀
				</option>
				<option value="D5"
				${ requestScope.SharedSelectCriteria.searchCondition1 eq "D5"? "selected": "" }>운영팀
				</option>
				<option value="D6"
				${ requestScope.SharedSelectCriteria.searchCondition1 eq "D6"? "selected": "" }>홍보팀
				</option>
			</select> 
			<select name="searchCondition2" id="job">
				<option value="J1" 
				${ requestScope.SharedSelectCriteria.searchCondition2 eq "J1"? "selected": "" }>사장
				</option>
				<option selected value="J2" 
				${ requestScope.SharedSelectCriteria.searchCondition2 eq "J2"? "selected": "" }>부장
				</option>
				<option value="J3"
				${ requestScope.SharedSelectCriteria.searchCondition2 eq "J3"? "selected": "" }>차장
				</option>
				<option value="J4"
				${ requestScope.SharedSelectCriteria.searchCondition2 eq "J4"? "selected": "" }>과장
				</option>
				<option value="J5"
				${ requestScope.SharedSelectCriteria.searchCondition2 eq "J5"? "selected": "" }>대리
				</option>
				<option value="J6"
				${ requestScope.SharedSelectCriteria.searchCondition2 eq "J6"? "selected": "" }>주임
				</option>
				<option value="J7"
				${ requestScope.SharedSelectCriteria.searchCondition2 eq "J7"? "selected": "" }>사원
				</option>
			</select> 
			<input type="search" id="userName" placeholder="사원 이름을 입력하세요" name="searchValue"
				   value="<c:out value="${ requestScope.selectCriteria1.searchValue }"/>">
			<button id="mySearchBtn" type="submit">검색</button>
		</div>
		 
		<div class="bottom">
			<input type="checkbox" id="ck1">
			<div class="menu">
			    <!-- <span>그룹명</span> -->
			    <span>이름</span>
			    <span>사원번호</span>
			    <span>부서명</span> 
			</div>
	    </div>
	
	<div class="sharingAddressList" id="sharingAddressList">
		 <c:forEach items="${ sharingAddressList }" var="address">
			<c:set var="i" value="${i+1}" />
			<div class="user">
				<div class="dept1">
				<input type="checkbox" name="chk" id="ck${i}" data-addressno="${ address.addressNo }"  data-userno="${ address.userNo}"/> 
				<%-- <span><c:out value="${ address.groupName}"/>1</span> --%>
				<span><c:out value="${ address.memberDTO.userName}"/></span>
				<span><c:out value="${ address.userNo}"/></span> 
				<span><c:out value="${ address.deptDTO.deptName}"/></span> 
				</div>
			</div>
		 </c:forEach>
<%--  		 <jsp:include page="../address/MainMangerSharedPaging.jsp" />  --%>
	</div>
	<div class="shareBtn">
		<button type="button" id="myShareBtn">공유</button>
		<button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/address/myManagerList'">취소</button>
	</div>
	</form>	
</body>
</html>