<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Password Change</title>
<style>
	@import url('https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300;400;500;700&display=swap');

	* {
		font-family: 'Noto Sans KR', sans-serif;
		font-weight: 400;
	}

	body, .layout {
		width: 100%; 
		height: 100vh;
		overflow: hidden;
	}
	
	a {
		text-decoration: none;
		color: #000;
	}
	
	.layout {

		background : linear-gradient(105deg, #FFF282 50%, white 50%);
		position: relative;
	}

	.logo {
		width: 200px;
		height: 150px;
		position: absolute;
		right: 20px;
		top: 20px;
		
	}
	
	img {
		width: 100%;
	}
	
	.pwChangeHead {
		display: block;
		font-size: 36px;
		font-weight: bold;
		text-align: center;
		margin-top: 40px;
	}
	
	.form {
		width: 600px;
		height: 400px;
		background: #fff;
		box-shadow: 5px 7px 15px #ddd;
		border-radius: 20px;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
	
	table {
		width: 100%;
		padding: 0 100px;
		margin-bottom: 5px;
	}
	
	label {
		color: #9B9B9B;
		font-size: 18px;
	}
	
	input {
		width: 100%;
		font-size: 18px;
		border: none;
	}
	
	.line {
		border-bottom: 2px solid #E7E7E7;
		padding-top: 30px;
	}
	
	.headTd {
		width: 150px;
	}
		
	.pwResult {
		color: #F00505;
	}
		
	
	.button-box {
		text-align: center;
	}
	
	#submit, #cancel {
		width: 150px;
		padding: 5px 0;
		border-radius: 10px;
		font-size: 18px;
		background: #FFF282;
		color: #fff;
		text-align: center;
	}
	
	#submit {
		display: inline-block;
	}
	
	#cancel {
		display: inline-block;
		margin-left: 100px;
	}
	

</style>
</head>
<body>

	<div class="layout">
		<h1 class="logo"><img src="/miracle/resources/images/logo.jpg"/></h1>
		<form class="form" action="${ pageContext.servletContext.contextPath }/login/pwChange" method="post">
			<span class="pwChangeHead">비밀번호 변경</span>
			<table align="center">
				<tbody>
					<tr>
						<td class="headTd line"><label>사원번호</label></td>
						<td class="line"><c:out value="${ requestScope.loginMember.userNo }"/></td>
					</tr>
					<tr>
						<td class="headTd line"><label>비밀번호</label></td>
						<td class="line"><input type="password" name="userPwd" id="password1" autofocus></td>
					</tr>
					<tr>
						<td class="headTd line"><label>비밀번호 확인</label></td>
						<td class="line"><input type="password" name="userPwd2" id="password2"></td>
					</tr>
					<tr>
						<td colspan="2" height="30px"><label class="pwResult">${ message }</label></td>
					</tr>
				</tbody>
			</table>
			<div class="button-box">
				<input type="submit" id="submit" value="확인">
				<%-- <a href="${ pageContext.servletContext.contextPath }/login/login.jsp" id="submit">확인</a> --%>
				<a href="${ pageContext.servletContext.contextPath }/login/login" id="cancel">취소</a>
			</div>
		</form>
	</div>
</body>
</html>