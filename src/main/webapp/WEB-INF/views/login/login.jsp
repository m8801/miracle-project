<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<style>
	@import url('https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300;400;500;700&display=swap');

	* {
		font-family: 'Noto Sans KR', sans-serif;
		font-weight: 400;
	}

	body, .layout {
		margin : 0;
		padding : 0;
		width: 100%; 
		height: 100vh;
		overflow: hidden;
	}
	
	a {
		text-decoration: none;
		color: #000;
	}
	
	.layout {

		background : linear-gradient(105deg, #FFF282 50%, white 50%);
		position: relative;
	}

	.logo {
		width: 200px;
		height: 150px;
		position: absolute;
		right: 20px;
		top: 20px;
		
	}
	
	img {
		width: 100%;
	}
	
	.loginHead {
		display: block;
		font-size: 36px;
		font-weight: bold;
		text-align: center;
		margin-top: 40px;
	}
	
	.form {
		width: 600px;
		height: 400px;
		background: #fff;
		box-shadow: 5px 7px 15px #ddd;
		border-radius: 20px;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
	
	table {
		width: 100%;
		padding: 0 100px;
	}
	
	label {
		color: #9B9B9B;
		font-size: 18px;
	}
	
	input {
		width: 100%;
		font-size: 18px;
		border: none;
	}
	
	.line {
		border-bottom: 2px solid #E7E7E7;
		padding-top: 40px;
	}
	
	.headTd {
		width: 100px;
	}
		
	#pwFind {
		text-align: right;
		font-size: 18px;
		margin-bottom: 40px;
		color: #424242;
	}
		
	.submit {
		text-align: center;
	}
	
	.submit a {
		display: inline-block;
		width: 80%;
		/* width: 40%; */
		padding: 10px 0;
		background: #FFF282;
		border-radius: 10px;
		font-size: 42px;
		/* font-size: 18px; */
		color: #fff;
	}
		
	#submit {
		width: 80%;
		padding: 10px 0;
		background: #FFF282;
		border-radius: 10px;
		font-size: 18px;
		color: #fff;
	}
	

</style>
</head>
<body>

	<div class="layout">
		<h1 class="logo"><img src="/miracle/resources/images/logo.jpg"/></h1>
		
		<%-- <c:if test ="${ empty sessionScope.loginMember }"> --%>
			<form class="form" action="${ pageContext.servletContext.contextPath }/login/login" method="post">
				<span class="loginHead">로그인</span>
				<table align="center">
					<tbody>
						<tr>
							<td class="headTd line"><label>아이디</label></td>
							<td class="line"><input type="text" name="userNo" id="userId" autofocus></td>
						</tr>
						<tr>
							<td class="headTd line"><label>비밀번호</label></td>
							<td class="line"><input type="password" name="userPwd" id="password"></td>
						</tr>
						<tr>
							<td colspan="2" id="pwFind"><a href="${ pageContext.servletContext.contextPath }/login/pwFind">비밀번호 찾기</a></td>
						</tr>
						<tr height="100px">
						<td colspan="2" class="submit">
							<input type="submit" id="submit" value="로그인">
							<!-- <a href="/miracle/views/mainpage/userMain.jsp">사용자 로그인</a>
							<a href="/miracle/views/mainpage/managerMain.jsp">관리자 로그인</a> -->
						</td>
						</tr>
					</tbody>
				</table>
			</form>
		<%-- </c:if>
		
		<c:if test="${ !empty sessionScope.loginMember }">
			<!-- 로그인 되어 있는 경우 -->
			<% 
				let userNo = "${userNo}";
			
				if(userNo >= 10000) { %>
				
				<jsp:forward page="../mainpage/managerMain.jsp"/>
				
			<% } else { %>
				
				<jsp:forward page="../mainpage/userMain.jsp"/>
				
			<% } %>
			
		</c:if> --%>
		
	</div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script>
	    const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			alert(message);
		}
	</script>
</body>
</html>