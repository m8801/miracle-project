<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Password Find</title>
<style>
	@import url('https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300;400;500;700&display=swap');

	* {
		font-family: 'Noto Sans KR', sans-serif;
		font-weight: 400;
	}

	body, .layout {
		width: 100%; 
		height: 100vh;
		overflow: hidden;
	}
	
	a {
		text-decoration: none;
		color: #000;
	}
	
	.layout {

		background : linear-gradient(105deg, #FFF282 50%, white 50%);
		position: relative;
	}

	.logo {
		width: 200px;
		height: 150px;
		position: absolute;
		right: 20px;
		top: 20px;
		
	}
	
	img {
		width: 100%;
	}
	
	.pwFindHead {
		display: block;
		font-size: 36px;
		font-weight: bold;
		text-align: center;
		margin-top: 40px;
	}
	
	#pwFindForm {
		width: 600px;
		height: 400px;
		background: #fff;
		box-shadow: 5px 7px 15px #ddd;
		border-radius: 20px;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
	}
	
	table {
		width: 100%;
		padding: 0 50px;
		margin-bottom: 20px;
	}
	
	label {
		color: #9B9B9B;
		font-size: 18px;
	}
	
	input {
		width: 100%;
		font-size: 18px;
		border: none;
	}
	
	.line {
		border-bottom: 2px solid #E7E7E7;
		padding-top: 25px;
	}
	
	.headTd {
		width: 100px;
	}
	
	#sendMail {
		width: 150px;
		padding: 10px 0;
		margin-left: 20px;
		background: #FFF282;
		color: #fff;
		border: none;
		border-radius: 10px;
		font-size: 18px;
	}
		
	.cnResult {
		color: #F00505;
	}
	
	.correct {
		color: green;
	}
		
	
	.button-box {
		text-align: center;
	}
	
	#submit, #cancel {
		width: 150px;
		padding: 5px 0;
		border-radius: 10px;
		font-size: 18px;
		background: #FFF282;
		color: #fff;
		text-align: center;
		border: none;
	}
	
	#submit {
		display: inline-block;
	}
	
	#cancel {
		display: inline-block;
		margin-left: 100px;
	}
	

</style>
</head>
<body>

	<div class="layout">
		<h1 class="logo"><img src="/miracle/resources/images/logo.jpg"/></h1>
		<div id="pwFindForm">
			<span class="pwFindHead">비밀번호 찾기</span>
			<table align="center">
				<tbody>
					<tr>
						<td class="headTd line"><label>사원번호</label></td>
						<td class="line"><input type="text" name="userNo" id="userNo" autofocus></td>
						<td></td>
					</tr>
					<tr>
						<td class="headTd line"><label>이메일</label></td>
						<td class="line"><input type="text" name="email" id="email"></td>
						<td><button type="button" id="sendMail">인증번호 보내기</button></td>
					</tr>
					<tr>
						<td class="headTd line"><label>인증번호</label></td>
						<td class="line"><input type="text" name="emailCode" id="emailCode" disabled="disabled"></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="3" height="30px"><label class="cnResult"></label></td>
					</tr>
				</tbody>
			</table>
			<div class="button-box">
				<!-- <input type="submit" id="submit" value="확인"> -->
				<button type="button" id="submit">확인</button>
				<a href="${ pageContext.servletContext.contextPath }/login/login" id="cancel">취소</a>
			</div>
		</div>
	</div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script>
		var code = "";	// 이메일로 전송된 인증번호
	
		$("#sendMail").click(function(){
				
				var userNo = $("#userNo").val();	// 사원번호
				var email = $("#email").val();		// 받을 이메일
				var emailCode = $("#emailCode");	// 인증번호 입력
				
				$.ajax({
					type:"GET",
					url: "/miracle/login/sendMail?userNo=" + userNo + "&email=" + email,
					success: function(data) {
						/* alert("data : " + data); */
						emailCode.attr("disabled", false);
						code = data;
					}
				});
				
				/* $("#pwFindForm").attr("action","${ pageContext.servletContext.contextPath }/login/sendMail").attr("method","post").submit();  */
		});
		
		$("#emailCode").blur(function() {
			
			var inputCode = $("#emailCode").val();	// 입력한 이메일 코드
			var checkResult = $(".cnResult");	// Label에 출력될 결과
			
			if(inputCode == code) {
				checkResult.html("인증번호가 일치합니다.");
				checkResult.attr("class", "correct");
			} else {
				checkResult.html("인증번호가 틀렸습니다. 다시 입력해주세요.");
			}
			
		});
		
			
		$("#submit").click(function(){
				
			var userNo = $("#userNo").val();	// 사원번호
			var email = $("#email").val();		// 받을 이메일
			var inputCode = $("#emailCode").val();	// 입력한 이메일 코드
			
			if(userNo != "" && email != "") {
				
				if(inputCode == code) {
					
					var userNo = $("#userNo").val();	// 사원번호
					
					$.ajax({
						type:"GET",
						url: "/miracle/login/pwFindOk?userNo=" + userNo,
						success: function(data) {
							location.href = "/miracle/login/pwChange";
						}
					});
					
				}
				
			} else {
				
				alert("이메일 인증 후 다시 클릭해주세요.");
	
			}
			
			
		});
		
		
		const message = '${ requestScope.message }';
		if(message != null && message !== '') {
			document.getElementByClass('cnResult').innerText = message;
		}
		
	</script>
</body>
</html>