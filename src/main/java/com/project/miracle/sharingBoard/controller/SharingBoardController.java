package com.project.miracle.sharingBoard.controller;

import java.io.File;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;
import com.project.miracle.newsBoard.model.dto.NewsBoardDTO;
import com.project.miracle.sharingBoard.model.dto.SharingReplyDTO;
import com.project.miracle.sharingBoard.model.dto.SharingAttachmentDTO;
import com.project.miracle.sharingBoard.model.dto.SharingBoardDTO;
import com.project.miracle.sharingBoard.model.service.SharingBoardService;

@Controller
@RequestMapping("/board")
public class SharingBoardController {
	
	private SharingBoardService sharingBoardService;
	
	public SharingBoardController(SharingBoardService sharingBoardService) {
		this.sharingBoardService = sharingBoardService;
	}
	
	
	@GetMapping("/sharingList")
	public ModelAndView sharingBoardList(@RequestParam(defaultValue = "1") int currentPage , ModelAndView mv, HttpServletRequest request) {
		
		int totalCount = sharingBoardService.selectTotalCount();
		
		System.out.println("totalCount : " + totalCount);
		
		int limit = 5; 
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		
		selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		
		List<SharingBoardDTO> sharingBoardList = sharingBoardService.selectAllSharingBoardList(selectCriteria);
		
		System.out.println("sharingBoardList : " + sharingBoardList);
		System.out.println("selectCriteria : " + selectCriteria);
		
		mv.addObject("sharingBoardList", sharingBoardList);
		mv.addObject("selectCriteria", selectCriteria);
		
//		if(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo() > 10000) {
//			mv.setViewName("/board/manager-sharingBoard");
//		}else {
//			mv.setViewName("/board/user-sharingBoard");
//		}
		
		mv.setViewName("/board/sharingBoard");
		
		return mv;
	}

	

	
	@GetMapping("sharingDetail")
	public String selectSharingBoardDetail(@RequestParam int no, Model model, HttpServletRequest  request) {
		
		
		SharingBoardDTO sharingBoardDetail = sharingBoardService.selectSharingBoardDetail(no);
		
		model.addAttribute("sharingBoardDetail", sharingBoardDetail);
		
		System.out.println("no : " + no);
		System.out.println("sharingBoardDetail :" + sharingBoardDetail);
		
		List<SharingReplyDTO> replyList = sharingBoardService.selectAllSharingReplyList(no);
		
		model.addAttribute("replyList", replyList);
		System.out.println("replyList : " + replyList);
		
//		if(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo() > 10000) {
//			return "/board/manager-sharingBoardDetail";
//		}else {
//			return "/board/user-sharingBoardDetail";
//		}
		return "/board/sharingBoardDetail";
	}
	
	
	@GetMapping("/sharingBoard-regist")
	public void registSharingBoard() {}

	@PostMapping("/sharingBoard-regist")
	public String registNewsBoard(@ModelAttribute SharingBoardDTO sharingBoardDTO, HttpServletRequest  request, 
			RedirectAttributes rttr, @RequestParam(required = false) MultipartFile file
			, @RequestParam(value="anonymous", required = false) String anonymous) {
		
		int MemberNo =((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo();
		String MemberName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		
		
		sharingBoardDTO.setUserNo(MemberNo);
		sharingBoardDTO.setUserName(MemberName);
		sharingBoardDTO.setFinalName(MemberName);
		if(anonymous != null) {
			sharingBoardDTO.setAnonymousYn("Y");
		}else {
			sharingBoardDTO.setAnonymousYn("N");
		}
		
		System.out.println("anonymous: " + anonymous); //on, null
		System.out.println("memberNo : " + MemberNo);
		System.out.println("memberName : " + MemberName);
		System.out.println("sharingBoardDTO : " + sharingBoardDTO);
		System.out.println("file :" +file);
		System.out.println("file.isEmpty : " + file.isEmpty()); //true,false
		
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		System.out.println("root : " + root);
		
		String filePath = root + "\\uploadFiles";
		
		File mkdir = new File(filePath);
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		
		if(!file.isEmpty()) {
			
			String originFileName = file.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String saveName = UUID.randomUUID().toString().replace("-","") + ext;
			

			SharingAttachmentDTO fileInfo = new SharingAttachmentDTO();
			try {
				
				fileInfo.setOriginName(originFileName);
				fileInfo.setSaveName(saveName);
				fileInfo.setSavePath(filePath);
				fileInfo.setFileType(ext);
			
				file.transferTo(new File(filePath + "\\" + fileInfo.getSaveName()));
			
			} catch(Exception e) {
				e.printStackTrace();
				
				new File(filePath + "\\" + fileInfo.getSaveName()).delete();
			}
				
			sharingBoardDTO.setAttachmentDTO(fileInfo);

			
		}
		
		sharingBoardService.registSharingBoard(sharingBoardDTO);
		
		rttr.addFlashAttribute("message", "글등록이 완료되었습니다. ");
		
		return "redirect:/board/sharingList";
	}
	
	
	@GetMapping("/sharingUpdate")
	public String modifySharingBoard(@RequestParam int boardNo, Model model) {
		
		SharingBoardDTO sharingBoardDetail = sharingBoardService.selectSharingBoardDetail(boardNo);
		
		model.addAttribute("sharingBoardDetail", sharingBoardDetail);
		
		System.out.println("sharingBoardDetail : " + sharingBoardDetail);
		
		return "/board/sharingBoard-update";
		
	}
	
	@PostMapping("/sharingBoard-update")
	public String modifySharingBoard(@ModelAttribute SharingBoardDTO sharingBoardDTO, 
			RedirectAttributes rttr, HttpServletRequest request,@RequestParam MultipartFile file, 
			@RequestParam(value="anonymous", required = false) String anonymous, @RequestParam(value = "fileSaveName", required = false)String fileSaveName
			, @RequestParam(value = "preFile", required = false)String preFile) {
//		, @RequestParam(value = "fileSaveName", required = false)String fileSaveName
		
		
		System.out.println("fileSaveName : " + fileSaveName);
		System.out.println("boardNo :" + sharingBoardDTO.getBoardNo() );
		System.out.println("anonymous: " + anonymous);
		System.out.println("preFile : "+ preFile);
		if(anonymous != null) {
			sharingBoardDTO.setAnonymousYn("Y");
		}else {
			sharingBoardDTO.setAnonymousYn("N");
		}
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		File mkdir = new File(filePath);
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
//		if(file.isEmpty() && preFile == "on") {
//			
//			
//			
//		}
		
		
		if(!file.isEmpty()) {//새로 올린 파일이 있을때
			
			String originFileName = file.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String saveName = UUID.randomUUID().toString().replace("-","") + ext;
			
			SharingAttachmentDTO fileInfo = new SharingAttachmentDTO();
			try {
				
				fileInfo.setBoardNo(sharingBoardDTO.getBoardNo());
				fileInfo.setOriginName(originFileName);
				fileInfo.setSaveName(saveName);
				fileInfo.setSavePath(filePath);
				fileInfo.setFileType(ext);
			
				file.transferTo(new File(filePath + "\\" + fileInfo.getSaveName()));
				
				
				
			
			} catch(Exception e) {
				e.printStackTrace();
				
				new File(filePath + "\\" + fileInfo.getSaveName()).delete();
			}
				
			
			sharingBoardDTO.setAttachmentDTO(fileInfo);
		} 
		
		if(!fileSaveName.isEmpty() && !file.isEmpty() || !fileSaveName.isEmpty() && file.isEmpty()) {
		//파일이 있는데 새로 다른파일을 업로드할때, 파일이 있는데 새로운 파일을 업로드 안할때 file.delete를 해줘야하겠지..
			
			File oldFile = new File(filePath + "\\" + fileSaveName);
			if(oldFile.isFile()) {
				oldFile.delete();
			}
		}
		
		
		
		sharingBoardService.modifySharingBoard(sharingBoardDTO, preFile);
		
		rttr.addFlashAttribute("message", "글수정이 완료되었습니다. ");
		
		return "redirect:/board/sharingList";
		
	}
	
	
	@GetMapping("/sharingDelete")
	public String removeSharingBoard(@RequestParam int boardNo, RedirectAttributes rttr, HttpServletRequest request) {
		
		SharingBoardDTO sharingBoardDetail = sharingBoardService.selectSharingBoardDetail(boardNo);
		
		System.out.println(sharingBoardDetail.getAttachmentDTO().getSaveName());
		System.out.println("boardNo : " + boardNo );
		sharingBoardService.removeSharingBoard(boardNo);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		File file = new File(filePath + "\\" + sharingBoardDetail.getAttachmentDTO().getSaveName());
		if(file.isFile()) {
			file.delete();
		}

		rttr.addFlashAttribute("message", "게시글이 삭제되었습니다.");
		
		return "redirect:/board/sharingList";
	}
	

	
	@PostMapping(value="/registSharingReply", produces = "application/json; charset=utf-8")
	@ResponseBody
	public List<SharingReplyDTO> registReply(@ModelAttribute SharingReplyDTO sharingReplyDTO, HttpServletRequest request){
		
		System.out.println("====================================");
		sharingReplyDTO.setUserNo(((LoginDTO)request.getSession().getAttribute("loginMember")).getUserNo());
		
		List<SharingReplyDTO> replyList = sharingBoardService.registReply(sharingReplyDTO);
		
		return replyList;
	}
	
	
	@PostMapping(value = "/removeSharingReply", produces = "applcation/json; charset=UTF-8")
	@ResponseBody
	public List<SharingReplyDTO> removeReply(@ModelAttribute SharingReplyDTO sharingReplyDTO, HttpServletRequest request){
		
		System.out.println("===================================");
		List<SharingReplyDTO> replyList = sharingBoardService.removeReply(sharingReplyDTO);
		
		return replyList;
	}
	
	
	
	
	
}
