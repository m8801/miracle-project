package com.project.miracle.sharingBoard.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.newsBoard.model.dto.NewsBoardDTO;
import com.project.miracle.sharingBoard.model.dao.SharingBoardMapper;
import com.project.miracle.sharingBoard.model.dto.SharingReplyDTO;
import com.project.miracle.sharingBoard.model.dto.SharingAttachmentDTO;
import com.project.miracle.sharingBoard.model.dto.SharingBoardDTO;

@Service
public class SharingBoardServiceImpl implements SharingBoardService{
	
	private SharingBoardMapper mapper;
	
	@Autowired
	public SharingBoardServiceImpl(SharingBoardMapper mapper) {
		this.mapper = mapper;
	}
	
	
	/**
	 * <pre>
	 * 	공유 게시판 전체 리스트 조회용 메소드 
	 * <pre>
	 */
	@Override
	public List<SharingBoardDTO> selectAllSharingBoardList(SelectCriteria selectCriteria) {
		
		List<SharingBoardDTO> sharingBoardList = mapper.selectAllSharingBoardList(selectCriteria);
		
		return sharingBoardList;
	}


	/**
	 * <pre>
	 * 전체 게시물수 조회용 메소드 
	 * </pre>
	 */
	@Override
	public int selectTotalCount() {
		
		return mapper.selectTotalCount();
	}


	/**
	 * <pre>
	 * 공유 게시판 상세보기 조회용 메소드
	 * </pre>
	 */
	@Override
	public SharingBoardDTO selectSharingBoardDetail(int no) {
	
		SharingBoardDTO sharingBoardDetail = null;
		
		sharingBoardDetail = mapper.selectSharingBoardDetail(no);
		
		return sharingBoardDetail;
	}


	/**
	 * <pre>
	 * 공유 게시판 댓글 조회용 메소드
	 * </pre>
	 */
	@Override
	public List<SharingReplyDTO> selectAllSharingReplyList(int no) {

		List<SharingReplyDTO> replyList = null;
		
		replyList = mapper.selectSharingReplyList(no);
		
		return replyList;
	}


	/**
	 * <pre>
	 * 공유 게시판 글 등록용 메소드 
	 * </pre>
	 *
	 */
	@Override
	public void registSharingBoard(SharingBoardDTO sharingBoardDTO) {

		int result = mapper.insertSharingBoard(sharingBoardDTO);
		
		if(result > 0 && sharingBoardDTO.getAttachmentDTO() != null) {
			
			int attachmentResult = mapper.insertAttachment(sharingBoardDTO.getAttachmentDTO());
		}
	}


	/**
	 * <pre>
	 * 	공유 게시글 수정용 메소드 
	 * </pre>
	 */
	@Override
	public void modifySharingBoard(SharingBoardDTO sharingBoardDTO, String preFile) {
		
		SharingBoardDTO selectResult = mapper.selectSharingBoardDetail(sharingBoardDTO.getBoardNo());
		System.out.println("preFile : " + preFile);
		System.out.println("selectResult :" + selectResult);
		System.out.println("selectResult : " + selectResult.getAttachmentDTO().getSaveName());  // null
		
		sharingBoardDTO.setBoardNo(selectResult.getBoardNo());
		int modifyResult = mapper.updateSharingBoard(sharingBoardDTO);
		
		int attachmentResult = 0;	
		
		
		// 원래 파일이 있는데 수정할때 다른 파일을 안올렸는데..?원래파일을 그대로 사용한다..?
		// modifyResult < 0 && selectResult.getAttachmentDTO().getSaveName() == null && preFile == on
		if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && preFile != null) {
//			attachmentResult = mapper.updateAttachment(sharingBoardDTO.getAttachmentDTO());
			sharingBoardDTO = mapper.selectSharingBoardDetail(selectResult.getBoardNo());
			
		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() == null && sharingBoardDTO.getAttachmentDTO() != null) {
			attachmentResult = mapper.insertAttach(sharingBoardDTO.getAttachmentDTO());
		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && sharingBoardDTO.getAttachmentDTO() == null) {
			attachmentResult = mapper.removeAttachment(sharingBoardDTO.getBoardNo());
		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && sharingBoardDTO.getAttachmentDTO() != null) {
			attachmentResult = mapper.updateAttachment(sharingBoardDTO.getAttachmentDTO());  //원래 파일이 있고 수정할 때 수정할때도 파일이 업로되면 바꿔줘벌여
		}
		
		
//		if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() == null && sharingBoardDTO.getAttachmentDTO() != null) {
//			attachmentResult = mapper.insertAttach(sharingBoardDTO.getAttachmentDTO()); //원래 파일이 없는데 수정할때 파일이 업로드 될때는 인서트를 해벌여
//		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && sharingBoardDTO.getAttachmentDTO() == null){
//			attachmentResult = mapper.removeAttachment(sharingBoardDTO.getBoardNo());   //원래 파일이 있는데 수정할때 파일이 업로드 될때는 원래 있던거를 지워벌여
//		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && sharingBoardDTO.getAttachmentDTO() != null) {
//			attachmentResult = mapper.updateAttachment(sharingBoardDTO.getAttachmentDTO());  //원래 파일이 있고 수정할 때 수정할때도 파일이 업로되면 바꿔줘벌여
//		}
		
//		if(modifyResult > 0 && sharingBoardDTO.getAttachmentDTO() != null) {
//			int attachmentResult = mapper.updateAttachment(sharingBoardDTO.getAttachmentDTO());
//		}
		
		
	}


	/**
	 * <pre>
	 * 	공유 게시글 삭제용 메소드 
	 * </pre>
	 */
	@Override
	public void removeSharingBoard(int boardNo) {
		
		int replyResult = mapper.removeReplyment(boardNo);
		
		int attachResult = mapper.removeAttachment(boardNo);
		
		int result = mapper.removeSharingBoard(boardNo);
		
	}


	/**
	 * <pre>
	 * 공유 게시판 댓글 작성용 메소드
	 * </pre>
	 */
	@Override
	public List<SharingReplyDTO> registReply(SharingReplyDTO sharingReplyDTO) {
		
		List<SharingReplyDTO> replyList = null;
		
		int result = mapper.insertReply(sharingReplyDTO);
		
		if(result > 0) {
			replyList = mapper.selectSharingReplyList(sharingReplyDTO.getBoardNo());
		}
		
		return replyList;
	}


	/**
	 *<pre>
	 *	댓글 삭제용 메소드
	 *</pre>
	 */
	@Override
	public List<SharingReplyDTO> removeReply(SharingReplyDTO sharingReplyDTO) {

		List<SharingReplyDTO> replyList = null;
		System.out.println(sharingReplyDTO);
		
		int result  = mapper.removeReply(sharingReplyDTO.getReplyNo());
		
		if(result > 0 ) {
			replyList = mapper.selectSharingReplyList(sharingReplyDTO.getBoardNo());
		}
		
		return replyList;
	}


	

}
