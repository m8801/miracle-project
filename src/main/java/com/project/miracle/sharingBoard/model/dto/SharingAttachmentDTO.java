package com.project.miracle.sharingBoard.model.dto;

public class SharingAttachmentDTO {

	private int boardNo;
	private String originName;
	private String saveName;
	private String savePath;
	private String fileType;
	private String status;
	
	public SharingAttachmentDTO() {}

	public SharingAttachmentDTO(int boardNo, String originName, String saveName, String savePath, String fileType,
			String status) {
		super();
		this.boardNo = boardNo;
		this.originName = originName;
		this.saveName = saveName;
		this.savePath = savePath;
		this.fileType = fileType;
		this.status = status;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getSaveName() {
		return saveName;
	}

	public void setSaveName(String saveName) {
		this.saveName = saveName;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AttachmentDTO [boardNo=" + boardNo + ", originName=" + originName + ", saveName=" + saveName
				+ ", savePath=" + savePath + ", fileType=" + fileType + ", status=" + status + "]";
	}
	
	
	
	
	
	
}
