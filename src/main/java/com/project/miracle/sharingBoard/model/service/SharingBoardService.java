package com.project.miracle.sharingBoard.model.service;

import java.util.List;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.sharingBoard.model.dto.SharingReplyDTO;
import com.project.miracle.sharingBoard.model.dto.SharingBoardDTO;

public interface SharingBoardService {
	
	List<SharingBoardDTO> selectAllSharingBoardList(SelectCriteria selectCriteria);

	int selectTotalCount();

	SharingBoardDTO selectSharingBoardDetail(int no);

	List<SharingReplyDTO> selectAllSharingReplyList(int no);

	void registSharingBoard(SharingBoardDTO sharingBoardDTO);

	void modifySharingBoard(SharingBoardDTO sharingBoardDTO, String preFile);

	void removeSharingBoard(int boardNo);

	List<SharingReplyDTO> registReply(SharingReplyDTO sharingReplyDTO);

	List<SharingReplyDTO> removeReply(SharingReplyDTO sharingReplyDTO);


}
