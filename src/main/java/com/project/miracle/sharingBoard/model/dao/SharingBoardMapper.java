package com.project.miracle.sharingBoard.model.dao;

import java.util.List;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.sharingBoard.model.dto.SharingReplyDTO;
import com.project.miracle.sharingBoard.model.dto.SharingAttachmentDTO;
import com.project.miracle.sharingBoard.model.dto.SharingBoardDTO;

public interface SharingBoardMapper {

	List<SharingBoardDTO> selectAllSharingBoardList(SelectCriteria selectCriteria);

	int selectTotalCount();

	SharingBoardDTO selectSharingBoardDetail(int no);

	List<SharingReplyDTO> selectSharingReplyList(int no);

	int insertSharingBoard(SharingBoardDTO sharingBoardDTO);

	int insertAttachment(SharingAttachmentDTO attachmentDTO);

	int updateSharingBoard(SharingBoardDTO sharingBoardDTO);

	int updateAttachment(SharingAttachmentDTO attachmentDTO);

	int removeSharingBoard(int boardNo);

	int removeAttachment(int boardNo);

	int insertAttach(SharingAttachmentDTO attachmentDTO);

	int insertReply(SharingReplyDTO sharingReplyDTO);

	int removeReply(int replyNo);

	int removeReplyment(int boardNo);


}
