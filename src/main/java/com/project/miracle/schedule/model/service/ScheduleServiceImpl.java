package com.project.miracle.schedule.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.schedule.model.dao.ScheduleMapper;
import com.project.miracle.schedule.model.dto.ScheduleDTO;

@Service
public class ScheduleServiceImpl implements ScheduleService {
	
	private ScheduleMapper mapper;
	
	@Autowired
	public void ScheduleService(ScheduleMapper schedulemapper) {
		this.mapper = schedulemapper;
	}

	@Override
	public int insertService(ScheduleDTO scheduleDTO) {
		
		return mapper.scheduleinsert(scheduleDTO);
	}

	@Override
	public List<ScheduleDTO> AllSchedule(ScheduleDTO scheduleDTO) {
		
		return mapper.scheduleList(scheduleDTO);
	}

	@Override
	public int ModfiySchedule(ScheduleDTO scheduleDTO) {
		
		return mapper.scheduleModify(scheduleDTO);
	}

	@Override
	public int ScheduleDel(ScheduleDTO scheduleDTO) {
		
		return mapper.scheduleDel(scheduleDTO);
	}

	@Override
	public int Managerinsert(ScheduleDTO scheduleDTO) {
		
		return mapper.Managerscheduleinsert(scheduleDTO);
	}

	@Override
	public List<ScheduleDTO> managerAllSchedule(ScheduleDTO scheduleDTO) {
		
	
		return mapper.managerScheduleList(scheduleDTO);
	}

	@Override
	public int managerModfiySchedule(ScheduleDTO scheduleDTO) {
		
		return mapper.managerscheduleModify(scheduleDTO);
	}

	@Override
	public int managerScheduleDel(ScheduleDTO scheduleDTO) {
		
		return mapper.managerscheduleDel(scheduleDTO);
	}


		


}
