package com.project.miracle.schedule.model.dto;

import java.sql.Date;

import com.project.miracle.common.model.dto.MemberDTO;

public class ScheduleDTO {
	
	private int no;
	private int userNo;
	private Date start;
	private Date end;
	private String title;
	private boolean allday;
	private String divide;
	private MemberDTO memberDTO;
	private int defId;
	private int instanceId;
	private String sortYN;
	
	
	public ScheduleDTO() {}


	public ScheduleDTO(int no, int userNo, Date start, Date end, String title, boolean allday, String divide, int defId,
			int instanceId, String sortYN, MemberDTO memberDTO) {
		super();
		this.no = no;
		this.userNo = userNo;
		this.start = start;
		this.end = end;
		this.title = title;
		this.allday = allday;
		this.divide = divide;
		this.memberDTO = memberDTO;
		this.defId = defId;
		this.instanceId = instanceId;
		this.sortYN = sortYN;
	}


	public int getNo() {
		return no;
	}


	public void setNo(int no) {
		this.no = no;
	}


	public int getUserNo() {
		return userNo;
	}


	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}


	public Date getStart() {
		return start;
	}


	public void setStart(Date start) {
		this.start = start;
	}


	public Date getEnd() {
		return end;
	}


	public void setEnd(Date end) {
		this.end = end;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public boolean isAllday() {
		return allday;
	}


	public void setAllday(boolean allday) {
		this.allday = allday;
	}


	public String getDivide() {
		return divide;
	}


	public void setDivide(String divide) {
		this.divide = divide;
	}


	public MemberDTO getMemberDTO() {
		return memberDTO;
	}


	public void setMemberDTO(MemberDTO memberDTO) {
		this.memberDTO = memberDTO;
	}


	public int getDefId() {
		return defId;
	}


	public void setDefId(int defId) {
		this.defId = defId;
	}


	public int getInstanceId() {
		return instanceId;
	}


	public void setInstanceId(int instanceId) {
		this.instanceId = instanceId;
	}


	public String getSortYN() {
		return sortYN;
	}


	public void setSortYN(String sortYN) {
		this.sortYN = sortYN;
	}


	@Override
	public String toString() {
		return "ScheduleDTO [no=" + no + ", userNo=" + userNo + ", start=" + start + ", end=" + end + ", title=" + title
				+ ", allday=" + allday + ", divide=" + divide + ", memberDTO=" + memberDTO + ", defId=" + defId
				+ ", instanceId=" + instanceId + ", sortYN=" + sortYN + "]";
	}

	
}
