package com.project.miracle.schedule.model.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.schedule.model.dto.ScheduleDTO;
import com.project.miracle.schedule.model.service.ScheduleService;

@Controller
@RequestMapping("/schedule")
public class ScheduleController {
	

	private ScheduleService scheduleService;
	
	@Autowired
	public ScheduleController(ScheduleService scheduleService) {
		this.scheduleService = scheduleService;
	}
	
	@GetMapping("/userScheduleMain")
	public void schedlueAdd() {}
	
	@GetMapping("/userScheduleAdd")
	public void userAdd() {}
	
	@GetMapping("/userScheduleUpdate")
	public void userUpdate(@RequestParam("type")String type, Model m) {
		m.addAttribute("type", type);
	}
	
	@GetMapping("/userScheduleDelete")
	public void userDelete(@RequestParam("type")String type, Model m) {
		m.addAttribute("type", type);
	}
	
	@GetMapping("/managerScheduleMain")
	public void managerMain() {}
	
	@GetMapping("/managerScheduleAdd")
	public void managerAdd() {}
	
	@GetMapping("/managerScheduleUpdate")
	public void managerUpdate(@RequestParam("type") String type, Model m) {
		m.addAttribute("type", type);
	}
	
	@GetMapping("/managerScheduleDelete")
	public void managerDelete(@RequestParam("type") String type, Model m) {
		m.addAttribute("type", type);
	}


	/**
	 * <pre>
	 *   user 일정추가
	 * </pre>
	 * @param addEvent 
	 * @param request
	 * @return
	 * @throws ParseException
	 */
	@PostMapping("/userScAdd")
	@ResponseBody
	public String insertSchedule(@RequestParam("addEvent") String addEvent,@RequestParam("checked")String checked,
			       HttpServletRequest request) throws ParseException {
		
		System.out.println("넘어온 addEvent 확인 : "+ addEvent);
		System.out.println("넘어온 type 확인 : " + checked);

	    JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(addEvent);
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		
		
		scheduleDTO.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());

		if(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo() > 10000) {
			scheduleDTO.setDivide("부서");
		} else {
			scheduleDTO.setDivide("개인");				
		}

		
        scheduleDTO.setStart(java.sql.Date.valueOf((String)jsonObject.get("start")));
        scheduleDTO.setEnd(java.sql.Date.valueOf((String)jsonObject.get("end")));
		scheduleDTO.setTitle((String) jsonObject.get("title"));
		scheduleDTO.setAllday((boolean) jsonObject.get("allday"));
		scheduleDTO.setDefId(Integer.parseInt((String)jsonObject.get("defId")));
		scheduleDTO.setInstanceId(Integer.parseInt((String)jsonObject.get("instanceId")));

		System.out.println("scheduleDTO : "+ scheduleDTO);

	    int result = scheduleService.insertService(scheduleDTO);
	    
	    String message = null;
	    
	    if(result > 0) {
	    	message = "success";
	    } else {
	    	message = "fail";
	    }

		return message;
	}
	
	
	
	/**
	 * <pre>
	 *   user 메인페이지 
	 * </pre>
	 * @return
	 */
	@GetMapping(value="/userListAll", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String ScheduleAllMenuList(@RequestParam(value="type", required=false)String type,
		      HttpServletRequest request) {
		
		System.out.println("user 메인 넘어온 type : " + type);
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();

		if(type.equals("dept")) {
			scheduleDTO.setDivide("부서");
		}else if(type.equals("user")) {
			scheduleDTO.setDivide("개인");
		} 
		System.out.println("user 메인 들어온 값 확인 : " + scheduleDTO);	
		
		MemberDTO memberDTO = new MemberDTO();

		String deptCode = (((LoginDTO)request.getSession().getAttribute("loginMember")).getDeptCode());
		System.out.println("user 메인 deptC 확인 : " + deptCode);
		memberDTO.setDeptCode(deptCode);
		
		scheduleDTO.setUserNo(((LoginDTO)request.getSession().getAttribute("loginMember")).getUserNo());

		System.out.println("user 메인 mem확인 : " + memberDTO);
		scheduleDTO.setMemberDTO(memberDTO);

		
		System.out.println("user 메인 scheduleDTO 값 출력 : " + scheduleDTO);
		
		// 1 조회
		List<ScheduleDTO> list = scheduleService.AllSchedule(scheduleDTO);

		// 2. 조회된 결과 출력
		System.out.println("User 메인 마지막 list : " +list);
		
		Gson gson =  new GsonBuilder()
				.setDateFormat("yyyy-MM-dd").create();
		
		String jsString = gson.toJson(list);
		System.out.println(jsString);
		
		return jsString;
	}
	
	
	
	/**
	 * <pre>
	 *   user 일정 수정 
	 * </pre>
	 * @param Modify
	 * @return
	 * @throws ParseException
	 */
	@PostMapping("/scheduleUpdate")
	@ResponseBody
	public String scheduleModify(@RequestParam("Modify") String Modify ,@RequestParam("textModify") String textModify) throws ParseException {
		System.out.println("넘어온 이벤트 : " + Modify);
		System.out.println("넘어온 text :" + textModify);
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(Modify);

		
		// modify {"title":"유리랑 개인약속","no":62}
		
		System.out.println(" No 확인중 : " + (Integer.parseInt((String) jsonObject.get("no"))));
		
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		scheduleDTO.setTitle(textModify);
		scheduleDTO.setNo((Integer.parseInt((String) jsonObject.get("no"))));
//		scheduleDTO.setNo(Integer.parseInt((String) jsonObject.get(Modify)));

		
		System.out.println("들어간 DTO 확인 : " + scheduleDTO);
		
		int result = scheduleService.ModfiySchedule(scheduleDTO);
		
		
		
		String message = "";
		
		if(result>0) {
			message = "success";
		} else {
			message = "fail";
		}
		
		return message;
	}
	
	
	
	/**
	 * <pre>
	 *   user 일정 삭제 
	 * </pre>
	 * @param title
	 * @return
	 * @throws ParseException
	 */
	@PostMapping("/scheduleDelete")
	@ResponseBody
	public String scheduleDelete(@RequestParam("title")String title) throws ParseException {
		
		System.out.println("delete 넘어온 title 값 : " + title);
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(title);
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		scheduleDTO.setTitle((String) jsonObject.get("title"));
		
		System.out.println(scheduleDTO);
		
		int result = scheduleService.ScheduleDel(scheduleDTO);
		
	
		String message = "";
		
		if(result>0) {
			message = "success";
		} else {
			message = "fail";
		}
		
		return message;
	}
	
	
	
	
	/**
	 * <pre>
	 *   manager 일정 추가
	 * </pre>
	 * @param addEvent
	 * @param request
	 * @return
	 * @throws ParseException
	 */
	@PostMapping("/managerScAdd")
	@ResponseBody
	public String managerScAdd(@RequestParam("addEvent")String addEvent,@RequestParam("checked")String checked,HttpServletRequest request) throws ParseException {
		
		System.out.println("addEvent 확인 : "+ addEvent);
		System.out.println("checked 확인 : " + checked);

	    JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(addEvent);
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		
		scheduleDTO.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());

		if(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo() > 10000) {
			
			if(checked.equals("dept")){
				scheduleDTO.setDivide("부서");	
			}else {
				scheduleDTO.setDivide("개인");
			}
		}
		
        scheduleDTO.setStart(java.sql.Date.valueOf((String)jsonObject.get("start")));
        scheduleDTO.setEnd(java.sql.Date.valueOf((String)jsonObject.get("end")));
		scheduleDTO.setTitle((String) jsonObject.get("title"));
		scheduleDTO.setAllday((boolean) jsonObject.get("allday"));
		scheduleDTO.setDefId(Integer.parseInt((String)jsonObject.get("defId")));
		scheduleDTO.setInstanceId(Integer.parseInt((String)jsonObject.get("instanceId")));

		System.out.println("scheduleDTO : "+ scheduleDTO);

	    int result = scheduleService.Managerinsert(scheduleDTO);
	    
	    String message = null;
	    
	    if(result > 0) {
	    	message = "success";
	    } else {
	    	message = "fail";
	    }

		return message;
	}
	
	
	/**
	 * <pre>
	 *   manager 메인 페이지 
	 * </pre>
	 * @return
	 */
	@GetMapping(value="/managerListAll", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String managerAllMenuList(@RequestParam(value="type", required=false)String type,
			      HttpServletRequest request) {
		
		System.out.println("넘어온 type : " + type);
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();

		if(type.equals("dept")) {
			scheduleDTO.setDivide("부서");
		}else if(type.equals("user")) {
			scheduleDTO.setDivide("개인");
		} 
		
		System.out.println("들어온 값 확인 : " + scheduleDTO);	
		
		MemberDTO memberDTO = new MemberDTO();

		String deptCode = (((LoginDTO)request.getSession().getAttribute("loginMember")).getDeptCode());
		System.out.println("deptC 확인 : " + deptCode);
		memberDTO.setDeptCode(deptCode);
		
		scheduleDTO.setUserNo(((LoginDTO)request.getSession().getAttribute("loginMember")).getUserNo());
		
		System.out.println("mem확인 : " + memberDTO);
		scheduleDTO.setMemberDTO(memberDTO);

		System.out.println("scheduleDTO 값 출력 : " + scheduleDTO);
	
		// 1 조회
		List<ScheduleDTO> list = scheduleService.managerAllSchedule(scheduleDTO);	
		// 2. 조회된 결과 출력
		System.out.println("마지막 전달할 list값 : " +list);
		
		Gson gson =  new GsonBuilder()
				.setDateFormat("yyyy-MM-dd").create();
		
		String jsString = gson.toJson(list);
		System.out.println(jsString);
		
		return jsString;
	}
	
	
	
	
	
	/**
	 * <pre>
	 *   manager 일정 수정 
	 * </pre>
	 * @param Modify
	 * @return
	 * @throws ParseException
	 */
	@PostMapping("/managerscheduleUpdate")
	@ResponseBody
	public String managerscheduleModify(@RequestParam("Modify") String Modify ,
			       @RequestParam("textModify") String textModify) throws ParseException {
		
		System.out.println("넘어온 이벤트 : " + Modify);
		System.out.println("넘어온 text :" + textModify);
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(Modify);

		System.out.println(" No 확인중 : " + (Integer.parseInt((String) jsonObject.get("no"))));
		
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		scheduleDTO.setTitle(textModify);
		scheduleDTO.setNo((Integer.parseInt((String) jsonObject.get("no"))));
		System.out.println("들어간 DTO 확인 : " + scheduleDTO);
		
		int result = scheduleService.managerModfiySchedule(scheduleDTO);

		String message = "";
		
		if(result>0) {
			message = "success";
		} else {
			message = "fail";
		}
		
		return message;
	}
	
	
	/**
	 * <pre>
	 *   manager 일정 삭제 
	 * </pre>
	 * @param title
	 * @return
	 * @throws ParseException
	 */
	@PostMapping("/managerscheduleDelete")
	@ResponseBody
	public String managerscheduleDelete(@RequestParam("title")String title) throws ParseException {
		
		System.out.println("delete 넘어온 title 값 : " + title);
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(title);
		
		ScheduleDTO scheduleDTO = new ScheduleDTO();
		scheduleDTO.setTitle((String) jsonObject.get("title"));
		
		System.out.println(scheduleDTO);
		
		int result = scheduleService.managerScheduleDel(scheduleDTO);
		
	
		String message = "";
		
		if(result>0) {
			message = "success";
		} else {
			message = "fail";
		}
		
		return message;
	}
	
}



