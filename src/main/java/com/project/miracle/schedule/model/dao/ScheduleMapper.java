package com.project.miracle.schedule.model.dao;

import java.util.List;

import com.project.miracle.schedule.model.dto.ScheduleDTO;

public interface ScheduleMapper {

	int scheduleinsert(ScheduleDTO scheduleDTO);

	List<ScheduleDTO> scheduleList(ScheduleDTO scheduleDTO);

	int scheduleModify(ScheduleDTO scheduleDTO);

	int scheduleDel(ScheduleDTO scheduleDTO);

	int Managerscheduleinsert(ScheduleDTO scheduleDTO);

	List<ScheduleDTO> managerScheduleList(ScheduleDTO scheduleDTO);

	int managerscheduleModify(ScheduleDTO scheduleDTO);

	int managerscheduleDel(ScheduleDTO scheduleDTO);

}
