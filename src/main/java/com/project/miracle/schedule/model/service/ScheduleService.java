package com.project.miracle.schedule.model.service;

import java.util.List;

import com.project.miracle.schedule.model.dto.ScheduleDTO;

public interface ScheduleService {

	int insertService(ScheduleDTO scheduleDTO);

	List<ScheduleDTO> AllSchedule(ScheduleDTO scheduleDTO);

	int ModfiySchedule(ScheduleDTO scheduleDTO);

	int ScheduleDel(ScheduleDTO scheduleDTO);

	int Managerinsert(ScheduleDTO scheduleDTO);

	List<ScheduleDTO> managerAllSchedule(ScheduleDTO scheduleDTO);

	int managerModfiySchedule(ScheduleDTO scheduleDTO);

	int managerScheduleDel(ScheduleDTO scheduleDTO);


}
