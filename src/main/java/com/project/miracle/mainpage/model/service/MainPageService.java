package com.project.miracle.mainpage.model.service;

import com.project.miracle.common.model.dto.ProfileDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.mainpage.exception.MemberModifyException;

public interface MainPageService {

	void modifyMember(LoginDTO member) throws MemberModifyException;

	Integer selectPno(int userNo);

	void modifyProfile(ProfileDTO member);

	void addProfile(ProfileDTO member);

}
