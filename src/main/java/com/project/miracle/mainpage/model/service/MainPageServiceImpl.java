package com.project.miracle.mainpage.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.common.model.dto.ProfileDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.mainpage.exception.MemberModifyException;
import com.project.miracle.mainpage.model.dao.MainPageMapper;

@Service
public class MainPageServiceImpl implements MainPageService {

	private final MainPageMapper mapper;

	
	@Autowired
	public MainPageServiceImpl(MainPageMapper mapper) {
		
		this.mapper = mapper;

	}


	@Override
	public void modifyMember(LoginDTO member) throws MemberModifyException {
		
		int result = mapper.updateMember(member);
		
		if(!(result > 0)) {
			throw new MemberModifyException("회원 정보 수정에 실패하셨습니다.");
		}
		
	}


	@Override
	public Integer selectPno(int userNo) {
		
		Integer result = mapper.selectPno(userNo);
		
		if(result != null) {
			return result;
		} else {
			return 0;
		}
	}


	@Override
	public void modifyProfile(ProfileDTO member) {
		
		int result = mapper.modifyProfile(member);
		
		if(!(result > 0)) {
			return;
		}
		
	}


	@Override
	public void addProfile(ProfileDTO member) {

		int result = mapper.addProfile(member);
		
		if(!(result > 0)) {
			return;
		}
		
	}


}