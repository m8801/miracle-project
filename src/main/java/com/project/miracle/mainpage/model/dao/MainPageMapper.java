package com.project.miracle.mainpage.model.dao;

import com.project.miracle.common.model.dto.ProfileDTO;
import com.project.miracle.login.model.dto.LoginDTO;

public interface MainPageMapper {

	int updateMember(LoginDTO member);

	Integer selectPno(int userNo);

	int modifyProfile(ProfileDTO member);

	int addProfile(ProfileDTO member);



	

}