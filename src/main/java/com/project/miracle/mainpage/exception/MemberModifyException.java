package com.project.miracle.mainpage.exception;

public class MemberModifyException extends Exception {

	public MemberModifyException(String msg) {
		
		super(msg);
		
	}
	
}
