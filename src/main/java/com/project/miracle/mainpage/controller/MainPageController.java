package com.project.miracle.mainpage.controller;

import java.io.File;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.model.dto.ProfileDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.mainpage.exception.MemberModifyException;
import com.project.miracle.mainpage.model.service.MainPageService;

@Controller
@RequestMapping("/mainpage")
public class MainPageController {

	private final MainPageService mainpageService;
	
	@Autowired
	public MainPageController(MainPageService mainpageService) {
		
		this.mainpageService = mainpageService;
		
	}
	
		
//	@GetMapping("/managerEditProfile")
//	public String managerEditProfile() {}

//	@PostMapping("/managerEditProfile")
//	public String managerEditProfile() {
//		
//		LoginDTO logindto = new LoginDTO();
//		
//		System.out.println(logindto.getUserNo());
//		
//		return "/login/pwChange";
//		
//	}
	
	
	
	@PostMapping("/managerEditProfile")
	public String managerEditProfile(@ModelAttribute LoginDTO member, HttpServletRequest request, RedirectAttributes rttr) throws MemberModifyException {
		
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		
		member.setUserName(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName());
		member.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());
		member.setDeptName(((LoginDTO) request.getSession().getAttribute("loginMember")).getDeptName());
		member.setJobName(((LoginDTO) request.getSession().getAttribute("loginMember")).getJobName());
		member.setPhone(phone);
		member.setEmail(email);
		member.setUpdateFile(((LoginDTO) request.getSession().getAttribute("loginMember")).getUpdateFile());
		
		System.out.println("member : " + member);
		
		mainpageService.modifyMember(member);
		
		request.getSession().setAttribute("loginMember", member);
		
		rttr.addFlashAttribute("message", "프로필 변경 완료");
		
		return "redirect:/mainpage/managerMain";
		
	}
	
	@PostMapping("/profile")
	public String singleFileUpload(@ModelAttribute ProfileDTO member, @RequestParam MultipartFile profileUpload, HttpServletRequest request, RedirectAttributes rttr, Model model) {
		
		System.out.println("profileUpload : " + profileUpload);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		System.out.println("root : " + root);
				
		String filePath = root + "\\images\\profileFiles";
		
		File mkdir = new File(filePath);
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		String originalFile = profileUpload.getOriginalFilename();
		String ext = originalFile.substring(originalFile.lastIndexOf("."));
		String savedName = UUID.randomUUID().toString().replace("-", "") + ext;
		
		System.out.println("originalFile : " + originalFile);
		System.out.println("ext : " + ext);
		System.out.println("savedName : " + savedName);
		
		
		try {
			
			profileUpload.transferTo(new File(filePath + "\\" + savedName));
			rttr.addFlashAttribute("message", "파일 업로드 성공");
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			new File(filePath + "\\" + savedName).delete();
			rttr.addFlashAttribute("message", "파일 업로드 실패");
			
		}
		
		member.setOriginalFile(originalFile);
		member.setUpdateFile(savedName);
		member.setFilePath(filePath);
		
		System.out.println(member);
		
		member.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());
		int pno = mainpageService.selectPno(member.getUserNo()).intValue();
		
		System.out.println("pno : " + pno);
		
		member.setUserPno(pno);
		
		System.out.println("getPno : " + member.getUserPno());
		
		if(pno != 0) {
			mainpageService.modifyProfile(member);
		} else {
			mainpageService.addProfile(member);
		}
		
//		model.addAttribute("profile", member.getUpdateFile());
		rttr.addFlashAttribute("message", "프로필 변경 완료");

//		if(member.getUserNo() >= 10000) {
//			return "redirect:/mainpage/managerMain";
//		} else {
//			return "redirect:/mainpage/userMain";
//		}
		
		return "redirect:/login/login";
	}
	
	
	
	@PostMapping("/userEditProfile")
	public String userEditProfile(@ModelAttribute LoginDTO member, HttpServletRequest request, RedirectAttributes rttr) throws MemberModifyException {
		
		String phone = request.getParameter("phone");
		String email = request.getParameter("email");
		
		member.setUserName(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName());
		member.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());
		member.setDeptName(((LoginDTO) request.getSession().getAttribute("loginMember")).getDeptName());
		member.setJobName(((LoginDTO) request.getSession().getAttribute("loginMember")).getJobName());
		member.setPhone(phone);
		member.setEmail(email);
		member.setUpdateFile(((LoginDTO) request.getSession().getAttribute("loginMember")).getUpdateFile());
		
		System.out.println("member : " + member);
		
		mainpageService.modifyMember(member);
		
		request.getSession().setAttribute("loginMember", member);
		
		rttr.addFlashAttribute("message", "프로필 변경 완료");
		
		return "redirect:/mainpage/managerMain";
		
	}
	
	
	
}