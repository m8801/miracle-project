package com.project.miracle.common.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.mainBoard.model.dto.NewsBoardDTO;
import com.project.miracle.mainBoard.service.MainBoardService;
import com.project.miracle.payment.model.service.PaymentService;
import com.project.miracle.schedule.model.service.ScheduleService;

@Controller
@RequestMapping("/*")
public class MainController {
	
	private MainBoardService mainBoardService;
	private PaymentService paymentService;
	
	@Autowired
	public MainController(MainBoardService mainBoardService, PaymentService paymentService) {
		this.mainBoardService = mainBoardService;
		this.paymentService = paymentService;
	}
	
	@GetMapping("/")
	public String test() {
		
		return "main/main";
	}
		
	@GetMapping("/mainpage/managerMain")
	@ResponseBody
	public ModelAndView managerMain(@RequestParam(defaultValue = "1") int currentPage, ModelAndView mv, HttpServletRequest request) {
		
		// 나중에 필요한 내용을 조회한 후에 객체 값을 메인페이지로 전달해서 보여준다.

		// 게시판 연동
		int limit = 5; 
		int buttonAmount = 5;
		
		int newsTotalCount = 5;
		
		SelectCriteria selectCriteria = null;
		
		selectCriteria = Pagenation.getSelectCriteria(currentPage, newsTotalCount, limit, buttonAmount);
		
		List<NewsBoardDTO> newsBoardList = mainBoardService.selectAllNewsBoardList(selectCriteria);
		List<JobBoardDTO> jobBoardList = mainBoardService.selectAllJobBoardList(selectCriteria);

		// 전자결재
		int reportWaitingCount = paymentService.selectReportWaitingCount();
		int vacationWaitingCount = paymentService.selectVacationWaitingCount();
		int reportCompleteCount = paymentService.selectReportCompleteCount();
		int vacationCompleteCount = paymentService.selectVacationCompleteCount();
		int reportRejectCount = paymentService.selectReportRejectCount();
		int vacationRejectCount = paymentService.selectVacationRejectCount();
					
		mv.addObject("reportWaitingCount", reportWaitingCount);
		mv.addObject("vacationWaitingCount", vacationWaitingCount);
		mv.addObject("reportCompleteCount", reportCompleteCount);
		mv.addObject("vacationCompleteCount", vacationCompleteCount);
		mv.addObject("reportRejectCount", reportRejectCount);
		mv.addObject("vacationRejectCount", vacationRejectCount);
		
		
		mv.addObject("newsBoardList", newsBoardList);
		mv.addObject("jobBoardList",jobBoardList);
				
		mv.addObject("selectCriteria", selectCriteria);
		
		mv.setViewName("mainpage/managerMain");
		
		return mv;
		
//		return  "mainpage/managerMain";
	}
	
	@GetMapping("/mainpage/userMain")
	public ModelAndView userMain(@RequestParam(defaultValue = "1") int currentPage , ModelAndView mv, HttpServletRequest request) {
		// 나중에 필요한 내용을 조회한 후에 객체 값을 메인페이지로 전달해서 보여준다.
		
		int limit = 5; 
		int buttonAmount = 5;
		
		int newsTotalCount = 5;
	
		SelectCriteria selectCriteria = null;
		
		selectCriteria = Pagenation.getSelectCriteria(currentPage, newsTotalCount, limit, buttonAmount);
		
		List<NewsBoardDTO> newsBoardList = mainBoardService.selectAllNewsBoardList(selectCriteria);
		List<JobBoardDTO> jobBoardList = mainBoardService.selectAllJobBoardList(selectCriteria);
		
		// 전자결재
		int reportWaitingCount = paymentService.selectReportWaitingCount();
		int vacationWaitingCount = paymentService.selectVacationWaitingCount();
		int reportCompleteCount = paymentService.selectReportCompleteCount();
		int vacationCompleteCount = paymentService.selectVacationCompleteCount();
		int reportRejectCount = paymentService.selectReportRejectCount();
		int vacationRejectCount = paymentService.selectVacationRejectCount();
			
		mv.addObject("reportWaitingCount", reportWaitingCount);
		mv.addObject("vacationWaitingCount", vacationWaitingCount);
		mv.addObject("reportCompleteCount", reportCompleteCount);
		mv.addObject("vacationCompleteCount", vacationCompleteCount);
		mv.addObject("reportRejectCount", reportRejectCount);
		mv.addObject("vacationRejectCount", vacationRejectCount);

		mv.addObject("newsBoardList", newsBoardList);
		mv.addObject("jobBoardList",jobBoardList);
		
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("mainpage/userMain");
		
		return mv;
		 
//		return  "mainpage/userMain";
	}
	
//	@GetMapping("/userManagement/managementMain")
//	public String userManagement() {
//		
//		return "userManagement/managementMain";
//		
//	}
	
//	@GetMapping("/address/addressMyManager")
//	public String addressMyManager() {
//		
//		return "/address/addressMyManager";
//		
//	}
	
	@GetMapping("/address/addressMyUser")
	public String addressMyUser() {
		
		return "/address/addressMyUser";
		
	}
	
	@GetMapping("/board/manager-board-main")
	public String managerBoardMain() {
		
		return "/board/manager-board-main";
		
	}
	
	@GetMapping("/board/user-board-main")
	public String userBoardMain() {
		
		return "/board/user-board-main";
		
	}
	
	@GetMapping("/board/manager-board1-1")
	public String managerBoard1() {
		
		return "/board/manager-board1-1";
		
	}
	
	@GetMapping("/board/manager-board1-2")
	public String managerBoard2() {
		
		return "/board/manager-board1-2";
		
	}
	
	@GetMapping("/board/manager-board1-3")
	public String managerBoard3() {
		
		return "/board/manager-board1-3";
		
	}
	
	@GetMapping("/board/manager-board1-4")
	public String managerBoard4() {
		
		return "/board/manager-board1-4";
		
	}
	
	@GetMapping("/board/manager-board1-5")
	public String managerBoard5() {
		
		return "/board/manager-board1-5";
		
	}
	
	@GetMapping("/board/user-board1-1")
	public String userBoard1() {
		
		return "/board/user-board1-1";
		
	}
	
	@GetMapping("/board/user-board1-2")
	public String userBoard2() {
		
		return "/board/user-board1-2";
		
	}
	
	@GetMapping("/board/user-board1-3")
	public String userBoard3() {
		
		return "/board/user-board1-3";
		
	}
	
	@GetMapping("/board/user-board1-4")
	public String userBoard4() {
		
		return "/board/user-board1-4";
		
	}
	
	@GetMapping("/board/user-board1-5")
	public String userBoard5() {
		
		return "/board/user-board1-5";
		
	}
	
	@GetMapping("/payment/paymentMainManager")
	public String paymentMainManager() {
		
		return "/payment/paymentMainManager";
		
	}
	
	@GetMapping("/payment/paymentMainUser")
	public String paymentMainUser() {
		
		return "/payment/paymentMainUser";
		
	}
	
//	@GetMapping("/schedule/managerScheduleMain")
//	public String managerScheduleMain() {
//		
//		return "/schedule/managerScheduleMain";
//		
//	}
//	
//	@GetMapping("/schedule/userScheduleMain")
//	public String userScheduleMain() {
//		
//		return "/schedule/userScheduleMain";
//		
//	}
	
	
	// 퀵슬롯
	@GetMapping("/userManagement/addUser")
	public String addUser() {
		
		return "/userManagement/addUser";
		
	}
	
//	@GetMapping("/schedule/managerScheduleAdd")
//	public String managerScheduleAdd() {
//		
//		return "/schedule/managerScheduleAdd";
//		
//	}
	
//	@GetMapping("/schdule/userScheduleAdd")
//	public String userScheduleAdd() {
//		
//		return "/schdule/userScheduleAdd";
//		
//	}
	
	@GetMapping("/payment/paymentReportUser")
	public String paymentReportUser() {
		
		return "/payment/paymentReportUser";
		
	}
	
	// 프로필 관리
	@GetMapping("/mainpage/managerEditProfile")
	public String managerEditProfile() {
		
		return "/mainpage/managerEditProfile";
		
	}
	
	@GetMapping("/mainpage/userEditProfile")
	public String userEditProfile() {
		
		return "/mainpage/userEditProfile";
		
	}
	
}
