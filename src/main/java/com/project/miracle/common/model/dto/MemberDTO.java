package com.project.miracle.common.model.dto;

public class MemberDTO {

	private int userNo;
	private String userName;
	private String gender;
	private int age;
	private String phone;
	private String birthDate;
	private String jobCode;
	private String deptCode;
	private String userYN;
	private String control;
	private String email;
	private String delYN;
	private String userPwd;
	public MemberDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MemberDTO(int userNo, String userName, String gender, int age, String phone, String birthDate, String jobCode,
			String deptCode, String userYN, String control, String email, String delYN, String userPwd) {
		super();
		this.userNo = userNo;
		this.userName = userName;
		this.gender = gender;
		this.age = age;
		this.phone = phone;
		this.birthDate = birthDate;
		this.jobCode = jobCode;
		this.deptCode = deptCode;
		this.userYN = userYN;
		this.control = control;
		this.email = email;
		this.delYN = delYN;
		this.userPwd = userPwd;
	}
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getBirtDate() {
		return birthDate;
	}
	public void setBirtDate(String birtDate) {
		this.birthDate = birtDate;
	}
	public String getJobCode() {
		return jobCode;
	}
	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}
	public String getDeptCode() {
		return deptCode;
	}
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}
	public String getUserYN() {
		return userYN;
	}
	public void setUserYN(String userYN) {
		this.userYN = userYN;
	}
	public String getControl() {
		return control;
	}
	public void setControl(String control) {
		this.control = control;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDelYN() {
		return delYN;
	}
	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	@Override
	public String toString() {
		return "MemberDTO [userNo=" + userNo + ", userName=" + userName + ", gender=" + gender + ", age=" + age
				+ ", phone=" + phone + ", birtDate=" + birthDate + ", jobCode=" + jobCode + ", deptCode=" + deptCode
				+ ", userYN=" + userYN + ", control=" + control + ", email=" + email + ", delYN=" + delYN + ", userPwd="
				+ userPwd + "]";
	}
	
}
