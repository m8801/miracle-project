package com.project.miracle.common.model.dto;

import java.sql.Date;

public class ProfileDTO {

	private int userPno;
	private String originalFile;
	private String updateFile;
	private Date createDate;
	private Date updateDate;
	private String delYn;
	private int userNo;
	private String filePath;
	
	public ProfileDTO() {}

	public ProfileDTO(int userPno, String originalFile, String updateFile, Date createDate, Date updateDate,
			String delYn, int userNo, String filePath) {
		super();
		this.userPno = userPno;
		this.originalFile = originalFile;
		this.updateFile = updateFile;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.delYn = delYn;
		this.userNo = userNo;
		this.filePath = filePath;
	}

	public int getUserPno() {
		return userPno;
	}

	public void setUserPno(int userPno) {
		this.userPno = userPno;
	}

	public String getOriginalFile() {
		return originalFile;
	}

	public void setOriginalFile(String originalFile) {
		this.originalFile = originalFile;
	}

	public String getUpdateFile() {
		return updateFile;
	}

	public void setUpdateFile(String updateFile) {
		this.updateFile = updateFile;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "ProfileDTO [userPno=" + userPno + ", originalFile=" + originalFile + ", updateFile=" + updateFile
				+ ", createDate=" + createDate + ", updateDate=" + updateDate + ", delYn=" + delYn + ", userNo="
				+ userNo + ", filePath=" + filePath + "]";
	}
	
}
