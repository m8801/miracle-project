package com.project.miracle.newsBoard.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;
import com.project.miracle.newsBoard.model.dto.NewsBoardDTO;
import com.project.miracle.newsBoard.model.service.NewsBoardService;
import com.project.miracle.sharingBoard.model.dto.SharingBoardDTO;
import com.project.miracle.sharingBoard.model.service.SharingBoardService;

@Controller
@RequestMapping("/board")
public class NewsBoardController {
	
	private NewsBoardService newsBoardService;
	
	@Autowired
	public NewsBoardController(NewsBoardService newsBoardService){
		this.newsBoardService = newsBoardService;
	}
	
	@GetMapping("/main")
	public String test(HttpServletRequest  request) {
		
//		if(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo() > 10000) {
//			return "/board/manager-board-main";
//		}else {
//			return "/board/user-board-main";
//		}
		return "/board/board-main";
	}
	
//	@GetMapping("/list")
//	public String seleteAllNewsBoardList(Model model, HttpServletRequest  request) {
//		
//		List<NewsBoardDTO> newsBoardList = newsBoardService.selectAllNewsBoardList();
//		
//		System.out.println(newsBoardList);
//		
//		model.addAttribute("newsBoardList", newsBoardList);
//		
//		if(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo() > 10000) {
//			return "/board/manager-newsBoard";
//		}else {
//			return "/board/user-newsBoard";
//		}
//		
//	}
	
	@GetMapping("/list")
	public ModelAndView newsBoardList(@RequestParam(defaultValue = "1") int currentPage , ModelAndView mv, HttpServletRequest request) {
		
		int totalCount = newsBoardService.selectTotalCount();
		
		System.out.println("totalCount : " + totalCount);
		
		int limit = 5; 
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		
		List<NewsBoardDTO> newsBoardList = newsBoardService.selectAllNewsBoardList(selectCriteria);
		
		mv.addObject("newsBoardList", newsBoardList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("/board/newsBoard");
		
		return mv;
	}
	
	@GetMapping("/detail")
	public String selectNewsBoardDetail(@RequestParam int no, Model model, HttpServletRequest  request) {
		
		NewsBoardDTO newsBoardDetail = newsBoardService.selectNewsBoardDetail(no);
		
		model.addAttribute("newsBoardDetail", newsBoardDetail);
		
		System.out.println("no : " + no);
		System.out.println("newsBoardDetail :" + newsBoardDetail);
//		
//		MemberDTO member = newsBoardService.selectWriterUser(no);
//		
//		model.addAttribute("member", member);
//		
//		System.out.println("member :" + member);
		
//		if(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo() > 10000) {
//			return "/board/manager-newsBoardDetail";
//		}else {
//			return "/board/user-newsBoardDetail";
//		}
		return "/board/newsBoardDetail";
	}
	

	
	@GetMapping("/newsBoard-regist") 
	public void registNewsBoard() {}
	
	@PostMapping("/newsBoard-regist")
	public String registNewsBoard(@ModelAttribute NewsBoardDTO newsBoardDTO, HttpServletRequest  request, 
			RedirectAttributes rttr, @RequestParam MultipartFile file) {
		
		int MemberNo =((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo();
		String MemberName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		
		newsBoardDTO.setUserNo(MemberNo);
		newsBoardDTO.setUserName(MemberName);
		newsBoardDTO.setFinalName(MemberName);
		
		System.out.println("memberNo : " + MemberNo);
		System.out.println("memberName : " + MemberName);
		
		System.out.println("newsBoardDTO : " + newsBoardDTO);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		System.out.println("root : " + root);
		
		String filePath = root + "\\uploadFiles";
		
		File mkdir = new File(filePath);
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		if(!file.isEmpty()) {
		// 파일명 변경 처리
			String originFileName = file.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String saveName = UUID.randomUUID().toString().replace("-","") + ext;
		

			AttachmentDTO fileInfo = new AttachmentDTO();
			try {
			
				fileInfo.setBoardNo(newsBoardDTO.getBoardNo());
				fileInfo.setOriginName(originFileName);
				fileInfo.setSaveName(saveName);
				fileInfo.setSavePath(filePath);
				fileInfo.setFileType(ext);
			
				file.transferTo(new File(filePath + "\\" + fileInfo.getSaveName()));
		
			} catch(Exception e) {
				e.printStackTrace();
				
				new File(filePath + "\\" + fileInfo.getSaveName()).delete();
			}
			
			newsBoardDTO.setAttachmentDTO(fileInfo);
		}
			
		newsBoardService.registNewsBoard(newsBoardDTO);
		
		rttr.addFlashAttribute("message", "글등록이 완료되었습니다. ");

		
		return "redirect:/board/list";
	}
	
	

	@GetMapping("/newsBoard-update")
	public String modifyNewsBoard(@RequestParam int boardNo, Model model) {
		
		NewsBoardDTO newsBoardDetail = newsBoardService.selectNewsBoardDetail(boardNo);
		
		model.addAttribute("newsBoardDetail", newsBoardDetail);;
		
		System.out.println("newsBoardDetail : " + newsBoardDetail);
		
		return "/board/newsBoard-update";
	}
	
	@PostMapping("/newsBoard-update")
	public String modifyNewsBoard(@ModelAttribute NewsBoardDTO newsBoardDTO, RedirectAttributes rttr, HttpServletRequest request,
			@RequestParam MultipartFile file, @RequestParam(value = "fileSaveName", required = false)String fileSaveName
			, @RequestParam(value = "preFile", required = false)String preFile) {
		
		
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		File mkdir = new File(filePath);
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		if(!file.isEmpty()) {
			
			String originFileName = file.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String saveName = UUID.randomUUID().toString().replace("-","") + ext;
			
			AttachmentDTO fileInfo = new AttachmentDTO();
			try {
				
				fileInfo.setBoardNo(newsBoardDTO.getBoardNo());
				fileInfo.setOriginName(originFileName);
				fileInfo.setSaveName(saveName);
				fileInfo.setSavePath(filePath);
				fileInfo.setFileType(ext);
			
				file.transferTo(new File(filePath + "\\" + fileInfo.getSaveName()));
			
			} catch(Exception e) {
				e.printStackTrace();
				
				new File(filePath + "\\" + fileInfo.getSaveName()).delete();
			}
			
			newsBoardDTO.setAttachmentDTO(fileInfo);
		}
		
		if(!fileSaveName.isEmpty() && !file.isEmpty() || !fileSaveName.isEmpty() && file.isEmpty()) {
			//파일이 있는데 새로 다른파일을 업로드할때, 파일이 있는데 새로운 파일을 업로드 안할때 file.delete를 해줘야하겠지..
				
			File oldFile = new File(filePath + "\\" + fileSaveName);
			if(oldFile.isFile()) {
				oldFile.delete();
			}
		}
		
		newsBoardService.modifyNewsBoard(newsBoardDTO, preFile);
		
		rttr.addFlashAttribute("message", "글수정이 완료되었습니다. ");
		
		return "redirect:/board/list";
	}
		
	@GetMapping("/delete")
	public String removeNewsBoard(@RequestParam int boardNo, RedirectAttributes rttr, HttpServletRequest request) {
		
		NewsBoardDTO newsBoardDTO = newsBoardService.selectNewsBoardDetail(boardNo);
		
		newsBoardService.removeNewsBoard(boardNo);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		File file = new File(filePath + "\\" + newsBoardDTO.getAttachmentDTO().getSaveName());
		if(file.isFile()) {
			file.delete();
		}
		
		rttr.addFlashAttribute("message", "게시글이 삭제 되었습니다.");
		
		return "redirect:/board/list";
	}

	

	
	
}
