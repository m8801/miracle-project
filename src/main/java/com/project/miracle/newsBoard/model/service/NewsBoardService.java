package com.project.miracle.newsBoard.model.service;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.newsBoard.model.dto.NewsBoardDTO;

public interface NewsBoardService {

	List<NewsBoardDTO> selectAllNewsBoardList(SelectCriteria selectCriteria);

	NewsBoardDTO selectNewsBoardDetail(int no);

	MemberDTO selectWriterUser(int no);

	void registNewsBoard(NewsBoardDTO newBoardDTO);

	void modifyNewsBoard(NewsBoardDTO newsBoardDTO, String preFile);

	void removeNewsBoard(int boardNo);

	int selectTotalCount();
	
}
