package com.project.miracle.newsBoard.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.newsBoard.model.dao.NewsBoardMapper;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;
import com.project.miracle.newsBoard.model.dto.NewsBoardDTO;

@Service
public class NewsBoardServiceImpl implements NewsBoardService{

	public NewsBoardMapper mapper;
	
	@Autowired
	public NewsBoardServiceImpl(NewsBoardMapper mapper) {
		this.mapper = mapper;
	}
	
	/**
	 * <pre>
	 * 	새소식 게시판 전체 목록 조회
	 * </pre>
	 */
	@Override
	public List<NewsBoardDTO> selectAllNewsBoardList(SelectCriteria selectCriteria) {
		
		List<NewsBoardDTO> newsBoardList = mapper.selectAllNewsBoardList(selectCriteria);
		
		return newsBoardList;
	}
	

	/**
	 * <pre>
	 * 	새소식 게시판 상세보기
	 * </pre>
	 * @param no 새소식게시글 번호
	 */
	@Override
	public NewsBoardDTO selectNewsBoardDetail(int no) {

		NewsBoardDTO newsBoardDetail = null;
		
		newsBoardDetail = mapper.selectNewsBoardDetail(no);
		
		return newsBoardDetail;
	}

	@Override
	public MemberDTO selectWriterUser(int no) {
		
		MemberDTO member = mapper.selectWriterUser(no); 
		
		return member;
	}

	/**
	 * <pre>
	 * 	새소식 게시글 등록용 메소드
	 * </pre>
	 * @param newBoardDTO 게시글 데이터 
	 */
	@Override
	public void registNewsBoard(NewsBoardDTO newBoardDTO) {
		
		
		int result = mapper.insertNewsBoard(newBoardDTO);
		
		if(result > 0 && newBoardDTO.getAttachmentDTO() != null) {
			
			int attachmentResult = mapper.insertAttachment(newBoardDTO.getAttachmentDTO());
		}
		
	}


	/**
	 * <pre>
	 * 	새소식 게시글 수정용 메소드 
	 * </pre>
	 */
	@Override
	public void modifyNewsBoard(NewsBoardDTO newsBoardDTO, String preFile) {
		
		NewsBoardDTO selectResult = mapper.selectNewsBoardDetail(newsBoardDTO.getBoardNo());
		
		newsBoardDTO.setBoardNo(selectResult.getBoardNo());
		
		int modeifyResult = mapper.updateNewsBoard(newsBoardDTO);
		
		int attachmentResult = 0;
		
		if(modeifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && preFile != null) {
			newsBoardDTO = mapper.selectNewsBoardDetail(selectResult.getBoardNo());
		}else if(modeifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() == null && newsBoardDTO.getAttachmentDTO() != null) {
			attachmentResult = mapper.insertAttach(newsBoardDTO.getAttachmentDTO());
		}else if(modeifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && newsBoardDTO.getAttachmentDTO() == null) {
			attachmentResult = mapper.removeAttachment(newsBoardDTO.getBoardNo());
		}else if(modeifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && newsBoardDTO.getAttachmentDTO() != null) {
			attachmentResult = mapper.updateAttachment(newsBoardDTO.getAttachmentDTO());
		}
		
//		if(modeifyResult > 0) {
//			
//			attachmentResult = mapper.updateAttachment(newsBoardDTO.getAttachmentDTO());
//		}
		
	}

	/**
	 * <pre>
	 * 	게시글 삭제용 메소드 
	 * </pre>
	 */
	@Override
	public void removeNewsBoard(int boardNo) {
		
		int attachResult = mapper.removeAttachment(boardNo);
		
		int result = mapper.removeNotice(boardNo);
	}

	/**
	 * <pre>
	 * 전체 게시글수 조회용 메소드
	 * </pre>
	 */
	@Override
	public int selectTotalCount() {
		
		return mapper.selectTotalCount();
	}

	


	
	
	
	
	
	
	
	
	
}
