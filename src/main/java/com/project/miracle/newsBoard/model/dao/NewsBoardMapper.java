package com.project.miracle.newsBoard.model.dao;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;
import com.project.miracle.newsBoard.model.dto.NewsBoardDTO;

public interface NewsBoardMapper {

	List<NewsBoardDTO> selectAllNewsBoardList(SelectCriteria selectCriteria);

	NewsBoardDTO selectNewsBoardDetail(int no);

	MemberDTO selectWriterUser(int no);

	int insertNewsBoard(NewsBoardDTO newBoardDTO);

	int insertAttachment(AttachmentDTO attachmentDTO);

	int updateNewsBoard(NewsBoardDTO newsBoardDTO);

	int updateAttachment(AttachmentDTO attachmentDTO);

	int removeNotice(int boardNo);

	int selectTotalCount();

	int removeAttachment(int boardNo);

	int insertAttach(AttachmentDTO attachmentDTO); 

	
}
