package com.project.miracle.freeBoard.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.freeBoard.model.service.FreeBoardService;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;


@Controller
@RequestMapping("/board")
public class FreeBoardController {
	
	private FreeBoardService freeBoardService;
	
	@Autowired
	public FreeBoardController(FreeBoardService freeBoardService) {
		this.freeBoardService = freeBoardService;
	}
	

	
	
	/**
	 * <pre>
	 *   자유게시판 메인화면
	 * </pre>
	 * @param request
	 * @param m
	 * @return
	 */
	@GetMapping("/manager-FreeBoard-Main")
	public ModelAndView FreeBoardMain(@RequestParam(defaultValue = "1")int currentPage,@ModelAttribute SelectCriteria searchCriteria,
	        HttpServletRequest  request,Model m, ModelAndView mv) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		
		// 데이터베이스 에서 전체 게시물조회
		int totalCount = freeBoardService.selectTotalCount(searchMap);
		
		System.out.println("totalBoardCount : " + totalCount);

		// 한 페이지에 보여줄 게시물 수 
		int limit = 10;
		// 보여징 페이징 버튼 갯수
		int buttonAmount = 5;
		// 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환 
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : "+ selectCriteria);
		
		List<FreeBoardDTO> freeboardPaging = freeBoardService.freeBoardPaging(selectCriteria);
		System.out.println("freeboardPaging : " + freeboardPaging);
		mv.addObject("freeboardlist" , freeboardPaging);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("/board/manager-FreeBoard-Main"); 
		
		return mv;
		
	}
	
	
	
	
	/**
	 * <pre>
	 *   자유게시판 게시글 등록
	 * </pre>
	 * 
	 */
	@GetMapping("/manager-FreeBoard-Regist")
	public void FreeRegist(@RequestParam(value="files",required = false) MultipartFile files) {
		System.out.println("여기는 get임 : " + files);
	}
	
	@PostMapping("/manager-Freeboard-Regist")
	public String FreeBoardRegistAndFile(@RequestParam(value="files",required = false) MultipartFile files,RedirectAttributes rttr,HttpServletRequest request, Model m,
			@RequestParam(value="check2", required = false) String check2, @RequestParam String title, @RequestParam String content) {
		
		int userNo =((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo();
		String userName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();

		FreeBoardDTO freeBoardDTO = new FreeBoardDTO();
		freeBoardDTO.setUserNo(userNo);
		freeBoardDTO.setUserName(userName);
		freeBoardDTO.setFinalName(userName);
		
		freeBoardDTO.setTitle(title);
		freeBoardDTO.setContent(content);
		
		System.out.println("넘어온 익명체크 ? check : " + check2);
		
		if(check2 != null) {
			freeBoardDTO.setAnoYN("Y");
			freeBoardDTO.setUserName("익명");
			freeBoardDTO.setFinalName("익명");
		} else {
			freeBoardDTO.setAnoYN("N");
			freeBoardDTO.setUserName(userName);
			freeBoardDTO.setFinalName(userName);
		}
		
		System.out.println("넘어온 title : " + title);
		System.out.println("넘어온 content : " + content);
		System.out.println("컨트롤러 확인중 : " + freeBoardDTO);
		
		System.out.println(userNo);	
		System.out.println(userName);	
		
		String root = request.getSession().getServletContext().getRealPath("resources");	
		String filePath = root + "\\uploadFiles" + "\\freeBoardFiles";	
		File mkdir = new File(filePath);													
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		AttachmentDTO fileExt = null;
		System.out.println("files : " + files);
		
		if(files != null) {
			
			String originFileName = files.getOriginalFilename();
			System.out.println("추가한 originFileName: " + originFileName);
			
			if(originFileName.equals("")) {
				System.out.println("파일을 추가 안하고 없로드합니다.");
	
					
				fileExt = new AttachmentDTO(); 
				fileExt.setOriginName(originFileName);
				
				freeBoardDTO.setAttachmentDTO(fileExt);
				
				System.out.println("NoFiles DTO : " + freeBoardDTO);
				//files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));

				
			} else {
			
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String savedName = UUID.randomUUID().toString().replace("-", "")+ ext;
	
				fileExt = new AttachmentDTO(); 
				
				try {
					
					fileExt.setOriginName(originFileName);
					fileExt.setSaveName(savedName);
					fileExt.setSavePath(filePath);
					fileExt.setFileType(ext);
					
					freeBoardDTO.setAttachmentDTO(fileExt);
					
					files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));
					
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
					
				}
			}
		} 
		
		
		System.out.println("freeBoardDTO 확인 : " + freeBoardDTO);
		
		freeBoardService.freeboardregist(freeBoardDTO);
		
		rttr.addFlashAttribute("message", "글등록이 완료되었습니다. ");
	
		
		return "redirect:/board/manager-FreeBoard-Main";
	}
	
	
	
	/**
	 * <pre>
	 *   자유게시판 게시글 상세보기
	 * </pre>
	 * @param no
	 * @param request
	 * @param m
	 * @return
	 */
	@GetMapping("/manager-FreeBoard-Detail")
	public String freeBoardDetailMain(@RequestParam int no,HttpServletRequest request, Model m) {

		FreeBoardDTO freeDetail = freeBoardService.freeboarddetailservice(no);

		m.addAttribute("freeDetile", freeDetail);
		
		// 댓글 조회 
		List<ReplyDTO> replyList = freeBoardService.selectAllReplyList(no);
		m.addAttribute("replyList", replyList);
		
		return "/board/manager-FreeBoard-Detail";
	}
	
	
	
	
	/**
	 * <pre>
	 *   자유게시판 게시글 detail 가져오기 
	 * </pre>
	 * @param boardNo
	 * @param m
	 * @return
	 */
	@GetMapping("/manager-FreeBoard-UpdateList")
	public String freeBoardUpdateList(@RequestParam int boardNo,Model m,HttpServletRequest request) {

		FreeBoardDTO freeDetile = freeBoardService.freeboarddetailservice(boardNo);
		
		m.addAttribute("freeDetile",freeDetile);
		
		
		return "/board/manager-FreeBoard-Update";
	}
	

	
	 /**
	  * <pre>
	  *   자유게시판 게시글 수정하기
	  * </pre>
	 * @param files
	 * @param rttr
	 * @param boardNo
	 * @param request
	 * @param title
	 * @param content
	 * @return0
	 */
	@PostMapping("/manager-FreeBoard-Update") 
	 public String freeBoardUpdate(@RequestParam MultipartFile files,RedirectAttributes rttr,@RequestParam int boardNo,
			    HttpServletRequest request, @RequestParam String title, @RequestParam String content,
		        @RequestParam(value="originFile", required = false) String originFile ) {
		 
		System.out.println("여기는 Post 입니다 : " + originFile);
		System.out.println("files 확인 : " + files);
		
		FreeBoardDTO freeBoardDTO = new FreeBoardDTO();
		freeBoardDTO.setTitle(title);
		freeBoardDTO.setContent(content);
		freeBoardDTO.setBoardNo(boardNo);
		freeBoardDTO.setOriginFile(originFile);
		
	 	String root = request.getSession().getServletContext().getRealPath("resources");	
		String filePath = root + "\\uploadFiles" + "\\freeBoardFiles";	
		File mkdir = new File(filePath);													
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		AttachmentDTO fileExt = null;
		
		if(files != null) {
			
			String originFileName = files.getOriginalFilename();
			
			if(originFileName.equals("")) {
				
				try {
					
					System.out.println("파일이 없습니다~ : " + originFileName);
					fileExt = new AttachmentDTO();	
					
					fileExt.setBoardNo(boardNo);				// 게시판 번호 가져오고,
//					fileExt.setOriginName(originFileName);	// "" 빈값
//					fileExt.setSavePath(filePath);				// 파일 경로,
					
					freeBoardDTO.setAttachmentDTO(fileExt);
					
					System.out.println("null값 DTO 확인중 : " + freeBoardDTO);
					
				}catch (Exception e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
				}
				
			} else {
				
				System.out.println("orgin : " + originFileName);
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String savedName = UUID.randomUUID().toString().replace("-", "")+ ext;

				fileExt = new AttachmentDTO(); 
				
				try {
					
					fileExt.setBoardNo(boardNo);
					fileExt.setOriginName(originFileName);
					fileExt.setSaveName(savedName);
					fileExt.setSavePath(filePath);
					fileExt.setFileType(ext);
					
					freeBoardDTO.setAttachmentDTO(fileExt);
					
					files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));
					
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
					
				}
			}
			
		} 
			
			System.out.println("freeBoardDTO 마지막 확인 : " + freeBoardDTO);
			
			freeBoardService.freeBoatdModify(freeBoardDTO);
			
			rttr.addFlashAttribute("message", "게시글 수정 완료. ");
			
			return "redirect:/board/manager-FreeBoard-Main";
	 }
	
	
	
	/**
	 * <pre>
	 *   자유게시판 게시글 삭제
	 * </pre>
	 * @param boardNo
	 * @param m
	 * @param rttr
	 * @return
	 */
	@GetMapping("/manager-FreeBoard-DeleteOne")
	public String freeBoardDelete(@RequestParam("boardNo")int boardNo, Model m,RedirectAttributes rttr) {
		System.out.println("딜리트 boardNo : " + boardNo);
		
		freeBoardService.freeBoardDeleteOne(boardNo);
		
		rttr.addFlashAttribute("message","게시글 삭제완료");
		
		return "redirect:/board/manager-FreeBoard-Main";
	}
	
	
	
	
	/**
	 * <pre>
	 *   자유게시판 댓글 작성 
	 * </pre>
	 * @param registReply
	 * @param request
	 * @return
	 */
	@PostMapping(value="/manager-FreeBoard-Reply", produces = "application/json; charset=utf-8")
	@ResponseBody
	public List<ReplyDTO> registReply(@ModelAttribute ReplyDTO registReply, HttpServletRequest request
			,@RequestParam int refBoardNo, @RequestParam String body) {
		
		registReply.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());
		registReply.setBoardNo(refBoardNo);
		registReply.setComments(body);

		
		List<ReplyDTO> replyList = freeBoardService.registReply(registReply);

		System.out.println("마지막 replyList : " + replyList);
		
		return replyList;
	}
	
	
	
	/**
	 * <pre>
	 *   자유게시판 manager 댓글 삭제 
	 * </pre>
	 * @param removeReply
	 * @param request
	 * @return
	 */
	@PostMapping(value="/manager-FreeBoard-RemoveReply", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String removeReply(@ModelAttribute ReplyDTO removeReply
			, HttpServletRequest request) {
		
		int result = freeBoardService.removeReply(removeReply);
		
	    String message = null;
	    
	    if(result > 0) {
	    	message = "success";
	    } else {
	    	message = "fail";
	    }

		return message;
	}
	

	/**
	 * <pre>
	 *  manager Free 게시글 강제삭제
	 * </pre>
	 * @param boardNo
	 * @param m
	 * @param rttr
	 * @return
	 */
	@GetMapping("/manager-FreeBoard-DeleteAll")
	public String questionsboardDeleteAll(@RequestParam("boardNo")int boardNo, Model m,RedirectAttributes rttr) {
		
		freeBoardService.qeustionsDeleteAll(boardNo);
		System.out.println("강제삭제 번호 확인중입니다." + boardNo);
		
		rttr.addFlashAttribute("message","게시글을 강제삭제 했습니다.");
		return "redirect:/board/manager-FreeBoard-Main";
	}
	
}


















