package com.project.miracle.freeBoard.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.freeBoard.model.service.FreeBoardService;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;

@Controller
@RequestMapping("/board")
public class FreeBoardUserController {
	
	private FreeBoardService freeBoardService;
	
	@Autowired
	public FreeBoardUserController(FreeBoardService freeBoardService) {
		this.freeBoardService = freeBoardService;
	}
	
	/**
	 * <pre>
	 *   자유게시판 user 게시판 메인
	 * </pre>
	 * 
	 */
	@GetMapping("/user-FreeBoard-Main")
	public ModelAndView userFreeBoard(@RequestParam(defaultValue = "1")int currentPage,@ModelAttribute SelectCriteria searchCriteria,
	        HttpServletRequest  request,Model m, ModelAndView mv) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		// 데이터베이스 에서 전체 게시물조회
		int totalCount = freeBoardService.selectTotalUserCount(searchMap);
		
		System.out.println("totalBoardCount : " + totalCount);
		
		// 검색 조건이 있는 경우 검색 조건에 맞는 전체 게시물 수를 조회
		
		// 한 페이지에 보여줄 게시물 수 
		int limit = 10;
		
		// 보여징 페이징 버튼 갯수
		int buttonAmount = 5;
		
		// 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환 
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : "+ selectCriteria);
		
		List<FreeBoardDTO> freeboardPaging = freeBoardService.freeBoardUserPaging(selectCriteria);
		
		System.out.println("freeboardPaging : " + freeboardPaging);
		mv.addObject("userfreeboardlist" , freeboardPaging);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("/board/user-FreeBoard-Main"); 
	
		
		return mv;
		
	}
	
	
	/**
	 * <pre>
	 *   자유게시판 user 게시글 추가 
	 * </pre>
	 * 
	 */
	@GetMapping("/user-FreeBoard-Regist")
	public void userFreeBoardRegist() {}
	
	@PostMapping("/user-FreeBoard-Regist")
	public String userFreeBoardRegistFile(@RequestParam(value="files",required = false) MultipartFile files,RedirectAttributes rttr,HttpServletRequest request, Model m,
			@RequestParam(value="check2", required = false) String check2, @RequestParam String title, @RequestParam String content) {
		int userNo =((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo();
		String userName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();

		FreeBoardDTO freeBoardDTO = new FreeBoardDTO();
		freeBoardDTO.setUserNo(userNo);
		freeBoardDTO.setUserName(userName);
		freeBoardDTO.setFinalName(userName);
		
		freeBoardDTO.setTitle(title);
		freeBoardDTO.setContent(content);
		
		System.out.println("넘어온 check : " + check2);
		
		if(check2 != null) {
			freeBoardDTO.setAnoYN("Y");
			freeBoardDTO.setUserName("익명");
			freeBoardDTO.setFinalName("익명");
		} else {
			freeBoardDTO.setAnoYN("N");
			freeBoardDTO.setUserName(userName);
			freeBoardDTO.setFinalName(userName);
		}
		
		System.out.println("넘어온 title : " + title);
		System.out.println("넘어온 content : " + content);
		System.out.println("컨트롤러 확인중 : " + freeBoardDTO);
		
		System.out.println(userNo);	
		System.out.println(userName);	
		
		String root = request.getSession().getServletContext().getRealPath("resources");	
		String filePath = root + "\\uploadFiles" + "\\freeBoardFiles";	
		File mkdir = new File(filePath);													
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		AttachmentDTO fileExt = null;
		System.out.println("files : " + files);
		if(files != null) {
			
			String originFileName = files.getOriginalFilename();
			System.out.println("추가한 originFileName: " + originFileName);
			
			if(originFileName.equals("")) {
				System.out.println("파일을 추가 안하고 없로드합니다.");
	
					
				fileExt = new AttachmentDTO(); 
				fileExt.setOriginName(originFileName);
				
				freeBoardDTO.setAttachmentDTO(fileExt);
				
				System.out.println("NoFiles DTO : " + freeBoardDTO);
				//files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));

				
			} else {
			
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String savedName = UUID.randomUUID().toString().replace("-", "")+ ext;
	
				fileExt = new AttachmentDTO(); 
				
				try {
					
					fileExt.setOriginName(originFileName);
					fileExt.setSaveName(savedName);
					fileExt.setSavePath(filePath);
					fileExt.setFileType(ext);
					
					freeBoardDTO.setAttachmentDTO(fileExt);
					
					files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));
					
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
					
				}
			}
		} 
		
		
		System.out.println("freeBoardDTO 확인 : " + freeBoardDTO);
		
		freeBoardService.freeboardUserRegist(freeBoardDTO);
		
		rttr.addFlashAttribute("message", "글등록이 완료되었습니다. ");
	
		
		return "redirect:/board/user-FreeBoard-Main";
	}
	
	
	
	/**
	 * <pre>
	 *   자유게시판 user 게시글 상세보기
	 * </pre>
	 * @param no
	 * @param request
	 * @param m
	 * @return
	 */
	@GetMapping("/user-FreeBoard-Detail")
	public String freeBoardUserDetailMain(@RequestParam int no,HttpServletRequest request, Model m) {

		FreeBoardDTO freeDetail = freeBoardService.freeboardUserDetail(no);

		m.addAttribute("freeDetile", freeDetail);
		
		// 댓글 조회 
		List<ReplyDTO> replyList = freeBoardService.selectAllReplyList(no);
		m.addAttribute("replyList", replyList);
		
		return "/board/user-FreeBoard-Detail";
	}
	
	
	
	/**
	 * <pre>
	 *   자유게시판 Update - Detail 정보 가져오기 
	 * </pre>
	 * 
	 */
	@GetMapping("/user-FreeBoard-UpdateList")
	public String userFreeBoardUpdateList(@RequestParam int boardNo,Model m,HttpServletRequest request) {

		FreeBoardDTO freeDetile = freeBoardService.freeboardUserDetail(boardNo);
		
		m.addAttribute("freeDetile",freeDetile);
		
		
		return "/board/user-FreeBoard-Update";
	}
	
	
	/**
	 * <pre>
	 *   자유게시판 Update 수정하기
	 * </pre>
	 * 
	 */
	@PostMapping("/user-FreeBoard-UpdateAll")
	public String userFreeBoardUpdate(@RequestParam MultipartFile files,RedirectAttributes rttr,@RequestParam int boardNo,
		    HttpServletRequest request, @RequestParam String title, @RequestParam String content,
	        @RequestParam(value="originFile", required = false) String originFile ) {
	 
		System.out.println("여기는 Post 입니다 : " + originFile);
		System.out.println("files 확인 : " + files);
		
		FreeBoardDTO freeBoardDTO = new FreeBoardDTO();
		freeBoardDTO.setTitle(title);
		freeBoardDTO.setContent(content);
		freeBoardDTO.setBoardNo(boardNo);
		freeBoardDTO.setOriginFile(originFile);
		
	 	String root = request.getSession().getServletContext().getRealPath("resources");	
		String filePath = root + "\\uploadFiles" + "\\freeBoardFiles";	
		File mkdir = new File(filePath);													
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		AttachmentDTO fileExt = null;
		
		if(files != null) {
			
			String originFileName = files.getOriginalFilename();
			
			if(originFileName.equals("")) {
				
				try {
					
					System.out.println("파일이 없습니다~ : " + originFileName);
					fileExt = new AttachmentDTO();	
					
					fileExt.setBoardNo(boardNo);				// 게시판 번호 가져오고,
//					fileExt.setOriginName(originFileName);	// "" 빈값
//					fileExt.setSavePath(filePath);				// 파일 경로,
					
					freeBoardDTO.setAttachmentDTO(fileExt);
					
					System.out.println("null값 DTO 확인중 : " + freeBoardDTO);
					
				}catch (Exception e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
				}
				
			} else {
				
				System.out.println("orgin : " + originFileName);
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String savedName = UUID.randomUUID().toString().replace("-", "")+ ext;

				fileExt = new AttachmentDTO(); 
				
				try {
					
					fileExt.setBoardNo(boardNo);
					fileExt.setOriginName(originFileName);
					fileExt.setSaveName(savedName);
					fileExt.setSavePath(filePath);
					fileExt.setFileType(ext);
					
					freeBoardDTO.setAttachmentDTO(fileExt);
					
					files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));
					
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
					
				}
			}
			
		} 
		
		System.out.println("freeBoardDTO 마지막 확인 : " + freeBoardDTO);
		
		freeBoardService.userFreeBoatdModify(freeBoardDTO);
		
		rttr.addFlashAttribute("message", "게시글 수정 완료. ");
		
		return "redirect:/board/user-FreeBoard-Main";
	}
	
	
	
	@GetMapping("/user-FreeBoard-DeleteOne")
	public String userFreeBoardDelete(@RequestParam("boardNo")int boardNo, Model m,RedirectAttributes rttr) {

		freeBoardService.freeBoardUserDelete(boardNo);
		
		rttr.addFlashAttribute("message","게시글 삭제완료");
		
		return "redirect:/board/user-FreeBoard-Main";
	}
	
	
	
	
	
	
	
}

















