package com.project.miracle.freeBoard.model.dao;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;

public interface FreeBoardMapper {

	// 자유게시판 메인 화면
	List<FreeBoardDTO> freeBoardListAll();
	
	// 자유게시판 페이징처리 
	int selectTotalCount(Map<String, String> searchMap);

	// 자유게시판 게시글 추가
	int freeBoardRegist(FreeBoardDTO freeBoardDTO);

	// 자유게시판 게시글 추가 & 이미지파일
	int freeBoardRegistAttr(AttachmentDTO attachmentDTO);

	List<FreeBoardDTO> selectBoardPagList(SelectCriteria selectCriteria);
	// 자유게시판 게시글 상세보기
	FreeBoardDTO freeBoardDetailAll(int no);
	
	// 자유게시판 게시글 수정 
	FreeBoardDTO freeBoardUpdateDetail(int boardNo);

	int freeBoardUpdateList(FreeBoardDTO freeBoardDTO);
	// 자유게시판 manager 게시글 수정하기
	int freeBoardModifyList(FreeBoardDTO freeBoardDTO);
	
//	// 게시글 수정 파일
//	int freeFileModify(AttachmentDTO attachmentDTO);
	
	int managerFileRemove(int boardNo);
	
	int managerfreeFileInsert(AttachmentDTO attachmentDTO);
//
//	void managerfreeFileModify(AttachmentDTO attachmentDTO);
//
//	void managernoFileModify(AttachmentDTO attachmentDTO);
	
	
	

	int freeBoardDeleteOne(int boardNo);
	
	// 자유게시판 manager 게시물삭제 ( 댓글먼저 )
	int freeBoardManagerDeltext(int boardNo);
	// 자유게시판 manager 게시물삭제 ( 사진먼저 )
	int freeBoardManagerDelPng(int boardNo);
	// 자유게시판 manager 게시물 삭제 최종
	int freeBoardDel(int boardNo);
	// 파일 없이 수정
	int noFileModify(AttachmentDTO attachmentDTO);
	// 파일 있게 수정
	int freeFileInsert(AttachmentDTO attachmentDTO);
	
	// 자유게시판 댓글
	int insertReply(ReplyDTO registReply);
	// 자유게시판 댓글 조회하기
	List<ReplyDTO> selectReplyList(int replayNo);
	// 자유게시판 댓글 지우기
	int deleteReply(int no);
	
	/* 자유게시판 user 게시글 추가 */
	int freeBoardUserRegist(FreeBoardDTO freeBoardDTO);
	void freeBoardUserRegistAttr(AttachmentDTO attachmentDTO);
	// 자유게시판 user Main
	List<FreeBoardDTO> freeBoardUserListAll(SelectCriteria selectCriteria);
	// 자유게시판 user 게시글 총 갯수
	int selectTotalUserCount(Map<String, String> searchMap);
	// 자유게시판 user 상세보기 디테일
	FreeBoardDTO freeBoardUserDetailAll(int no);
	// 자유게시판 user 수정시 디테일 정보 가져오기
	FreeBoardDTO freeBoardDetailUpdate(int boardNo);
	// 자유게시판 user 게시글 삭제 
	int freeBoardUserDelete(int boardNo);
	// 자유게시판 게시글삭제 ( 댓글삭제 )
	int freeBoardDeltext(int boardNo);
	// 자유게시판 게시글삭제 ( 사진삭제 )
	int freeBoardDelPng(int boardNo);
	
	// 자유게시판 user 업데이트메인
	int userfreeBoardModifyList(FreeBoardDTO freeBoardDTO);



	

	





	






}
