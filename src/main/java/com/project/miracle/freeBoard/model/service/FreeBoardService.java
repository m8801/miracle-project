package com.project.miracle.freeBoard.model.service;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;

public interface FreeBoardService {
	
	// 자유게시판 게시글 메인 전체 보기
	List<FreeBoardDTO> freeBoardAllList();
	// 자유게시판 manager 게시글 메인 전체목록 조회
	int selectTotalCount(Map<String, String> searchMap);
	// 자유게시판 manger 게시글 메인 페이징 처리
	List<FreeBoardDTO> freeBoardPaging(SelectCriteria selectCriteria);
	// 자유게시판 manager 게시글 추가 
	void freeboardregist(FreeBoardDTO freeBoardDTO);
	// 자유게시판 manager 상세보기 
	FreeBoardDTO freeboarddetailservice(int no);
	// 자유게시판 manager 수정클릭시 detail 가져오기.
	FreeBoardDTO freeBoardUpdateList(int boardNo);
	// 자유게시판 manager 수정하기 
	void freeBoatdModify(FreeBoardDTO freeBoardDTO);
	// 자유게시판 manager 게시글 삭제하기
	void freeBoardDeleteOne(int boardNo);
	// 자유게시판 manager 게시글 강제삭제
	void qeustionsDeleteAll(int boardNo);
	
	// 자유게시판 댓글 
	List<ReplyDTO> registReply(ReplyDTO registReply);

	List<ReplyDTO> selectAllReplyList(int no);

	int removeReply(ReplyDTO removeReply);

	/* 자유게시판 user 쪽  */
	void freeboardUserRegist(FreeBoardDTO freeBoardDTO);
	// 자유게시판 user 게시글 메인 페이징 처리
	List<FreeBoardDTO> freeBoardUserPaging(SelectCriteria selectCriteria);
	// 자유게시판 user 게시글 전체목록 조회
	int selectTotalUserCount(Map<String, String> searchMap);
	// 자유게시판 user 상세보기 
	FreeBoardDTO freeboardUserDetail(int no);
	// 자유게시판 user 수정클릭시 detail 가져오기.
	FreeBoardDTO freeboardDetailUpdate(int boardNo);
	// 자유게시판 user 수정하기 
	void userFreeBoatdModify(FreeBoardDTO freeBoardDTO);
	// 자유게시판 user 게시글 삭제 
	void freeBoardUserDelete(int boardNo);








	

}
