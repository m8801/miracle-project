package com.project.miracle.freeBoard.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dao.FreeBoardMapper;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;

@Service
public class FreeBoardServiceImpl implements FreeBoardService{
	
	private FreeBoardMapper mapper;
	
	@Autowired
	public void FreeBoardService(FreeBoardMapper mapper) {
		this.mapper = mapper;
	}

	/**
	 * <pre>
	 *   자유게시판 전체 리스트
	 * </pre>
	 *
	 */
	@Override
	public List<FreeBoardDTO> freeBoardAllList() {
		
		return mapper.freeBoardListAll();
	}

	/**
	 * <pre>
	 *   자유게시판 manager  게시글 추가 
	 * </pre>
	 *
	 */
	@Override
	public void freeboardregist(FreeBoardDTO freeBoardDTO) {
		
		int result = mapper.freeBoardRegist(freeBoardDTO);
		
		if(result > 0 && !(freeBoardDTO.getAttachmentDTO().getOriginName().equals("")) ) {
			System.out.println("사진 있고 게시글 추가");
			mapper.freeBoardRegistAttr(freeBoardDTO.getAttachmentDTO());
			
		} else {
			System.out.println("사진 없이 게시글 추가 ");
		}
		
	}


	/**
	 * <pre>
	 *   게시글 전체 manager 갯수 조회용 메소드
	 * </pre>
	 *
	 */
	@Override
	public int selectTotalCount(Map<String, String> searchMap) {

		return mapper.selectTotalCount(searchMap);
	}

	
	
	@Override
	public List<FreeBoardDTO> freeBoardPaging(SelectCriteria selectCriteria) {
	
		return mapper.selectBoardPagList(selectCriteria);
	}

	/**
	 * <pre>
	 *   자유게시판 manager 게시글 상세보기
	 * </pre>
	 *
	 */
	@Override
	public FreeBoardDTO freeboarddetailservice(int no) {
		
		FreeBoardDTO freeDetail = null;
		freeDetail = mapper.freeBoardDetailAll(no);
		
		return freeDetail;
	}

	
	
	/**
	 * <pre>
	 *   자유게시판 manager Update Datail 가져오기
	 * </pre>
	 *
	 */
	@Override
	public FreeBoardDTO freeBoardUpdateList(int boardNo) {
		
		return mapper.freeBoardUpdateDetail(boardNo);
	}


	
	
	 
	/**
	 * <pre>
	 *   자유게시판 manager Update Post 수정
	 * </pre>
	 *
	 */
	@Override
	public void freeBoatdModify(FreeBoardDTO freeBoardDTO) {
		
		int result = mapper.freeBoardModifyList(freeBoardDTO);
		
		System.out.println("서비스 result : " + result );
		
		FreeBoardDTO freeDetail = mapper.freeBoardDetailAll(freeBoardDTO.getBoardNo());
		System.out.println("원래기존파일이름 freeDetail : " + freeDetail.getAttachmentDTO().getOriginName());
		
		/*
		 수정하기 정리
		1. 기존파일 있을때
		 - 기존파일 유지하고 제목 & 내용이 바뀐다
		 - 새로운파일이오고 제목&내용이 그대로다
		 - 수정하기 했지만 그대로 그냥 저장한다

		2. 기존파일이 없을때
		 - 새로운파일을 추가한다. 제목 & 내용은 그대로다
		 - 새로운파일을 추가한다. 제목 & 내용도 바뀐다
		 */
		
		// 기존에있던 제목 내용 파일 이름이 있고, 새로운 파일이 들어오면  이면 
		if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && freeBoardDTO.getAttachmentDTO().getOriginName() != null) {
			
			System.out.println("기존에 파일이 있네요. 기존파일을 지우고 새로운 파일을 추가합니다. ");
			// 기존에 있던 파일 삭제
			mapper.freeBoardDelPng(freeBoardDTO.getBoardNo());
			// 새로운 파일 추가
			mapper.freeFileInsert(freeBoardDTO.getAttachmentDTO());

			
		// 기존에 있던 파일 이름이 없고, 새로운 파일이 들어오면 
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && freeBoardDTO.getAttachmentDTO().getOriginName() != null){
			
			System.out.println("기존에 파일이 없었네요. 새로운 파일을 추가합니다.");
			// 새로운 파일 추가
			mapper.freeFileInsert(freeBoardDTO.getAttachmentDTO());
			
		// 기존에에있던 파일 이 있지만, 새로운 파일이 들어오지 않는다. 
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && freeBoardDTO.getAttachmentDTO().getOriginName() == null) {
			
			System.out.println("기존에 파일이 있었지만 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요.");
			mapper.userfreeBoardModifyList(freeBoardDTO);
			
		// 기존에파일도 없고, 새로운 파일도 없다. 	
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && freeBoardDTO.getAttachmentDTO().getOriginName() == null) {
			
			System.out.println("파일이 아무것도 없는데 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요. ");
			mapper.userfreeBoardModifyList(freeBoardDTO);
		}

		
		
	}

	
	
	/**
	 * <pre>
	 *   자유게시판 manager 게시글 삭제  
	 * </pre>
	 *
	 */
	@Override
	public void freeBoardDeleteOne(int boardNo) {

		int resultText = mapper.freeBoardManagerDeltext(boardNo);
		
		if(resultText>0) {
			System.out.println("댓글 삭제 성공!");
			 
		} 

		int resultPng = mapper.freeBoardManagerDelPng(boardNo);
		
		if(resultPng>0) {
			System.out.println("사진 삭제 성공!");
			 
		} 
		
		int result = mapper.freeBoardDel(boardNo);

		if(result>0) {
			System.out.println("게시물 삭제 성공!");
			 
		} 
		
		
	}

	
	
	/**
	 * <pre>
	 *   자유게시판 manager  댓글 등록용 메소드 
	 * </pre>
	 *
	 */
	@Override
	public List<ReplyDTO> registReply(ReplyDTO registReply) {
		
		List<ReplyDTO> replyList =null;
		
		// 댓글등록 
		int result = mapper.insertReply(registReply);
		
		if(result > 0) {
			System.out.println("서비스쪽 registReply : " + registReply);
			System.out.println("서비스쪽 result : " + result);
			
			// 댓글 등록이 성공하면 리스트 조회 
			replyList = mapper.selectReplyList(registReply.getBoardNo());
			
			System.out.println("서비스쪽 replyList : " + replyList);
			
		} else {
			System.out.println("댓글 등록 실패 ");
		}
		
		return replyList;
	}

	/**
	 * <pre>
	 *   detail 에서 댓글 목록 조회
	 * </pre>
	 *
	 */
	@Override
	public List<ReplyDTO> selectAllReplyList(int no) {
		
		List<ReplyDTO> replyList = null;
		
		replyList = mapper.selectReplyList(no);
		
		return replyList;
	}

	
	@Override
	public int removeReply(ReplyDTO removeReply) {
		
		List<ReplyDTO> replyList = null;
		System.out.println(removeReply);
		
		int result = mapper.deleteReply(removeReply.getNo());
		
		if(result > 0) {
			
			replyList = mapper.selectReplyList(removeReply.getBoardNo());
			
		} 
		
		return result;

	}

	/**
	 * <pre>
	 *   자유게시판 user 게시글 추가 
	 * </pre>
	 *
	 */
	@Override
	public void freeboardUserRegist(FreeBoardDTO freeBoardDTO) {
	
		int result = mapper.freeBoardUserRegist(freeBoardDTO);
		
		if(result > 0 && !(freeBoardDTO.getAttachmentDTO().getOriginName().equals("")) ) {
			System.out.println("사진 있고 게시글 추가");
			mapper.freeBoardUserRegistAttr(freeBoardDTO.getAttachmentDTO());
			
		} else if(freeBoardDTO.getAttachmentDTO().getOriginName().equals("")){
			System.out.println("사진 없이 게시글 추가 ");
		} else {
			System.out.println("응 둘다아님.");
		}
		
	}

	/**
	 * <pre>
	 *   자유게시판 user 페이징처리 Main
	 * </pre>
	 *
	 */
	@Override
	public List<FreeBoardDTO> freeBoardUserPaging(SelectCriteria selectCriteria) {
	
		return mapper.freeBoardUserListAll(selectCriteria);
	}

	@Override
	public int selectTotalUserCount(Map<String, String> searchMap) {
		
		return mapper.selectTotalUserCount(searchMap);
	}

	@Override
	public FreeBoardDTO freeboardUserDetail(int no) {
		
		FreeBoardDTO freeDetail = null;
		freeDetail = mapper.freeBoardUserDetailAll(no);
		
		return freeDetail;
	}

	@Override
	public FreeBoardDTO freeboardDetailUpdate(int boardNo) {
		
		return mapper.freeBoardDetailUpdate(boardNo);
	}

	/**
	 * <pre>
	 *   자유게시판 user 수정하기
	 * </pre>
	 *
	 */
	@Override
	public void userFreeBoatdModify(FreeBoardDTO freeBoardDTO) {
		
			int result = mapper.userfreeBoardModifyList(freeBoardDTO);
			
			System.out.println("서비스 result : " + result );
			
			FreeBoardDTO freeDetail = mapper.freeBoardDetailAll(freeBoardDTO.getBoardNo());
			System.out.println("원래기존파일이름 freeDetail : " + freeDetail.getAttachmentDTO().getOriginName());
			
			/*
			 수정하기 정리
			1. 기존파일 있을때
			 - 기존파일 유지하고 제목 & 내용이 바뀐다
			 - 새로운파일이오고 제목&내용이 그대로다
			 - 수정하기 했지만 그대로 그냥 저장한다

			2. 기존파일이 없을때
			 - 새로운파일을 추가한다. 제목 & 내용은 그대로다
			 - 새로운파일을 추가한다. 제목 & 내용도 바뀐다
			 */
			
			// 기존에있던 제목 내용 파일 이름이 있고, 새로운 파일이 들어오면  이면 
			if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && freeBoardDTO.getAttachmentDTO().getOriginName() != null) {
				
				System.out.println("기존에 파일이 있네요. 기존파일을 지우고 새로운 파일을 추가합니다. ");
				// 기존에 있던 파일 삭제
				mapper.freeBoardDelPng(freeBoardDTO.getBoardNo());
				// 새로운 파일 추가
				mapper.freeFileInsert(freeBoardDTO.getAttachmentDTO());

				
			// 기존에 있던 파일 이름이 없고, 새로운 파일이 들어오면 
			} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && freeBoardDTO.getAttachmentDTO().getOriginName() != null){
				
				System.out.println("기존에 파일이 없었네요. 새로운 파일을 추가합니다.");
				// 새로운 파일 추가
				mapper.freeFileInsert(freeBoardDTO.getAttachmentDTO());
				
			// 기존에에있던 파일 이 있지만, 새로운 파일이 들어오지 않는다. 
			} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && freeBoardDTO.getAttachmentDTO().getOriginName() == null) {
				
				System.out.println("기존에 파일이 있었지만 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요.");
				mapper.userfreeBoardModifyList(freeBoardDTO);
				
			// 기존에파일도 없고, 새로운 파일도 없다. 	
			} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && freeBoardDTO.getAttachmentDTO().getOriginName() == null) {
				
				System.out.println("파일이 아무것도 없는데 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요. ");
				mapper.userfreeBoardModifyList(freeBoardDTO);
			}
			
				
	}


	/**
	 * <pre>
	 *   자유게시판 user 게시글  삭제
	 * </pre>
	 *
	 */
	@Override
	public void freeBoardUserDelete(int boardNo) {
		

		int resultText = mapper.freeBoardDeltext(boardNo);
		
		if(resultText>0) {
			System.out.println("댓글 삭제 성공!");
			 
		} 

		int resultPng = mapper.freeBoardDelPng(boardNo);
		
		if(resultPng>0) {
			System.out.println("사진 삭제 성공!");
			 
		} 
		
		int result = mapper.freeBoardUserDelete(boardNo);

		if(result>0) {
			System.out.println("게시물 삭제 성공!");
			 
		} 
		
	}

	@Override
	public void qeustionsDeleteAll(int boardNo) {
		int resultText = mapper.freeBoardManagerDeltext(boardNo);
		
		if(resultText>0) {
			System.out.println("댓글 삭제 성공!");
			 
		} 

		int resultPng = mapper.freeBoardManagerDelPng(boardNo);
		
		if(resultPng>0) {
			System.out.println("사진 삭제 성공!");
			 
		} 
		
		int result = mapper.freeBoardDel(boardNo);

		if(result>0) {
			System.out.println("게시물 삭제 성공!");
			 
		} 
		
	}





	
	
}







