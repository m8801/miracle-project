package com.project.miracle.freeBoard.model.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.project.miracle.common.model.dto.MemberDTO;

public class ReplyDTO {
	

	private int no;					
	private int boardNo;			
	private String comments;
	private int userNo;			
	private String delYN;		
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM,dd", timezone = "Asia/Seoul")
	private Date date;			
	private MemberDTO member;
	
	public ReplyDTO() {}

	public ReplyDTO(int no, int boardNo, String comments, int userNo, String delYN, Date date, MemberDTO member) {
		super();
		this.no = no;
		this.boardNo = boardNo;
		this.comments = comments;
		this.userNo = userNo;
		this.delYN = delYN;
		this.date = date;
		this.member = member;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public MemberDTO getMember() {
		return member;
	}

	public void setMember(MemberDTO member) {
		this.member = member;
	}

	@Override
	public String toString() {
		return "ReplyDTO [no=" + no + ", boardNo=" + boardNo + ", comments=" + comments + ", userNo=" + userNo
				+ ", delYN=" + delYN + ", date=" + date + ", member=" + member + "]";
	}

	
	
}
