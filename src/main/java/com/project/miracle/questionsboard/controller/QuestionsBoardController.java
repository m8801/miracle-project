package com.project.miracle.questionsboard.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;
import com.project.miracle.questionsboard.model.dto.QuestionsBoardDTO;
import com.project.miracle.questionsboard.model.service.QuestionsBoardService;

@Controller
@RequestMapping("/board")
public class QuestionsBoardController {
	
	private QuestionsBoardService questionsBoardService;
	
	@Autowired
	public QuestionsBoardController(QuestionsBoardService questionsBoardService) {
		this.questionsBoardService = questionsBoardService;
	}
	
	

	@GetMapping("/manager-QuestionsBoard-Main")
	public ModelAndView QuestionsBoardMain(@RequestParam(defaultValue = "1")int currentPage,@ModelAttribute SelectCriteria searchCriteria,
	        HttpServletRequest  request,Model m, ModelAndView mv) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		// 데이터베이스 에서 전체 게시물조회
		int totalCount = questionsBoardService.questionsBoardselectTotalCount(searchMap);
		
		System.out.println("totalBoardCount : " + totalCount);
		
		// 검색 조건이 있는 경우 검색 조건에 맞는 전체 게시물 수를 조회
		
		// 한 페이지에 보여줄 게시물 수 
		int limit = 10;
		
		// 보여징 페이징 버튼 갯수
		int buttonAmount = 5;
		
		// 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환 
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : "+ selectCriteria);
		
		List<QuestionsBoardDTO> questionsPaging = questionsBoardService.questionsBoardPaging(selectCriteria);
		
		System.out.println("questionsPaging : " + questionsPaging);
		mv.addObject("questionsBoardlist" , questionsPaging);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("/board/manager-QuestionsBoard-Main"); 
		// --> paging 처리 
		
		return mv;
	}
	

	
	/**
	 * <pre>
	 *   질문게시판 게시글 등록
	 * </pre>
	 * 
	 */
	@GetMapping("/manager-QuestionsBoard-Regist")
	public void QuestionsBoardRegist(@RequestParam(value="files",required = false) MultipartFile files) {
		System.out.println("여기는 get임 : " + files);
	}
	
	@PostMapping("/manager-QuestionsBoard-Regist")
	public String QuestionsRegistAndFile(@RequestParam(value="files",required = false) MultipartFile files,RedirectAttributes rttr,HttpServletRequest request, Model m,
			@RequestParam(value="check2", required = false) String check2, @RequestParam String title, @RequestParam String content) {
		
		int userNo =((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo();
		String userName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();

		QuestionsBoardDTO questionsDTO = new QuestionsBoardDTO();
		questionsDTO.setUserNo(userNo);
		questionsDTO.setUserName(userName);
		questionsDTO.setFinalName(userName);
		
		questionsDTO.setTitle(title);
		questionsDTO.setContent(content);
		
		System.out.println("넘어온 익명체크 ? check : " + check2);
		
		if(check2 != null) {
			questionsDTO.setAnoYN("Y");
			questionsDTO.setUserName("익명");
			questionsDTO.setFinalName("익명");
		} else {
			questionsDTO.setAnoYN("N");
			questionsDTO.setUserName(userName);
			questionsDTO.setFinalName(userName);
		}
		
		System.out.println("넘어온 title : " + title);
		System.out.println("넘어온 content : " + content);
		System.out.println("컨트롤러 확인중 : " + questionsDTO);
		
		System.out.println(userNo);	
		System.out.println(userName);	
		
		String root = request.getSession().getServletContext().getRealPath("resources");	
		String filePath = root + "\\uploadFiles" + "\\questionsBoardFiles";	
		File mkdir = new File(filePath);													
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		AttachmentDTO fileExt = null;
		System.out.println("files : " + files);
		
		if(files != null) {
			
			String originFileName = files.getOriginalFilename();
			System.out.println("추가한 originFileName: " + originFileName);
			
			if(originFileName.equals("")) {
				System.out.println("파일을 추가 안하고 없로드합니다.");
	
					
				fileExt = new AttachmentDTO(); 
				fileExt.setOriginName(originFileName);
				
				questionsDTO.setAttachmentDTO(fileExt);
				
				System.out.println("NoFiles DTO : " + questionsDTO);

				
			} else {
			
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String savedName = UUID.randomUUID().toString().replace("-", "")+ ext;
	
				fileExt = new AttachmentDTO(); 
				
				try {
					
					fileExt.setOriginName(originFileName);
					fileExt.setSaveName(savedName);
					fileExt.setSavePath(filePath);
					fileExt.setFileType(ext);
					
					questionsDTO.setAttachmentDTO(fileExt);
					
					files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));
					
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
					
				}
			}
		} 
		
		
		System.out.println("questionsDTO 확인 : " + questionsDTO);
		
		questionsBoardService.questionBoardRegist(questionsDTO);
		
		rttr.addFlashAttribute("message", "글등록이 완료되었습니다. ");
	
		
		return "redirect:/board/manager-QuestionsBoard-Main";
	}
	
	
	
	
	/**
	 * <pre>
	 *   질문게시판 게시글 상세보기
	 * </pre>
	 * @param no
	 * @param request
	 * @param m
	 * @return
	 */
	@GetMapping("/manager-QuestionsBoard-Detail")
	public String managerQuestionsBoardDetial(@RequestParam int no,HttpServletRequest request, Model m) {

		QuestionsBoardDTO questionsDTO = questionsBoardService.questionsBoardDeatil(no);

		m.addAttribute("freeDetile", questionsDTO);
		
		// 댓글 조회 
		List<ReplyDTO> replyList = questionsBoardService.questionsBoardDeatilReply(no);
		m.addAttribute("replyList", replyList);
		
		return "/board/manager-QuestionsBoard-Detail";
	}
	
	
	
	/**
	 * <pre>
	 *   질문게시판 게시글 수정시 상세보기 가져오기
	 * </pre>
	 * @param boardNo
	 * @param m
	 * @param request
	 * @return
	 */
	@GetMapping("/manager-QuestionsBoard-UpdateList")
	public String questionsBoardUpdateDetail(@RequestParam int boardNo,Model m,HttpServletRequest request) {

		QuestionsBoardDTO questionDetail = questionsBoardService.questionsBoardDeatil(boardNo);
		
		m.addAttribute("freeDetile",questionDetail);
		
		
		return "/board/manager-QuestionsBoard-Update";
	}
	
	

	
	/**
	 * <pre>
	 *   질문게시판 게시글 수정하기 Main
	 * </pre>
	 * @param files
	 * @param rttr
	 * @param boardNo
	 * @param request
	 * @param title
	 * @param content
	 * @param originFile
	 * @return
	 */
	@PostMapping("/manager-QuestionsBoard-Update")
	public String managerQuestionsBoardUpdate(@RequestParam MultipartFile files,RedirectAttributes rttr,@RequestParam int boardNo,
		    HttpServletRequest request, @RequestParam String title, @RequestParam String content,
	        @RequestParam(value="originFile", required = false) String originFile ) {
		
		System.out.println("여기는 Post 입니다 : " + originFile);
		System.out.println("files 확인 : " + files);
		
		QuestionsBoardDTO questionsDTO = new QuestionsBoardDTO();
		questionsDTO.setTitle(title);
		questionsDTO.setContent(content);
		questionsDTO.setBoardNo(boardNo);
		questionsDTO.setOriginFile(originFile);
		
	 	String root = request.getSession().getServletContext().getRealPath("resources");	
		String filePath = root + "\\uploadFiles" + "\\freeBoardFiles";	
		File mkdir = new File(filePath);													
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		AttachmentDTO fileExt = null;
		
		if(files != null) {
			
			String originFileName = files.getOriginalFilename();
			
			if(originFileName.equals("")) {
				
				try {
					
					System.out.println("파일이 없습니다~ : " + originFileName);
					fileExt = new AttachmentDTO();	
					
					fileExt.setBoardNo(boardNo);				// 게시판 번호 가져오고,
//					fileExt.setOriginName(originFileName);	// "" 빈값
//					fileExt.setSavePath(filePath);				// 파일 경로,
					
					questionsDTO.setAttachmentDTO(fileExt);
					
					System.out.println("null값 DTO 확인중 : " + questionsDTO);
					
				}catch (Exception e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
				}
				
			} else {
				
				System.out.println("orgin : " + originFileName);
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String savedName = UUID.randomUUID().toString().replace("-", "")+ ext;

				fileExt = new AttachmentDTO(); 
				
				try {
					
					fileExt.setBoardNo(boardNo);
					fileExt.setOriginName(originFileName);
					fileExt.setSaveName(savedName);
					fileExt.setSavePath(filePath);
					fileExt.setFileType(ext);
					
					questionsDTO.setAttachmentDTO(fileExt);
					
					files.transferTo(new File(filePath + "\\" + fileExt.getSaveName()));
					
				} catch (IllegalStateException | IOException e) {
					e.printStackTrace();
					new File(filePath + "\\" + fileExt.getSaveName()).delete();
					
				}
			}
			
		} 
			
			System.out.println("questionsDTO 마지막 확인 : " + questionsDTO);
			
			questionsBoardService.questionsModify(questionsDTO);
			
			rttr.addFlashAttribute("message", "게시글 수정 완료. ");
			
		return "redirect:/board/manager-QuestionsBoard-Main";
	}
	
	
	/**
	 * <pre>
	 *   질문게시판 게시글 삭제
	 * </pre>
	 * @param boardNo
	 * @param m
	 * @param rttr
	 * @return
	 */
	@GetMapping("/manager-QuestionsBoard-DeleteOne")
	public String questionsboardDelete(@RequestParam("boardNo")int boardNo, Model m,RedirectAttributes rttr) {
		System.out.println("딜리트 boardNo : " + boardNo);
		
		questionsBoardService.questionsDeleteOne(boardNo);
		
		rttr.addFlashAttribute("message","게시글 삭제완료");
		
		return "redirect:/board/manager-QuestionsBoard-Main";
	}
	
	/**
	 * <pre>
	 *  manager Questions 게시글 강제삭제
	 * </pre>
	 * @param boardNo
	 * @param m
	 * @param rttr
	 * @return
	 */
	@GetMapping("/manager-QuestionsBoard-DeleteAll")
	public String questionsboardDeleteAll(@RequestParam("boardNo")int boardNo, Model m,RedirectAttributes rttr) {
		
		questionsBoardService.qeustionsDeleteAll(boardNo);
		System.out.println("강제삭제 번호 확인중입니다." + boardNo);
		
		rttr.addFlashAttribute("message","게시글을 강제삭제 했습니다.");
		return "redirect:/board/manager-QuestionsBoard-Main";
	}
	
	
	/**
	 * <pre>
	 *   질문게시판 댓글 작성 
	 * </pre>
	 * @param registReply
	 * @param request
	 * @return
	 */
	@PostMapping(value="/manager-QuestionsBoard-Reply", produces = "application/json; charset=utf-8")
	@ResponseBody
	public List<ReplyDTO> registReply(@ModelAttribute ReplyDTO registReply, HttpServletRequest request
			,@RequestParam int refBoardNo, @RequestParam String body) {
		
		registReply.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());
		registReply.setBoardNo(refBoardNo);
		registReply.setComments(body);

		
		List<ReplyDTO> replyList = questionsBoardService.registReply(registReply);

		System.out.println("마지막 replyList : " + replyList);
		
		return replyList;
	}
	
	
	
	/**
	 * <pre>
	 *   자유게시판 manager 댓글 삭제 
	 * </pre>
	 * @param removeReply
	 * @param request
	 * @return
	 */
	@PostMapping(value="/manager-QuestionsBoard-RemoveReply", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String removeReply(@ModelAttribute ReplyDTO removeReply
			, HttpServletRequest request) {
		
		int result = questionsBoardService.removeReply(removeReply);
		
	    String message = null;
	    
	    if(result > 0) {
	    	message = "success";
	    } else {
	    	message = "fail";
	    }

		return message;
	}
	
	
	
	
	


}






















