package com.project.miracle.questionsboard.model.dto;

import java.sql.Date;

import com.project.miracle.newsBoard.model.dto.AttachmentDTO;

public class QuestionsBoardDTO {

	private Date date;
	private int boardNo;
	private String title;
	private String userName;
	private String delYn;
	private String anoYN;
	private String finalName;
	private int userNo;
	private String content;
	private String originFile;
	private AttachmentDTO attachmentDTO;
	
	public QuestionsBoardDTO() {}
	
	public QuestionsBoardDTO(Date date, int boardNo, String title, String userName, String delYn, String anoYN,
			String finalName, int userNo, String content, String originFile, AttachmentDTO attachmentDTO) {
		super();
		this.date = date;
		this.boardNo = boardNo;
		this.title = title;
		this.userName = userName;
		this.delYn = delYn;
		this.anoYN = anoYN;
		this.finalName = finalName;
		this.userNo = userNo;
		this.content = content;
		this.originFile = originFile;
		this.attachmentDTO = attachmentDTO;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getAnoYN() {
		return anoYN;
	}
	public void setAnoYN(String anoYN) {
		this.anoYN = anoYN;
	}
	public String getFinalName() {
		return finalName;
	}
	public void setFinalName(String finalName) {
		this.finalName = finalName;
	}
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getOriginFile() {
		return originFile;
	}
	public void setOriginFile(String originFile) {
		this.originFile = originFile;
	}
	public AttachmentDTO getAttachmentDTO() {
		return attachmentDTO;
	}
	public void setAttachmentDTO(AttachmentDTO attachmentDTO) {
		this.attachmentDTO = attachmentDTO;
	}
	@Override
	public String toString() {
		return "QuestionsBoardDTO [date=" + date + ", boardNo=" + boardNo + ", title=" + title + ", userName="
				+ userName + ", delYn=" + delYn + ", anoYN=" + anoYN + ", finalName=" + finalName + ", userNo=" + userNo
				+ ", content=" + content + ", originFile=" + originFile + ", attachmentDTO=" + attachmentDTO + "]";
	}
	
	
	
}
