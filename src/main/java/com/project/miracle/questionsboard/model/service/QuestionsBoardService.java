package com.project.miracle.questionsboard.model.service;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.questionsboard.model.dto.QuestionsBoardDTO;

public interface QuestionsBoardService {

	/* 질문게시판 manager */
	// 질문게시판 총 컬럼 갯수 
	int questionsBoardselectTotalCount(Map<String, String> searchMap);
	// 질문게시판 페이징
	List<QuestionsBoardDTO> questionsBoardPaging(SelectCriteria selectCriteria);
	// 질문게시판 게시글 추가시
	void questionBoardRegist(QuestionsBoardDTO questionsDTO);
	// 질문게시판 게시글 상세보기
	// 질문게시판 게시글 상세보기 + 댓글 조회하기
    //List<ReplyDTO> selectAllReplyList(int no);
	QuestionsBoardDTO questionsBoardDeatil(int no);
	// 질문게시판 게시글 수정하기 
	void questionsModify(QuestionsBoardDTO questionsDTO);
	// 질문게시판 게시글 삭제하기
	void questionsDeleteOne(int boardNo);
	// 질문게시판 댓글 달기
	List<ReplyDTO> registReply(ReplyDTO registReply);
	// 질문게시판 댓글 조회하기
	List<ReplyDTO> questionsBoardDeatilReply(int no);
	// 질문게시판 댓글 삭제하기
	int removeReply(ReplyDTO removeReply);
	
	/* 질문게시판 user */
	// 질문게시판 총 컬럼 갯수 
	int userQuestionsBoardCount(Map<String, String> searchMap);
	// 질문게시판 페이징
	List<QuestionsBoardDTO> userQuestionsBoardPaging(SelectCriteria selectCriteria);
	// 질문게시판 게시글 추가시
	void questionBoardUserRegist(QuestionsBoardDTO questionsDTO);
	// 질문게시판 게시글 상세보기
	QuestionsBoardDTO questionsUserBoardDeatil(int no);
	// 질문게시판 게시글 상세보기 + 댓글 조회하기
	List<ReplyDTO> questionsUserBoardDeatilReply(int no);
	// 질문게시판 게시글 수정하기 
	void questionsUserModify(QuestionsBoardDTO questionsDTO);
	// 질문게시판 게시글 삭제하기
	void questionsUserDeleteOne(int boardNo);
	// 질문게시판 댓글 달기
	List<ReplyDTO> registUserReply(ReplyDTO registReply);
	// 질문게시판 댓글 삭제하기
	int removeUserReply(ReplyDTO removeReply);
	// 질문게시판 댓글 강제삭제하기
	void qeustionsDeleteAll(int boardNo);
	

}





















