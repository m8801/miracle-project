package com.project.miracle.questionsboard.model.dao;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.newsBoard.model.dto.AttachmentDTO;
import com.project.miracle.questionsboard.model.dto.QuestionsBoardDTO;

public interface QuestionsBoardMapper {
	
	// 질문 게시판 Main페이징 처리 ( 총 컬럼 갯수 확인 )용 메소드 
	int questionsTotalCount(Map<String, String> searchMap);
	// 질문 게시판 페이징 처리 메소드 
	List<QuestionsBoardDTO> questionsBoardPaging(SelectCriteria selectCriteria);
	// 질문 게시판 게시글 추가 메소드 2개 
	int questionsRegist(QuestionsBoardDTO questionsDTO);
	void questionsRegistAttr(AttachmentDTO attachmentDTO);
	// 질문 게시판 게시글 상세보기
	QuestionsBoardDTO questionDetailAll(int no);
	// 질문게시판 수정하기 1
	int questionsBoardModify(QuestionsBoardDTO questionsDTO);
	// 질문게시판 수정하기 2 (insert)
	int questionsInsert(AttachmentDTO attachmentDTO);
	// 질문게시판 수정하기 3 (delete)
	int questionsDeletePng(int boardNo);
	// 질문게시판 게시글 삭제 1 
	int questionsManagerDeltext(int boardNo);
	// 질문게시판 게시글 삭제 2
	int questionsManagerDelPng(int boardNo);
	// 질문게시판 게시글 삭제 3
	int questionsDel(int boardNo);
	// 질문게시판 댓글 등록
	int insertReply(ReplyDTO registReply);
	// 질문게시판 댓글 등록이 성공하면 리스트 조회
	List<ReplyDTO> selectReplyList(int boardNo);
	// 질문게시판 댓글 삭제
	int deleteReply(int no);
	
	
	/* 질문게시판 user */
	
	// 질문 게시판 Main페이징 처리 ( 총 컬럼 갯수 확인 )용 메소드 
	int questionsMainCount(Map<String, String> searchMap);
	// 질문 게시판 페이징 처리 메소드 
	List<QuestionsBoardDTO> questionsBoardPagings(SelectCriteria selectCriteria);
	// 질문 게시판 게시글 추가 메소드 2개 
	int questionsUserRegist(QuestionsBoardDTO questionsDTO);
	void questionsRegistUserAttr(AttachmentDTO attachmentDTO);
	// 질문 게시판 게시글 상세보기
	QuestionsBoardDTO questionUserDetailAll(int no);
	// 질문게시판 수정하기 1
	int questionsUserBoardModify(QuestionsBoardDTO questionsDTO);
	// 질문게시판 수정하기 2 (insert)
	void questionsUserInsert(AttachmentDTO attachmentDTO);
	// 질문게시판 수정하기 3 (delete)
	void questionsUserDeletePng(int boardNo);
	// 질문게시판 게시글 삭제 1 
	int questionsUsersDeltext(int boardNo);
	// 질문게시판 게시글 삭제 2
	int questionsUserDelPng(int boardNo);
	// 질문게시판 게시글 삭제 3
	int questionsUserDel(int boardNo);
	// 질문게시판 댓글 등록
	int insertUserReply(ReplyDTO registReply);
	// 질문게시판 댓글 등록이 성공하면 리스트 조회
	List<ReplyDTO> selectReplyUserList(int boardNo);
	// 질문게시판 댓글 삭제
	int deleteUserReply(int no);

	
	
	
	
}
