package com.project.miracle.questionsboard.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.questionsboard.model.dao.QuestionsBoardMapper;
import com.project.miracle.questionsboard.model.dto.QuestionsBoardDTO;

@Service
public class QuestionsBoardServiceImpl implements QuestionsBoardService {
	
	private QuestionsBoardMapper mapper;
	
	@Autowired
	public void QuestionsBoardService(QuestionsBoardMapper mapper) {
		this.mapper = mapper;
	}
	

	/**
	 * <pre>
	 *   질문 게시판 user Main페이징 처리 ( 총 컬럼 갯수 확인 )용 메소드 
	 * </pre>
	 */
	@Override
	public int questionsBoardselectTotalCount(Map<String, String> searchMap) {
		
		return mapper.questionsTotalCount(searchMap);
	}

	/**
	 * <pre>
	 *   질문 게시판 user 페이징 처리 메소드 
	 * </pre>
	 *
	 */
	@Override
	public List<QuestionsBoardDTO> questionsBoardPaging(SelectCriteria selectCriteria) {
		
		return mapper.questionsBoardPaging(selectCriteria);
	}

	
	/**
	 * <pre>
	 *   자유게시판 user 게시글 추가시 메소드
	 * </pre>
	 *
	 */
	@Override
	public void questionBoardRegist(QuestionsBoardDTO questionsDTO) {
		
		int result = mapper.questionsRegist(questionsDTO);
		
		if(result > 0 && !(questionsDTO.getAttachmentDTO().getOriginName().equals("")) ) {
			System.out.println("사진 있고 게시글 추가");
			mapper.questionsRegistAttr(questionsDTO.getAttachmentDTO());
			
		} else if(questionsDTO.getAttachmentDTO().getOriginName().equals("")){
			System.out.println("사진 없이 게시글 추가 ");
		} else {
			System.out.println("응 둘다아님.");
		}
		
	}


	/**
	 * <pre>
	 *   질문게시판 user 게시글 상세보기 Detail
	 * </pre>
	 *
	 */
	@Override
	public QuestionsBoardDTO questionsBoardDeatil(int no) {
		
		QuestionsBoardDTO questionsDetail = null;
		questionsDetail = mapper.questionDetailAll(no);
		
		return questionsDetail;
	}


	/**
	 * <pre>
	 *  질문게시판 user Update 수정
	 * </pre>
	 *
	 */
	@Override
	public void questionsModify(QuestionsBoardDTO questionsDTO) {
		
	int result = mapper.questionsBoardModify(questionsDTO);
		
		System.out.println("서비스 result : " + result );
		
		QuestionsBoardDTO freeDetail = mapper.questionDetailAll(questionsDTO.getBoardNo());
		System.out.println("원래기존파일이름 freeDetail : " + freeDetail.getAttachmentDTO().getOriginName());
		
		/*
		 수정하기 정리
		1. 기존파일 있을때
		 - 기존파일 유지하고 제목 & 내용이 바뀐다
		 - 새로운파일이오고 제목&내용이 그대로다
		 - 수정하기 했지만 그대로 그냥 저장한다

		2. 기존파일이 없을때
		 - 새로운파일을 추가한다. 제목 & 내용은 그대로다
		 - 새로운파일을 추가한다. 제목 & 내용도 바뀐다
		 */
		
		// 기존에있던 제목 내용 파일 이름이 있고, 새로운 파일이 들어오면  이면 
		if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && questionsDTO.getAttachmentDTO().getOriginName() != null) {
			
			System.out.println("기존에 파일이 있네요. 기존파일을 지우고 새로운 파일을 추가합니다. ");
			// 기존에 있던 파일 삭제
			mapper.questionsDeletePng(questionsDTO.getBoardNo());
			// 새로운 파일 추가
			mapper.questionsInsert(questionsDTO.getAttachmentDTO());

			
		// 기존에 있던 파일 이름이 없고, 새로운 파일이 들어오면 
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && questionsDTO.getAttachmentDTO().getOriginName() != null){
			
			System.out.println("기존에 파일이 없었네요. 새로운 파일을 추가합니다.");
			// 새로운 파일 추가
			mapper.questionsInsert(questionsDTO.getAttachmentDTO());
			
		// 기존에에있던 파일 이 있지만, 새로운 파일이 들어오지 않는다. 
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && questionsDTO.getAttachmentDTO().getOriginName() == null) {
			
			System.out.println("기존에 파일이 있었지만 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요.");
			mapper.questionsBoardModify(questionsDTO);
			
		// 기존에파일도 없고, 새로운 파일도 없다. 	
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && questionsDTO.getAttachmentDTO().getOriginName() == null) {
			
			System.out.println("파일이 아무것도 없는데 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요. ");
			mapper.questionsBoardModify(questionsDTO);
		}

		
		
	}


	/**
	 * <pre>
	 *   질문게시판 user 게시글 삭제 
	 * </pre>
	 *
	 */
	@Override
	public void questionsDeleteOne(int boardNo) {
		
		int resultText = mapper.questionsManagerDeltext(boardNo);
		
		if(resultText>0) {
			System.out.println("댓글 삭제 성공!");
			 
		} 

		int resultPng = mapper.questionsManagerDelPng(boardNo);
		
		if(resultPng>0) {
			System.out.println("사진 삭제 성공!");
			 
		} 
		
		int result = mapper.questionsDel(boardNo);

		if(result>0) {
			System.out.println("게시물 삭제 성공!");
			 
		} 
		
	}


	/**
	 * <pre>
	 *   질문게시판 user 댓글 작성 
	 * </pre>
	 *
	 */
	@Override
	public List<ReplyDTO> registReply(ReplyDTO registReply) {
		List<ReplyDTO> replyList =null;
		
		// 댓글등록 
		int result = mapper.insertReply(registReply);
		
		if(result > 0) {
			System.out.println("서비스쪽 registReply : " + registReply);
			System.out.println("서비스쪽 result : " + result);
			
			// 댓글 등록이 성공하면 리스트 조회 
			replyList = mapper.selectReplyList(registReply.getBoardNo());
			
			System.out.println("서비스쪽 replyList : " + replyList);
			
		} else {
			System.out.println("댓글 등록 실패 ");
		}
		
		return replyList;
	}


	/**
	 * <pre>
	 *   질문게시판 user 댓글 전체조회
	 * </pre>
	 *
	 */
	@Override
	public List<ReplyDTO> questionsBoardDeatilReply(int no) {
		
		List<ReplyDTO> replyList = null;
		
		replyList = mapper.selectReplyList(no);
		
		return replyList;
	}


	@Override
	public int removeReply(ReplyDTO removeReply) {
		
		List<ReplyDTO> replyList = null;
		System.out.println(removeReply);
		
		int result = mapper.deleteReply(removeReply.getNo());
		
		if(result > 0) {
			
			replyList = mapper.selectReplyList(removeReply.getBoardNo());
			
		} 
		
		return result;
	}

	
/*  질문게시판 user  */
	
	/**
	 * <pre>
	 *   질문 게시판 Main페이징 처리 ( 총 컬럼 갯수 확인 )용 메소드 
	 * </pre>
	 */
	@Override
	public int userQuestionsBoardCount(Map<String, String> searchMap) {
		return mapper.questionsMainCount(searchMap);
	}

	/**
	 * <pre>
	 *   질문 게시판 페이징 처리 메소드 
	 * </pre>
	 *
	 */
	@Override
	public List<QuestionsBoardDTO> userQuestionsBoardPaging(SelectCriteria selectCriteria) {
		return mapper.questionsBoardPagings(selectCriteria);
	}


	
	/**
	 *<pre>
	 *  질문게시판 게시글 추가하기
	 *</pre>
	 *
	 */
	@Override
	public void questionBoardUserRegist(QuestionsBoardDTO questionsDTO) {
		
	int result = mapper.questionsUserRegist(questionsDTO);
		
		if(result > 0 && !(questionsDTO.getAttachmentDTO().getOriginName().equals("")) ) {
			System.out.println("사진 있고 게시글 추가");
			mapper.questionsRegistUserAttr(questionsDTO.getAttachmentDTO());
			
		} else if(questionsDTO.getAttachmentDTO().getOriginName().equals("")){
			System.out.println("사진 없이 게시글 추가 ");
		} else {
			System.out.println("응 둘다아님.");
		}
		
	}


	/**
	 *<pre>
	 *  질문게시판 상세보기
	 *</pre>
	 *
	 */
	@Override
	public QuestionsBoardDTO questionsUserBoardDeatil(int no) {
		
		QuestionsBoardDTO questionsUserDetail = null;
		questionsUserDetail = mapper.questionUserDetailAll(no);
		
		return questionsUserDetail;
	}


	@Override
	public void questionsUserModify(QuestionsBoardDTO questionsDTO) {
		int result = mapper.questionsUserBoardModify(questionsDTO);
		
		System.out.println("서비스 result : " + result );
		
		QuestionsBoardDTO freeDetail = mapper.questionUserDetailAll(questionsDTO.getBoardNo());
		System.out.println("원래기존파일이름 freeDetail : " + freeDetail.getAttachmentDTO().getOriginName());
		
		/*
		 수정하기 정리
		1. 기존파일 있을때
		 - 기존파일 유지하고 제목 & 내용이 바뀐다
		 - 새로운파일이오고 제목&내용이 그대로다
		 - 수정하기 했지만 그대로 그냥 저장한다

		2. 기존파일이 없을때
		 - 새로운파일을 추가한다. 제목 & 내용은 그대로다
		 - 새로운파일을 추가한다. 제목 & 내용도 바뀐다
		 */
		
		// 기존에있던 제목 내용 파일 이름이 있고, 새로운 파일이 들어오면  이면 
		if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && questionsDTO.getAttachmentDTO().getOriginName() != null) {
			
			System.out.println("기존에 파일이 있네요. 기존파일을 지우고 새로운 파일을 추가합니다. ");
			// 기존에 있던 파일 삭제
			mapper.questionsUserDeletePng(questionsDTO.getBoardNo());
			// 새로운 파일 추가
			mapper.questionsUserInsert(questionsDTO.getAttachmentDTO());

			
		// 기존에 있던 파일 이름이 없고, 새로운 파일이 들어오면 
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && questionsDTO.getAttachmentDTO().getOriginName() != null){
			
			System.out.println("기존에 파일이 없었네요. 새로운 파일을 추가합니다.");
			// 새로운 파일 추가
			mapper.questionsUserInsert(questionsDTO.getAttachmentDTO());
			
		// 기존에에있던 파일 이 있지만, 새로운 파일이 들어오지 않는다. 
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() != null && questionsDTO.getAttachmentDTO().getOriginName() == null) {
			
			System.out.println("기존에 파일이 있었지만 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요.");
			mapper.questionsUserBoardModify(questionsDTO);
			
		// 기존에파일도 없고, 새로운 파일도 없다. 	
		} else if(result > 0 && freeDetail.getAttachmentDTO().getOriginName() == null && questionsDTO.getAttachmentDTO().getOriginName() == null) {
			
			System.out.println("파일이 아무것도 없는데 제목이나 내용만 바뀌었습니다. & 안바뀌었을수도 있네요. ");
			mapper.questionsUserBoardModify(questionsDTO);
		}

	}


	@Override
	public void questionsUserDeleteOne(int boardNo) {
		
		int resultText = mapper.questionsUsersDeltext(boardNo);
		
		if(resultText>0) {
			System.out.println("댓글 삭제 성공!");
			 
		} 

		int resultPng = mapper.questionsUserDelPng(boardNo);
		
		if(resultPng>0) {
			System.out.println("사진 삭제 성공!");
			 
		} 
		
		int result = mapper.questionsUserDel(boardNo);

		if(result>0) {
			System.out.println("게시물 삭제 성공!");
			 
		} 
		
	}


	@Override
	public List<ReplyDTO> questionsUserBoardDeatilReply(int no) {
		
		List<ReplyDTO> replyList = null;
		
		replyList = mapper.selectReplyUserList(no);
		
		return replyList;
	}


	@Override
	public List<ReplyDTO> registUserReply(ReplyDTO registReply) {
		
		List<ReplyDTO> replyList =null;
		
		// 댓글등록 
		int result = mapper.insertUserReply(registReply);
		
		if(result > 0) {
			System.out.println("서비스쪽 registReply : " + registReply);
			System.out.println("서비스쪽 result : " + result);
			
			// 댓글 등록이 성공하면 리스트 조회 
			replyList = mapper.selectReplyUserList(registReply.getBoardNo());
			
			System.out.println("서비스쪽 replyList : " + replyList);
			
		} else {
			System.out.println("댓글 등록 실패 ");
		}
		
		return replyList;
	}


	@Override
	public int removeUserReply(ReplyDTO removeReply) {
		
		List<ReplyDTO> replyList = null;
		System.out.println(removeReply);
		
		int result = mapper.deleteUserReply(removeReply.getNo());
		
		if(result > 0) {
			
			replyList = mapper.selectReplyUserList(removeReply.getBoardNo());
			
		} 
		
		return result;
	}


	@Override
	public void qeustionsDeleteAll(int boardNo) {
		
		int resultText = mapper.questionsUsersDeltext(boardNo);
		
		if(resultText>0) {
			System.out.println("댓글 삭제 성공!");
			 
		} 

		int resultPng = mapper.questionsUserDelPng(boardNo);
		
		if(resultPng>0) {
			System.out.println("사진 삭제 성공!");
			 
		} 
		
		int result = mapper.questionsUserDel(boardNo);

		if(result>0) {
			System.out.println("게시물 삭제 성공!");
			 
		} 
		
	}


}














