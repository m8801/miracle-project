package com.project.miracle.usermanagement.model.service;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.usermanagement.model.dto.SelectUserCriteria;
import com.project.miracle.usermanagement.model.dto.registUserDTO;

public interface UserManagementService {

	List<LoginDTO> selectAllUserInfoList();

//	List<LoginDTO> selectUserInfo(LoginDTO searchUser);

//	List<LoginDTO> selectUserInfo();

//	List<LoginDTO> selectUserInfo(SelectUser selectUser);

	List<LoginDTO> selectUserInfo(Map<String, String> searchMap);

	void registUser(registUserDTO member);

	void registManager(registUserDTO member);

	LoginDTO userSelectOne(int userNo);

	void modifyUser(LoginDTO member);

//	int deleteUser(String userNo);

	List<LoginDTO> deleteUserList();

	void deleteUser(Integer no);

	int selectTotalCount(Map<String, String> searchMap);

	List<LoginDTO> selectUserList(SelectUserCriteria selectUserCriteria);



}
