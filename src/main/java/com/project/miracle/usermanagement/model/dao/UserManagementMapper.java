package com.project.miracle.usermanagement.model.dao;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.usermanagement.model.dto.SelectUserCriteria;
import com.project.miracle.usermanagement.model.dto.registUserDTO;

public interface UserManagementMapper {

	List<LoginDTO> selectAllUserInfoList();

	List<LoginDTO> selectUserInfo();

	int insertUser(registUserDTO member);

	int insertManager(registUserDTO member);

	int insertUserProfile(registUserDTO member);

	int insertManagerProfile(registUserDTO member);

	LoginDTO userSelectOne(int userNo);

	int updateUser(LoginDTO member);

	List<LoginDTO> deleteUserList();

	int deleteUser(Integer no);

	int selectTotalCount(Map<String, String> searchMap);

	List<LoginDTO> selectUserList(SelectUserCriteria selectUserCriteria);



//	int deleteUser(String userNo);



}
