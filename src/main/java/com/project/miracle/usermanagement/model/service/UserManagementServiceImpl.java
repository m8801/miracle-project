package com.project.miracle.usermanagement.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.address.exception.AddressDeleteException;
import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.usermanagement.model.dao.UserManagementMapper;
import com.project.miracle.usermanagement.model.dto.SelectUserCriteria;
import com.project.miracle.usermanagement.model.dto.registUserDTO;

@Service
public class UserManagementServiceImpl implements UserManagementService {

	private final UserManagementMapper mapper;
	
	@Autowired
	public UserManagementServiceImpl(UserManagementMapper mapper) {
		
		this.mapper = mapper;
	}

//	@Override
//	public List<LoginDTO> selectUserInfo(SelectUser selectUser) {
//		return mapper.selectUserInfo();
//	}
	
	@Override
	public List<LoginDTO> selectAllUserInfoList() {
		
		return mapper.selectAllUserInfoList();
	}

	@Override
	public List<LoginDTO> selectUserInfo(Map<String, String> searchMap) {
		return mapper.selectUserInfo();
	}

	@Override
	public void registUser(registUserDTO member) {
		
		int result1 = mapper.insertUser(member);
		int result2 = mapper.insertUserProfile(member);
		
		if(!(result1 > 0) || !(result2 > 0)) {
			return;
		}
		
	}

	@Override
	public void registManager(registUserDTO member) {

		int result1 = mapper.insertManager(member);
		int result2 = mapper.insertManagerProfile(member);
		
		if(!(result1 > 0) || !(result2 > 0)) {
			return;
		}
		
	}

	@Override
	public LoginDTO userSelectOne(int userNo) {
		
		return mapper.userSelectOne(userNo);
	}

	@Override
	public void modifyUser(LoginDTO member) {
		
		int result = mapper.updateUser(member);
		
		if(!(result > 0)) {
			return;
		}
		
	}

//	@Override
//	public int deleteUser(String userNo) {
//		
//		int result = mapper.deleteUser(userNo);
//		
//		if(!(result > 0)) {
//			return 0;
//		}
//		
//		return result;
//	}

	@Override
	public List<LoginDTO> deleteUserList() {
		return mapper.deleteUserList();
	}

//	@Override
//	public void deleteUser(String userNo) {
//		int result = mapper.deleteUser(userNo);
//		
//		if(!(result > 0)) {
//			return;
//		}
//	}

	@Override
	public void deleteUser(Integer no) {
		int result = mapper.deleteUser(no);
		
		if(!(result > 0)) {
			return;
		}	
		
	}

	@Override
	public int selectTotalCount(Map<String, String> searchMap) {

		return mapper.selectTotalCount(searchMap);
	}

	@Override
	public List<LoginDTO> selectUserList(SelectUserCriteria selectUserCriteria) {

		return mapper.selectUserList(selectUserCriteria);
	}

//	@Override
//	public void deleteUser(String[] deleteNoArr) {
//		
//		int result = 0;
//		
//		for (int i = 0; i < deleteNoArr.length; i++) {
//			result += mapper.deleteUser(deleteNoArr[i]);
//		}
//		if (!(result > 0)) {
//			return;
//		}
//	
//		
//	}


//	@Override
//	public List<LoginDTO> selectUserInfo(LoginDTO searchUser) {
//		return mapper.selectUserInfo();
//	}

//	@Override
//	public List<LoginDTO> selectUserInfo() {
//		return mapper.selectUserInfo();
//	}

	

}
