package com.project.miracle.usermanagement.model.dto;

public class SelectUserDTO {

	private int userNo;
	private String userName;
	private int age;
	private String birthDate;
	private String phone;
	private String email;
	private String deptName;
	private String jobName;
	private String userYN;
	private String control;
	
	public SelectUserDTO() {}

	public SelectUserDTO(int userNo, String userName, int age, String birthDate, String phone, String email,
			String deptName, String jobName, String userYN, String control) {
		super();
		this.userNo = userNo;
		this.userName = userName;
		this.age = age;
		this.birthDate = birthDate;
		this.phone = phone;
		this.email = email;
		this.deptName = deptName;
		this.jobName = jobName;
		this.userYN = userYN;
		this.control = control;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getUserYN() {
		return userYN;
	}

	public void setUserYN(String userYN) {
		this.userYN = userYN;
	}

	public String getControl() {
		return control;
	}

	public void setControl(String control) {
		this.control = control;
	}

	@Override
	public String toString() {
		return "SelectUserDTO [userNo=" + userNo + ", userName=" + userName + ", age=" + age + ", birthDate="
				+ birthDate + ", phone=" + phone + ", email=" + email + ", deptName=" + deptName + ", jobName="
				+ jobName + ", userYN=" + userYN + ", control=" + control + "]";
	}

	
}
