package com.project.miracle.usermanagement.model.dto;

import java.sql.Date;

import org.springframework.web.multipart.MultipartFile;

public class registUserDTO {

	private int userNo;
	private String userPwd;
	private String userName;
	private int age;
	private String gender;
	private String deptCode;
	private String phone;
	private String email;
	private Date birthDate;
	private String jobCode;
	private String userYn;
	private String control;
	private String originalFile;
	private String updateFile;
	private String filePath;
	private Date createDate;
	private MultipartFile uploadFile;
	
	public registUserDTO() {}

	public registUserDTO(int userNo, String userPwd, String userName, int age, String gender, String deptCode,
			String phone, String email, Date birthDate, String jobCode, String userYn, String control,
			String originalFile, String updateFile, String filePath, Date createDate, MultipartFile uploadFile) {
		super();
		this.userNo = userNo;
		this.userPwd = userPwd;
		this.userName = userName;
		this.age = age;
		this.gender = gender;
		this.deptCode = deptCode;
		this.phone = phone;
		this.email = email;
		this.birthDate = birthDate;
		this.jobCode = jobCode;
		this.userYn = userYn;
		this.control = control;
		this.originalFile = originalFile;
		this.updateFile = updateFile;
		this.filePath = filePath;
		this.createDate = createDate;
		this.uploadFile = uploadFile;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getUserYn() {
		return userYn;
	}

	public void setUserYn(String userYn) {
		this.userYn = userYn;
	}

	public String getControl() {
		return control;
	}

	public void setControl(String control) {
		this.control = control;
	}

	public String getOriginalFile() {
		return originalFile;
	}

	public void setOriginalFile(String originalFile) {
		this.originalFile = originalFile;
	}

	public String getUpdateFile() {
		return updateFile;
	}

	public void setUpdateFile(String updateFile) {
		this.updateFile = updateFile;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public MultipartFile getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(MultipartFile uploadFile) {
		this.uploadFile = uploadFile;
	}

	@Override
	public String toString() {
		return "registUserDTO [userNo=" + userNo + ", userPwd=" + userPwd + ", userName=" + userName + ", age=" + age
				+ ", gender=" + gender + ", deptCode=" + deptCode + ", phone=" + phone + ", email=" + email
				+ ", birthDate=" + birthDate + ", jobCode=" + jobCode + ", userYn=" + userYn + ", control=" + control
				+ ", originalFile=" + originalFile + ", updateFile=" + updateFile + ", filePath=" + filePath
				+ ", createDate=" + createDate + ", uploadFile=" + uploadFile + "]";
	}
	
}
