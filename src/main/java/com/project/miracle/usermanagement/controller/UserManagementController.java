package com.project.miracle.usermanagement.controller;

import java.io.File;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.usermanagement.model.dto.SelectUserCriteria;
import com.project.miracle.usermanagement.model.dto.registUserDTO;
import com.project.miracle.usermanagement.model.service.UserManagementService;

@Controller
@RequestMapping("/userManagement")
public class UserManagementController {

	private final UserManagementService umService;
	private final BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	public UserManagementController(UserManagementService umService, BCryptPasswordEncoder passwordEncoder) {
		
		this.umService = umService;
		this.passwordEncoder = passwordEncoder;
		
	}
	
	
	@GetMapping("/managementMain")
	public String AllUserInfo(Model model) {
		
//		List<LoginDTO> umList = umService.selectAllUserInfoList();
//		
////		System.out.println(umList);
//			 
//		model.addAttribute("userManagementList", umList);
		
		return "/userManagement/managementMain";
	}
	
	@GetMapping("/search")
	public ModelAndView selectUserInfo(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectUserCriteria selectUser, ModelAndView mv) {
		
		String userNo = selectUser.getUserNo();
		String userName = selectUser.getUserName();
		String phone = selectUser.getPhone();
		String deptCode = selectUser.getDeptCode();
		String jobCode = selectUser.getJobCode();
		
		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("userNo", userNo);
		searchMap.put("userName", userName);
		searchMap.put("phone", phone);
		searchMap.put("deptCode", deptCode);
		searchMap.put("jobCode", jobCode);
		
		System.out.println("searchMap : " + searchMap);
		
		int totalCount = umService.selectTotalCount(searchMap);
		
		System.out.println("totalUserCount : " + totalCount);
		
		int limit = 20;
		
		int buttonAmount = 5;
		
		SelectUserCriteria selectUserCriteria = Pagenation.getSelectUserCriteria(currentPage, totalCount, limit, buttonAmount, userNo, userName, phone, deptCode, jobCode);
		
		System.out.println("selectUserCritera : " + selectUserCriteria);
		
		List<LoginDTO> umList = umService.selectUserList(selectUserCriteria);
		
		System.out.println("umList : " + umList);
		
//		LoginDTO selectUser = new LoginDTO(userNo, userName, phone, deptName, jobName);
//		selectUser = new SelectUser(userNo, userName, phone, deptName, jobName);
//		
//		System.out.println("selectUser : " + selectUser);
		

		
//		List<Map<String, Object>> searchList = new ArrayList<Map<String, Object>>();
//		
//		Map<String, Object> umMap = new HashMap<String, Object>();
//		umMap.put("userNo", userNo);
//		umMap.put("userName", userName);
//		umMap.put("phone", phone);
//		umMap.put("deptName", deptName);
//		umMap.put("jobName", jobName);
//		searchList.add(umMap);
	
//		System.out.println(searchList);
		
//		LoginDTO searchUser = null;
		
//		if( (userNo != 0) || (userName != null && !"".equals(userName))
//				|| (phone != null && !"".equals(phone)) || (deptName != null && !"".equals(deptName))
//				|| (jobName != null && !"".equals(jobName))) {
//			searchUser = LoginDTO.getSelectUser(userNo, userName, phone, deptName, jobName);
//		} 
		
//		System.out.println("searchUser" + searchUser);
		
//		List<LoginDTO> umList = umService.selectUserInfo(searchMap);
//		List<LoginDTO> umList = umService.selectUserInfo(searchUser);
//		List<LoginDTO> umList = umService.selectUserInfo(searchList);
		
//		System.out.println("umList : " + umList);
//		System.out.println("searchMap : " + searchMap);
//		System.out.println("selectUser : " + selectUser);
		
//		mv.addObject("userManagementList", umList);
//		mv.setViewName("/userManagement/managementMain");
//		return mv;
		
		mv.addObject("userManagementList", umList);
		mv.addObject("selectUserCriteria", selectUserCriteria);
		mv.setViewName("/userManagement/managementMain");
		return mv;
	}
	

	@PostMapping("/addUser") 
	public String registUser(@ModelAttribute registUserDTO	member,@RequestParam MultipartFile uploadFile, 
			HttpServletRequest request, RedirectAttributes rttr) {
		
		System.out.println("uploadFile : " + uploadFile);
		
		/* 파일을 저장할 경로 설정 */
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		System.out.println("root : " + root);
				
		String filePath = root + "\\images\\profileFiles";
		
		File mkdir = new File(filePath);
	
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		String originalFile = null;
		
		if(uploadFile.isEmpty()) {
			originalFile = "adminUser.png";
		} else {
			originalFile = uploadFile.getOriginalFilename();
		}
		
		
		String ext = originalFile.substring(originalFile.lastIndexOf("."));
		String savedName = UUID.randomUUID().toString().replace("-", "") + ext;
		
		System.out.println("originalFile : " + originalFile);
		System.out.println("ext : " + ext);
		System.out.println("savedName : " + savedName);
		
		/* 파일을 저장한다. */
		
		try {
			uploadFile.transferTo(new File(filePath + "\\" + savedName));
			rttr.addFlashAttribute("message", "파일 업로드 성공!");
		} catch (Exception e) {
			e.printStackTrace();
			/* 실패시 파일 삭제 */
			new File(filePath + "\\" + savedName).delete();
			rttr.addFlashAttribute("message", "파일 업로드 실패!!");
		}

		LocalDate nowDate = LocalDate.now();
		int nowYear = nowDate.getYear();
		int birthYear = member.getBirthDate().getYear();
		int age = (nowYear - (birthYear+1900)) + 1;
		
		member.setAge(age);
		member.setOriginalFile(originalFile);
		member.setUpdateFile(savedName);
		member.setFilePath(filePath);
		member.setUserPwd(passwordEncoder.encode(member.getUserPwd()));
		
		System.out.println(member);
		
		if(member.getControl().equals("관리자")) {
			
			umService.registManager(member);
			
		} else {
			
			umService.registUser(member);
			
		}
		
//		System.out.println("birthYear" + birthYear); 
		
		rttr.addFlashAttribute("message","사용자 등록에 성공하였습니다.");
		
		
		return "redirect:/userManagement/managementMain";
		
	}
	
	@GetMapping("/updateUser")
	public void modifyUser() {}

	@GetMapping("/searchUserNo")
	public String searchUserNo(@RequestParam("userNo") int userNo, Model model) {
		
		System.out.println(userNo);
		
		LoginDTO member = umService.userSelectOne(userNo);

		model.addAttribute("modifyUser", member);

		return "/userManagement/updateUser";
		
	}
	
	@PostMapping("/updateUser")
	public String modifyUser(@ModelAttribute LoginDTO member, HttpServletRequest request, RedirectAttributes rttr, Model model) {
		
		System.out.println("member : " + member);
		
		umService.modifyUser(member);
		
		model.addAttribute("modifyUser", member);
		rttr.addFlashAttribute("message", "회원 정보가 변경되었습니다.");
		
		return "redirect:/userManagement/managementMain";
	}
	
	@GetMapping("/deleteUser")
	public String deleteUser(Model model) {
		
		List<LoginDTO> deleteList = umService.deleteUserList();
			 
		model.addAttribute("deleteList", deleteList);
		
		return "/userManagement/deleteUser";
		
	}
	
	@PostMapping("/deleteUser")
	public String deleteUser(@RequestParam("delChk") List<Integer> nos, RedirectAttributes rttr) {
		
		for(Integer no : nos) {
			
			umService.deleteUser(no);
			
		}
		
		rttr.addFlashAttribute("message","삭제에 성공하였습니다.");
		
		return "redirect:/userManagement/deleteUser";
		
	}
	
	
	
//	@PostMapping("/deleteUser")
//	public String deleteUser(HttpServletRequest request) {
//		
//		String[] userNoList = request.getParameterValues("valueArr");
//		int size = userNoList.length;
//		
//		System.out.println("userNoList : " + userNoList);
//		
//		
//		for(int i = 0; i < size; i++) {
//			
//			umService.deleteUser(userNoList[i]);
//		
//		}
//		
//		return "redirect:/userManagement/deleteUser";
//		
//	}
	
	
//	@ResponseBody
//	public String deleteUser(@RequestParam("noArr") String noArr,RedirectAttributes rttr) {
//	public String deleteUser(@RequestParam("delChk") String[] delId, Model model) {
//		
//		for (String userNo : delId) {
//			System.out.println("사용자 삭제 = " + userNo);;
//			int delCnt = umService.deleteUser(userNo);
//		}
//		
//		
////		System.out.println("noArr : " + noArr);
////		
////		String[] deleteNoArr = noArr.split(",");
////		
////		System.out.println("deleteNoArr : " + Arrays.toString(deleteNoArr));
////		
////		umService.deleteUser(deleteNoArr);
////		
////		rttr.addAttribute("message","선택한 유저를 삭제하였습니다.");
////		
//		return "redirect:/userManagement/managementMain";
//		
//	}
	
}
