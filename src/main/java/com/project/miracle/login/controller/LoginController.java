package com.project.miracle.login.controller;

import java.util.Random;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.login.exception.LoginFailedException;
import com.project.miracle.login.exception.passwodChangeException;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.login.model.dto.emailDTO;
import com.project.miracle.login.model.service.LoginService;

@Controller
@RequestMapping("/login")
@SessionAttributes("loginMember")
public class LoginController {

	private final LoginService loginService;
	private final BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	@Autowired
	public LoginController(LoginService loginService,
			BCryptPasswordEncoder passwordEncoder) {	
		
		this.loginService = loginService;
		this.passwordEncoder = passwordEncoder;
		
	}
	
	@GetMapping("/login")
	public void login() {}
	
	@PostMapping("/login")
	public String login(@ModelAttribute LoginDTO member, Model model) throws LoginFailedException {
		
		model.addAttribute("loginMember", loginService.findMember(member));
		
		if(member.getUserNo() >= 10000) {
			
			return "redirect:/mainpage/managerMain";
			
		} else {
			
			return "redirect:/mainpage/userMain";
			
		}
		
	}
	
	@GetMapping("/logout")
	public String logout(SessionStatus status) {
		
		status.setComplete();
		
		return "redirect:/login/login";
		
	}
	
	@GetMapping("/pwChange")
	public void pwChange() {}
	
	@PostMapping("/pwChange")
	public String pwChange(@ModelAttribute LoginDTO member, HttpServletRequest request, RedirectAttributes rttr, Model model) throws passwodChangeException {
		
		member.setUserNo(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo());
		
		member.setUserPwd(passwordEncoder.encode(member.getUserPwd()));
		
		System.out.println("member : " + member);
		
		loginService.passwordChange(member);
		
		request.getSession().setAttribute("loginMember", member);
		
		rttr.addFlashAttribute("message", "비밀번호가 변경되었습니다.");
		
		return "redirect:/login/login";
	}
	
	@GetMapping("/pwFind")
	public void pwFind(){}
	
//	@RequestMapping(value="/sendMail", method=RequestMethod.GET)
	@GetMapping("/sendMail")
	@ResponseBody
//	public String sendMailAuth(HttpSession session, @RequestParam(value="email") String email, Model model) {
	public String sendMailAuth(int userNo, String email) throws Exception {
		
		System.out.println("번호 : " + userNo);
		System.out.println("이메일 : " + email);
	
		Random random = new Random();
		int checkNum = random.nextInt(100000) + 10000;
		System.out.println("인증코드 : " + checkNum);
		
		emailDTO emailDTO = new emailDTO();
		emailDTO.setUserNo(userNo);
		emailDTO.setEmailCode(checkNum);
		String setFrom = "himediaMailTest@gmail.com";
		String toMail = email;
		String title = "이메일 인증코드입니다.";
		String content = "이메일 인증코드는 " + checkNum + "입니다.";
		
		try {
			
			MimeMessage sendMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(sendMessage, true, "UTF-8");
			helper.setFrom(setFrom);
			helper.setTo(toMail);
			helper.setSubject(title);
			helper.setText(content, true);

			javaMailSender.send(sendMessage);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		String num = Integer.toString(checkNum);
		
		return num;
		
//		int random = new Random().nextInt(100000) + 10000;
//		String emailCode = String.valueOf(random);
//		session.setAttribute("emailCode", emailCode);
//		
//		String subject = "이메일 인증코드입니다.";
//		StringBuilder sb = new StringBuilder();
//		sb.append("이메일 인증 코드는 " + emailCode + "입니다.");
//		
//		emailDTO emailDTO = new emailDTO();
//		String inputCode = emailDTO.getEmailCode();
//		
//		boolean result = loginService.sendMail(subject, sb.toString(), "himediaMailTest@gmail.com", email);
//		
//		System.out.println("result" + result);
//		
//		if (result = true) {
//			
//			model.addAttribute("message", "인증 성공");
//			
//		} else {
//			
//			model.addAttribute("message", "인증 실패");
//			
//		}
//		
//		return "/login/pwFind";
		
	}
	
	@GetMapping("/pwFindOk")
	public String pwFindOk(int userNo, Model model) {
		
		LoginDTO member = new LoginDTO();
		member.setUserNo(userNo);
		
		model.addAttribute("loginMember", member);
		
		return "redirect:/login/pwChange";
		
	}
	
	
}
