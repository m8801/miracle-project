package com.project.miracle.login.model.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.project.miracle.login.exception.LoginFailedException;
import com.project.miracle.login.exception.passwodChangeException;
import com.project.miracle.login.model.dao.LoginMapper;
import com.project.miracle.login.model.dto.LoginDTO;

@Service
public class LoginServiceImpl implements LoginService {

	private final LoginMapper mapper;
	private final BCryptPasswordEncoder passwordEncoder;
	private final JavaMailSender javaMailSender;
	
	@Autowired
	public LoginServiceImpl(LoginMapper mapper,
			BCryptPasswordEncoder passwordEncoder, JavaMailSender javaMailSender) {
		
		this.mapper = mapper;
		this.passwordEncoder = passwordEncoder;
		this.javaMailSender = javaMailSender;
		
	}
	
	/**
	 * <pre>
	 * 	회원 로그인용 메소드
	 * </pre>
	 * @param member : 로그인 정보(아이디, 비밀번호)
	 * @throws LoginFailedException
	 */
	@Override
	public LoginDTO findMember(LoginDTO member) throws LoginFailedException {
		
		System.out.println("check : " + passwordEncoder.matches(
				member.getUserPwd(), mapper.selectEncryptedPwd(member)));
		
		System.out.println(passwordEncoder.encode(member.getUserPwd()));
		System.out.println(passwordEncoder.encode(mapper.selectEncryptedPwd(member)));
		
		if(!passwordEncoder.matches(member.getUserPwd(), mapper.selectEncryptedPwd(member))) {
//		if(!member.getUserPwd().equals(mapper.selectEncryptedPwd(member))) {
			
			throw new LoginFailedException("로그인 실패!"); 
			
		}
		
		return mapper.selectMember(member);
	}

	@Override
	public void passwordChange(LoginDTO member) throws passwodChangeException {
		
		System.out.println(member.getUserPwd());
		
		int result = mapper.updatePassword(member);
		
		System.out.println(result);
		
		if(!(result > 0)) {
			throw new passwodChangeException("비밀번호 변경에 실패하셨습니다.");
		}
		
		
	}
	

	@Override
	public boolean sendMail(String subject, String text, String from, String to) {
		MimeMessage sendMessage = javaMailSender.createMimeMessage();
		
		try {
			MimeMessageHelper helper = new MimeMessageHelper(sendMessage, true, "UTF-8");
			helper.setSubject(subject);
			helper.setText(text, true);
			helper.setFrom(from);
			helper.setTo(to);
			
			javaMailSender.send(sendMessage);
			
			return true;
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
		return false;
	}

}
