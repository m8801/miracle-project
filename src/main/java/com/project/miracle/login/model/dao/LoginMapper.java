package com.project.miracle.login.model.dao;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.login.model.dto.LoginDTO;

public interface LoginMapper {

	String selectEncryptedPwd(LoginDTO member);

	LoginDTO selectMember(LoginDTO member);

	int updatePassword(LoginDTO member);

}
