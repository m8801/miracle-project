package com.project.miracle.login.model.dto;

public class emailDTO {
	private int certifyNo;
	private int emailCode;
	private int userNo;
	
	public emailDTO() {}

	public emailDTO(int certifyNo, int emailCode, int userNo) {
		super();
		this.certifyNo = certifyNo;
		this.emailCode = emailCode;
		this.userNo = userNo;
	}

	public int getCertifyNo() {
		return certifyNo;
	}

	public void setCertifyNo(int certifyNo) {
		this.certifyNo = certifyNo;
	}

	public int getEmailCode() {
		return emailCode;
	}

	public void setEmailCode(int emailCode) {
		this.emailCode = emailCode;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	@Override
	public String toString() {
		return "emailDTO [certifyNo=" + certifyNo + ", emailCode=" + emailCode + ", userNo=" + userNo + "]";
	}

	
	
}
