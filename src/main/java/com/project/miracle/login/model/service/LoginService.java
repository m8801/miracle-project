package com.project.miracle.login.model.service;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.login.exception.LoginFailedException;
import com.project.miracle.login.exception.passwodChangeException;
import com.project.miracle.login.model.dto.LoginDTO;

public interface LoginService {

	LoginDTO findMember(LoginDTO member) throws LoginFailedException;

	void passwordChange(LoginDTO member) throws passwodChangeException;

	boolean sendMail(String subject, String text, String from, String to);

}
