package com.project.miracle.login.exception;

public class LoginFailedException extends Exception {

	public LoginFailedException(String msg) {
		
		super(msg);
		
	}
	
}
