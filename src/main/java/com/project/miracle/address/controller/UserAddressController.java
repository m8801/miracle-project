package com.project.miracle.address.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.address.exception.AddressRegistException;
import com.project.miracle.address.model.dto.AddressDTO;
import com.project.miracle.address.model.dto.MyAddressDTO;
import com.project.miracle.address.model.service.AddressService;
import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.common.paging.SharedSelectCriteria;
import com.project.miracle.login.model.dto.LoginDTO;

@Controller
@RequestMapping("/address")
@SessionAttributes("loginMember")
public class UserAddressController {

	public final AddressService addressService;

	public UserAddressController(AddressService addressService) {
		super();
		this.addressService = addressService;
	}

	/* 사용자 컨트롤러 */
	/* 회사 공용 메서드 */
	// 회사 공용 메서드 조회
	@GetMapping("addressCompanyUser")
	public ModelAndView CompanyUserList(@RequestParam(defaultValue = "1") int currentPage,
			@ModelAttribute SelectCriteria searchCriteria, ModelAndView mv) {
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);

		int totalCount = addressService.selectTotalCount(searchMap);

		System.out.println(totalCount);

		int limit = 13;
		int buttonAmount = 5;

		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {

			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);

		List<AddressDTO> addressList = addressService.selectAddressList(selectCriteria);

		System.out.println(addressList);

		mv.addObject("addressList", addressList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type", "addressCompanyUser");
		mv.setViewName("/address/addressCompanyUser");
		return mv;
	}

	// 주소록 공유 페이지
	@GetMapping("companyUserSharing")
	public void companyManagerSharing() {
	}

	// 주소록 공유 .ajax로 공유하기 위한 값을 가져올 메서드
	@GetMapping("companyUserSharing/{data}")
	public String CompanySearchShared(@PathVariable String data, Model model) {

		System.out.println("data : " + data);
		model.addAttribute("addressNoArr", data);
		return "/address/companyUserSharing";
	}

	// 주소록을 보낼 사원들 리스트 조회
	@PostMapping("companyUserSearchList")
	@ResponseBody
	public ModelAndView CompanymanagerSharedSearch(@RequestParam("addressNoArr") String addressNoArr,
			@ModelAttribute SharedSelectCriteria searchCriteria, ModelAndView mv) {
		System.out.println(addressNoArr);
		String searchCondition1 = searchCriteria.getSearchCondition1();
		String searchCondition2 = searchCriteria.getSearchCondition2();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition1", searchCondition1);
		searchMap.put("searchCondition2", searchCondition2);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);

		List<AddressDTO> sharingAddressList = addressService.selectSharedList(searchMap);

		System.out.println("sharingAddressList : " + sharingAddressList);

		mv.addObject("addressNoArr", addressNoArr);
		mv.addObject("sharingAddressList", sharingAddressList);
		mv.setViewName("/address/companyUserSharing");
		return mv;
	}

	// 주소록 공유
	@PostMapping("companyUserSharingRegist")
	@ResponseBody
	public String CompanysharingAddressRegist(@RequestParam("addressNoArr") int[] addressNoArr,
			@RequestParam("shareUserNoArr") int[] shareUserNoArr, HttpServletRequest request, RedirectAttributes rttr) {

//		SharedAddressDTO test = new SharedAddressDTO();
//		test.setSharer(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName());
		String sharer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		System.out.println(sharer);
		System.out.println(Arrays.toString(addressNoArr));

		int sharingResult = addressService.sharingRegist(sharer, addressNoArr, shareUserNoArr);
		String result = "";
		System.out.println("sharingResult 결과값 : " + sharingResult);
		if (sharingResult > 0) {
			result = "success";
		} else {
			result = "fail";
		}
		// rttr.addAttribute("message","주소록을 삭제했습니다.");

		return result;
	}

	/* 내 주소록 */
	@GetMapping("/myUserList")
	public ModelAndView myAddressSelectList(@RequestParam(defaultValue = "1") int currentPage,
			@ModelAttribute SelectCriteria searchCriteria, ModelAndView mv, HttpServletRequest request) {
		String loginNo = Integer.valueOf(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo()).toString();
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, Object> searchMap = new HashMap<>();

		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		searchMap.put("loginNo", loginNo);

		System.out.println(searchMap);

		int totalCount = addressService.selectMyAddressTotalCount(searchMap);

		System.out.println(totalCount);

		int limit = 11;
		int buttonAmount = 5;

		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {

			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);

		List<AddressDTO> addressList = addressService.selectMyAddressList(selectCriteria,loginNo);
		Set<String> keys = new HashSet<>();
		for(AddressDTO address : addressList) {
			keys.add(address.getGroupName());
		}
		
		System.out.println("keys : " + keys);
		
		System.out.println(addressList);

		mv.addObject("keys", keys);
		mv.addObject("addressList", addressList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type", "addressMyUser");
		mv.setViewName("/address/addressMyUser");
		return mv;
	}

	// 내 주소록 추가 페이지 
	@GetMapping("myAddressInsertUser")
	public void myAddressInsertManager() {
	}
	
	// 내 주소록 추가 페이지 조회 기능 
	@GetMapping("/myAddressUserInsertSelect")
	public String myAddressInsertSelect(@RequestParam int userNo, Model model) {

		MemberDTO user = addressService.selectOneAddress(userNo);

		AddressDTO address = new AddressDTO();
		address.setMemberDTO(user);

//			System.out.println(address);

		model.addAttribute("myAddress", address);

		/* System.out.println("btn1 클릭 "); */

		return "/address/myAddressInsertUser";

	}

	// 내 주소록 추가 기능 메서드
    @PostMapping("myAddressInsertUser")
	public String myAddressInsertManager(@RequestParam int userNo, @RequestParam String groupName,
			RedirectAttributes rttr, Model model, HttpServletRequest request) throws AddressRegistException {

		String loginNo = Integer.valueOf(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo()).toString();

		MemberDTO user = addressService.selectOneAddress(userNo);

		AddressDTO address = new AddressDTO();
		address.setMemberDTO(user);
		address.setGroupName(groupName);

		System.out.println(address);

		address.setLoginNo(loginNo);

		addressService.registMyAddress(address);

		model.addAttribute("myInsertAddress", address);
		rttr.addFlashAttribute("message", "주소록을 등록했습니다.");

		/* System.out.println("btn2 클릭 "); */

		return "redirect:/address/myUserList";
	}

    // 내 주소록 삭제 기능 메서드
	@PostMapping("/deleteMyAddressUser")
	@ResponseBody
	public String deleteMyAddressUser(@RequestParam("noArr") String noArr) throws Exception {

		System.out.println("noArr : " + noArr);

		String[] deleteNoArr = noArr.split(",");

		System.out.println(Arrays.toString(deleteNoArr));

		int result = addressService.deleteMyAddress(deleteNoArr);	
		
		String check = "";
		
		if( result < 0 ) {
			check = "fail";
		} else {
			check = "success";
		}
		return check; 
	}

	// 내 주소록 수정 페이지
	@GetMapping("myAddressModifyUser")
	public void myAddressModifyUser() {
	}

	// 내 주소록 수정을 위한 값을 .ajax에서 가져오는 메서드
	@GetMapping("myAddressModifyUser/{data}")
	public String myAddressModifyUser(@PathVariable String data, Model model) {
		System.out.println("data : " + data);
		model.addAttribute("no", data);
		return "/address/myAddressModifyUser";
	}

	// 내 주소록 수정 기능 메서드
	@PostMapping("/modifyMyAddressUser")
	public String modifyMyAddressUser(@RequestParam("no") int no, @RequestParam String groupName,
			@RequestParam String userName, RedirectAttributes rttr) throws Exception {
		AddressDTO addressDTO = new AddressDTO();
		MemberDTO memberDTO = new MemberDTO();
		addressDTO.setAddressNo(no);
		addressDTO.setGroupName(groupName);
		addressDTO.setMemberDTO(memberDTO);
		memberDTO.setUserName(userName);
		System.out.println(memberDTO);

		addressDTO.getMemberDTO().getUserName();
		System.out.println("addressDTO :" + addressDTO);
		addressService.modifyMyAddress(addressDTO);
		rttr.addFlashAttribute("message", "주소록을 수정했습니다.");

		return "redirect:/address/myUserList";

	}

	// 내 주소록 공유 팝업 창 
	@GetMapping("myUserSharing")
	public void myManagerSharing() {
	}

	// 내 주소록 공유를 위한 값을 .ajax에서 가져오는 메서드
	@GetMapping("myUserSharing/{data}")
	public String myManagerSearchShared(@PathVariable String data, Model model) {

		System.out.println("data : " + data);
		model.addAttribute("addressNoArr", data);
		return "/address/myUserSharing";
	}

	// 내 주소록 공유를 위한 사원 조회
	@PostMapping("myUserSearchList")
	@ResponseBody
	public ModelAndView myManagerSharedSearch(@RequestParam("addressNoArr") String addressNoArr,
			@ModelAttribute SharedSelectCriteria searchCriteria, ModelAndView mv) {
		System.out.println(addressNoArr);
		String searchCondition1 = searchCriteria.getSearchCondition1();
		String searchCondition2 = searchCriteria.getSearchCondition2();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition1", searchCondition1);
		searchMap.put("searchCondition2", searchCondition2);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);

		List<AddressDTO> sharingAddressList = addressService.selectSharedList(searchMap);

		System.out.println(sharingAddressList);

		mv.addObject("addressNoArr", addressNoArr);
		mv.addObject("sharingUserAddressList", sharingAddressList);
		mv.setViewName("/address/myUserSharing");
		return mv;
	}

	// 내 주소록 공유 기능 메서드
	@PostMapping("myUserSharingRegist")
	@ResponseBody
	public String myUserSharingAddressRegist(@RequestParam("addressNoArr") int[] addressNoArr,
			@RequestParam("shareUserNoArr") int[] shareUserNoArr, HttpServletRequest request, RedirectAttributes rttr) {
		String sharer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		System.out.println(sharer);
		int sharingResult = addressService.mySharingRegist(sharer, addressNoArr, shareUserNoArr);
		String result = "";
		System.out.println("sharingResult 결과값 : " + sharingResult);
		if (sharingResult > 0) {
			result = "success";
		} else {
			result = "fail";
		}

		return result;
	}

	/* 공유 받은 주소록 */
	// 공유 주소록 조회 
	@GetMapping("/sharedUserList")
	public ModelAndView sharedUserList(@RequestParam(defaultValue = "1") int currentPage, HttpServletRequest request,
			@ModelAttribute SelectCriteria searchCriteria, ModelAndView mv) {
		// 로그인된 사원번호
		String loginNo = Integer.valueOf(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo()).toString();
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, Object> searchMap = new HashMap<>();

		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		searchMap.put("loginNo", loginNo);

		System.out.println(searchMap);

		int totalCount = addressService.selectSharedAddressTotalCount(searchMap);

		System.out.println(totalCount);

		int limit = 11;
		int buttonAmount = 5;

		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {

			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);

		System.out.println(loginNo);
		List<AddressDTO> addressList = addressService.selectSharedAddressList(selectCriteria, loginNo);
		System.out.println(addressList);
		Set<String> keys = new HashSet<>();
		for (AddressDTO address : addressList) {
			keys.add(address.getGroupName());
		}

		System.out.println("keys : " + keys);

		System.out.println(addressList);

		mv.addObject("keys", keys);
		mv.addObject("addressList", addressList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type", "addressSharedUser");
		mv.setViewName("/address/addressSharedUser");
		return mv;
	}

	// 공유 주소록 수정 페이지
	@GetMapping("sharedAddressModifyUser")
	public void sharedAddressModifyUser() {
	}

	// 공유 주소록 수정을 위한 값을 .ajax에서 가져오는 메서드
	@GetMapping("sharedAddressModifyUser/{data}")
	public String sharedAddressModifyUser(@PathVariable String data, Model model) {
		System.out.println("data : " + data);
		model.addAttribute("no", data);
		return "/address/sharedAddressModifyUser";
	}

	// 공유 주소록 수정 기능 메서드
	@PostMapping("/modifyUserSharedAddress")
	public String modifyUserSharedAddress(@RequestParam("no") int no, @RequestParam String groupName,
			RedirectAttributes rttr) throws Exception {
		AddressDTO addressDTO = new AddressDTO();
		MemberDTO memberDTO = new MemberDTO();
		addressDTO.setAddressNo(no);
		addressDTO.setGroupName(groupName);
		addressDTO.setMemberDTO(memberDTO);
		System.out.println(memberDTO);

		System.out.println("addressDTO :" + addressDTO);
		addressService.modifySharedAddress(addressDTO);

		rttr.addFlashAttribute("message", "주소록을 수정했습니다.");

		return "redirect:/address/sharedUserList";

	}

	// 공유 주소록 삭제 메서드
	@PostMapping("/deleteUserSharedAddress")
	@ResponseBody
	public String deleteUserSharedAddress(@RequestParam("noArr") String noArr) throws Exception {

		System.out.println("noArr : " + noArr);

		String[] deleteNoArr = noArr.split(",");

		System.out.println(Arrays.toString(deleteNoArr));

		int result = addressService.deleteSharedAddress(deleteNoArr);	

		String check = "";
		
		if( result < 0 ) {
			check = "fail";
		} else {
			check = "success";
		}
		return check;
	}
}
