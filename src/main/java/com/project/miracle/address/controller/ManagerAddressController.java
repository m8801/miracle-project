package com.project.miracle.address.controller;

import java.net.http.HttpRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.taglibs.standard.tag.common.fmt.ParseDateSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.address.exception.AddressRegistException;
import com.project.miracle.address.model.dto.AddressDTO;
import com.project.miracle.address.model.dto.MyAddressDTO;
import com.project.miracle.address.model.service.AddressService;
import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.common.paging.SharedSelectCriteria;
import com.project.miracle.login.model.dto.LoginDTO;

@Controller
@RequestMapping("/address")
@SessionAttributes("loginMember")
public class ManagerAddressController {

	public final AddressService addressService;

	@Autowired
	public ManagerAddressController(AddressService addressService) {
		this.addressService = addressService;
	}
	
	/* 관리자 컨트롤러 */
	/* 주소록 관리자 */
	// 메인 페이지 
	@GetMapping("/list")
	public ModelAndView AddressSelectList(@RequestParam(defaultValue = "1") int currentPage,
			@ModelAttribute SelectCriteria searchCriteria, ModelAndView mv) {

		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);

		int totalCount = addressService.selectTotalCount(searchMap);

		System.out.println(totalCount);

		int limit = 13;
		int buttonAmount = 5;

		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {

			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);

		List<AddressDTO> addressList = addressService.selectAddressList(selectCriteria);

		System.out.println(addressList);

		mv.addObject("addressList", addressList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type","list");
		mv.setViewName("/address/addressMainManager");
		return mv;
	}
	
	// 주소 추가 페이지
	@GetMapping("/addressInsertManager")
	public void addressRegist() {
	}
	
	// 주소록 추가 페이지에서 사원번호로 사원정보 조회
	@GetMapping("/registSelect")
	public String addressRegist(@RequestParam int userNo, Model model) {

		MemberDTO user = addressService.selectOneAddress(userNo);

		System.out.println(user);

		model.addAttribute("user", user);

		return "/address/addressInsertManager";

	}
	// 주소록 추가
	@PostMapping("/regist")
	public String addressRegist(@RequestParam int userNo, RedirectAttributes rttr) throws AddressRegistException {

		MemberDTO user = addressService.selectOneAddress(userNo);

		System.out.println(user);

		addressService.registAddress(user);

		rttr.addFlashAttribute("message", "주소록을 등록했습니다.");
		
		return "redirect:/address/list";

	}
	
	// 주소록 삭제
	@PostMapping("/deleteAddress")
	@ResponseBody
	public String deleteAddress(@RequestParam("noArr") String noArr) throws Exception {
		
		System.out.println("noArr : " + noArr);
		
		String[] deleteNoArr = noArr.split(",");
		
		System.out.println(Arrays.toString(deleteNoArr));
		
		int result = addressService.deleteAddress(deleteNoArr);
		
		String check = "";
		
		if( result < 0 ) {
			check = "fail";
		} else {
			check = "success";
		}
		return check; 
	}
	
	// 주소록 공유 페이지
	@GetMapping("mainManagerSharing")
	public void mainManagerSharing() {}
	
	@GetMapping("mainManagerSharing/{data}")
	public String searchShared(@PathVariable String data, Model model) {
		
		System.out.println("data : " + data );
		model.addAttribute("addressNoArr", data);
		return "/address/mainManagerSharing";
	}
	
	// 주소록을 보낼 사원들 리스트 조회
	@PostMapping("managerSearchList")
	@ResponseBody
	public ModelAndView managerSharedSearch(@RequestParam("addressNoArr") String addressNoArr, @ModelAttribute SharedSelectCriteria searchCriteria, ModelAndView mv){
		System.out.println(addressNoArr);
		String searchCondition1 = searchCriteria.getSearchCondition1();
		String searchCondition2 = searchCriteria.getSearchCondition2();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition1", searchCondition1);
		searchMap.put("searchCondition2", searchCondition2);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);


		List<AddressDTO> sharingAddressList = addressService.selectSharedList(searchMap);

		System.out.println(sharingAddressList);
		
		mv.addObject("addressNoArr",addressNoArr);
		mv.addObject("sharingAddressList", sharingAddressList);
		mv.setViewName("/address/mainManagerSharing");
		return mv;
	}
	
	// 주소록 공유 
	@PostMapping("sharingRegist")
	@ResponseBody
	public String sharingAddressRegist(@RequestParam("addressNoArr") int[] addressNoArr,@RequestParam("shareUserNoArr") int[] shareUserNoArr,
	        HttpServletRequest request) {	
	

		String sharer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		System.out.println(sharer);
		
		int sharingResult = addressService.sharingRegist(sharer,addressNoArr,shareUserNoArr);
		String result = "";
		System.out.println("sharingResult 결과값 : " + sharingResult);
		if( sharingResult > 0 ) {
			result = "success";
		} else {
			result = "fail";
		}

		return result;
	}
	
	
	/* 회사 공용 주소록 */
	// 회사 공용 주소록 메서드	
	@GetMapping("addressCompanyManager")
	public ModelAndView CompanyManagerList(@RequestParam(defaultValue = "1") int currentPage,
			@ModelAttribute SelectCriteria searchCriteria, ModelAndView mv) {
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);

		int totalCount = addressService.selectTotalCount(searchMap);

		System.out.println(totalCount);

		int limit = 13;
		int buttonAmount = 5;

		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {

			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);

		List<AddressDTO> addressList = addressService.selectAddressList(selectCriteria);

		System.out.println(addressList);

		mv.addObject("addressList", addressList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type","addressCompanyManager");
		mv.setViewName("/address/addressCompanyManager");
		return mv;
	}

	// 회사 공용 페이지 공유 팝업 창 이동 메서드
	@GetMapping("companyManagerSharing")
	public void companyManagerSharing(){}	
	
	// 값을 .ajax에서 가져오기위한 메서드
	@GetMapping("companyManagerSharing/{data}")
	public String CompanySearchShared(@PathVariable String data, Model model) {
		
		System.out.println("data : " + data );
		model.addAttribute("addressNoArr", data);
		return "/address/companyManagerSharing";
	}

	// 회사 공용 페이지 공유 팝업 창 내 조회 기능 메서드
	@PostMapping("companyManagerSearchList")
	@ResponseBody
	public ModelAndView CompanymanagerSharedSearch(@RequestParam("addressNoArr") String addressNoArr, @ModelAttribute SharedSelectCriteria searchCriteria, ModelAndView mv){
		System.out.println(addressNoArr);
		String searchCondition1 = searchCriteria.getSearchCondition1();
		String searchCondition2 = searchCriteria.getSearchCondition2();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition1", searchCondition1);
		searchMap.put("searchCondition2", searchCondition2);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);

		List<AddressDTO> sharingAddressList = addressService.selectSharedList(searchMap);

		System.out.println(sharingAddressList);
		
		mv.addObject("addressNoArr",addressNoArr);
		mv.addObject("sharingAddressList", sharingAddressList);
		mv.setViewName("/address/companyManagerSharing");
		return mv;
	}
	
	
	// 회사 공용 페이지 공유 메서드
	@PostMapping("companySharingRegist")
	@ResponseBody
	public String CompanysharingAddressRegist(@RequestParam("addressNoArr") int[] addressNoArr
			,@RequestParam("shareUserNoArr") int[] shareUserNoArr,HttpServletRequest request) {	
	

		String sharer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		System.out.println(sharer);
		System.out.println(Arrays.toString(shareUserNoArr));
		int sharingResult = addressService.sharingRegist(sharer,addressNoArr,shareUserNoArr);
		String result = "";
		System.out.println("sharingResult 결과값 : " + sharingResult);
		if( sharingResult > 0 ) {
			result = "success";
		} else {
			result = "fail";
		}

		return result;
	}
	
	
	/* 내 주소록 */
	// 내 주소록 페이지
	@GetMapping("myAddressInsertManager")
	public void myAddressInsertManager() {}
	
	// 내 주소록 페이지 조회 메서드
	@GetMapping("/myManagerList")
	public ModelAndView myAddressSelectList(@RequestParam(defaultValue = "1") int currentPage,
			@ModelAttribute SelectCriteria searchCriteria, ModelAndView mv,HttpServletRequest request) {
		String loginNo = Integer.valueOf(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo()).toString();
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, Object> searchMap = new HashMap<>();

		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		searchMap.put("loginNo", loginNo);

		System.out.println(searchMap);

		int totalCount = addressService.selectMyAddressTotalCount(searchMap);

		System.out.println(totalCount);

		int limit = 11;
		int buttonAmount = 5;

		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {

			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);

		List<AddressDTO> addressList = addressService.selectMyAddressList(selectCriteria,loginNo);
		Set<String> keys = new HashSet<>();
		for(AddressDTO address : addressList) {
			keys.add(address.getGroupName());
		}
		
		System.out.println("keys : " + keys);
		
		System.out.println(addressList);

		mv.addObject("keys", keys);
		mv.addObject("addressList", addressList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type","addressMyManager");
		mv.setViewName("/address/addressMyManager");
		return mv;
	}
	
	// 내 주소록 추가 페이지 조회 기능 메서드
	@GetMapping("/myAddressInsertSelect")
	public String myAddressInsertSelect(@RequestParam int userNo, Model model) {

		MemberDTO user = addressService.selectOneAddress(userNo);

		AddressDTO address = new AddressDTO();
		address.setMemberDTO(user);
		
		System.out.println(address);

		model.addAttribute("myAddress", address);
		
		/* System.out.println("btn1 클릭 "); */

		return "/address/myAddressInsertManager";

	}
	
	// 내 주소록 추가 페이지 추가 메서드
	@PostMapping("myAddressInsertManager")
	public String myAddressInsertManager(@RequestParam int userNo,@RequestParam String groupName, RedirectAttributes rttr 
			,Model model, HttpServletRequest request) throws AddressRegistException {
		
		String loginNo = Integer.valueOf(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo()).toString();

		MemberDTO user = addressService.selectOneAddress(userNo);

		AddressDTO address = new AddressDTO();
		address.setMemberDTO(user);
		address.setGroupName(groupName);

		System.out.println(address);

		address.setLoginNo(loginNo);

		addressService.registMyAddress(address);

		model.addAttribute("myInsertAddress", address);
		rttr.addFlashAttribute("message", "주소록을 등록했습니다.");
		
		/* System.out.println("btn2 클릭 "); */
		
		return "redirect:/address/myManagerList";	
	} 
	
	// 내 주소록 페이지 삭제 메서드
	@PostMapping("/deleteMyAddress")
	@ResponseBody
	public String deleteMyAddress(@RequestParam("noArr") String noArr) throws Exception {
		
		System.out.println("noArr : " + noArr);
		
		String[] deleteNoArr = noArr.split(",");
		
		System.out.println(Arrays.toString(deleteNoArr));
		
		int result = addressService.deleteMyAddress(deleteNoArr);	
		
		String check = "";
		
		if( result < 0 ) {
			check = "fail";
		} else {
			check = "success";
		}
		return check; 		
	}

	// 내 주소록 수정 페이지
	@GetMapping("myAddressModifyManager")
	public void myAddressModifyManager() {}
	
	// 내 주소록 수정을 위한 값을 .ajax에서 가져오기 위한 메서드
	@GetMapping("myAddressModifyManager/{data}")
	public String myAddressModifyManager(@PathVariable String data, Model model) {
		System.out.println("data : " + data );
		model.addAttribute("no", data);
		return "/address/myAddressModifyManager";
	}
	
	// 내 주소록 페이지 수정 기능 메서드
	@PostMapping("/modifyMyAddress") 
	public String modifyMyAddress(@RequestParam("no") int no, @RequestParam String groupName, @RequestParam String userName,
			RedirectAttributes rttr) throws Exception {
	 AddressDTO addressDTO = new AddressDTO();
	 MemberDTO memberDTO = new MemberDTO(); 
	 addressDTO.setAddressNo(no);
	 addressDTO.setGroupName(groupName);  
	 addressDTO.setMemberDTO(memberDTO);
	 memberDTO.setUserName(userName);
	 System.out.println(memberDTO);
	  
	 addressDTO.getMemberDTO().getUserName();
	 System.out.println("addressDTO :" + addressDTO );
	 addressService.modifyMyAddress(addressDTO);
	 rttr.addFlashAttribute("message","주소록을 수정했습니다.");
	  
	 return "redirect:/address/myManagerList"; 
	  
	 }
	 
	// 내 주소록 공유 페이지
	@GetMapping("myManagerSharing")
	public void myManagerSharing(){}	
	
	// 내 주소록 공유기능을 위한 값을 .ajax에서 가져오기 위한 메서드
	@GetMapping("myManagerSharing/{data}")
	public String myManagerSearchShared(@PathVariable String data, Model model) {
		
		System.out.println("data : " + data );
		model.addAttribute("addressNoArr", data);
		return "/address/myManagerSharing";
	}

	// 내 주소록 공유 페이지 팝업창 조회 기능
	@PostMapping("myManagerSearchList")
	@ResponseBody
	public ModelAndView myManagerSharedSearch(@RequestParam("addressNoArr") String addressNoArr, @ModelAttribute SharedSelectCriteria searchCriteria, ModelAndView mv){
		System.out.println(addressNoArr);
		String searchCondition1 = searchCriteria.getSearchCondition1();
		String searchCondition2 = searchCriteria.getSearchCondition2();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, String> searchMap = new HashMap<>();

		searchMap.put("searchCondition1", searchCondition1);
		searchMap.put("searchCondition2", searchCondition2);
		searchMap.put("searchValue", searchValue);

		System.out.println(searchMap);

		List<AddressDTO> sharingAddressList = addressService.selectSharedList(searchMap);

		System.out.println(sharingAddressList);
		
		mv.addObject("addressNoArr",addressNoArr);
		mv.addObject("sharingAddressList", sharingAddressList);
		mv.setViewName("/address/myManagerSharing");
		return mv;
	}
	
	
	// 내 주소록 공유 페이지 팝업창 공유 기능
	@PostMapping("mySharingRegist")
	@ResponseBody
	public String mySharingAddressRegist(@RequestParam("addressNoArr") int[] addressNoArr
			,@RequestParam("shareUserNoArr") int[] shareUserNoArr,HttpServletRequest request,RedirectAttributes rttr) {	
		String sharer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		System.out.println(sharer);
		int sharingResult = addressService.mySharingRegist(sharer,addressNoArr,shareUserNoArr);
		String result = "";
		System.out.println("sharingResult 결과값 : " + sharingResult);
		if( sharingResult > 0 ) {
			result = "success";
		} else {
			result = "fail";
		}

		return result;
	}
	
	/* 공유 받은 주소록 */	
	// 공유 주소록 조회
	@GetMapping("/sharedManagerList")
	public ModelAndView sharedManagerList(@RequestParam(defaultValue = "1") int currentPage,HttpServletRequest request,
			@ModelAttribute SelectCriteria searchCriteria, ModelAndView mv) {
		// 로그인된 사원번호
		String loginNo = Integer.valueOf(((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo()).toString();
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();

		Map<String, Object> searchMap = new HashMap<>();

		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		searchMap.put("loginNo", loginNo);

		System.out.println(searchMap);

		int totalCount = addressService.selectSharedAddressTotalCount(searchMap);

		System.out.println(totalCount);

		int limit = 11;
		int buttonAmount = 5;

		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {

			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		System.out.println(selectCriteria);

		System.out.println(loginNo);
		List<AddressDTO> addressList = addressService.selectSharedAddressList(selectCriteria, loginNo);
		System.out.println(addressList);		
		Set<String> keys = new HashSet<>();
		for(AddressDTO address : addressList) {
			keys.add(address.getGroupName());
		}
		
		System.out.println("keys : " + keys);
		
		System.out.println("addressList = " + addressList);

		mv.addObject("keys", keys);
		mv.addObject("shareAddressList", addressList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type","addressSharedManager");
		mv.setViewName("/address/addressSharedManager");
		return mv;
	}
	
	// 공유 주소록 수정 페이지 
	@GetMapping("sharedAddressModifyManager")
	public void sharedAddressModifyManager() {}
	
	// 공유 주소록 수정에 필요한 값을 .ajax에서 가져오기 위한 메서드
	@GetMapping("sharedAddressModifyManager/{data}")
	public String sharedAddressModifyManager(@PathVariable String data, Model model) {
		System.out.println("data : " + data );
		model.addAttribute("no", data);
		return "/address/sharedAddressModifyManager";
	}
	
	// 공유 주소록 수정 기능 메서드
	@PostMapping("/modifySharedAddress") 
	public String modifySharedAddress(@RequestParam("no") int no, @RequestParam String groupName, 
			RedirectAttributes rttr) throws Exception {
	 AddressDTO addressDTO = new AddressDTO();
	 MemberDTO memberDTO = new MemberDTO(); 
	 addressDTO.setAddressNo(no);
	 addressDTO.setGroupName(groupName);  
	 addressDTO.setMemberDTO(memberDTO);
	 System.out.println(memberDTO);
	 
	 System.out.println("addressDTO :" + addressDTO );
	 addressService.modifySharedAddress(addressDTO);
	  
	 rttr.addFlashAttribute("message","주소록을 수정했습니다.");
	  
	 return "redirect:/address/sharedManagerList"; 
	  
	 }

	// 공유 조소록 삭제 메서드
	 @PostMapping("/deleteSharedAddress")
	 @ResponseBody
		public String deleteSharedAddress(@RequestParam("noArr") String noArr) throws Exception {
			
			System.out.println("noArr : " + noArr);
			
			String[] deleteNoArr = noArr.split(",");
			
			System.out.println(Arrays.toString(deleteNoArr));
			
			int result = addressService.deleteSharedAddress(deleteNoArr);	

			String check = "";
			
			if( result < 0 ) {
				check = "fail";
			} else {
				check = "success";
			}
			return check; 
		}
}
