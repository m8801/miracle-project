package com.project.miracle.address.exception;

public class AddressDeleteException extends Exception{
	
	public AddressDeleteException(String msg) {
		super(msg);
	}
}
