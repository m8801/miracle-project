package com.project.miracle.address.exception;

public class AddressRegistException extends Exception{

	public AddressRegistException(String msg) {
		super(msg);
	}
}
