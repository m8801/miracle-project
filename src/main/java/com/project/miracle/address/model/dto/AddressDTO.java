package com.project.miracle.address.model.dto;

import com.project.miracle.common.model.dto.DeptDTO;
import com.project.miracle.common.model.dto.MemberDTO;

public class AddressDTO {

	private int addressNo; 	   
	private int userNo;     
	private String loginNo;
	private String groupName;  
	private DeptDTO  deptDTO;
	private MemberDTO memberDTO;
	private SharedAddressDTO sharedAddressDTO;
	
	public AddressDTO() {
		super();
		
	}

	public AddressDTO(int addressNo, int userNo, String loginNo, String groupName, DeptDTO deptDTO, MemberDTO memberDTO,
			SharedAddressDTO sharedAddressDTO) {
		super();
		this.addressNo = addressNo;
		this.userNo = userNo;
		this.loginNo = loginNo;
		this.groupName = groupName;
		this.deptDTO = deptDTO;
		this.memberDTO = memberDTO;
		this.sharedAddressDTO = sharedAddressDTO;
	}

	public int getAddressNo() {
		return addressNo;
	}

	public void setAddressNo(int addressNo) {
		this.addressNo = addressNo;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getLoginNo() {
		return loginNo;
	}

	public void setLoginNo(String loginNo) {
		this.loginNo = loginNo;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public DeptDTO getDeptDTO() {
		return deptDTO;
	}

	public void setDeptDTO(DeptDTO deptDTO) {
		this.deptDTO = deptDTO;
	}

	public MemberDTO getMemberDTO() {
		return memberDTO;
	}

	public void setMemberDTO(MemberDTO memberDTO) {
		this.memberDTO = memberDTO;
	}

	public SharedAddressDTO getSharedAddressDTO() {
		return sharedAddressDTO;
	}

	public void setSharedAddressDTO(SharedAddressDTO sharedAddressDTO) {
		this.sharedAddressDTO = sharedAddressDTO;
	}

	@Override
	public String toString() {
		return "AddressDTO [addressNo=" + addressNo + ", userNo=" + userNo + ", loginNo=" + loginNo + ", groupName="
				+ groupName + ", deptDTO=" + deptDTO + ", memberDTO=" + memberDTO + ", sharedAddressDTO="
				+ sharedAddressDTO + "]";
	}

	
	
	

	
	
	
	
	
}
