package com.project.miracle.address.model.dto;



public class SharedAddressDTO {

	private int addressCommon;       // 공유 주소록번호
	private int userNo;		     // 공유된 사원번호
	private int userPno;	     // 공유한 사원번호
	private String groupName;    // 그룹명
	private String sharer;       // 공유자
	
	public SharedAddressDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SharedAddressDTO(int addressCommon, int userNo, int userPno, String groupName, String sharer) {
		super();
		this.addressCommon = addressCommon;
		this.userNo = userNo;
		this.userPno = userPno;
		this.groupName = groupName;
		this.sharer = sharer;
	}

	public int getAddressCommon() {
		return addressCommon;
	}

	public void setAddressCommon(int addressCommon) {
		this.addressCommon = addressCommon;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public int getUserPno() {
		return userPno;
	}

	public void setUserPno(int userPno) {
		this.userPno = userPno;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getSharer() {
		return sharer;
	}

	public void setSharer(String sharer) {
		this.sharer = sharer;
	}

	@Override
	public String toString() {
		return "SharedAddressDTO [addressCommon=" + addressCommon + ", userNo=" + userNo + ", userPno=" + userPno
				+ ", groupName=" + groupName + ", sharer=" + sharer + "]";
	}

	

	

	
	
	
}
