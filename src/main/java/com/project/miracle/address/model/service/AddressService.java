package com.project.miracle.address.model.service;

import java.util.List;
import java.util.Map;

import com.project.miracle.address.exception.AddressDeleteException;
import com.project.miracle.address.exception.AddressModifyeException;
import com.project.miracle.address.exception.AddressRegistException;
import com.project.miracle.address.model.dto.AddressDTO;
import com.project.miracle.address.model.dto.MyAddressDTO;
import com.project.miracle.address.model.dto.SharedAddressDTO;
import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.common.paging.SharedSelectCriteria;

public interface AddressService {

	/* 공통  */
	MemberDTO selectOneAddress(int userNo);
	
	/* 주소록 관리자 및 회사 공용 주소록 */
	int selectTotalCount(Map<String, String> searchMap);

	List<AddressDTO> selectAddressList(SelectCriteria selectCriteria);

	void registAddress(MemberDTO user) throws AddressRegistException;

	int deleteAddress(String[] deleteNoArr) throws AddressDeleteException;
	
	List<AddressDTO> selectSharedList(Map<String, String> searchMap);

	int selectSharedCount(Map<String, String> searchMap);

	int sharingRegist(String sharer, int[] addressNoArr, int[] shareUserNoArr);

	
	/* 내주소록 */
	int registMyAddress(AddressDTO address) throws AddressRegistException;
	
	int selectMyAddressTotalCount(Map<String, Object> searchMap);

	List<AddressDTO> selectMyAddressList(SelectCriteria selectCriteria, String loginNo);
	
	int deleteMyAddress(String[] deleteNoArr) throws AddressDeleteException;

	int modifyMyAddress(AddressDTO addressDTO) throws AddressModifyeException;
	
	int mySharingRegist(String sharer, int[] addressNoArr, int[] shareUserNoArr);

	/* 공유 받은 주소록 */
	int selectSharedAddressTotalCount(Map<String, Object> searchMap);

	List<AddressDTO> selectSharedAddressList(SelectCriteria selectCriteria, String loginNo);

//	List<AddressDTO> selectSharedAddressList(SelectCriteria selectCriteria);

	int modifySharedAddress(AddressDTO addressDTO) throws AddressModifyeException;

	int deleteSharedAddress(String[] deleteNoArr) throws AddressDeleteException;


	

	
	


	



	


	

}
