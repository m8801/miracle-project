package com.project.miracle.address.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.address.exception.AddressDeleteException;
import com.project.miracle.address.exception.AddressModifyeException;
import com.project.miracle.address.exception.AddressRegistException;
import com.project.miracle.address.model.dao.AddressMapper;
import com.project.miracle.address.model.dto.AddressDTO;
import com.project.miracle.address.model.dto.MyAddressDTO;
import com.project.miracle.address.model.dto.SharedAddressDTO;
import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.SelectCriteria;

@Service
public class AddressServiceImpl implements AddressService {

	private final AddressMapper addressMapper;

	@Autowired
	public AddressServiceImpl(AddressMapper addressMapper) {
		this.addressMapper = addressMapper;
	}

	/* 주소록 관리자 및 회사 공용 주소록 */
	/**
	 * <pre>
	 * 	주소록 관리자, 회사 공용 주소록 조회 개수 메서드
	 * </pre>
	 */
	@Override
	public int selectTotalCount(Map<String, String> searchMap) {
		return addressMapper.selectTotalCount(searchMap);
	}

	/**
	 * <pre>
	 * 주소록 조회 메서드
	 * </pre>
	 */
	@Override
	public List<AddressDTO> selectAddressList(SelectCriteria selectCriteria) {

		return addressMapper.selectAddressList(selectCriteria);
	}

	/**
	 * <pre>
	 * 	사원 조회 메서드
	 * </pre>
	 * 
	 * @throws AddressRegistSelectException
	 */
	@Override
	public MemberDTO selectOneAddress(int userNo) {
		MemberDTO user = addressMapper.selectOneAddress(userNo);

		return user;
	}

	/**
	 * <pre>
	 * 	사원 등록 메서드
	 * &#64;param 사원 정보
	 * </pre>
	 * 
	 * @throws AddressRegistException
	 */
	@Override
	public void registAddress(MemberDTO user) throws AddressRegistException {

		int result = addressMapper.insertAddress(user);

		if (!(result > 0)) {
			throw new AddressRegistException("주소록 둥록에 실패했습니다.");
		}

	}

	/**
	 * <pre>
	 *  주소록 삭제 메서드
	 * </pre>
	 * 
	 * @param 삭제할 주소록 번호 배열
	 * @throws AddressDeleteException
	 */
	@Override
	public int deleteAddress(String[] deleteNoArr) throws AddressDeleteException {
		int result = 0;
		for (int i = 0; i < deleteNoArr.length; i++) {
			result += addressMapper.deleteAddress(deleteNoArr[i]);
		}
		if (!(result > 0)) {
			throw new AddressDeleteException("주소록 삭제에 실패하셨습니다.");
		}
		System.out.println(result);
		
		return result;

	}

	/**
	 * <pre>
	 *	주소록 공유 내 개수 조회
	 * </pre>
	 */
	@Override
	public int selectSharedCount(Map<String, String> searchMap) {
		int count = addressMapper.selectSharedCount(searchMap);
		return count;
	}

	/**
	 * <pre>
	 *	주소록 공유를 위한 사원 조회
	 * </pre>
	 */
	@Override
	public List<AddressDTO> selectSharedList(Map<String, String> searchMap) {
		return addressMapper.selectSharedList(searchMap);
	}

	/**
	 * <pre>
	 *	주소록관리자에서 주소록 공유 등록 메서드
	 * </pre>
	 * 
	 * @param sharer 공유자 , shareNoArr 주소록 사원번호 , userNoArr 보낼 사원의 사원번호
	 */
	@Override
	public int sharingRegist(String sharer, int[] shareUserNoArr, int[] addressNoArr) {

		int result = 0;

		for (int i = 0; i < addressNoArr.length; i++) {
			SharedAddressDTO shareAddress = new SharedAddressDTO();
			shareAddress.setSharer(sharer);
			shareAddress.setUserNo(addressNoArr[i]);
			for (int j = 0; j < shareUserNoArr.length; j++) {
				shareAddress.setUserPno(shareUserNoArr[j]);
				result += addressMapper.sharingRegist(shareAddress);
			}
		}



		return result;
	}

	/* 내 주소록 */
	/**
	 * <pre>
	 * 	내 주소록 등록 메서드
	 *  @param user 사원 정보
	 * </pre>
	 * 
	 * @throws AddressRegistException
	 */
	@Override
	public int registMyAddress(AddressDTO address) throws AddressRegistException {
		int result = addressMapper.insertMyAddress(address);

		if (!(result > 0)) {
			throw new AddressRegistException("주소록 둥록에 실패했습니다.");
		} else {
			return result;
		}
	}

	/**
	 * <pre>
	 * 	내 주소록 조회 개수 메서드
	 * </pre>
	 * 
	 * @param 내 주소록 조회할 주소록 수
	 * 
	 */
	@Override
	public int selectMyAddressTotalCount(Map<String, Object> searchMap) {

		return addressMapper.selectMyAddressCount(searchMap);
	}

	/**
	 * <pre>
	 * 	내 주소록 조회 메서드
	 * </pre>
	 * 
	 * @param selectCriteria 검색기준
	 */
	@Override
	public List<AddressDTO> selectMyAddressList(SelectCriteria selectCriteria, String loginNo) {
		List<AddressDTO> addressList = new ArrayList<>();
		Map<String,Object> map = new HashMap<>();
		map.put("selectCriteria", selectCriteria);
		map.put("loginNo", loginNo);
		addressList =  addressMapper.selectMyAddressList(map);
		
		System.out.println("addressList : " + addressList);
		
		return addressList;
	}

	/**
	 * <pre>
	 *  내 주소록 삭제 메서드
	 * </pre>
	 * 
	 * @param deleteNoArr 삭제할 주소록 번호 배열
	 * @throws AddressDeleteException
	 */
	@Override
	public int deleteMyAddress(String[] deleteNoArr) throws AddressDeleteException {
		int result = 0;
		for (int i = 0; i < deleteNoArr.length; i++) {
			result += addressMapper.deleteMyAddress(deleteNoArr[i]);
		}
		if (!(result > 0)) {
			throw new AddressDeleteException("주소록 삭제에 실패하셨습니다.");
		}
		System.out.println(result);

		return result;
	}

	/**
	 * <pre>
	 * 	내 주소록 수정 메서드
	 * </pre>
	 * 
	 * @param addressDTO 수정할 주소록
	 * @throws AddressModifyeException
	 */
	@Override
	public int modifyMyAddress(AddressDTO addressDTO) throws AddressModifyeException {
		int result = 0;
		result = addressMapper.modifyMyAddress(addressDTO);
		if (!(result > 0)) {
			throw new AddressModifyeException("주소록 수정에 실패하셨습니다.");
		}
		return result;
	}

	/**
	 * <pre>
	 *	내 주소록에서 주소록 공유 등록 메서드
	 *	&#64;param sharer 공유자 , shareNoArr 주소록 사원번호 , userNoArr 보낼 사원의 사원번호
	 * </pre>
	 */
	@Override
	public int mySharingRegist(String sharer, int[] shareNoArr, int[] userNoArr) {

		int result = 0;

		for (int i = 0; i < userNoArr.length; i++) {
			SharedAddressDTO shareAddress = new SharedAddressDTO();
			shareAddress.setSharer(sharer);
			shareAddress.setUserNo(userNoArr[i]);
			for (int j = 0; j < shareNoArr.length; j++) {
				shareAddress.setUserPno(shareNoArr[j]);
				result += addressMapper.sharingRegist(shareAddress);
			}
		}
		return result;
	}

	/**
	 * <pre>
	 * 	공유 받은 주소록
	 * </pre>
	 * 
	 * @param 공유 받은 주소록 조회 주소록 수
	 */
	@Override
	public int selectSharedAddressTotalCount(Map<String, Object> searchMap) {

		return addressMapper.selectSharedAddressCount(searchMap);
	}

	/**
	 * <pre>
	 * 	공유받은 주소록 주소록 조회 메서드
	 * </pre>
	 * 
	 * @param selectCriteria 검색기준
	 */
	@Override
	public List<AddressDTO> selectSharedAddressList(SelectCriteria selectCriteria, String loginNo) {
		List<AddressDTO> addressList = new ArrayList<>();
		Map<String,Object> map = new HashMap<>();
		map.put("selectCriteria", selectCriteria);
		map.put("loginNo", loginNo);
		addressList =  addressMapper.selectSharedAddressList(map);
		
		System.out.println("addressList : " + addressList);
		
		return addressList;
	}

	
	/**
	 * <pre>
	 * 	공유 받은 주소록 수정 메서드
	 * </pre>
	 * 
	 * @param addressDTO 수정할 주소록
	 * @throws AddressModifyeException
	 */
	@Override
	public int modifySharedAddress(AddressDTO addressDTO) throws AddressModifyeException{
		int result = 0;
		result = addressMapper.modifySharedAddress(addressDTO);
		if (!(result > 0)) {
			throw new AddressModifyeException("주소록 수정에 실패하셨습니다.");
		}
		return result;	
	}

	/**
	 * <pre>
	 *  주소록 삭제 메서드
	 * </pre>
	 * 
	 * @param 삭제할 주소록 번호 배열
	 * @throws AddressDeleteException
	 */
	@Override
	public int deleteSharedAddress(String[] deleteNoArr) throws AddressDeleteException{
		int result = 0;
		for (int i = 0; i < deleteNoArr.length; i++) {
			result += addressMapper.deleteSharedAddress(deleteNoArr[i]);
		}
		if (!(result > 0)) {
			throw new AddressDeleteException("주소록 삭제에 실패하셨습니다.");
		}
		System.out.println(result);
		return result;
	}



	
}
