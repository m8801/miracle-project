package com.project.miracle.address.model.dao;

import java.util.List;
import java.util.Map;

import com.project.miracle.address.model.dto.AddressDTO;
import com.project.miracle.address.model.dto.MyAddressDTO;
import com.project.miracle.address.model.dto.SharedAddressDTO;
import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.SelectCriteria;

public interface AddressMapper {

	/* 공통  */
	MemberDTO selectOneAddress(int userNo);
	
	/* 주소록 관리자 및 회사 공용 주소록 */
	int selectTotalCount(Map<String, String> searchMap);

	List<AddressDTO> selectAddressList(SelectCriteria selectCriteria);

	int insertAddress(MemberDTO user);

	int deleteAddress(String string);

	int selectSharedCount(Map<String, String> searchMap);

	List<AddressDTO> selectSharedList(Map<String, String> searchMap);

	int sharingRegist(SharedAddressDTO shareList);
	
	/* 내주소록 */
	int insertMyAddress(AddressDTO address);

	int selectMyAddressCount(Map<String, Object> searchMap);

	List<AddressDTO> selectMyAddressList(Map<String, Object> map);

	int deleteMyAddress(String string);

	int modifyMyAddress(AddressDTO modifyAddress);

	/* 공유 받은 주소록 */
	int selectSharedAddressCount(Map<String, Object> searchMap);

	List<AddressDTO> selectSharedAddressList(Map<String, Object> map);

	int checkUserCode(int sharedUserCode);

	int modifySharedAddress(AddressDTO addressDTO);

	int deleteSharedAddress(String string);





	

}
