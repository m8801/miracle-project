package com.project.miracle.address.model.dto;

import java.util.List;

public class MyAddressDTO {

 
	private int loginNo; 
	private AddressDTO addressDTO;
	
	
	public MyAddressDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public MyAddressDTO(int loginNo, AddressDTO addressDTO) {
		super();
		this.loginNo = loginNo;
		this.addressDTO = addressDTO;
	}


	public int getLoginNo() {
		return loginNo;
	}


	public void setLoginNo(int loginNo) {
		this.loginNo = loginNo;
	}


	public AddressDTO getAddressDTO() {
		return addressDTO;
	}


	public void setAddressDTO(AddressDTO addressDTO) {
		this.addressDTO = addressDTO;
	}


	@Override
	public String toString() {
		return "MyAddressDTO [loginNo=" + loginNo + ", addressDTO=" + addressDTO + "]";
	}
	
	
	
}
