package com.project.miracle.jobBoard.service;

import java.util.List;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.jobBoard.model.dto.JobReplyDTO;

public interface JobBoardService {

	int selectTotalCount();

	List<JobBoardDTO> selectAllJobBoardList(SelectCriteria selectCriteria);

	JobBoardDTO selectJobBoardDeatil(int no);

	List<JobReplyDTO> selectAllJobReplyList(int no);

	void registJobBoard(JobBoardDTO jobBoardDTO);

	void modifyJobBoard(JobBoardDTO jobBoardDTO, String preFile);

	void removeJobBoard(int boardNo);

	List<JobReplyDTO> registReply(JobReplyDTO jobReplyDTO);

	List<JobReplyDTO> removeReply(JobReplyDTO jobReplyDTO);

}
