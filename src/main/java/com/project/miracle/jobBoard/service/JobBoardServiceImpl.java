package com.project.miracle.jobBoard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.ReplyDTO;
import com.project.miracle.jobBoard.model.dao.JobBoardMapper;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.jobBoard.model.dto.JobReplyDTO;

@Service
public class JobBoardServiceImpl implements JobBoardService{
	
	
	private JobBoardMapper mapper;
	
	@Autowired
	public JobBoardServiceImpl(JobBoardMapper mapper) {
		this.mapper = mapper;
	}

	/**
	 * <pre>
	 * 전체 게시물 수 조회용 메소드
	 * </pre>
	 *
	 */
	@Override
	public int selectTotalCount() {
		
		return mapper.selectTotalCount();
	}

	/**
	 * <pre>
	 * 	사내 게시판 전체 리스트 조회용 메소드 
	 * </pre>
	 *
	 */
	@Override
	public List<JobBoardDTO> selectAllJobBoardList(SelectCriteria selectCriteria) {

		List<JobBoardDTO> jobBoardList = mapper.selectAllJobBoardList(selectCriteria);
		
		return jobBoardList;
	}

	/**
	 *<pre>
	 *	사내 게시판 상세보기 메소드
	 *</pre>
	 */
	@Override
	public JobBoardDTO selectJobBoardDeatil(int no) {

		JobBoardDTO jobBoardDetail = null;
		
		jobBoardDetail = mapper.selectJobBoardDetail(no);
		
		return jobBoardDetail;
	}

	/**
	 *<pre>
	 *	사내 게시판 댓글 조회용 메소드
	 *</pre>
	 */
	@Override
	public List<JobReplyDTO> selectAllJobReplyList(int no) {

		List<JobReplyDTO> replyList = null;
		
		replyList = mapper.selectJobReplyList(no);
		
		return replyList;
	}

	/**
	 *<pre>
	 *	사내 게시판 글 등록용 메소드
	 *</pre>
	 */
	@Override
	public void registJobBoard(JobBoardDTO jobBoardDTO) {
		
		int result = mapper.insertJobBoard(jobBoardDTO);
		
		if(result > 0 && jobBoardDTO.getAttachmentDTO() != null) {
			int attachmentResult = mapper.insertAttachment(jobBoardDTO.getAttachmentDTO());
		}
	}
	
	/**
	 *<pre>
	 *	사내 게시판 글 수정용 메소드 
	 *</pre>
	 */
	@Override
	public void modifyJobBoard(JobBoardDTO jobBoardDTO, String preFile) {
		
		JobBoardDTO selectResult = mapper.selectJobBoardDetail(jobBoardDTO.getBoardNo());
		
		jobBoardDTO.setBoardNo(selectResult.getBoardNo());
		int modifyResult = mapper.updateJobBoard(jobBoardDTO);

		int attachmentResult = 0;	
		
		if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && preFile != null) {
//			attachmentResult = mapper.updateAttachment(sharingBoardDTO.getAttachmentDTO());
			jobBoardDTO = mapper.selectJobBoardDetail(selectResult.getBoardNo());
		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() == null && jobBoardDTO.getAttachmentDTO() != null) {
			attachmentResult = mapper.insertAttach(jobBoardDTO.getAttachmentDTO());
		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && jobBoardDTO.getAttachmentDTO() == null){
			attachmentResult = mapper.removeAttachment(jobBoardDTO.getBoardNo());
		}else if(modifyResult > 0 && selectResult.getAttachmentDTO().getSaveName() != null && jobBoardDTO.getAttachmentDTO() != null) {
			attachmentResult = mapper.updateAttachment(jobBoardDTO.getAttachmentDTO());
		}
	
		
	}

	/**
	 *<pre>
	 * 공유 게시판 글삭제 메소드
	 *</pre>
	 */
	@Override
	public void removeJobBoard(int boardNo) {
		
		int replyResult = mapper.removeReplyment(boardNo);
		
		int attachResult = mapper.removeAttachment(boardNo);
		
		int result = mapper.removeJobBoard(boardNo);
	}

	/**
	 *<pre>
	 *	사내 게시판 댓글 작성용 메소드
	 *</pre>
	 */
	@Override
	public List<JobReplyDTO> registReply(JobReplyDTO jobReplyDTO) {
		
		List<JobReplyDTO> replyList = null;
		
		int result = mapper.insertReply(jobReplyDTO);
		
		if(result > 0) {
			replyList = mapper.selectJobReplyList(jobReplyDTO.getBoardNo());
		}
		
		return replyList;
	}

	/**
	 *<pre>
	 *	사내 게시판 댓글 삭제용 메소드
	 *</pre>
	 */
	@Override
	public List<JobReplyDTO> removeReply(JobReplyDTO jobReplyDTO) {
		
		List<JobReplyDTO> replyList = null;
		
		int result  = mapper.removeReply(jobReplyDTO.getReplyNo());
		
		if(result > 0 ) {
			replyList = mapper.selectJobReplyList(jobReplyDTO.getBoardNo());
		}
		
		return replyList;
		
	}
	


	

	
	
}
