package com.project.miracle.jobBoard.model.dao;

import java.util.List;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.jobBoard.model.dto.JobAttachmentDTO;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.jobBoard.model.dto.JobReplyDTO;

public interface JobBoardMapper {

	int selectTotalCount();

	List<JobBoardDTO> selectAllJobBoardList(SelectCriteria selectCriteria);

	JobBoardDTO selectJobBoardDetail(int no);

	List<JobReplyDTO> selectJobReplyList(int no);

	int insertJobBoard(JobBoardDTO jobBoardDTO);

	int insertAttachment(JobAttachmentDTO attachmentDTO);

	int updateJobBoard(JobBoardDTO jobBoardDTO);

	int insertAttach(JobAttachmentDTO attachmentDTO);

	int removeAttachment(int boardNo);

	int updateAttachment(JobAttachmentDTO attachmentDTO);

	int removeReplyment(int boardNo);

	int removeJobBoard(int boardNo);

	int insertReply(JobReplyDTO jobReplyDTO);

	int removeReply(int replyNo);


}
