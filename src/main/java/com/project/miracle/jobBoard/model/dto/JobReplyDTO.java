package com.project.miracle.jobBoard.model.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.project.miracle.common.model.dto.MemberDTO;

public class JobReplyDTO {

	
	private int boardNo;
	private int replyNo;
	private String comments;
	private int userNo;
	private MemberDTO writer;
	private String delYn;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
	private Date createDate;
	
	
	public JobReplyDTO() {}

	public JobReplyDTO(int boardNo, int replyNo, String comments, int userNo, MemberDTO writer, String delYn,
			Date createDate) {
		super();
		this.boardNo = boardNo;
		this.replyNo = replyNo;
		this.comments = comments;
		this.userNo = userNo;
		this.writer = writer;
		this.delYn = delYn;
		this.createDate = createDate;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public int getReplyNo() {
		return replyNo;
	}

	public void setReplyNo(int replyNo) {
		this.replyNo = replyNo;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public MemberDTO getWriter() {
		return writer;
	}

	public void setWriter(MemberDTO writer) {
		this.writer = writer;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "JobReplyDTO [boardNo=" + boardNo + ", replyNo=" + replyNo + ", comments=" + comments + ", userNo="
				+ userNo + ", writer=" + writer + ", delYn=" + delYn + ", createDate=" + createDate + "]";
	}
	
	
	
}
