package com.project.miracle.jobBoard.controller;

import java.io.File;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.jobBoard.model.dto.JobAttachmentDTO;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.jobBoard.model.dto.JobReplyDTO;
import com.project.miracle.jobBoard.service.JobBoardService;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.sharingBoard.model.dto.SharingAttachmentDTO;
import com.project.miracle.sharingBoard.model.dto.SharingBoardDTO;
import com.project.miracle.sharingBoard.model.dto.SharingReplyDTO;

@Controller
@RequestMapping("/board")
public class JobBoardController {
	
private JobBoardService jobBoardService;
	
	public JobBoardController(JobBoardService jobBoardService) {
		this.jobBoardService = jobBoardService;
	}
	
	@GetMapping("/jobList")
	public ModelAndView jobBoardList(@RequestParam (defaultValue = "1") int currentPage , ModelAndView mv, HttpServletRequest request) {
		
		int totalCount = jobBoardService.selectTotalCount();
		
		int limit = 5;
		int buttonAmount = 5;
		
		SelectCriteria selectCriteria = null;
		
		selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		
		List<JobBoardDTO> jobBoardList = jobBoardService.selectAllJobBoardList(selectCriteria);
		
		mv.addObject("jobBoardList", jobBoardList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("/board/jobBoard");
		
		return mv;
	}
	
	@GetMapping("jobDetail")
	public String selectJobBoardDetail(@RequestParam int no, Model model, HttpServletRequest request) {
		
		JobBoardDTO jobBoardDetail = jobBoardService.selectJobBoardDeatil(no);
		
		model.addAttribute("jobBoardDetail", jobBoardDetail);
		
		List<JobReplyDTO> replyList = jobBoardService.selectAllJobReplyList(no);
		
		model.addAttribute("replyList", replyList);
		
		return "/board/jobBoardDetail";
	}
	
	@GetMapping("/jobBoard-regist")
	public void registJobBoard() {}
	
	@PostMapping("/jobBoard-regist")
	public String registJobBoard(@ModelAttribute JobBoardDTO jobBoardDTO, HttpServletRequest request,
			RedirectAttributes rttr, @RequestParam(required = false) MultipartFile file, @RequestParam(value="anonymous", required = false) String anonymous) {

		int MemberNo =((LoginDTO) request.getSession().getAttribute("loginMember")).getUserNo();
		String MemberName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		
		jobBoardDTO.setUserNo(MemberNo);
		jobBoardDTO.setUserName(MemberName);
		jobBoardDTO.setFinalName(MemberName);
		if(anonymous != null) {
			jobBoardDTO.setAnonymousYn("Y");
		}else {
			jobBoardDTO.setAnonymousYn("N");
		}
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		File mkdir = new File(filePath);
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		if(!file.isEmpty()) {
			
			String originFileName = file.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String saveName = UUID.randomUUID().toString().replace("-","") + ext;

			JobAttachmentDTO fileInfo = new JobAttachmentDTO();
			try {
				
				fileInfo.setOriginName(originFileName);
				fileInfo.setSaveName(saveName);
				fileInfo.setSavePath(filePath);
				fileInfo.setFileType(ext);
			
				file.transferTo(new File(filePath + "\\" + fileInfo.getSaveName()));
			
			} catch(Exception e) {
				e.printStackTrace();
				
				new File(filePath + "\\" + fileInfo.getSaveName()).delete();
			}
				
			jobBoardDTO.setAttachmentDTO(fileInfo);

			
		}	
		
		jobBoardService.registJobBoard(jobBoardDTO);
		
		rttr.addFlashAttribute("message", "글등록이 완료되었습니다. ");
		
		return "redirect:/board/jobList";
	}
	
	
	@GetMapping("/jobUpdate")
	public String modifyJobBoard(@RequestParam int boardNo, Model model) {
		
		JobBoardDTO jobBoardDetail = jobBoardService.selectJobBoardDeatil(boardNo);
		
		model.addAttribute("jobBoardDetail", jobBoardDetail);
		
		return "/board/jobBoard-update";
	}
	
	@PostMapping("/jobBoard-update")
	public String modifyJobBoard(@ModelAttribute JobBoardDTO jobBoardDTO, 
			RedirectAttributes rttr, HttpServletRequest request,@RequestParam MultipartFile file, 
			@RequestParam(value="anonymous", required = false) String anonymous, 
			@RequestParam(value = "fileSaveName", required = false)String fileSaveName, @RequestParam(value = "preFile", required = false)String preFile) {

		if(anonymous != null) {
			jobBoardDTO.setAnonymousYn("Y");
		}else {
			jobBoardDTO.setAnonymousYn("N");
		}
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		
		File mkdir = new File(filePath);
		
		if(!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		if(!file.isEmpty()) {
			
			String originFileName = file.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String saveName = UUID.randomUUID().toString().replace("-","") + ext;
			
			JobAttachmentDTO fileInfo = new JobAttachmentDTO();
			try {
				
				fileInfo.setBoardNo(jobBoardDTO.getBoardNo());
				fileInfo.setOriginName(originFileName);
				fileInfo.setSaveName(saveName);
				fileInfo.setSavePath(filePath);
				fileInfo.setFileType(ext);
			
				file.transferTo(new File(filePath + "\\" + fileInfo.getSaveName()));

			} catch(Exception e) {
				e.printStackTrace();
				
				new File(filePath + "\\" + fileInfo.getSaveName()).delete();
			}
			
			jobBoardDTO.setAttachmentDTO(fileInfo);
		} 
		
		if(!fileSaveName.isEmpty() && !file.isEmpty() || !fileSaveName.isEmpty() && file.isEmpty()) {
			
			File oldFile = new File(filePath + "\\" + fileSaveName);
			if(oldFile.isFile()) {
				oldFile.delete();
			}
		}
		
		jobBoardService.modifyJobBoard(jobBoardDTO, preFile);
		
		rttr.addFlashAttribute("message", "글수정이 완료되었습니다. ");
		
		return "redirect:/board/jobList";
	}
	
	@GetMapping("/jobDelete")
	public String removeJobBoard(@RequestParam int boardNo, RedirectAttributes rttr, HttpServletRequest request) {
		
		JobBoardDTO jobBoardDetail = jobBoardService.selectJobBoardDeatil(boardNo);
		
		jobBoardService.removeJobBoard(boardNo);
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		String filePath = root + "\\uploadFiles";
		File file = new File(filePath + "\\" + jobBoardDetail.getAttachmentDTO().getSaveName());
		if(file.isFile()) {
			file.delete();
		}

		rttr.addFlashAttribute("message", "게시글이 삭제되었습니다.");
		
		return "redirect:/board/jobList";
	}
	
	@PostMapping(value="/registJobReply", produces = "application/json; charset=utf-8")
	@ResponseBody
	public List<JobReplyDTO> registReply(@ModelAttribute JobReplyDTO jobReplyDTO, HttpServletRequest request){
		
		jobReplyDTO.setUserNo(((LoginDTO)request.getSession().getAttribute("loginMember")).getUserNo());
		
		List<JobReplyDTO> replyList = jobBoardService.registReply(jobReplyDTO);
		
		return replyList;
	}
	
	
	@PostMapping(value = "/removeJobReply", produces = "applcation/json; charset=UTF-8")
	@ResponseBody
	public List<JobReplyDTO> removeReply(@ModelAttribute JobReplyDTO jobReplyDTO, HttpServletRequest request){
		
		List<JobReplyDTO> replyList = jobBoardService.removeReply(jobReplyDTO);
		
		return replyList;
	}
	
}
