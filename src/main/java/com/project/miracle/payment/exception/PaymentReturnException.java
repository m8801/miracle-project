package com.project.miracle.payment.exception;

public class PaymentReturnException extends Exception {

	public PaymentReturnException(String message) {
		super(message);
	}

	
}
