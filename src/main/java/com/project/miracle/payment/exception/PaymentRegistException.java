package com.project.miracle.payment.exception;

public class PaymentRegistException extends Exception{

	public PaymentRegistException(String message) {
		super(message);
	}
	
	
}
