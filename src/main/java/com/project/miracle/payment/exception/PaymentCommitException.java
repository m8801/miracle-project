package com.project.miracle.payment.exception;

public class PaymentCommitException extends Exception{

	public PaymentCommitException(String message) {
		super(message);
	}
	
	
}
