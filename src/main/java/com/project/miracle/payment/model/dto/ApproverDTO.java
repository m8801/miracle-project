package com.project.miracle.payment.model.dto;

import com.project.miracle.common.model.dto.DeptDTO;
import com.project.miracle.common.model.dto.JobDTO;
import com.project.miracle.common.model.dto.MemberDTO;

public class ApproverDTO {

	private int userNo;
	private String approverName;
	
	private MemberDTO memberDTO;
	private DeptDTO deptDTO;
	private JobDTO jobDTO;
	
	public ApproverDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ApproverDTO(int userNo, String approverName, MemberDTO memberDTO, DeptDTO deptDTO, JobDTO jobDTO) {
		super();
		this.userNo = userNo;
		this.approverName = approverName;
		this.memberDTO = memberDTO;
		this.deptDTO = deptDTO;
		this.jobDTO = jobDTO;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public MemberDTO getMemberDTO() {
		return memberDTO;
	}

	public void setMemberDTO(MemberDTO memberDTO) {
		this.memberDTO = memberDTO;
	}

	public DeptDTO getDeptDTO() {
		return deptDTO;
	}

	public void setDeptDTO(DeptDTO deptDTO) {
		this.deptDTO = deptDTO;
	}

	public JobDTO getJobDTO() {
		return jobDTO;
	}

	public void setJobDTO(JobDTO jobDTO) {
		this.jobDTO = jobDTO;
	}

	@Override
	public String toString() {
		return "ApproverDTO [userNo=" + userNo + ", approverName=" + approverName + ", memberDTO=" + memberDTO
				+ ", deptDTO=" + deptDTO + ", jobDTO=" + jobDTO + "]";
	}
	
	
	

	

	
	
	
}
