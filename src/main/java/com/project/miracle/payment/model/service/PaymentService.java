package com.project.miracle.payment.model.service;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.PaymentCriteria;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.payment.exception.PaymentCommitException;
import com.project.miracle.payment.exception.PaymentRegistException;
import com.project.miracle.payment.exception.PaymentReturnException;
import com.project.miracle.payment.model.dto.ApproverDTO;
import com.project.miracle.payment.model.dto.PaymentReportDTO;
import com.project.miracle.payment.model.dto.PaymentVacationDTO;
import com.project.miracle.payment.model.dto.ReportDTO;
import com.project.miracle.payment.model.dto.VacationDTO;

public interface PaymentService {

//	int selectTotalCount(Map<String, String> searchMap);

	List<PaymentReportDTO> selectReportList(PaymentCriteria selectReportCriteria);

	List<PaymentVacationDTO> selectVacationList(PaymentCriteria selectVacationCriteria);

	int selectReportCount(Map<String, String> searchMap);

	int selectVacatoinCount(Map<String, String> searchMap);
	
	// 사용자 업무 보고서 관련 메서드
	ApproverDTO basicInfo(ReportDTO reportDTO);
	
	ApproverDTO managerBasicInfo(ReportDTO reportDTO);

	int reportRegist(ReportDTO reportDTO) throws PaymentRegistException;

	PaymentReportDTO selecReportInfo(String data);
	
	// 휴가 신청서  관련 메서드
	ApproverDTO basicInfo(VacationDTO vacationDTO);
	
	ApproverDTO managerBasicInfo(VacationDTO vacationDTO);

	int vacationRegist(VacationDTO vacationDTO);

	PaymentVacationDTO selecVacationInfo(String data);

	int commitReport(int paymentNo) throws PaymentCommitException;

	int returnReport(int paymentNo) throws PaymentReturnException;

	
	// 숫자 카운트
	int selectReportWaitingCount();

	int selectVacationWaitingCount();

	int selectReportCompleteCount();

	int selectVacationCompleteCount();

	int selectReportRejectCount();

	int selectVacationRejectCount();

//	int reportUserRegist(ReportDTO reportDTO);


}
