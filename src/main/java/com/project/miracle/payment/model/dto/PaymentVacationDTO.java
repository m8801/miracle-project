package com.project.miracle.payment.model.dto;

public class PaymentVacationDTO {

	private int paymentNo;
	private String dueForm;
	private VacationDTO vacationDTO;
	private String paymentStatus;
	
	public PaymentVacationDTO() {
		super();
		
	}
	
	public PaymentVacationDTO(int paymentNo, String dueForm, VacationDTO vacationDTO, String paymentStatus) {
		super();
		this.paymentNo = paymentNo;
		this.dueForm = dueForm;
		this.vacationDTO = vacationDTO;
		this.paymentStatus = paymentStatus;
	}

	public int getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(int paymentNo) {
		this.paymentNo = paymentNo;
	}

	public String getDueForm() {
		return dueForm;
	}

	public void setDueForm(String dueForm) {
		this.dueForm = dueForm;
	}

	public VacationDTO getVacationDTO() {
		return vacationDTO;
	}

	public void setVacationDTO(VacationDTO vacationDTO) {
		this.vacationDTO = vacationDTO;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Override
	public String toString() {
		return "PaymentVacationDTO [paymentNo=" + paymentNo + ", dueForm=" + dueForm + ", vacationDTO=" + vacationDTO
				+ ", paymentStatus=" + paymentStatus + "]";
	}

	
	
	
}
