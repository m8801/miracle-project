package com.project.miracle.payment.model.dto;


public class PaymentReportDTO {
	
	private int paymentNo;
	private String dueForm;
	private ReportDTO reportDTO;
	private String paymentStatus;
	
	
	public PaymentReportDTO() {
		super();
	}


	public PaymentReportDTO(int paymentNo, String dueForm, ReportDTO reportDTO, String paymentStatus) {
		super();
		this.paymentNo = paymentNo;
		this.dueForm = dueForm;
		this.reportDTO = reportDTO;
		this.paymentStatus = paymentStatus;
	}


	public int getPaymentNo() {
		return paymentNo;
	}


	public void setPaymentNo(int paymentNo) {
		this.paymentNo = paymentNo;
	}


	public String getDueForm() {
		return dueForm;
	}


	public void setDueForm(String dueForm) {
		this.dueForm = dueForm;
	}


	public ReportDTO getReportDTO() {
		return reportDTO;
	}


	public void setReportDTO(ReportDTO reportDTO) {
		this.reportDTO = reportDTO;
	}


	public String getPaymentStatus() {
		return paymentStatus;
	}


	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}


	@Override
	public String toString() {
		return "PaymentReportDTO [paymentNo=" + paymentNo + ", dueForm=" + dueForm + ", reportDTO=" + reportDTO
				+ ", paymentStatus=" + paymentStatus + "]";
	}
	
	
}
