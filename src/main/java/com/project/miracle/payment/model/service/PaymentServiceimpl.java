package com.project.miracle.payment.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.project.miracle.common.paging.PaymentCriteria;
import com.project.miracle.payment.exception.PaymentCommitException;
import com.project.miracle.payment.exception.PaymentRegistException;
import com.project.miracle.payment.exception.PaymentReturnException;
import com.project.miracle.payment.model.dao.PaymentMapper;
import com.project.miracle.payment.model.dto.ApproverDTO;
import com.project.miracle.payment.model.dto.PaymentReportDTO;
import com.project.miracle.payment.model.dto.PaymentVacationDTO;
import com.project.miracle.payment.model.dto.ReportDTO;
import com.project.miracle.payment.model.dto.VacationDTO;

@Service
public class PaymentServiceimpl implements PaymentService{

	private final PaymentMapper paymentMapper;
	
	
	public PaymentServiceimpl(PaymentMapper paymentMapper) {
		this.paymentMapper = paymentMapper;
	}


//	/**
//	 * <pre>
//	 * 	결재문서함 조회 개수
//	 * </pre>
//	 * @param searchMap 조회 조건
//	 */
//	@Override
//	public int selectTotalCount(Map<String, String> searchMap) {
//		return paymentMapper.selectTotalCount(searchMap);
//	}


	/**
	 * <pre>
	 * 	결재문서함 보고서 조회
	 * </pre>
	 */
	@Override
	public List<PaymentReportDTO> selectReportList(PaymentCriteria selectReportCriteria) {
		
		return paymentMapper.selectReportList(selectReportCriteria);
	}

	/**
	 * <pre>
	 * 	결재문서함 휴가 신청서 조회
	 * </pre>
	 */
	@Override
	public List<PaymentVacationDTO> selectVacationList(PaymentCriteria selectVacationCriteria) {
		
		return paymentMapper.selectVacationList(selectVacationCriteria);
	}

	
	/**
	 * <pre>
	 * 	결재문서함 보고서 개수 조회
	 * </pre>
	 */
	@Override
	public int selectReportCount(Map<String, String> searchMap) {
		return paymentMapper.selectReportCount(searchMap);
	}


	/**
	 * <pre>
	 * 	결재문서함 휴가 신청서 개수 조회
	 * </pre>
	 */
	@Override
	public int selectVacatoinCount(Map<String, String> searchMap) {
		return paymentMapper.selectVacationCount(searchMap);
	}

	
	/**
	 * <pre>
	 * 	결재문서함 결재자 조회
	 * </pre>
	 */
	@Override
	public ApproverDTO basicInfo(ReportDTO reportDTO) {
		return paymentMapper.selectApproverInfoReport(reportDTO);
	}


	/**
	 * <pre>
	 * 	결재문서함 보고서 등록
	 * </pre>
	 */
	@Override
	public int reportRegist(ReportDTO reportDTO) throws PaymentRegistException{
		int reportResult = 0;
		int officialResult = 0;
		int paymentResult = 0;
		reportResult = paymentMapper.insertReport(reportDTO);
		if( reportResult == 1) {
			officialResult = paymentMapper.insertOfficialReport(reportDTO);
			if(  officialResult == 1) {
				paymentResult = paymentMapper.insertPaymentReport(reportDTO);
			}
		}
		return reportResult; 
	}

	/**
	 * <pre>
	 * 	결재문서함 보고서 보기
	 * </pre>
	 */
	@Override
	public PaymentReportDTO selecReportInfo(String data) {
		
		return paymentMapper.selectReportInfo(data);
	}

	/**
	 * <pre>
	 * 	결재문서함 보고서 결재자 조회
	 * </pre>
	 */
	@Override
	public ApproverDTO basicInfo(VacationDTO vacationDTO) {
		return paymentMapper.selectApproverInfoVacation(vacationDTO);
	}

	/**
	 * <pre>
	 * 	결재문서함 휴가신청서 등록
	 * </pre>
	 */
	@Override
	public int vacationRegist(VacationDTO vacationDTO) {
		int vacationResult = 0;
		int officialResult = 0;
		int paymentResult = 0;
		vacationResult = paymentMapper.insertVacation(vacationDTO);
		if( vacationResult == 1) {
			officialResult = paymentMapper.insertOfficialVacation(vacationDTO);
			if(  officialResult == 1) {
				paymentResult = paymentMapper.insertPaymentVacation(vacationDTO);
			}
		}
		return vacationResult; 
	}

	/**
	 * <pre>
	 * 	결재문서함 휴가 신청서 보기
	 * </pre>
	 */
	@Override
	public PaymentVacationDTO selecVacationInfo(String data) {
		return paymentMapper.selectVacationInfo(data);

	}


	@Override
	public int commitReport(int paymentNo) throws PaymentCommitException {
		int commitResult = paymentMapper.modifyReportCommitStatus(paymentNo);
		if( !(commitResult > 0)) {
			throw new PaymentCommitException("승인에 실패하셨습니다.");
		}
		return commitResult;
	}


	@Override
	public int returnReport(int paymentNo) throws PaymentReturnException {
		int returnResult = paymentMapper.modifyReportReturnStatus(paymentNo);
		if( !(returnResult > 0)) {
			throw new PaymentReturnException("승인에 실패하셨습니다.");
		}
		return returnResult;
	}


	@Override
	public ApproverDTO managerBasicInfo(ReportDTO reportDTO) {
		return paymentMapper.selectManagerApproverInfoReport(reportDTO);
	}


	@Override
	public ApproverDTO managerBasicInfo(VacationDTO vacationDTO) {
		return paymentMapper.selectManagerApproverInfoVacation(vacationDTO);
	}


	
	

	// Count
	@Override
	public int selectReportWaitingCount() {
		return paymentMapper.selectReportWaitingCount();
	}


	@Override
	public int selectVacationWaitingCount() {
		return paymentMapper.selectVacationWaitingCount();
	}


	@Override
	public int selectReportCompleteCount() {
		return paymentMapper.selectReportCompleteCount();
	}


	@Override
	public int selectVacationCompleteCount() {
		return paymentMapper.selectVacationCompleteCount();
	}


	@Override
	public int selectReportRejectCount() {
		return paymentMapper.selectReportRejectCount();
	}


	@Override
	public int selectVacationRejectCount() {
		return paymentMapper.selectVacationRejectCount();
	}







	
}
