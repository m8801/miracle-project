package com.project.miracle.payment.model.dao;

import java.util.List;
import java.util.Map;

import com.project.miracle.common.model.dto.MemberDTO;
import com.project.miracle.common.paging.PaymentCriteria;
import com.project.miracle.payment.model.dto.ApproverDTO;
import com.project.miracle.payment.model.dto.PaymentReportDTO;
import com.project.miracle.payment.model.dto.PaymentVacationDTO;
import com.project.miracle.payment.model.dto.ReportDTO;
import com.project.miracle.payment.model.dto.VacationDTO;

public interface PaymentMapper {
	
	// 결재문서함
	int selectReportCount(Map<String, String> searchMap);

	int selectVacationCount(Map<String, String> searchMap);

	List<PaymentReportDTO> selectReportList(PaymentCriteria selectReportCriteria);

	List<PaymentVacationDTO> selectVacationList(PaymentCriteria selectVacationCriteria);

	// 보고서 관련 메서드
	int insertReport(ReportDTO reportDTO);	// PAYMENT_REPORT 등록

	int insertOfficialReport(ReportDTO reportDTO);  // PAYMENT_OFFICIAL 등록

	int insertPaymentReport(ReportDTO reportDTO);  // PAYMENT 등록

	ApproverDTO selectApproverInfoReport(ReportDTO reportDTO);  // 사용자 결재자 조회
	
	ApproverDTO selectManagerApproverInfoReport(ReportDTO reportDTO);  // 관리자 결재자 조회
	
	PaymentReportDTO selectReportInfo(String data);  // 보고서 상세보기
	

	// 휴가 신청서 보고서 관련 메서드
	
	int insertVacation(VacationDTO vacationDTO); // PAYMENT_VACATION 등록

	int insertOfficialVacation(VacationDTO vacationDTO); // PAYMENT_OFFICIAL 등록

	int insertPaymentVacation(VacationDTO vacationDTO); // PAYMENT 등록

	ApproverDTO selectApproverInfoVacation(VacationDTO vacationDTO);  // 사용자 결재자 조회
	
	ApproverDTO selectManagerApproverInfoVacation(VacationDTO vacationDTO);  // 관리자 결재자 조회
	
	PaymentVacationDTO selectVacationInfo(String data);  // 휴가 신청서 상세보기

	int modifyReportCommitStatus(int paymentNo);

	int modifyReportReturnStatus(int paymentNo);


	//Count
	int selectReportWaitingCount();

	int selectVacationWaitingCount();

	int selectReportCompleteCount();

	int selectVacationCompleteCount();

	int selectReportRejectCount();

	int selectVacationRejectCount();

	
}
