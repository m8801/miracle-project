package com.project.miracle.payment.model.dto;

import java.sql.Date;
import java.util.List;

import com.project.miracle.common.model.dto.DeptDTO;
import com.project.miracle.common.model.dto.JobDTO;
import com.project.miracle.common.model.dto.MemberDTO;

public class ReportDTO {

	private String payNo;     		// 순번  	
	private String title;			// 결재문서 제목 
	private String writer;			// 기안자
	private String reasonPrev;		// 전일실적
	private String reasonAfter;		// 금일계획
	private DeptDTO deptDTO;   	    // 부서
	private JobDTO jobDTO;			// 직급
	private ApproverDTO approverDTO;// 결재자 사원정보
	private String delYN;			// 삭제 여부
	private Date createDate; 		// 작성 날짜
	
	public ReportDTO() {
		super();
	}

	public ReportDTO(String payNo, String title, String writer, String reasonPrev, String reasonAfter, DeptDTO deptDTO,
			JobDTO jobDTO, ApproverDTO approverDTO, String delYN, Date createDate) {
		super();
		this.payNo = payNo;
		this.title = title;
		this.writer = writer;
		this.reasonPrev = reasonPrev;
		this.reasonAfter = reasonAfter;
		this.deptDTO = deptDTO;
		this.jobDTO = jobDTO;
		this.approverDTO = approverDTO;
		this.delYN = delYN;
		this.createDate = createDate;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getReasonPrev() {
		return reasonPrev;
	}

	public void setReasonPrev(String reasonPrev) {
		this.reasonPrev = reasonPrev;
	}

	public String getReasonAfter() {
		return reasonAfter;
	}

	public void setReasonAfter(String reasonAfter) {
		this.reasonAfter = reasonAfter;
	}

	public DeptDTO getDeptDTO() {
		return deptDTO;
	}

	public void setDeptDTO(DeptDTO deptDTO) {
		this.deptDTO = deptDTO;
	}

	public JobDTO getJobDTO() {
		return jobDTO;
	}

	public void setJobDTO(JobDTO jobDTO) {
		this.jobDTO = jobDTO;
	}

	public ApproverDTO getApproverDTO() {
		return approverDTO;
	}

	public void setApproverDTO(ApproverDTO approver) {
		this.approverDTO = approver;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "ReportDTO [payNo=" + payNo + ", title=" + title + ", writer=" + writer + ", reasonPrev=" + reasonPrev
				+ ", reasonAfter=" + reasonAfter + ", deptDTO=" + deptDTO + ", jobDTO=" + jobDTO + ", approverDTO="
				+ approverDTO + ", delYN=" + delYN + ", createDate=" + createDate + "]";
	}

	

	
	

	
	
	
	
	
	
}
