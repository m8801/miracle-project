package com.project.miracle.payment.model.dto;

import java.sql.Date;

import com.project.miracle.common.model.dto.DeptDTO;
import com.project.miracle.common.model.dto.JobDTO;

public class VacationDTO {

	private String payNo;
	private String period;
	private String kind;
	private String reason;
	private String title;
	private JobDTO jobDTO;
	private DeptDTO deptDTO;
	private String writer;
	private ApproverDTO approverDTO;    // 결재자 정보
	private char del_YN;
	private Date createDate;
	
	
	public VacationDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public VacationDTO(String payNo, String period, String kind, String reason, String title, JobDTO jobDTO,
			DeptDTO deptDTO, String writer, ApproverDTO approverDTO, char del_YN, Date createDate) {
		super();
		this.payNo = payNo;
		this.period = period;
		this.kind = kind;
		this.reason = reason;
		this.title = title;
		this.jobDTO = jobDTO;
		this.deptDTO = deptDTO;
		this.writer = writer;
		this.approverDTO = approverDTO;
		this.del_YN = del_YN;
		this.createDate = createDate;
	}


	public String getPayNo() {
		return payNo;
	}


	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}


	public String getPeriod() {
		return period;
	}


	public void setPeriod(String period) {
		this.period = period;
	}


	public String getKind() {
		return kind;
	}


	public void setKind(String kind) {
		this.kind = kind;
	}


	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public JobDTO getJobDTO() {
		return jobDTO;
	}


	public void setJobDTO(JobDTO jobDTO) {
		this.jobDTO = jobDTO;
	}


	public DeptDTO getDeptDTO() {
		return deptDTO;
	}


	public void setDeptDTO(DeptDTO deptDTO) {
		this.deptDTO = deptDTO;
	}


	public String getWriter() {
		return writer;
	}


	public void setWriter(String writer) {
		this.writer = writer;
	}


	public ApproverDTO getApproverDTO() {
		return approverDTO;
	}


	public void setApproverDTO(ApproverDTO approverDTO) {
		this.approverDTO = approverDTO;
	}


	public char getDel_YN() {
		return del_YN;
	}


	public void setDel_YN(char del_YN) {
		this.del_YN = del_YN;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	@Override
	public String toString() {
		return "VacationDTO [payNo=" + payNo + ", period=" + period + ", kind=" + kind + ", reason=" + reason
				+ ", title=" + title + ", jobDTO=" + jobDTO + ", deptDTO=" + deptDTO + ", writer=" + writer
				+ ", approverDTO=" + approverDTO + ", del_YN=" + del_YN + ", createDate=" + createDate + "]";
	}

	


	


	

	
	
	
}
