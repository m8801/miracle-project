package com.project.miracle.payment.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.miracle.common.model.dto.DeptDTO;
import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.PaymentCriteria;
import com.project.miracle.login.model.dto.LoginDTO;
import com.project.miracle.payment.exception.PaymentRegistException;
import com.project.miracle.payment.model.dto.ApproverDTO;
import com.project.miracle.payment.model.dto.PaymentReportDTO;
import com.project.miracle.payment.model.dto.PaymentVacationDTO;
import com.project.miracle.payment.model.dto.ReportDTO;
import com.project.miracle.payment.model.dto.VacationDTO;
import com.project.miracle.payment.model.service.PaymentService;


@Controller
@RequestMapping("/payment")
@SessionAttributes("loginMember")
public class UserPaymentController {
		
	public final PaymentService  paymentService;

	public UserPaymentController(PaymentService  paymentService) {
		this.paymentService = paymentService;
	}
	
	/* 결재 문서함 */
	// 결재문서함 리스트 (업무 보고서 리스트, 류가 신청서 리스트)
	@GetMapping("/paymentMainListUser")
	public ModelAndView reportUserSelectList(@RequestParam(defaultValue = "1") int currentPage,
			@ModelAttribute PaymentCriteria searchCriteria, ModelAndView mv) {
			
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
	
		Map<String, String> searchMap = new HashMap<>();
	
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
	
		System.out.println(searchMap);
	
		int reportCount = paymentService.selectReportCount(searchMap);
		int vacationCount = paymentService.selectVacatoinCount(searchMap);
	
		System.out.println("reportCount : " + reportCount);
		System.out.println("vacationCount : " + vacationCount);
	
		int limit = 7;
		int buttonAmount = 5;
	
		PaymentCriteria selectReportCriteria = null;
		PaymentCriteria selectVacationCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {
	
			selectReportCriteria = Pagenation.getPaymentReportCriteria(currentPage, reportCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectReportCriteria = Pagenation.getPaymentReportCriteria(currentPage, reportCount, limit, buttonAmount);
		}
		
		if (searchCondition != null && !"".equals(searchCondition)) {
			
			selectVacationCriteria = Pagenation.getPaymentVacationCriteria(currentPage, vacationCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectVacationCriteria = Pagenation.getPaymentVacationCriteria(currentPage, vacationCount, limit, buttonAmount);
		}
		
		
		System.out.println(selectReportCriteria);
		System.out.println(selectVacationCriteria);
		
		List<PaymentReportDTO> reportList = paymentService.selectReportList(selectReportCriteria);
		List<PaymentVacationDTO> vacationList = paymentService.selectVacationList(selectVacationCriteria);
		
		
		System.out.println(reportList);
		System.out.println(vacationList);

		mv.addObject("reportList", reportList);
		mv.addObject("selectReportCriteria", selectReportCriteria);
		mv.addObject("vacationList", vacationList);
		mv.addObject("selectVacationCriteria", selectVacationCriteria);
		mv.addObject("type","paymentMainListUser");
		mv.setViewName("/payment/paymentMainUser");
		return mv;
	}
	
	/* 업무 보고서*/
	// 업무 보고서 페이지	
	@GetMapping("/reportUser")
	public ModelAndView reportUserSelect(@ModelAttribute ReportDTO reportDTO, HttpServletRequest request
			,ModelAndView mv) {
		String writer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		String deptName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getDeptName();
		DeptDTO deptDTO = new DeptDTO();
		
		deptDTO.setDeptName(deptName);
		
		reportDTO.setWriter(writer);
		reportDTO.setDeptDTO(deptDTO);
		
		System.out.println(reportDTO);
		
		ApproverDTO approver = paymentService.basicInfo(reportDTO);
		
		System.out.println(approver);
		
		mv.addObject("reportDTO",reportDTO);
		mv.addObject("approver",approver);
		mv.setViewName("/payment/paymentReportUser");
		
		
		return mv;
		
	}
		
	// 업무 보고서 등록
	@PostMapping("/reportUserRegist")
	public String reportUserRegist(@ModelAttribute ReportDTO reportDTO, HttpServletRequest request
			, RedirectAttributes rttr) throws PaymentRegistException{
		String writer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		String deptName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getDeptName();
		
		DeptDTO deptDTO = new DeptDTO();
		
		deptDTO.setDeptName(deptName);
		
		reportDTO.setWriter(writer);
		reportDTO.setDeptDTO(deptDTO);
		
		ApproverDTO approver = paymentService.basicInfo(reportDTO);
		
		System.out.println(approver);
		
		reportDTO.setApproverDTO(approver);
		
		System.out.println(reportDTO);
		
		paymentService.reportRegist(reportDTO);
		
		rttr.addFlashAttribute("message","보고서 기안 등록 성공");
		
		return "redirect:/payment/paymentMainListUser";
	}
	
	
	/* 휴가 신청서 */
	// 휴가 신청서 페이지
	@GetMapping("/vacationUser")
	public ModelAndView vacationUserSelect(@ModelAttribute VacationDTO vacationDTO, HttpServletRequest request
			,ModelAndView mv) {
		String writer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		String deptName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getDeptName();
		DeptDTO deptDTO = new DeptDTO();
		
		deptDTO.setDeptName(deptName);
		
		vacationDTO.setWriter(writer);
		vacationDTO.setDeptDTO(deptDTO);
		
		System.out.println(vacationDTO);
		
		ApproverDTO approver = paymentService.basicInfo(vacationDTO);
		
		System.out.println(approver);
		
		mv.addObject("vacationDTO",vacationDTO);
		mv.addObject("approver",approver);
		mv.setViewName("/payment/paymentVacationUser");
		
		
		return mv;
		
	}
	
	// 휴가 신청서 등록
	@PostMapping("/vacationUserRegist")
	public String vacationUserRegist(@ModelAttribute VacationDTO vacationDTO, HttpServletRequest request
			, RedirectAttributes rttr) throws PaymentRegistException{
		String writer = ((LoginDTO) request.getSession().getAttribute("loginMember")).getUserName();
		String deptName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getDeptName();
		
		DeptDTO deptDTO = new DeptDTO();
		
		deptDTO.setDeptName(deptName);
		
		vacationDTO.setWriter(writer);
		vacationDTO.setDeptDTO(deptDTO);
		
		ApproverDTO approver = paymentService.basicInfo(vacationDTO);
		
		System.out.println(approver);
		
		vacationDTO.setApproverDTO(approver);
		
		System.out.println(vacationDTO);
		
		paymentService.vacationRegist(vacationDTO);
		
		rttr.addFlashAttribute("message","휴가 신청서 기안 등록 성공");
		
		return "redirect:/payment/paymentMainListUser";
	}
	


}
