package com.project.miracle.mainBoard.service;

import java.util.List;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.mainBoard.model.dto.NewsBoardDTO;
import com.project.miracle.mainBoard.model.dto.SharingBoardDTO;
import com.project.miracle.questionsboard.model.dto.QuestionsBoardDTO;

public interface MainBoardService {

	List<NewsBoardDTO> selectAllNewsBoardList(SelectCriteria selectCriteria);

	List<SharingBoardDTO> selectAllSharingBoardList(SelectCriteria selectCriteria);

	List<JobBoardDTO> selectAllJobBoardList(SelectCriteria selectCriteria);

	List<FreeBoardDTO> selectAllFreeBoardList(SelectCriteria selectCriteria);

	List<QuestionsBoardDTO> selectAllQuestionBoardList(SelectCriteria selectCriteria);

}
