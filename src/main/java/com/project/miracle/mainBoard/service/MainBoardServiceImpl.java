package com.project.miracle.mainBoard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.mainBoard.model.dao.MainBoardMapper;
import com.project.miracle.mainBoard.model.dto.NewsBoardDTO;
import com.project.miracle.mainBoard.model.dto.SharingBoardDTO;
import com.project.miracle.questionsboard.model.dto.QuestionsBoardDTO;

@Service
public class MainBoardServiceImpl implements MainBoardService{
	
	public MainBoardMapper mapper;
	
	@Autowired
	public MainBoardServiceImpl(MainBoardMapper mapper) {
		this.mapper = mapper;
	}
	
	@Override
	public List<NewsBoardDTO> selectAllNewsBoardList(SelectCriteria selectCriteria) {
		
		List<NewsBoardDTO> newsBoardList = mapper.selectAllNewsBoardList(selectCriteria);
		
		return newsBoardList;
	}

	@Override
	public List<SharingBoardDTO> selectAllSharingBoardList(SelectCriteria selectCriteria) {

		List<SharingBoardDTO> sharingBoardList = mapper.selectAllSharingBoardList(selectCriteria);
		return sharingBoardList;
	}

	@Override
	public List<JobBoardDTO> selectAllJobBoardList(SelectCriteria selectCriteria) {
		List<JobBoardDTO> jobBoardList = mapper.selectAllJobBoardList(selectCriteria);
		return jobBoardList;
	}

	@Override
	public List<FreeBoardDTO> selectAllFreeBoardList(SelectCriteria selectCriteria) {
		List<FreeBoardDTO> freeBoardList = mapper.selectAllFReeBoardList(selectCriteria);
		return freeBoardList;
	}

	@Override
	public List<QuestionsBoardDTO> selectAllQuestionBoardList(SelectCriteria selectCriteria) {
		List<QuestionsBoardDTO> questionBoardList = mapper.selectAllQuestionBoardList(selectCriteria);
		return questionBoardList;
	}
	
}
