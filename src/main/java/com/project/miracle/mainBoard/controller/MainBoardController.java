package com.project.miracle.mainBoard.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.project.miracle.common.paging.Pagenation;
import com.project.miracle.common.paging.SelectCriteria;
import com.project.miracle.freeBoard.model.dto.FreeBoardDTO;
import com.project.miracle.jobBoard.model.dto.JobBoardDTO;
import com.project.miracle.mainBoard.model.dto.NewsBoardDTO;
import com.project.miracle.mainBoard.model.dto.SharingBoardDTO;
import com.project.miracle.mainBoard.service.MainBoardService;
import com.project.miracle.questionsboard.model.dto.QuestionsBoardDTO;

@Controller
@RequestMapping("/board")
public class MainBoardController {
	
	private MainBoardService mainBoardService;
	
	@Autowired
	public MainBoardController(MainBoardService mainBoardService) {
		this.mainBoardService = mainBoardService;
	}
	
	@GetMapping("/main-boardList")
	public ModelAndView mainBoardList(@RequestParam(defaultValue = "1") int currentPage , ModelAndView mv, HttpServletRequest request) {
		
		int limit = 5; 
		int buttonAmount = 5;
		int totalCount = 5;
//		
		SelectCriteria selectCriteria = null;
		
		selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		
		List<NewsBoardDTO> newsBoardList = mainBoardService.selectAllNewsBoardList(selectCriteria);
		List<SharingBoardDTO> sharingBoardList = mainBoardService.selectAllSharingBoardList(selectCriteria);
		List<JobBoardDTO> jobBoardList = mainBoardService.selectAllJobBoardList(selectCriteria);
		List<FreeBoardDTO> freeBoardList = mainBoardService.selectAllFreeBoardList(selectCriteria);
		List<QuestionsBoardDTO> questionBoardList = mainBoardService.selectAllQuestionBoardList(selectCriteria);
		
		System.out.println("newsBoardListttttt : " + newsBoardList);
		System.out.println("selectCriteriaaaaaa : " + selectCriteria);
		
		mv.addObject("newsBoardList", newsBoardList);
		mv.addObject("sharingBoardList", sharingBoardList);
		mv.addObject("jobBoardList",jobBoardList);
		mv.addObject("freeBoardList", freeBoardList);
		mv.addObject("questionBoardList",questionBoardList);
		
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("/board/board-main");
		
		return mv;
	}
	
	
	@GetMapping("/mainPage-board")
	public ModelAndView mainPageBoardList(@RequestParam(defaultValue = "1") int currentPage , ModelAndView mv, HttpServletRequest request) {
		
		int limit = 5; 
		int buttonAmount = 5;
		int totalCount = 5;
		
		SelectCriteria selectCriteria = null;
		
		selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		
		List<NewsBoardDTO> newsBoardList = mainBoardService.selectAllNewsBoardList(selectCriteria);
		List<JobBoardDTO> jobBoardList = mainBoardService.selectAllJobBoardList(selectCriteria);
		
		mv.addObject("newsBoardList", newsBoardList);
		mv.addObject("jobBoardList",jobBoardList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.setViewName("/mainpage/managerMain");
		
		return mv;
	}
	
	
	
}
