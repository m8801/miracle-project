package com.project.miracle.mainBoard.model.dto;

import java.sql.Date;


public class SharingBoardDTO {

	private Date createDate;
	private int boardNo;
	private String title;
	private String userName;
	private String delYn;
	private String anonymousYn;
	private String finalName;
	private int userNo;
	private String content;
	private SharingAttachmentDTO attachmentDTO;
	
	public SharingBoardDTO() {}

	public SharingBoardDTO(Date createDate, int boardNo, String title, String userName, String delYn,
			String anonymousYn, String finalName, int userNo, String content, SharingAttachmentDTO attachmentDTO) {
		super();
		this.createDate = createDate;
		this.boardNo = boardNo;
		this.title = title;
		this.userName = userName;
		this.delYn = delYn;
		this.anonymousYn = anonymousYn;
		this.finalName = finalName;
		this.userNo = userNo;
		this.content = content;
		this.attachmentDTO = attachmentDTO;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getBoardNo() {
		return boardNo;
	}

	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getAnonymousYn() {
		return anonymousYn;
	}

	public void setAnonymousYn(String anonymousYn) {
		this.anonymousYn = anonymousYn;
	}

	public String getFinalName() {
		return finalName;
	}

	public void setFinalName(String finalName) {
		this.finalName = finalName;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public SharingAttachmentDTO getAttachmentDTO() {
		return attachmentDTO;
	}

	public void setAttachmentDTO(SharingAttachmentDTO attachmentDTO) {
		this.attachmentDTO = attachmentDTO;
	}

	@Override
	public String toString() {
		return "SharingBoardDTO [createDate=" + createDate + ", boardNo=" + boardNo + ", title=" + title + ", userName="
				+ userName + ", delYn=" + delYn + ", anonymousYn=" + anonymousYn + ", finalName=" + finalName
				+ ", userNo=" + userNo + ", content=" + content + ", attachmentDTO=" + attachmentDTO + "]";
	}

	
}
